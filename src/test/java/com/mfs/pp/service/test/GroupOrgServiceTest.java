package com.mfs.pp.service.test;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.mfs.pp.dto.GroupsResponse;
import com.mfs.pp.service.GroupOrgService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml",
		"classpath:spring/hibernateConfig.xml", "classpath:spring/spring-security.xml" })
public class GroupOrgServiceTest {
	private static final Logger log = LoggerFactory.getLogger(GroupOrgServiceTest.class);
	@Autowired 
	private GroupOrgService groupOrgService;
	@Ignore
	@Test
	public void getAllGroup() {
//		log.debug("Inside Group Org Service Test getAllGroup");
		GroupsResponse object=null;
		try {
			object=groupOrgService.getAllGroup();
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting All Group Org Info " + e.getMessage());
		}
		
	}
}

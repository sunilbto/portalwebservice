package com.mfs.pp.service.test;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.mfs.pp.dto.AllBalanceByIdRequestDto;
import com.mfs.pp.dto.AllBalanceRequestDto;
import com.mfs.pp.service.BalanceService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml",
		"classpath:spring/hibernateConfig.xml", "classpath:spring/spring-security.xml" })
public class BalanceServiceTest {

	@Autowired
	private BalanceService balanceService;
	private static final Logger log = LoggerFactory.getLogger(BalanceServiceTest.class);
	@Ignore
	@Test
	public void getAllBalance() {
//		log.debug("Inside Balance Service Test getAllBalance");
		AllBalanceRequestDto request = new AllBalanceRequestDto();
		request.setPartnerCode(1);
		Object object = null;
		try {
			object = balanceService.getAllBalance(request);
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting All Balance Info " + e.getMessage());
		}
	}
	@Ignore
	@Test
	public void getAllBalanceById() {
//		log.debug("Inside Balance Service Test getAllBalanceById");
		AllBalanceByIdRequestDto request=new AllBalanceByIdRequestDto();
		request.setTransactionId(72500);
		
		Object object = null;
		try {
			object = balanceService.getAllBalanceById(request);
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting All Balance By Id Info " + e.getMessage());
		}
		
	}
	@Ignore
	@Test
	public void getBalanceSummury() {
//		log.debug("Inside Balance Service Test getBalanceSummury");
		List<Object> object=null;
		try {
			object=balanceService.getBalanceSummury();
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting Balance Summary Info " + e.getMessage());
		}
		
	}
}

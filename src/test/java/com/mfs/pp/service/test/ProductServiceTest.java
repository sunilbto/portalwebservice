package com.mfs.pp.service.test;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.mfs.pp.service.ProductService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml",
		"classpath:spring/hibernateConfig.xml", "classpath:spring/spring-security.xml" })
public class ProductServiceTest {
	private static final Logger log = LoggerFactory.getLogger(ProductServiceTest.class);
	@Autowired
	private ProductService productService;
	@Ignore
	@Test
	public void getAllProduct() {
//		log.debug("Inside Product Service Test getAllProduct");
		List<Object> object=null;
		try {
			object=productService.getAllProduct();
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting All Product Info " + e.getMessage());
		}
		
	}

}

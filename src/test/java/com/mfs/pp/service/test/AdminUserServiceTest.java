package com.mfs.pp.service.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.mfs.mdm.common.dto.UserDto;
import com.mfs.pp.service.AdminUserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml", "classpath:spring/hibernateConfig.xml",
		"classpath:spring/spring-security.xml" })
public class AdminUserServiceTest {
	@Autowired
	private AdminUserService adminUserService;
	private static final Logger log = LoggerFactory.getLogger(AdminUserServiceTest.class);

	@Ignore
	@Test
	public void getAllSystemAdminUsers() {
//		log.debug("admin user service Test getAllSystemAdminUsers");
		List<UserDto> object = new ArrayList<UserDto>();
		try {
			object = adminUserService.getAllSystemAdminUsers();
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting All Sysyten Admin User Info " + e.getMessage());
		}

	}

	@Ignore
	@Test
	public void deleteUserByEmail() {
//		log.debug("admin user service Test deleteUserByEmail");
		String email = "ayamityadav00@gmail.com";
		boolean flag = false;
		try {
			flag = adminUserService.deleteUserByEmail(email);
			Assert.isTrue(flag);
		} catch (Exception e) {
			log.error("Exception While Deleting User By Email " + e.getMessage());
		}

	}

}

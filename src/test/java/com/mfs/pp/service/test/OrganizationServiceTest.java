package com.mfs.pp.service.test;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.mfs.mdm.common.dto.CurrencyDto;
import com.mfs.mdm.common.dto.OrganisationProfileDto;
import com.mfs.mdm.common.dto.RolesDto;
import com.mfs.mdm.common.dto.UserDto;
import com.mfs.pp.dto.OrganizationResponseByName;
import com.mfs.pp.service.OrganisationService;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml",
		"classpath:spring/hibernateConfig.xml", "classpath:spring/spring-security.xml" })
public class OrganizationServiceTest {
	private static final Logger log = LoggerFactory.getLogger(OrganizationServiceTest.class);
	@Autowired
    private	OrganisationService organisationService;
	@Ignore
	@Test
	public void getAllUserByOrganisation(){
//		log.debug("Inside Organization Service Test getAllUserByOrganisation");
		String organisationName="dsk";
		List<UserDto> object=null;
		try {
			object=organisationService.getAllUserByOrganisation(organisationName);
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting All User By Organisation Info " + e.getMessage());
		}
		
	}

	@Ignore
	@Test
	public void getAllRoles(){
//		log.debug("Inside Organization Service Test getAllRoles");
		List<RolesDto> object=null;
		try {
			object=organisationService.getAllRoles();
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting All Roles Info " + e.getMessage());
		}
		
	}
	@Ignore
	@Test
	public void getAllCurruncyDtos(){
//		log.debug("Inside Organization Service Test getAllCurruncyDtos");
		List<CurrencyDto> object=null;
		try {
			object=organisationService.getAllCurruncyDtos();
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting All Curruncy Dtos  Info " + e.getMessage());
		}
		
	}
	@Ignore
	@Test
	public void getAllReportTypesList() {
//		log.debug("Inside Organization Service Test getAllReportTypesList");
		List<String> object=null;
		try {
			object=organisationService.getAllReportTypesList();
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting All Report Types  Info " + e.getMessage());
		}
		
	}
	@Ignore
	@Test
	public void  getAllStatusTypes(){
//		log.debug("Inside Organization Service Test getAllStatusTypes");
		List<String> object=null;
		try {
			object=organisationService.getAllStatusTypes();
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting All Status Types Info " + e.getMessage());
		}
		
		
		
	}
	@Ignore
	@Test
	public void getAllOrganisationListForAdmin(){
//		log.debug("Inside Organization Service Test getAllOrganisationListForAdmin");
		List<OrganisationProfileDto> object=null;
		try {
			object=organisationService.getAllOrganisationListForAdmin();
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting All Organisation List For Admin Info " + e.getMessage());
		}
		
	}
	@Ignore
	@Test
	public void getOrgDetails() {
//		log.debug("Inside Organization Service Test getOrgDetails");
		String orgName="dsk";
		OrganizationResponseByName object=null;
		try {
			object=organisationService.getOrgDetails(orgName);
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting Org details Info " + e.getMessage());
		}
		
	}
}

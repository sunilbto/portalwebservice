package com.mfs.pp.service.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.mfs.pp.dto.AllTransactionRequestDto;
import com.mfs.pp.dto.AlltransactionByIdRequestDto;
import com.mfs.pp.service.TransactionService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml",
		"classpath:spring/hibernateConfig.xml", "classpath:spring/spring-security.xml" })
public class TransactionServiceTest {
	@Autowired
	private TransactionService transactionService;
	private static final Logger log = LoggerFactory.getLogger(TransactionServiceTest.class);
	@Test
	public void getAllTransaction() {
//		log.debug("Inside Transaction Service Test getAllTransaction");
		AllTransactionRequestDto request=new AllTransactionRequestDto();
		Object object=null;
		request.setPartnerCode(1);
		
		try {
			object=transactionService.getAllTransaction(request);
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting Transaction Info " + e.getMessage());
		}
		
	}

	
	@Test
	public void getTransactionById() {
//		log.debug("Inside Transaction Service Test getTransactionById");
		AlltransactionByIdRequestDto request=new AlltransactionByIdRequestDto();
		Object object=null;
		request.setTransactionId(72500);
		try {
			object=transactionService.getTransactionById(request);
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting Transaction By Id Info" + e.getMessage());
		}
		
	}
	
}

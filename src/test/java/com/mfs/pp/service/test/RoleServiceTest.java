package com.mfs.pp.service.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.mfs.pp.dto.MenuActionDTO;
import com.mfs.pp.dto.MenuDTO;
import com.mfs.pp.dto.RoleDTO;
import com.mfs.pp.service.RoleService;
import com.mfs.pp.util.RoleTypeEnum;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml",
		"classpath:spring/hibernateConfig.xml", "classpath:spring/spring-security.xml" })
public class RoleServiceTest {
	private static final Logger log = LoggerFactory.getLogger(RoleServiceTest.class);
	@Autowired
	private RoleService roleService;
	@Ignore
	@Test
	public void createRoleTest () {
//		log.debug("Inside Role Service Test createRoleTest");
		try {
			RoleDTO roleDTO = new RoleDTO();
			roleDTO.setRoleName("Role Test 4");
			roleDTO.setRoleDescription("Role Test 4 desc");
			roleDTO.setRoleType(RoleTypeEnum.CUSTOM.name());
			List<MenuActionDTO> actionDTOs = new ArrayList<>();
			MenuActionDTO actionDTO = new MenuActionDTO();
			actionDTO.setMenuActionId(2);
			actionDTOs.add(actionDTO);
			roleDTO.setMenuActionDTOList(actionDTOs);
			
			roleService.createAdminRole(roleDTO);

		} catch (Exception e) {
			log.error("Exception While Creating Role " + e.getMessage());
		}
		

	}
	@Ignore
	@Test
	public void getListOfAdminRole(){
//		log.debug("Inside Role Service Test getListOfAdminRole");
		List<RoleDTO> object=null;
		try {
			object=roleService.getListOfAdminRole();
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting All Admin Role Info " + e.getMessage());
		}
		
		
	}
	@Ignore
	@Test
	public void getListOfOrgRole(){
//		log.debug("Inside Role Service Test getListOfOrgRole");
		List<RoleDTO> object=null;
		try {
	//		object=roleService.getListOfOrgRole();
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting All Org Role Info " + e.getMessage());
		}
		
		
	}
	@Ignore
	@Test
	public void getMenuList(){
//		log.debug("Inside Role Service Test getMenuList");
		List<MenuDTO> object=null;
		try {
		object=roleService.getMenuList();
		Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting All Menu Info " + e.getMessage());
		}
	
		
		
	}
}

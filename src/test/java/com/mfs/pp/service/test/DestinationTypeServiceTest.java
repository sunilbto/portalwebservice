package com.mfs.pp.service.test;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.mfs.pp.service.DestinationTypeService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml",
		"classpath:spring/hibernateConfig.xml", "classpath:spring/spring-security.xml" })
public class DestinationTypeServiceTest {
	@Autowired
	private DestinationTypeService destinationTypeService;
	private static final Logger log = LoggerFactory.getLogger(DestinationTypeServiceTest.class);
	@Ignore
	@Test
	public void getDestinationTypeList() {
//		log.debug("Inside Destination Type Service Test getDestinationTypeList");
		List<Object> object=null;
		try {
			object=destinationTypeService.getDestinationTypeList();
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting Destination Type Info " + e.getMessage());
		}
	
	}
	

}

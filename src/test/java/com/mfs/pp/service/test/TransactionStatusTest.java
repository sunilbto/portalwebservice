package com.mfs.pp.service.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.mfs.pp.service.TransactionStatusService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml",
		"classpath:spring/hibernateConfig.xml", "classpath:spring/spring-security.xml" })
public class TransactionStatusTest {
	private static final Logger log = LoggerFactory.getLogger(TransactionStatusTest.class);
	@Autowired
	private TransactionStatusService transactionStatusService;
	
	@Test
	public void getTransactionStatus() {
//		log.debug("Inside Transaction Status Service Test getTransactionStatus");
		 List<Object> object=null;
		 try {
			object=transactionStatusService.getTransactionStatus();
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting Transaction Status Info" + e.getMessage());
		}
		 
	}
	
}

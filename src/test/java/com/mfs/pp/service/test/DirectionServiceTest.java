package com.mfs.pp.service.test;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.mfs.pp.service.DirectionService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml",
		"classpath:spring/hibernateConfig.xml", "classpath:spring/spring-security.xml" })
public class DirectionServiceTest {

	@Autowired
	private DirectionService directionService;
	private static final Logger log = LoggerFactory.getLogger(DirectionServiceTest.class);
	@Ignore
	@Test
	public void getDirectionList() {
//		log.debug("Inside Direction Service Test getDirectionList");
		List<Object> object=null;
		try {
			object=directionService.getDirectionList();
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting Direction Info " + e.getMessage());
		}
		
	}
}

package com.mfs.pp.service.test;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.mfs.pp.dto.ZonesResponse;
import com.mfs.pp.service.CountryZoneService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml",
		"classpath:spring/hibernateConfig.xml", "classpath:spring/spring-security.xml" })
public class CountryZoneServiceTest {
	private static final Logger log = LoggerFactory.getLogger(CountryZoneServiceTest.class);

	@Autowired
	private CountryZoneService countryZoneService;
	@Ignore
	@Test
	public void getAllZone() {
//		log.debug("Inside Country Zone Service Test getAllZone");
		ZonesResponse object=null;
		try {
			object=countryZoneService.getAllZone();
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting All Zones Info " + e.getMessage());
		}
		
	}

}

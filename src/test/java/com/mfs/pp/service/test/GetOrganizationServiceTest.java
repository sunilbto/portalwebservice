package com.mfs.pp.service.test;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.mfs.pp.service.GetOrganizationService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml",
		"classpath:spring/hibernateConfig.xml", "classpath:spring/spring-security.xml" })
public class GetOrganizationServiceTest {

	private static final Logger log = LoggerFactory.getLogger(GetOrganizationServiceTest.class);
	@Autowired
	private GetOrganizationService  getOrganizationService;
	@Ignore
	@Test
	public void getOrganizationList(){
//		log.debug("Inside Organizaton Service Test getOrganizationList");
		List<Object> object=null;
		try {
			object=getOrganizationService.getOrganizationList();
			 Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting Organization Info " + e.getMessage());
		}
		
	}

}

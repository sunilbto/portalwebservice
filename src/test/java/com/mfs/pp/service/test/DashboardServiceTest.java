package com.mfs.pp.service.test;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.mfs.pp.dto.AllDashBoardRequestDto;
import com.mfs.pp.service.DashBoardService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml",
		"classpath:spring/hibernateConfig.xml", "classpath:spring/spring-security.xml" })
public class DashboardServiceTest {
	@Autowired
	private DashBoardService dashBoardService;
	
	private static final Logger log = LoggerFactory.getLogger(DashboardServiceTest.class);

	@Ignore
	@Test
	public void getDashBoard() {
//		log.debug("Inside Dashboard Service Test getDashBoard");
		AllDashBoardRequestDto request=new AllDashBoardRequestDto();
		Object object=null;
		request.setPartnerCode(1);
		request.setStartDate("2019-04-05 10:05:22");
		request.setEndDate("2019-04-16 13:28:17");
		try {
			object=dashBoardService.getDashBoard(request);
			Assert.notNull(object);
		} catch (Exception e) {
			log.error("Exception While Getting DashBoard Info " + e.getMessage());
		}
		
		
	}

}

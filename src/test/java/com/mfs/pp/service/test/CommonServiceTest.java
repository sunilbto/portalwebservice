package com.mfs.pp.service.test;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.mfs.pp.dto.CategoryResponse;
import com.mfs.pp.dto.OrganizationTypes;
import com.mfs.pp.dto.StatusResponse;
import com.mfs.pp.service.CommonService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml",
		"classpath:spring/hibernateConfig.xml", "classpath:spring/spring-security.xml" })
public class CommonServiceTest {

	@Autowired
	private CommonService commonService;
	
	private static final Logger log = LoggerFactory.getLogger(CommonServiceTest.class);
	@Ignore
	@Test
	public void getOrgTypeList(){
//		log.debug("Inside Common Service Test getOrgTypeList");
		List<OrganizationTypes> object=null;
		try {
		object=commonService.getOrgTypeList();
		Assert.notNull(object);
		}catch(Exception e) {
			log.error("Exception While Getting Org Type Info " + e.getMessage());	
		}
		
	}
	@Ignore
	@Test
	public void getCategoryList(){
//		log.debug("Inside Common Service Test getCategoryList");
		List<CategoryResponse> object=null;
		try {
			object=commonService.getCategoryList();
			Assert.notNull(object);
			}catch(Exception e) {
				log.error("Exception While Getting Category Info " + e.getMessage());
			}
			
		
	}
	@Ignore
	@Test
	public void getStatusList() {
//		log.debug("Inside Common Service Test getStatusList");
		List<StatusResponse> object=null;
		try {
			object=commonService.getStatusList();
			Assert.notNull(object);
			}catch(Exception e) {
				log.error("Exception While Getting Status Info " + e.getMessage());
			}
			
		
	}
	
}

package com.mfs.pp.dao.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.mfs.mdm.model.AdminRole;
import com.mfs.mdm.model.Menu;
import com.mfs.pp.dao.RoleDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml",
		"classpath:spring/hibernateConfig.xml", "classpath:spring/spring-security.xml" })
public class RoleDaoTest {
	
	@Autowired
	private RoleDao roleDao;
	
	@Test
	public void getDefaultAdminRoleListTest () {
		List<AdminRole> adminRoleList = null;		
		try {
			adminRoleList = roleDao.getDefaultAdminRoleList();

		} catch (Exception e) {
		}
		Assert.notNull(adminRoleList);

	}
	
	@Test
	public void getMenuListTest () {
		List<Menu> menuList = null;		
		try {
			menuList = roleDao.getMenuList();

		} catch (Exception e) {
		}
		Assert.notNull(menuList);

	}
	
}

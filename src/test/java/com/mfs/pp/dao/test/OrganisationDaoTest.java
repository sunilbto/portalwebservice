package com.mfs.pp.dao.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.mfs.mdm.model.OrganizationUser;
import com.mfs.pp.dao.OrganisationDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml",
		"classpath:spring/hibernateConfig.xml", "classpath:spring/spring-security.xml" })
public class OrganisationDaoTest {
	
	@Autowired
	private OrganisationDao organisationDao;
	
	@Test
	public void getOrganizationUserByUserEmailTest () {
		OrganizationUser organizationUser = null;		
		try {
			organizationUser = organisationDao.getOrganizationUserByUserEmail("otest@otest.com1");

		} catch (Exception e) {
		}
		Assert.notNull(organizationUser);

	}
	
}

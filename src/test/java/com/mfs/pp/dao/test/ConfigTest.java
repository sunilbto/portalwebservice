package com.mfs.pp.dao.test;

import java.util.Map;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mfs.pp.dao.TransactionDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml", "classpath:spring/hibernateConfig.xml",
		"classpath:spring/spring-security.xml" })
     public class ConfigTest {
	@Autowired
	TransactionDao transactionDao;

	@Ignore
	@Test
	public void systemConfigTest() {
		Map<String, String> map = transactionDao.getConfigDetailsMap();
		Assert.assertNotNull(map);
		
	}
	

}

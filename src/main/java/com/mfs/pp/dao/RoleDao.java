
package com.mfs.pp.dao;

import java.util.List;

import com.mfs.mdm.model.AdminRole;
import com.mfs.mdm.model.AdminRoleAccess;
import com.mfs.mdm.model.AdminUser;
import com.mfs.mdm.model.AdminUserRole;
import com.mfs.mdm.model.Menu;
import com.mfs.mdm.model.OrgRoleAccess;
import com.mfs.mdm.model.OrganizationRole;
import com.mfs.mdm.model.OrganizationUser;
import com.mfs.mdm.model.OrganizationUserRole;
import com.mfs.mdm.model.RoleType;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface RoleDao extends BaseDao {

	public List<AdminRole> getDefaultAdminRoleList();

	public List<OrganizationRole> getDefaultOrgRoleList(String orgType);

	public List<AdminRole> getCustomAdminRoleList();

	public List<OrganizationRole> getCustomOrgRoleList(int orgId);

	public OrganizationRole getOrgRoleDetail(int roleId);

	public AdminRole getAdminRoleDetail(int roleId);

	public List<Menu> getMenuList();

	public RoleType getRoleType(String roleTypeName);

	public AdminRoleAccess getRoleAccess(int roleId, int menuAccessId);

	public OrgRoleAccess getOrgRoleAccess(int roleId, int menuAccessId);

	public List<OrgRoleAccess> getOrgRoleAccessList(int orgRoleAccessId);

	public OrganizationUserRole getOrgUserRole(int organisationRoleId);

	public AdminUserRole adminUserRole(int adminRoleId);

	public List<AdminRoleAccess> getAdminRoleAccessList(int orgRoleAccessId);

	public List<AdminRole> adminRoleList(String roleName);
	
	public List<OrganizationRole> orgRoleList(int orgId, String roleName);
	
	public AdminUser getAdminUser(int userId);
	
	public OrganizationUser getOrgUser(int userId);
	
	public OrganizationRole getOrgRoleById(int organisationRoleId);

}

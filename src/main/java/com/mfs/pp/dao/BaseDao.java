/**
 * 
 */
package com.mfs.pp.dao;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */
public interface BaseDao {

	boolean save(Object obj);

	boolean update(Object obj);

	boolean saveOrUpdate(Object obj);

	boolean delete(Object obj);

	boolean saveBatchObjects(Object obj, int count);

	boolean persist(Object obj);

	Object saveAndReturnObject(Object obj);

	Object get(Class classs, Integer id);
}

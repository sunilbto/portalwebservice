package com.mfs.pp.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.model.CountryMaster;
import com.mfs.mdm.model.CountryZone;
import com.mfs.mdm.model.Zone;
import com.mfs.pp.dao.CountryZoneDao;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@EnableTransactionManagement
@Repository("CountryZoneDao")
public class CountryZoneDaoImpl extends BaseDaoImpl implements CountryZoneDao {

	@Autowired
	private SessionFactory sessionFactory;

	//get Country By zone
	@Transactional
	public List<CountryZone> getCountryByzone(int zoneId) {

		Session session = sessionFactory.getCurrentSession();

		String hql = "from Zone where zoneId=?";
		Query query = session.createQuery(hql);
		query.setInteger(0, zoneId);
		Zone zone = (Zone) query.uniqueResult();

		String hql2 = "from CountryZone where zone=?";
		Query query2 = session.createQuery(hql2);
		query2.setInteger(0, zone.getZoneId());
		List<CountryZone> CountryZone = query2.list();
		return CountryZone;
	}

	//get all Zone
	@Transactional
	public List<CountryZone> getallZone(int zoneId) {

		Session session = sessionFactory.getCurrentSession();

		String hql = "from CountryZone where zone=?";
		Query query = session.createQuery(hql);
		query.setInteger(0, zoneId);
		List<CountryZone> CountryZoneList = query.list();

		return CountryZoneList;
	}

	//check Zone Exist by zoneID
	@Transactional
	public Zone checkZoneExist(int zoneId) {

		Session session = sessionFactory.getCurrentSession();

		String hql = "from Zone where zoneId=?";
		Query query = session.createQuery(hql);
		query.setInteger(0, zoneId);
		Zone zone = (Zone) query.uniqueResult();

		return zone;
	}

	//check Zone Exist by zoneName
	@Transactional
	public Zone checkZoneExist(String zoneName) {

		Session session = sessionFactory.getCurrentSession();

		String hql = "from Zone where zoneName=:zoneName";
		Query query = session.createQuery(hql);
		query.setString("zoneName", zoneName);
		Zone zone = (Zone) query.uniqueResult();

		return zone;
	}

	//get Country Details by countryName
	@Transactional
	public CountryMaster getCountryDetails(String countryName) {
		Session session = sessionFactory.getCurrentSession();

		String hql = "from CountryMaster where countryName=:countryName";
		Query query = session.createQuery(hql);
		query.setString("countryName", countryName);
		CountryMaster countryMaster = (CountryMaster) query.uniqueResult();

		return countryMaster;
	}

	//get all zones
	@Transactional
	public List<Zone> getallZones() {

		Session session = sessionFactory.getCurrentSession();

		String hql = "from Zone";
		Query query = session.createQuery(hql);
		List<Zone> zoneList = query.list();

		return zoneList;
	}

}

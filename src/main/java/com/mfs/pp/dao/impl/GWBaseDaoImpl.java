/**
 * 
 */
package com.mfs.pp.dao.impl;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.pp.dao.BaseDao;
import com.mfs.pp.exception.MPDuplicateDataException;
import com.mfs.pp.exception.MPHibernateCommonException;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Repository
public class GWBaseDaoImpl implements BaseDao {

	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(GWBaseDaoImpl.class);

	// get session
	public Session getSession() {
		
		Session session = sessionFactory.getCurrentSession();
		if (session == null) {
			session = sessionFactory.openSession();
		}
		return session;
	}

	//save
	@Transactional
	@Override
	public boolean save(Object obj) {
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().save(obj);
			isSuccess = true;
		} catch (ConstraintViolationException e) {
			throw new MPDuplicateDataException("Duplicate records found"+e);
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e);
		}
		return isSuccess;
	}

	//update object
	@Transactional
	@Override
	public boolean update(Object obj) {
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().update(obj);
			isSuccess = true;
		} catch (HibernateException e) {
			LOGGER.error("Error occured while saving data in TEBaseDaoImpl"+e.getMessage());
		}
		return isSuccess;
	}

	//save Or Update Object
	@Transactional
	@Override
	public boolean saveOrUpdate(Object obj) {
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().saveOrUpdate(obj);
			isSuccess = true;
		} catch (ConstraintViolationException e) {
			throw new MPDuplicateDataException(e.getMessage());
		} catch (HibernateException e) {

			throw new MPHibernateCommonException(e.getMessage());
		}
		return isSuccess;
	}

	//delete Object
	@Transactional
	@Override
	public boolean delete(Object obj) {
		boolean isSuccess = false;
		//LOGGER.info("Start of save method in TEBaseDaoImpl");
		try {
			this.sessionFactory.getCurrentSession().delete(obj);
			isSuccess = true;
		} catch (HibernateException e) {

			throw new MPHibernateCommonException(e.getMessage());
		}
		//LOGGER.info("End of save method in TEBaseDaoImpl");
		return isSuccess;
	}

	//save Batch Objects
	@Transactional
	@Override
	public boolean saveBatchObjects(Object obj, int count) {
		boolean isSuccess = false;
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			session.saveOrUpdate(obj);
			if (count >= 50) {
				session.flush();
				session.clear();
			}
			isSuccess = true;
		} catch (ConstraintViolationException e) {
			throw new MPDuplicateDataException(e.getMessage());
		} catch (HibernateException e) {

			throw new MPHibernateCommonException(e.getMessage());
		}
		return isSuccess;
	}

	//persist
	@Override
	public boolean persist(Object obj) {
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().persist(obj);
			isSuccess = true;
		} catch (ConstraintViolationException e) {
			throw new MPDuplicateDataException(e.getMessage());
		} catch (HibernateException e) {

			throw new MPHibernateCommonException(e.getMessage());
		}
		return isSuccess;
	}

	//get
	@Transactional
	@Override
	public Object get(Class classs, Integer id) {
		Object obj = null;
		try {
			obj = this.sessionFactory.getCurrentSession().get(classs, id);
		} catch (ConstraintViolationException e) {
			throw new MPDuplicateDataException(e);
		} catch (HibernateException e) {

			throw new MPHibernateCommonException(e);
		}
		return obj;
	}

	//save And Return Object
	@Transactional
	@Override
	public Object saveAndReturnObject(Object obj) {
		boolean isSuccess = false;
		Object currentSavedObj = null;
		try {
			currentSavedObj = this.sessionFactory.getCurrentSession().save(obj);
			isSuccess = true;
		} catch (ConstraintViolationException e) {
			throw new MPDuplicateDataException("Duplicate records found"+e.getMessage());
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e);
		} 
		
		return currentSavedObj;
	}

}

package com.mfs.pp.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.model.ForexMargin;
import com.mfs.pp.dao.ForexMarginDao;

/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@EnableTransactionManagement
@Repository("ForexMarginDao")
public class ForexMarginDaoImpl implements ForexMarginDao{

//	private static final Logger LOGGER = Logger.getLogger(ForexMarginDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	//get forex margin
	@Transactional
	public double getForexMargin() {
		Session session = sessionFactory.getCurrentSession();
		double margin=0.0;
		
		
		String forexMarginQuery = "from ForexMargin";
		Query query = session.createQuery(forexMarginQuery);
		List<ForexMargin> forexMarginList=query.list();
		
		for(ForexMargin forexMargin:forexMarginList) {
			margin=	forexMargin.getForexMargin();
		}
	   
		 return margin;
	}

}

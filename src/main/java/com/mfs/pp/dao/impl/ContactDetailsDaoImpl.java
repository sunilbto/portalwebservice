package com.mfs.pp.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.model.QueryType;
import com.mfs.pp.dao.ContactDetailsDao;
import com.mfs.pp.dto.QueryTypeDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */


@EnableTransactionManagement
@Repository("ContactDetailsDaoImpl")
public class ContactDetailsDaoImpl extends BaseDaoImpl implements ContactDetailsDao{
	@Autowired
	private SessionFactory sessionFactory;

//	private static final Logger log = LoggerFactory.getLogger(ContactDetailsDaoImpl.class);
	//check Query Type Exits
	@Transactional
	public QueryType checkQueryTypeExits(String queryType) {
		//log.info("Inside checkQueryTypeExits() method of ContactDetailsDaoImpl");
		Session session = sessionFactory.getCurrentSession();

		String hql = "from QueryType where queryType=:queryType";
		Query query = session.createQuery(hql);
		query.setString("queryType", queryType);
		QueryType type =  (QueryType) query.uniqueResult();

		return type;
	}

	//get Query Type
	@Transactional
	public List<QueryTypeDto> getQueryType() {
		//log.info("Inside getQueryType() method of ContactDetailsDaoImpl");
		Session session = sessionFactory.getCurrentSession();
		
		String hql = "from QueryType";
		Query query = session.createQuery(hql);
		
		List<QueryTypeDto> queryTypes =  query.list();
				
		return queryTypes;
	}

}
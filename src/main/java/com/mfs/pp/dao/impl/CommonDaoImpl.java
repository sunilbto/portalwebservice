package com.mfs.pp.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.model.Address;
import com.mfs.mdm.model.Category;
import com.mfs.mdm.model.CountryMaster;
import com.mfs.mdm.model.CurrencyMaster;
import com.mfs.mdm.model.IdType;
import com.mfs.mdm.model.OrgGroup;
import com.mfs.mdm.model.OrganizationType;
import com.mfs.mdm.model.OrganizationUser;
import com.mfs.mdm.model.PartnerType;
import com.mfs.mdm.model.StateRegion;
import com.mfs.mdm.model.Status;
import com.mfs.mdm.model.TimeZone;
import com.mfs.mdm.model.User;
import com.mfs.pp.dao.CommonDao;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@EnableTransactionManagement
@Repository("CommonDao")
public class CommonDaoImpl extends GWBaseDaoImpl implements CommonDao {

	@Autowired
	SessionFactory sessionFactory;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CommonDaoImpl.class);

	//get all organization type
	@Transactional
	public List<OrganizationType> getAllOrgType() {
		Session session = sessionFactory.getCurrentSession();

		String hql = "from OrganizationType";
		Query query = session.createQuery(hql);
		List<OrganizationType> organizationTypeList = query.list();

		return organizationTypeList;
	}

	//get all category
	@Transactional
	public List<Category> getAllCategory() {
		Session session = sessionFactory.getCurrentSession();

		String hql = "from Category";
		Query query = session.createQuery(hql);
		List<Category> CategoryList = query.list();

		return CategoryList;
	}

	//get all status
	@Transactional
	public List<Status> getAllStatus() {
		Session session = sessionFactory.getCurrentSession();

		String hql = "from Status";
		Query query = session.createQuery(hql);
		List<Status> statusList = query.list();

		return statusList;
	}

	//get category
	@Transactional
	public Category getcategory(String catergoryName) {

		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from Category where categoryName=?";
		Query query2 = session.createQuery(hql2);
		query2.setString(0, catergoryName);
		Category category = (Category) query2.uniqueResult();
		return category;
	}

	//get user
	@Transactional
	public User getUser(String email) {
		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from User where email=?";
		Query query2 = session.createQuery(hql2);
		query2.setString(0, email);
		User user = (User) query2.uniqueResult();
		return user;
	}

	//get organization
	@Transactional
	public OrgGroup getOrg(int orgId) {
		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from OrgGroup where orgId=?";
		Query query2 = session.createQuery(hql2);
		query2.setInteger(0, orgId);
		OrgGroup orgGroup = (OrgGroup) query2.uniqueResult();
		return orgGroup;
	}

	//get address
	@Transactional
	public Address getAddress(int orgId) {
		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from Address where organization=?";
		Query query2 = session.createQuery(hql2);
		query2.setInteger(0, orgId);
		Address address = (Address) query2.uniqueResult();
		return address;
	}

	//get organization type
	@Transactional
	public OrganizationType getOrgType(String OrgTypeName) {

		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from OrganizationType where orgType=?";
		Query query2 = session.createQuery(hql2);
		query2.setString(0, OrgTypeName);
		OrganizationType organizationType = (OrganizationType) query2.uniqueResult();
		return organizationType;

	}

	//get country details
	@Transactional
	public CountryMaster getCountryDteails(int countryId) {
		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from CountryMaster where countryId=?";
		Query query2 = session.createQuery(hql2);
		query2.setInteger(0, countryId);
		CountryMaster countryMaster = (CountryMaster) query2.uniqueResult();
		return countryMaster;
	}

	//get time zone
	@Override
	@Transactional
	public TimeZone getTimeZone(String timeZone) {
		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from TimeZone where timeZoneName=:timeZoneName";
		Query query2 = session.createQuery(hql2);
		query2.setString("timeZoneName", timeZone);
		TimeZone timeZones = (TimeZone) query2.uniqueResult();
		return timeZones;
	}

	//get user address
	@Override
	@Transactional
	public Address getUserAddress(int userId) {
		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from Address where user=?";
		Query query2 = session.createQuery(hql2);
		query2.setInteger(0, userId);
		Address address = (Address) query2.uniqueResult();
		return address;
	}

	
	//ger organization user
	@SuppressWarnings("unchecked")
	@Transactional
	public List<OrganizationUser> getOrgUser(String orgName) {
		
		List<OrganizationUser> user = null;
		try {
			user = (ArrayList<OrganizationUser>) getSession()
					.createQuery("from OrganizationUser where organization.orgName=?")
					.setParameter(0, orgName).list();
			
			
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching user" + e.getMessage());
		}
		LOGGER.info("End of method getOrgUser in CommonDaoImpl");
		return user;
		
	}

	

	//get couurency detail
	@Transactional
	public CurrencyMaster getCourencyDteail(String couerncyCode) {
		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from CurrencyMaster where currencyCode=?";
		Query query2 = session.createQuery(hql2);
		query2.setParameter(0, couerncyCode);
		CurrencyMaster currencyMaster = (CurrencyMaster) query2.uniqueResult();
		return currencyMaster;
	}

	//get states list
	@Transactional
	public List<StateRegion> getStateList() {
		Session session = sessionFactory.getCurrentSession();

		String hql = "from StateRegion";
		Query query = session.createQuery(hql);
		List<StateRegion> stateRegionList = query.list();

		return stateRegionList;
	}

	// get list of ID type
	@Transactional
	public List<IdType> getIdType() {
		Session session = sessionFactory.getCurrentSession();

		String hql = "from IdType";
		Query query = session.createQuery(hql);
		List<IdType> idTypeList = query.list();

		return idTypeList;
	}

	//get id type
	@Transactional
	public IdType getIdType(String idType) {
		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from IdType where idType=?";
		Query query2 = session.createQuery(hql2);
		query2.setParameter(0, idType);
		IdType idTypes = (IdType) query2.uniqueResult();
		return idTypes;
	}

	//get partner type list
	@Transactional
	public List<PartnerType> getPartnerTypeList() {
		Session session = sessionFactory.getCurrentSession();

		String hql = "from PartnerType";
		Query query = session.createQuery(hql);
		List<PartnerType> partnerTypeList = query.list();
		return partnerTypeList;
		
	}

	//get partner type
	@Transactional
	public PartnerType getPartnerType(String partnerType) {
		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from PartnerType where partnerType=?";
		Query query2 = session.createQuery(hql2);
		query2.setParameter(0, partnerType);
		PartnerType partnerTypes = (PartnerType) query2.uniqueResult();
		return partnerTypes;
	}

}

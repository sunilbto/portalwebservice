/**
 * 
 */
package com.mfs.pp.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.model.Address;
import com.mfs.mdm.model.CountryMaster;
import com.mfs.mdm.model.Status;
import com.mfs.mdm.model.TimeZone;
import com.mfs.mdm.model.User;
import com.mfs.pp.dao.UserDao;
import com.mfs.pp.exception.MPDuplicateDataException;
import com.mfs.pp.exception.MPHibernateCommonException;

/**
 * @author sandeep.jakkula
 * 
 */
@Repository
public class UserDaoImpl extends GWBaseDaoImpl implements UserDao {
	
	@Autowired
	SessionFactory sessionFactory;

	private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

	//register User
	@Override
	@Transactional
	public Boolean registerUser(User user) {
		Boolean flag = false;
		//LOGGER.debug("start registeruser UserDaoImpl");
		try {
			user.setFirstActivationDate(new Date());
			getSession().save(user);
			flag = true;
		} catch (Exception e) {
			LOGGER.error("exception while saving user" + e.getMessage());
		}
		return flag;
	}

	//update User
	@Override
	@Transactional
	public Boolean updateUser(User user) throws MPHibernateCommonException {
		Boolean flag = false;
		//LOGGER.debug("start updateUser UserDaoImpl");
		try {
			getSession().saveOrUpdate(user);
			flag = true;
		} catch (Exception e) {
			LOGGER.error("updateUser: while update user" + e.getMessage());
			throw new MPHibernateCommonException(e.getMessage());
		}
		return flag;
	}

	//get Address Set By User Email
	@SuppressWarnings("unchecked")
	@Override
	public List<Address> getAddressSetByUserEmail(String email) {
		//LOGGER.info("Start of method getAddressSetByUserId in UserDaoImpl");
		List<Address> addressInfoList = null;
		try {
			addressInfoList = (ArrayList<Address>) getSession().createQuery("from Address where user.email=?")
					.setParameter(0, email).list();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching user" + e.getMessage());
		}
		//LOGGER.info("End of method getAddressSetByUserId in UserDaoImpl");
		return addressInfoList;
	}
// get Status info
	@Override
	@Transactional
	public Status getStatusInfoByStatusType(String statusType) {
		//LOGGER.info("Start of method getStatusInfoByStatusType in UserDaoImpl");
		Status status = null;
		try {
			status = (Status) getSession().createQuery("from Status where status=?").setParameter(0, statusType)
					.uniqueResult();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching user" + e.getMessage());
		}
		//LOGGER.info("End of method getStatusInfoByStatusType in UserDaoImpl");
		return status;
	}

	
	// get time zone
	@Override
	@Transactional
	public TimeZone getTimeZoneMasterByTimeZoneName(String timeZoneName) {
		//LOGGER.info("Start of method getTimeZoneMasterByTimeZoneName in UserDaoImpl");
		TimeZone timeZoneMaster = null;
		try {
			timeZoneMaster = (TimeZone) getSession().createQuery("from TimeZone where timeZoneName=?")
					.setParameter(0, timeZoneName).uniqueResult();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching user" + e.getMessage());
		}
		//LOGGER.info("End of method getTimeZoneMasterByTimeZoneName in UserDaoImpl");
		return timeZoneMaster;
	}


	//get Address Set By User Email
	@Transactional
	public User getUserInfoByEmail(String email) {
		//LOGGER.info("Start of method checkIfEmailExists in UserDaoImpl");
		User user = null;
		try {
			
			user = (User) getSession().createQuery("from User where email=?").setParameter(0, email).uniqueResult();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching user" + e.getMessage());
		}
		//LOGGER.info("End of method checkIfEmailExists in UserDaoImpl");
		return user;
	}

	//get Address By User
	@Override
	@Transactional
	public Address getAddressByUser(String email) {
		Address addressModel = null;
		try {
			addressModel = (Address) getSession().createCriteria(Address.class, "Address").createAlias("user", "user")
					.add(Restrictions.eq("user.email", email)).uniqueResult();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching user" + e.getMessage());
		}
		//LOGGER.info("End of method getAddressByUser in UserDaoImpl");
		return addressModel;
	}

	//get Country Master By CountryName
	@Override
	@Transactional
	public CountryMaster getCountryMasterByCountryName(String countryName) {
		//LOGGER.info("Start of method getCountryMasterByCountryCode in UserDaoImpl");
		CountryMaster countryMaster = null;
		try {
			countryMaster = (CountryMaster) getSession().createQuery("from CountryMaster where countryName=?")
					.setParameter(0, countryName).uniqueResult();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching user" + e.getMessage());
		}
		//LOGGER.info("End of method getCountryMasterByCountryCode in UserDaoImpl");
		return countryMaster;
	}

	//create Or Update Password
	@Override
	@Transactional
	public boolean createOrUpdatePassword(User user) {
		Boolean flag = false;
		//LOGGER.debug("start updatePassword UserDaoImpl");
		try {
			getSession().saveOrUpdate(user);
			flag = true;
		} catch (Exception e) {
			LOGGER.error("exception while saving user" + e.getMessage());
		}
		return flag;
	}
	
	//save Organization
	@Transactional
	@Override
	public int saveOrganization(Object obj) {
		
		int pk=0;

		try {
		pk=	(int) getSession().save(obj);
			
		} catch (ConstraintViolationException e) {
			throw new MPDuplicateDataException("exception while get primary key");
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e);
		}

		return pk;
	}
	
	//get User
	@Transactional
	public User getUser(int userId) {

		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from User where userId=?";
		Query query2 = session.createQuery(hql2);
		query2.setInteger(0, userId);
		User user = (User) query2.uniqueResult();
		return user;
	}
	
	
}

package com.mfs.pp.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.model.AdminRole;
import com.mfs.mdm.model.AdminRoleAccess;
import com.mfs.mdm.model.AdminUser;
import com.mfs.mdm.model.AdminUserRole;
import com.mfs.mdm.model.Menu;
import com.mfs.mdm.model.OrgRoleAccess;
import com.mfs.mdm.model.OrganizationRole;
import com.mfs.mdm.model.OrganizationUser;
import com.mfs.mdm.model.OrganizationUserRole;
import com.mfs.mdm.model.RoleType;
import com.mfs.pp.dao.RoleDao;
import com.mfs.pp.util.RoleTypeEnum;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Repository
@Transactional
public class RoleDaoImpl extends GWBaseDaoImpl implements RoleDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(RoleDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	//get Default Admin Role List
	@Override
	public List<AdminRole> getDefaultAdminRoleList() {
		List<AdminRole> adminRoleList = null;
		try {
			adminRoleList = (ArrayList<AdminRole>) getSession()
					.createQuery("from AdminRole where roleType.roleTypeName=?")
					.setParameter(0, RoleTypeEnum.DEFAULT.roleTypeName()).list();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching getDefaultAdminRoleList ", e.getMessage());
		}
		return adminRoleList;
	}

	//get Default Org Role List
	@Transactional
	public List<OrganizationRole> getDefaultOrgRoleList(String orgType) {
		List<OrganizationRole> organizationRole = null;
		try {
			organizationRole = (ArrayList<OrganizationRole>) getSession()
					.createQuery("from OrganizationRole where organizationType.orgType=?")
					.setParameter(0, orgType).list();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching getDefaultOrgRoleList ", e.getMessage());
		}
		return organizationRole;
	}

	//get Custom Admin Role List
	@Override
	public List<AdminRole> getCustomAdminRoleList() {
		List<AdminRole> adminRoleList = null;
		try {
			adminRoleList = (ArrayList<AdminRole>) getSession()
					.createQuery("from AdminRole where roleType.roleTypeName=?")
					.setParameter(0, RoleTypeEnum.CUSTOM.roleTypeName()).list();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching getCustomAdminRoleList ", e.getMessage());
		}
		return adminRoleList;
	}

	//get Custom Org Role List
	@Transactional
	public List<OrganizationRole> getCustomOrgRoleList(int organisationId) {
		List<OrganizationRole> organizationRole = null;
		try {
			organizationRole = (ArrayList<OrganizationRole>) getSession()
					.createQuery("from OrganizationRole where organizationId=?")
					.setParameter(0,organisationId).list();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching getCustomAdminRoleList ", e.getMessage());
		}
		return organizationRole;
	}

	//get Org Role Detail
	@Override
	public OrganizationRole getOrgRoleDetail( int organisationRoleId) {

		OrganizationRole orgRole = null;
		try {
			orgRole = (OrganizationRole) getSession().createQuery("from OrganizationRole where organisationRoleId = ? ")
					.setParameter(0, organisationRoleId).uniqueResult();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching getOrgRoleDetail ", e.getMessage());
		}
		return orgRole;

	}

	//get Admin Role Detail
	@Override
	public AdminRole getAdminRoleDetail(int adminRoleId) {

		AdminRole adminRole = null;
		try {
			adminRole = (AdminRole) getSession().createQuery("from AdminRole where adminRoleId = ? ")
					.setParameter(0, adminRoleId).uniqueResult();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching getAdminRoleDetail ", e.getMessage());
		}
		return adminRole;

	}

	//get Menu List
	@Override
	public List<Menu> getMenuList() {
		List<Menu> menuList = null;
		try {
			menuList = (ArrayList<Menu>) getSession().createQuery("from Menu").list();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching getMenuList ", e.getMessage());
		}
		return menuList;
	}

	//get Role Type
	@Override
	public RoleType getRoleType(String roleTypeName) {
		RoleType roleType = null;
		try {
			roleType = (RoleType) getSession().createQuery("from RoleType where roleTypeName=?")
					.setParameter(0, roleTypeName).uniqueResult();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching getRoleType "+ e.getMessage());
		}
		return roleType;
	}

	//get Role Access
	@Override
	public AdminRoleAccess getRoleAccess(int roleId, int menuAccessId) {
		AdminRoleAccess adminRoleAccess = null;
		try {
			adminRoleAccess = (AdminRoleAccess) getSession()
					.createQuery("from AdminRoleAccess where adminRole.adminRoleId=? and menuAccess.menuAccessId = ?")
					.setParameter(0, roleId).setParameter(1, menuAccessId).uniqueResult();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching getRoleAccess "+ e.getMessage());
		}
		return adminRoleAccess;
	}

	//get Org Role Access
	@Override
	public OrgRoleAccess getOrgRoleAccess(int roleId, int menuAccessId) {
		OrgRoleAccess orgRoleAccess = null;
		try {
			orgRoleAccess = (OrgRoleAccess) getSession().createQuery(
					"from OrgRoleAccess where organizationRole.organisationRoleId=? and menuAccess.menuAccessId = ?")
					.setParameter(0, roleId).setParameter(1, menuAccessId).uniqueResult();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching getOrgRoleAccess "+e.getMessage());
		}
		return orgRoleAccess;
	}

	//get Org Role Access List
	@Override
	public List<OrgRoleAccess> getOrgRoleAccessList(int organisationRoleId) {
		List<OrgRoleAccess> organizationAccessRole = null;
		try {
			organizationAccessRole = (ArrayList<OrgRoleAccess>) getSession()
					.createQuery("from OrgRoleAccess where organizationRole.organisationRoleId=?")
					.setParameter(0,organisationRoleId).list();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching getOrgRoleAccessList " +e.getMessage());
		}
		return organizationAccessRole;
	}

	//get Org User Role
	@Override
	public OrganizationUserRole getOrgUserRole(int organisationRoleId) {
		OrganizationUserRole orgRole = null;
		try {
			orgRole = (OrganizationUserRole) getSession().createQuery("from OrganizationUserRole where organizationRole.organisationRoleId = ? ")
					.setParameter(0, organisationRoleId).uniqueResult();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching getOrgUserRole "+e.getMessage());
		}
		return orgRole;
	
	}

	//admin User Role
	@Override
	public AdminUserRole adminUserRole(int adminRoleId) {
		AdminUserRole adminUserRole = null;
		try {
			adminUserRole = (AdminUserRole) getSession().createQuery("from AdminUserRole where adminRole.adminRoleId = ? ")
					.setParameter(0, adminRoleId).uniqueResult();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching adminUserRole "+e.getMessage());
		}
		return adminUserRole;
	}
	
	//admin Role List
	@Transactional
	public List<AdminRole> adminRoleList(String roleName ) {
		
		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from AdminRole where roleName=?";
		Query query2 = session.createQuery(hql2);
		query2.setString(0, roleName);
		List<AdminRole> adminRole = query2.list();
		
	
		return adminRole;
	}

	//get Admin Role Access List
	@Override
	public List<AdminRoleAccess> getAdminRoleAccessList(int adminRoleId) {
		List<AdminRoleAccess> adminAccessRole = null;
		try {
			adminAccessRole = (ArrayList<AdminRoleAccess>) getSession()
					.createQuery("from AdminRoleAccess where adminRole.adminRoleId=?")
					.setParameter(0,adminRoleId).list();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching getAdminRoleAccessList "+e.getMessage());
		}
		return adminAccessRole;
}

	//org Role List
	@Transactional
	public List<OrganizationRole> orgRoleList(int orgId, String roleName) {
		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from OrganizationRole where roleName=? and organizationId=?";
		Query query2 = session.createQuery(hql2);
		query2.setString(0, roleName);
		query2.setInteger(1, orgId);
		List<OrganizationRole> organizationRole = query2.list();
		return organizationRole;
	}

	//get Admin User
	@Transactional
	public AdminUser getAdminUser(int userId) {
		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from AdminUser where user=?";
		Query query2 = session.createQuery(hql2);
		query2.setInteger(0, userId);
		AdminUser adminUser = (AdminUser) query2.uniqueResult();
		return adminUser;
	}
	//get Org User
	@Transactional
	public OrganizationUser getOrgUser(int userId) {
		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from OrganizationUser where user=?";
		Query query2 = session.createQuery(hql2);
		query2.setInteger(0, userId);
		OrganizationUser organizationUser = (OrganizationUser) query2.uniqueResult();
		return organizationUser;
	}
	
	//get Org Role By Id
	@Transactional
	public OrganizationRole getOrgRoleById(int organisationRoleId) {
		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from OrganizationRole where organisationRoleId=?";
		Query query2 = session.createQuery(hql2);
		query2.setInteger(0, organisationRoleId);
		OrganizationRole organizationRole = (OrganizationRole) query2.uniqueResult();
		return organizationRole;
	}	
	

}

/**
 * 
 */
package com.mfs.pp.dao.impl;


import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.pp.dao.BaseDao;
import com.mfs.pp.exception.MPDuplicateDataException;
import com.mfs.pp.exception.MPHibernateCommonException;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Repository
public class BaseDaoImpl implements BaseDao {

	@Autowired
	private SessionFactory sessionFactory;


	//save
	@Transactional
	@Override
	public boolean save(Object obj) {
		boolean isSuccess = false;

		try {
			this.sessionFactory.getCurrentSession().save(obj);
			isSuccess = true;
		} catch (ConstraintViolationException e) {
			throw new MPDuplicateDataException("Duplicate records found");
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}

		return isSuccess;
	}

	//save And Return Object
	@Transactional
	@Override
	public Object saveAndReturnObject(Object obj)  {
	
		Object currentSavedObj = null;

		try {
			currentSavedObj = this.sessionFactory.getCurrentSession().save(obj);
		
		} catch (ConstraintViolationException e) {
			throw new MPDuplicateDataException("Duplicate records found");
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}

		return currentSavedObj;
	}

	//update
	@Transactional
	@Override
	public boolean update(Object obj)  {
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().update(obj);
			isSuccess = true;
		} catch (HibernateException e) {
			
		}
		return isSuccess;
	}

	//save or update
	@Transactional
	@Override
	public boolean saveOrUpdate(Object obj) {
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().saveOrUpdate(obj);
			isSuccess = true;
		} catch (ConstraintViolationException e) {
			throw new MPDuplicateDataException(e);
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e);
		}
		return isSuccess;
	}

	//delete
	@Transactional
	@Override
	public boolean delete(Object obj) {
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().delete(obj);
			isSuccess = true;
		} catch (HibernateException e) {

			throw new MPHibernateCommonException(e);
		}
		return isSuccess;
	}

	//save Batch objects
	@Transactional
	@Override
	public boolean saveBatchObjects(Object obj, int count) {
		boolean isSuccess = false;
		Session session = null;
		try {
			session = this.sessionFactory.getCurrentSession();
			session.saveOrUpdate(obj);
			if (count >= 100) {
				session.flush();
				session.clear();
			}
			isSuccess = true;
		} catch (ConstraintViolationException e) {
			throw new MPDuplicateDataException(e);
		} catch (HibernateException e) {

			throw new MPHibernateCommonException(e);
		}
		return isSuccess;
	}

	//persist
	@Override
	public boolean persist(Object obj) {
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().persist(obj);
			isSuccess = true;
		} catch (ConstraintViolationException e) {
			throw new MPDuplicateDataException(e);
		} catch (HibernateException e) {

			throw new MPHibernateCommonException(e);
		}
		return isSuccess;
	}

	//get
	@Transactional
	@Override
	public Object get(Class clazz, Integer id) {
		Object obj = null;
		try {
			obj = this.sessionFactory.getCurrentSession().get(clazz, id);
		} catch (ConstraintViolationException e) {
			throw new MPDuplicateDataException(e);
		} catch (HibernateException e) {

			throw new MPHibernateCommonException(e);
		}
		return obj;
	}

}

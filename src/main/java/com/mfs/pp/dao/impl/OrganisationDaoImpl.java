/**
 * 
 */
package com.mfs.pp.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.common.type.StatusType;
import com.mfs.mdm.model.Address;
import com.mfs.mdm.model.Category;
import com.mfs.mdm.model.CurrencyMaster;
import com.mfs.mdm.model.LoginAudit;
import com.mfs.mdm.model.Organization;
import com.mfs.mdm.model.OrganizationRole;
import com.mfs.mdm.model.OrganizationUser;
import com.mfs.mdm.model.OrganizationUserRole;
import com.mfs.mdm.model.Status;
import com.mfs.mdm.model.User;
import com.mfs.pp.dao.OrganisationDao;
import com.mfs.pp.exception.GWCommonException;
import com.mfs.pp.exception.MPHibernateCommonException;
import com.mfs.pp.util.Format;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Repository
public class OrganisationDaoImpl extends GWBaseDaoImpl implements OrganisationDao {


	@Autowired
	SessionFactory sessionFactory;

	//get All Oraganisation User By Name
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<OrganizationUser> getAllOraganisationUserByName(String orgName) {
		List<OrganizationUser> organisationUserList = null;
		try {
			if (Format.isStringNotEmptyAndNotNull(orgName)) {
				organisationUserList = new ArrayList<OrganizationUser>();
				Criteria criteria = getSession().createCriteria(OrganizationUser.class, "organizationUser")
						.createAlias("organization", "organization")
						.add(Restrictions.eq("organization.orgName", orgName));
				criteria.createAlias("user", "status");
			//	criteria.createAlias("status", "status");
				criteria.add(Restrictions
						.not(Restrictions.in("status.status", new String[] { StatusType.CLOSED.toString() })));
				organisationUserList = criteria.list();

			}
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}
		return organisationUserList;

	}

	//get Organization By OrgName
	@Override
	@Transactional
	public Organization getOrganizationByOrgName(String organisationName) {
		Organization organization = null;
		try {
			organization = (Organization) getSession().createQuery("from Organization where orgName=?")
					.setParameter(0, organisationName).uniqueResult();
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}
		return organization;
	}

	//get Organization Role By RoleName
	@Override
	@Transactional
	public OrganizationRole getOrganizationRoleByRoleName(String roleName) {
		OrganizationRole organizationRole = null;
		try {
			organizationRole = (OrganizationRole) getSession().createQuery("from OrganizationRole where roleName=?")
					.setParameter(0, roleName).uniqueResult();
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}
		return organizationRole;
	}

	@SuppressWarnings("unchecked")
	
	//get All Roles
	@Override
	@Transactional
	public List<OrganizationRole> getAllRoles() {
		List<OrganizationRole> roleList = null;
		try {
			roleList = getSession().createQuery(" from  OrganizationRole").list();
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}
		return roleList;
	}

	//get Organization User By User Email
	@Override
	@Transactional
	public OrganizationUser getOrganizationUserByUserEmail(String email) {
		OrganizationUser organizationUser = null;
		
		try {
			organizationUser = (OrganizationUser) getSession().createQuery("from OrganizationUser where user.email=?").setParameter(0, email)
					.uniqueResult();
			
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}
		return organizationUser;
	}



	//get Currency Master By CurrencyName
	@Override
	@Transactional
	public CurrencyMaster getCurrencyMasterByCurrencyName(String currencyName) {
		CurrencyMaster currencyMaster = null;
		try {
			currencyMaster = (CurrencyMaster) getSession().createQuery("from CurrencyMaster where currencyName=?")
					.setParameter(0, currencyName).uniqueResult();
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}
		return currencyMaster;
	}


	//get All Currency Masters List
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<CurrencyMaster> getAllCurrencyMastersList() {

		List<CurrencyMaster> curruncyMasterList = null;
		try {
			curruncyMasterList = getSession().createQuery(" from  CurrencyMaster").list();

		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}
		return curruncyMasterList;
	}

	//getOrganization User Roles List By OrganizationUserId
	@SuppressWarnings("unchecked")
	@Override
	public List<OrganizationUserRole> getOrganizationUserRolesListByOrganizationUserId(Integer orgUserId) {
		List<OrganizationUserRole> organizationUserRoleList = null;
		try {
			organizationUserRoleList = getSession()
					.createQuery(" from OrganizationUserRole where organizationUser.id=?").setParameter(0, orgUserId)
					.list();

		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}
		return organizationUserRoleList;
	}

	//get Organization User Role By OrgUserId
	@Override
	public OrganizationUserRole getOrganizationUserRoleByOrgUserId(Integer id) {
		OrganizationUserRole organizationUserRole = null;
		try {
			organizationUserRole = (OrganizationUserRole) getSession()
					.createQuery(" from OrganizationUserRole where organizationUser.id=?").setParameter(0, id)
					.uniqueResult();

		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}
		return organizationUserRole;
	}

	//get Address Set By OrganisationName
	@SuppressWarnings("unchecked")
	@Override
	public List<Address> getAddressSetByOrganisationName(String orgName) {

		List<Address> addressModelList = null;
		try {
			addressModelList = getSession().createCriteria(Address.class, "Address")
					.createAlias("organization", "organization").add(Restrictions.eq("organization.orgName", orgName))
					.list();
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}

		return addressModelList;

	}

	//get Audit Log List By Dates
	@SuppressWarnings("unchecked")
	@Override
	public List<LoginAudit> getAuditLogListByDates(Date fromDate, Date toDate, String orgName) {
		List<LoginAudit> loginAuditList = null;
		try {
			loginAuditList = getSession().createCriteria(LoginAudit.class)
					.add(Restrictions.between("dateTime", fromDate, toDate)).createAlias("organization", "organization")
					.add(Restrictions.eq("organization.orgName", orgName)).list();

		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}
		return loginAuditList;
	}


	//get Organisation List
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Organization> getOrganisationList() throws GWCommonException {
		List<Organization> organisationList = null;
		try {
			organisationList = getSession().createQuery("from Organization ").list();
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}
		return organisationList;
	}

	//get Admin Audit Log List By Dates
	@SuppressWarnings("unchecked")
	@Override
	public List<LoginAudit> getAdminAuditLogListByDates(Date fromDate, Date toDate) {
		List<LoginAudit> loginAuditList = null;
		try {
			loginAuditList = getSession().createCriteria(LoginAudit.class)
					.add(Restrictions.between("dateTime", fromDate, toDate)).add(Restrictions.isNull("organization"))
					.list();

		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}
		return loginAuditList;
	}

	//get All Status Info List
	@SuppressWarnings("unchecked")
	@Override
	public List<Status> getAllStatusInfoList() {
		List<Status> statusInfoList = null;
		try {
			statusInfoList = getSession().createQuery("from Status").list();
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}
		return statusInfoList;
	}

	//get Currency By CountryName
	@Override
	public CurrencyMaster getCurrencyByCountryName(String countryName) {
		CurrencyMaster currencyMaster=null;
		try
		{
			currencyMaster=(CurrencyMaster) getSession().createQuery("from CurrencyMaster where  country=?").setParameter(0, countryName).uniqueResult();
		}
		 catch (HibernateException e) {
				throw new MPHibernateCommonException(e.getMessage());
			}
		return currencyMaster;
	}



	

	//get user
	@Override
	public User getUser(String email) {
		
		User user=null;
		
		try {
			user=(User)getSession().createQuery("select * from User where email=?").setParameter(0, email).uniqueResult();
		}
		catch(HibernateException e)
		{
			
		}
		
		return user;
	}

	//get address
	@Override
	public Address getAddress(String orgId) {
	

		Address address=null;
		
		try {
			address=(Address)getSession().createQuery("select * from Address where organization=?").setParameter(0, orgId).uniqueResult();
		}
		catch(HibernateException e)
		{
			
		}
		
		return address;
	}

	//get Organization Category By CategoryType
	@Override
	@Transactional
	public Category getOrganizationCategoryByCategoryType(String category) {
		Category categoryType = null;
		try {
			categoryType = (Category) getSession().createQuery("from Category where categoryName=?")
					.setParameter(0, category).uniqueResult();
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}
		return categoryType;
	}
	//get list of Organization By Id
	@Override
	@Transactional
	public List<Organization> getOrganizationById(int orgid) {
		List<Organization> organization = null;
		try {
			organization = (ArrayList<Organization>) getSession()
			.createQuery("from Organization where organisationId=?")
			.setParameter(0,0).list();
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}
		return organization;
	}
	
	//get Organization By ID
	@Transactional
	public Organization getOrganizationByID(int orgId) {
		Organization organization = null;
		try {
			organization = (Organization) getSession()
			.createQuery("from Organization where organisationId=?")
			.setParameter(0,orgId).uniqueResult();
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}
		return organization;
	}
	//get Organization User By Id
	@Transactional
	public OrganizationUser getOrganizationUserById(int userId) {
		OrganizationUser organizationUser = null;
		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from OrganizationUser where user=?";
		Query query2 = session.createQuery(hql2);
		query2.setInteger(0, userId);
		 organizationUser = (OrganizationUser) query2.uniqueResult();
		return organizationUser;
	}
	
	//get Organization By Partner Code
	@Transactional
	public Organization getOrganizationByPartnerCode(String partnerCode) {
		Organization organization = null;
		Session session = sessionFactory.getCurrentSession();

		String hql2 = "from Organization where orgId=?";
		Query query2 = session.createQuery(hql2);
		query2.setString(0, partnerCode);
		organization = (Organization) query2.uniqueResult();
		return organization;
	}
	
}

package com.mfs.pp.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.model.CountryMaster;
import com.mfs.mdm.model.TimeZone;
import com.mfs.pp.dao.AccountDao;
import com.mfs.pp.exception.MPHibernateCommonException;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Repository
public class AccountDaoImpl extends GWBaseDaoImpl implements AccountDao {

	/**
	 * Start Account Profile DAO------
	 */

	//get all country list
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<CountryMaster> getAllCountryList() {
		List<CountryMaster> countryList = null;
		try {
			countryList = getSession().createQuery("from CountryMaster ").list();

		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}
		return countryList;
	}

	//get all time zone list
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<TimeZone> getAllTimeZoneList() {
		List<TimeZone> timezoneList = null;
		try {
			timezoneList = getSession().createQuery("from TimeZone").list();
		} catch (HibernateException e) {
			throw new MPHibernateCommonException(e.getMessage());
		}

		return timezoneList;
	}

	/**
	 * End Account Profile DAO------
	 */

}

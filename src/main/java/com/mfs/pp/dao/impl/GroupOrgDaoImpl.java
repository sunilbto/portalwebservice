package com.mfs.pp.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.model.Group;
import com.mfs.mdm.model.OrgGroup;
import com.mfs.mdm.model.Organization;
import com.mfs.pp.dao.GroupOrgDao;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */
@EnableTransactionManagement
@Repository("GroupOrgDao")
public class GroupOrgDaoImpl extends BaseDaoImpl implements GroupOrgDao {

	@Autowired
	SessionFactory sessionFactory;

	//check group by name
	@Transactional
	public Group check(String groupName) {
		Session session = sessionFactory.getCurrentSession();

		String hql = "from Group where groupName=:groupName";
		Query query = session.createQuery(hql);
		query.setString("groupName", groupName);
		Group group = (Group) query.uniqueResult();

		return group;
	}

	//get all groups
	@Transactional
	public List<Group> getAllGroup() {

		Session session = sessionFactory.getCurrentSession();

		String hql = "from Group";
		Query query = session.createQuery(hql);
		List<Group> GroupList = query.list();

		return GroupList;
	}

	//get Organization By Group Name
	@Transactional
	public Group getOrganizationByGroupName(String groupName) {

		Session session = sessionFactory.getCurrentSession();

		String hql = "from Group where groupName=?";
		Query query = session.createQuery(hql);
		query.setString(0, groupName);
		Group group = (Group) query.uniqueResult();

		return group;
	}

	//get Organization details by orgId
	@Transactional
	public Organization getOrgdetails(int orgId) {
		Session session = sessionFactory.getCurrentSession();
		
		String hql = "from OrgGroup where group=?";
		Query query = session.createQuery(hql);
		query.setInteger(0, orgId);
		OrgGroup orgGroup = (OrgGroup) query.uniqueResult();
	  
 
		String hql1 = "from Organization where organisationId=?";
		Query query1 = session.createQuery(hql1);
        query1.setInteger(0, orgGroup.getOrgId());
		Organization organization = (Organization) query1.uniqueResult();
		return organization;
	}

	
	//get Organization Group
	@Transactional
	public OrgGroup getOrgGroup(int orgId) {

		Session session = sessionFactory.getCurrentSession();

		String hql = "from OrgGroup where orgId=?";
		Query query = session.createQuery(hql);
		query.setInteger(0, orgId);
		OrgGroup orgGroup = (OrgGroup) query.uniqueResult();

		return orgGroup;
	}

	//get All Organization
	@Transactional
	public List<Organization> getAllOrg() {
		Session session = sessionFactory.getCurrentSession();

		String hql = "from Organization";
		Query query = session.createQuery(hql);
		List<Organization> organizationList = query.list();

		return organizationList;

	}

	//get Organization details by orgName
	@Transactional
	public Organization getOrgdetails(String orgName) {
		Session session = sessionFactory.getCurrentSession();

		String hql = "from Organization where orgName=:orgName";
		Query query = session.createQuery(hql);
		query.setString("orgName", orgName);
		Organization organization = (Organization) query.uniqueResult();

		return organization;
	}

	//get Organization groups by groupId
	@Transactional
	public List<OrgGroup> getOrgGroups(int groupId) {
		Session session = sessionFactory.getCurrentSession();
		String hql2 = "from OrgGroup where group=?";
		Query query2 = session.createQuery(hql2);
		query2.setInteger(0, groupId);
		List<OrgGroup> orgGroup = query2.list();
		return orgGroup;
	}
	
	//get Organization by orgId
	@Transactional
	public Organization getOrg(int orgId) {
		Session session = sessionFactory.getCurrentSession();

		String hql = "from Organization where organisationId=?";
		Query query = session.createQuery(hql);
		query.setInteger(0, orgId);
		Organization organization = (Organization) query.uniqueResult();

		return organization;
	}
	
	//get Group  by groupId
	@Transactional
	public Group getOrganizationByGroupId(int groupId) {

		Session session = sessionFactory.getCurrentSession();

		String hql = "from Group where groupId=?";
		Query query = session.createQuery(hql);
		query.setInteger(0, groupId);
		Group group = (Group) query.uniqueResult();

		return group;
	}
	

	
}

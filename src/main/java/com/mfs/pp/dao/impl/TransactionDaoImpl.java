package com.mfs.pp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mfs.mdm.model.SystemConfig;
import com.mfs.pp.dao.TransactionDao;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Repository("TransactionDaoImpl")
public class TransactionDaoImpl  implements TransactionDao  {
	
	
	private static final Logger LOGGER = Logger.getLogger(TransactionDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	//get Config Details Map
	@Transactional
	public Map<String, String> getConfigDetailsMap() {

		//LOGGER.debug("Inside getConfigDetailsMap of TransactionDaoImpl");

		Map<String, String> SystemConfigurationMap = null;

		Session session = sessionFactory.getCurrentSession();

		String hql = " From SystemConfig";
		Query query = session.createQuery(hql);
		List<SystemConfig> SystemConfigurationList = query.list();
		if (SystemConfigurationList != null && !SystemConfigurationList.isEmpty()) {
			SystemConfigurationMap = new HashMap<String, String>();
			for (SystemConfig euSystemConfiguration : SystemConfigurationList) {
				SystemConfigurationMap.put(euSystemConfiguration.getConfigKey(),
						euSystemConfiguration.getConfigValue());
			}
		}
		if (SystemConfigurationMap != null && !SystemConfigurationMap.isEmpty()) {
			//LOGGER.info("getConfigDetailsMap response size -> " + SystemConfigurationMap.size());
		}
		return SystemConfigurationMap;
	}
}

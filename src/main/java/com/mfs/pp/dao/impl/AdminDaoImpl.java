/**
 * 
 */
package com.mfs.pp.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.mfs.mdm.common.type.StatusType;
import com.mfs.mdm.model.AdminRole;
import com.mfs.mdm.model.AdminUser;
import com.mfs.mdm.model.RoleType;
import com.mfs.pp.dao.AdminDao;
import com.mfs.pp.exception.MPHibernateCommonException;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Repository
public class AdminDaoImpl extends GWBaseDaoImpl implements AdminDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminDaoImpl.class);

	//get admin user by email
	@Override
	public AdminUser getAdminUserByUserEmail(String email) {
		AdminUser adminUser = null;
		try {
			adminUser = (AdminUser) getSession().createQuery("from AdminUser where user.email=?").setParameter(0, email)
					.uniqueResult();
		} catch (HibernateException e) {
			LOGGER.error("Exception in class AdminDaoImpl method getAdminUserByUserEmail : " + e.getMessage());
			throw new MPHibernateCommonException(e.getMessage());
		}
		return adminUser;
	}

	//get admin role by roleID
	@Override
	public AdminRole getAdminRoleByRoleId(int adminRoleId) {
		AdminRole adminRole = null;
		try {
			adminRole = (AdminRole) getSession().createQuery("from AdminRole where adminRoleId=?")
					.setParameter(0, adminRoleId).uniqueResult();
		} catch (HibernateException e) {
			LOGGER.error("Exception in class AdminDaoImpl method getAdminUserByUserId : " + e.getMessage());
			throw new MPHibernateCommonException(e.getMessage());
		}
		return adminRole;
	}

	//get all system admin users
	@SuppressWarnings("unchecked")
	@Override
	public List<AdminUser> getAllSystemAdminUsers() {
		//LOGGER.info("Start of method getAllSystemAdminUsers in UserDaoImpl");
		List<AdminUser> user = null;
		try {
			user = (List<AdminUser>) getSession().createQuery("from AdminUser au where au.user.status.status not in ('"+StatusType.CLOSED.toString()+"') ").list();
		} catch (HibernateException e) {
			LOGGER.error("exception while fetching user" + e.getMessage());
		}
		LOGGER.info("End of method getAllSystemAdminUsers in UserDaoImpl");
		return user;
	}
	
	//get role type
	@Override
	public RoleType getRoleType(int roleTypeId) {
		RoleType roleType = null;
		try {
			roleType = (RoleType) getSession().createQuery("from RoleType where roleTypeId=?")
					.setParameter(0, roleTypeId).uniqueResult();
		} catch (HibernateException e) {
			LOGGER.error("Exception in class AdminDaoImpl method getRoleType : " + e.getMessage());
			throw new MPHibernateCommonException(e.getMessage());
		}
		return roleType;
	}

}

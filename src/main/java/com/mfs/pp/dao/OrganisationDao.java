/**
 * 
 */
package com.mfs.pp.dao;

import java.util.Date;
import java.util.List;

import com.mfs.mdm.model.Address;
import com.mfs.mdm.model.Category;
import com.mfs.mdm.model.CurrencyMaster;
import com.mfs.mdm.model.LoginAudit;
import com.mfs.mdm.model.Organization;
import com.mfs.mdm.model.OrganizationRole;
import com.mfs.mdm.model.OrganizationUser;
import com.mfs.mdm.model.OrganizationUserRole;
import com.mfs.mdm.model.Status;
import com.mfs.mdm.model.User;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface OrganisationDao extends BaseDao {

	public List<OrganizationUser> getAllOraganisationUserByName(String orgName);

	public Organization getOrganizationByOrgName(String organisationName);

	public OrganizationRole getOrganizationRoleByRoleName(String roleName);

	public List<OrganizationRole> getAllRoles();

	public OrganizationUser getOrganizationUserByUserEmail(String email);

	public CurrencyMaster getCurrencyMasterByCurrencyName(String currencyCode);

	public List<CurrencyMaster> getAllCurrencyMastersList();

	public List<OrganizationUserRole> getOrganizationUserRolesListByOrganizationUserId(Integer orgUserId);

	public OrganizationUserRole getOrganizationUserRoleByOrgUserId(Integer id);

	public List<Address> getAddressSetByOrganisationName(String orgName);

	public List<LoginAudit> getAuditLogListByDates(Date fromDate, Date toDate, String orgName);

	public List<Organization> getOrganisationList();

	List<LoginAudit> getAdminAuditLogListByDates(Date fromDate, Date toDate);

	public List<Status> getAllStatusInfoList();

	public CurrencyMaster getCurrencyByCountryName(String countryName);

	public List<Organization> getOrganizationById(int orgId);
	
	public User getUser(String email);
	
	public Address getAddress(String orgId);

	Category getOrganizationCategoryByCategoryType(String category);
	
	public Organization getOrganizationByID(int orgId);
	
	public OrganizationUser getOrganizationUserById(int userId);
	
	public Organization getOrganizationByPartnerCode(String partnerCode);
	
	
	
	
}

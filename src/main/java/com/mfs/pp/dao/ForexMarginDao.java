package com.mfs.pp.dao;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface ForexMarginDao {
	public double getForexMargin();

}

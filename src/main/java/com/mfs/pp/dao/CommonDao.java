package com.mfs.pp.dao;

import java.util.List;

import com.mfs.mdm.model.Address;
import com.mfs.mdm.model.Category;
import com.mfs.mdm.model.CountryMaster;
import com.mfs.mdm.model.CurrencyMaster;
import com.mfs.mdm.model.IdType;
import com.mfs.mdm.model.OrgGroup;
import com.mfs.mdm.model.OrganizationType;
import com.mfs.mdm.model.OrganizationUser;
import com.mfs.mdm.model.PartnerType;
import com.mfs.mdm.model.StateRegion;
import com.mfs.mdm.model.Status;
import com.mfs.mdm.model.TimeZone;
import com.mfs.mdm.model.User;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */
public interface CommonDao {

public List<OrganizationType> getAllOrgType();
	
	public List<Category> getAllCategory();
	
	public List<Status> getAllStatus();
	
	public Category getcategory(String catergoryName);
	
	public User getUser(String email);
	
	public OrgGroup getOrg(int orgId);
	
	public Address getAddress(int orgId);
	
	public OrganizationType getOrgType(String OrgTypeName);
	
	public CountryMaster getCountryDteails(int countryId);
	
	public TimeZone getTimeZone(String timeZone);
	
	public Address getUserAddress(int userId);
	
	public List<OrganizationUser> getOrgUser(String orgName);
	
	public CurrencyMaster getCourencyDteail(String countryCode);
	
	public List<StateRegion> getStateList();
	
	public List<IdType> getIdType();
	
	public IdType getIdType(String idType);
	
	public List<PartnerType> getPartnerTypeList(); 
	
	public PartnerType getPartnerType(String partnerType);
	
	
}

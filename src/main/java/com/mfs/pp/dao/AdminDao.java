/**
 * 
 */
package com.mfs.pp.dao;

import java.util.List;

import com.mfs.mdm.model.AdminRole;
import com.mfs.mdm.model.AdminUser;
import com.mfs.mdm.model.RoleType;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */
public interface AdminDao extends BaseDao {

	AdminUser getAdminUserByUserEmail(String email);

	AdminRole getAdminRoleByRoleId(int roleId);
	
	public  List<AdminUser> getAllSystemAdminUsers();

	RoleType getRoleType(int roleTypeId);

}

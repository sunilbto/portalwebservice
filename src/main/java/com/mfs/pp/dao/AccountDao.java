/**
 * 
 */
package com.mfs.pp.dao;

import java.util.List;

import com.mfs.mdm.model.CountryMaster;
import com.mfs.mdm.model.TimeZone;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */
public interface AccountDao  extends BaseDao {
	
	public List<CountryMaster> getAllCountryList();
	
	public List<TimeZone> getAllTimeZoneList();

}

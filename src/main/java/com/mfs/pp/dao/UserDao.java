/**
 * 
 */
package com.mfs.pp.dao;

import java.util.List;

import com.mfs.mdm.model.Address;
import com.mfs.mdm.model.CountryMaster;
import com.mfs.mdm.model.Status;
import com.mfs.mdm.model.TimeZone;
import com.mfs.mdm.model.User;

/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface UserDao extends BaseDao {

	public Boolean registerUser(User user);

	public List<Address> getAddressSetByUserEmail(String id);

	public Status getStatusInfoByStatusType(String statusType);

	public TimeZone getTimeZoneMasterByTimeZoneName(String timeZoneName);

	public User getUserInfoByEmail(String email);

	public Address getAddressByUser(String email);

	public Boolean updateUser(User user);

	public CountryMaster getCountryMasterByCountryName(String countryCode);

	public boolean createOrUpdatePassword(User user);
	
	public int saveOrganization(Object obj);
	
	public User getUser(int userId);

}

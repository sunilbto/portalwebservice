package com.mfs.pp.dao;

import java.util.List;

import com.mfs.mdm.model.QueryType;
import com.mfs.pp.dto.QueryTypeDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface ContactDetailsDao extends BaseDao{
	public QueryType checkQueryTypeExits(String queryType);
	
	public List<QueryTypeDto> getQueryType();

}
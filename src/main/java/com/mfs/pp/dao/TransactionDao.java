package com.mfs.pp.dao;

import java.util.Map;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface TransactionDao {
	
	public Map<String, String> getConfigDetailsMap();

}

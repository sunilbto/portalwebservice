package com.mfs.pp.dao;

import java.util.List;

import com.mfs.mdm.model.CountryMaster;
import com.mfs.mdm.model.CountryZone;
import com.mfs.mdm.model.Zone;


/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface CountryZoneDao extends BaseDao {
	
	public List<CountryZone> getCountryByzone(int zoneId); 
	
	public List<CountryZone> getallZone(int zoneId);

	public Zone checkZoneExist(int zoneId);
	
	public Zone checkZoneExist(String zoneName);
	
	public CountryMaster getCountryDetails(String countryName);
	
	public List<Zone> getallZones();
	
	
	 
	 
	
	
	
	
}

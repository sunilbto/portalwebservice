package com.mfs.pp.dao;

import java.util.List;

import com.mfs.mdm.model.Group;
import com.mfs.mdm.model.OrgGroup;
import com.mfs.mdm.model.Organization;

/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface GroupOrgDao extends BaseDao {

	public Group check(String groupName);

	public List<Group> getAllGroup();
	
	public List<Organization> getAllOrg();

	public Group getOrganizationByGroupName(String groupName);
	
	public Organization getOrgdetails(int orgId);
	
	public OrgGroup getOrgGroup(int orgId);
	
	public Organization getOrgdetails(String orgName);
	
	public Group getOrganizationByGroupId(int groupId);
	
	public List<OrgGroup> getOrgGroups(int groupId);
	
	public Organization getOrg(int orgId);
	
	
	
	
	
}

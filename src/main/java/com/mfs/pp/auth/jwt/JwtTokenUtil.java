package com.mfs.pp.auth.jwt;

import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import com.mfs.pp.dto.UserResponse;
import com.mfs.pp.security.userdetails.CustomUserDetails;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClock;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Component
public class JwtTokenUtil {
    
	private static final SignatureAlgorithm SIGN_ALGO_HS512 = SignatureAlgorithm.HS512;
    private static Clock clock = DefaultClock.INSTANCE;
    
    @Autowired
    @Qualifier("jwtTokenProperties")
 	private Properties jwtTokenProperties;    
 

    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    public Date getIssuedAtDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getIssuedAt);
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
            .setSigningKey(jwtTokenProperties.getProperty("secret"))
            .parseClaimsJws(token)
            .getBody();
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(clock.now());
    }

    private Boolean isCreatedBeforeLastPasswordReset(Date created, Date lastPasswordReset) {
        return (lastPasswordReset != null && created.before(lastPasswordReset));
    }

    private Boolean ignoreTokenExpiration(String token) {
        // Additionally specify those tokens for which the expiration can be ignored
        return false;
    }

    public String generateToken(String email) {
        Claims claims = Jwts.claims().setSubject(email);
        final Date expirationDate = calculateExpirationDate();
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(expirationDate)
                .signWith(SIGN_ALGO_HS512, jwtTokenProperties.getProperty("secret"))
                .compact();
    }

    public Boolean canTokenBeRefreshed(String token, Date lastPasswordReset) {
        final Date created = getIssuedAtDateFromToken(token);
        return !isCreatedBeforeLastPasswordReset(created, lastPasswordReset)
            && (!isTokenExpired(token) || ignoreTokenExpiration(token));
    }

    public String refreshToken(String token) {
        final Date createdDate = clock.now();
        final Date expirationDate = calculateExpirationDate();

        final Claims claims = getAllClaimsFromToken(token);
        claims.setIssuedAt(createdDate);
        claims.setExpiration(expirationDate);

        return Jwts.builder()
            .setClaims(claims)
            .signWith(SIGN_ALGO_HS512, jwtTokenProperties.getProperty("secret"))
            .compact();
    }

    public Boolean validateToken(String token) {
        return ( !isTokenExpired(token));
    }

    private Date calculateExpirationDate() {
    	Calendar cal = Calendar.getInstance();
    	cal.setTimeInMillis(cal.getTimeInMillis()+Long.parseLong(jwtTokenProperties.getProperty("tokenExpiry")));
        return cal.getTime();
    }

    public CustomUserDetails parseToken(String token) {
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(jwtTokenProperties.getProperty("secret"))
                    .parseClaimsJws(token)
                    .getBody();
            CustomUserDetails customUserDetails = null;
            if(null != body) {
            	 customUserDetails = new CustomUserDetails();   
            	 User user = new User(body.getSubject(), null, null);
                 // customized way of getting value from body, object of Claims, body.get("id")
                 customUserDetails.setUser(user);
            }
            return customUserDetails;

        } catch (JwtException | ClassCastException e) {
            return null;
        }
    }

    
    public String parseTokenForUserName(String token) {
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(jwtTokenProperties.getProperty("secret"))
                    .parseClaimsJws(token)
                    .getBody();

            return body.getSubject();

        } catch (JwtException | ClassCastException e) {
            return null;
        }
    }

	public void generateTokenAndSetInRestResponse(final String emailCurrentUser, final UserResponse response) {
		String generatedToken = generateToken(emailCurrentUser);
		response.setToken(generatedToken);
	}

	public Properties getJwtTokenProperties() {
		return jwtTokenProperties;
	}

}

package com.mfs.pp.auth.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import com.mfs.pp.service.impl.CustomUserDetailsService;

import io.jsonwebtoken.ExpiredJwtException;

/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class JwtAuthorizationTokenFilter extends AbstractAuthenticationProcessingFilter  {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CustomUserDetailsService userDetailsService;
    
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
       
    public JwtAuthorizationTokenFilter() {
        super("/**");
    }

    @Override
    protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {
        return true;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        
        final String header = request.getHeader(jwtTokenUtil.getJwtTokenProperties().getProperty("authHeader"));
        String authToken = null;
        String username = null;
        
        if (header != null && header.startsWith(jwtTokenUtil.getJwtTokenProperties().getProperty("startsWith"))) {
            authToken = header.substring(7);
            try {
                username = jwtTokenUtil.getUsernameFromToken(authToken);
            } catch (IllegalArgumentException e) {
                log.error("an error occured during getting username from token", e);
            } catch (ExpiredJwtException e) {
//                log.warn("the token is expired and not valid anymore", e);
            }
        } else {
//            log.warn("couldn't find bearer string, will ignore the header");
        }
     
//        log.debug("checking authentication for user '{}'", username);
        
        if (!jwtTokenUtil.validateToken(authToken) && null != username) {
//            log.debug("authToken was invalid, so authorizing user");

            UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//                log.info("authorizated user '{}', setting security context", username);
                SecurityContextHolder.getContext().setAuthentication(authentication);
                return authentication;
        }
       String userEmail =  jwtTokenUtil.parseTokenForUserName(authToken);
       UserDetails userDetails = this.userDetailsService.loadUserByUsername(userEmail);
       JwtAuthenticationToken authenticationToken = new JwtAuthenticationToken(userDetails, null, userDetails.getAuthorities(), authToken);
       authenticationToken.setDetails(userDetails);
       SecurityContextHolder.getContext().setAuthentication(authenticationToken);
       return authenticationToken;
    }
    	

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult)
            throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);

         //The authentication is in HTTP header, after success continue the request normally
         //return the response such as resource was not secured at all
        chain.doFilter(request, response);  
    }
    
  
}

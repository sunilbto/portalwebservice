package com.mfs.pp.auth.jwt;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class JwtAuthenticationToken extends UsernamePasswordAuthenticationToken {

	/** Default serial version id	 */
	private static final long serialVersionUID = -443436671742026920L;
    private String token;

  public JwtAuthenticationToken(String token) {
      super(null, null);
      this.token = token;
  }
  
  public JwtAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authoroties, String token) {
		super(principal, credentials,authoroties);
		this.token = token;
	} 

  @Override
  public Object getCredentials() {
    return "N/A";
  }

  public String getToken() {
    return token;
  }
  
}
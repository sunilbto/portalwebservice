package com.mfs.pp.service;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface StaticsticFilterService {
	
	public Object getStaticsticFilter()throws Exception;

}

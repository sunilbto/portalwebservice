package com.mfs.pp.service;

import com.mfs.pp.dto.MoneyFlowDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface MoneyFlowService {

	public Object getMoneyFlows(MoneyFlowDto moneyFlowDto);
}

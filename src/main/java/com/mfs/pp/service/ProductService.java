package com.mfs.pp.service;

import java.util.List;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface ProductService {

	public List<Object> getAllProduct() throws Exception;
}

package com.mfs.pp.service;

import com.mfs.pp.dto.StatisticTransactionRequestDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface StatisticTransactionService {
	
	public Object getStatisticTransactionDetailService(StatisticTransactionRequestDto request)throws Exception;

}

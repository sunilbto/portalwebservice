package com.mfs.pp.service;

import com.mfs.pp.dto.AllDashBoardRequestDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface DashBoardService {
	
	public Object getDashBoard(AllDashBoardRequestDto request) throws Exception;

}

package com.mfs.pp.service;

import java.util.List;

import com.mfs.mdm.common.dto.CountryDto;
import com.mfs.mdm.common.dto.PasswordDto;
import com.mfs.mdm.common.dto.TimeZoneDto;
import com.mfs.mdm.common.dto.UserDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface UserService {

	public Boolean registerUser(UserDto userDTO) throws Exception;

	public List<CountryDto> getAllCountryDtoList() throws Exception;

	public List<TimeZoneDto> getAllTimeZoneDtoList() throws Exception;

	public boolean updateUser(UserDto userDTO) throws Exception;

	public UserDto getUserByUserEmailId(String email) throws Exception;

	public boolean createOrUpdatePassword(UserDto userDto) throws Exception;

	public boolean invalidateSession() throws Exception;

	public boolean isAdminUser(String email);

	public boolean deleteUserByEmail(String email) throws Exception;

	public boolean changePartnerPassword(PasswordDto passwordDto) throws Exception;

}

package com.mfs.pp.service;

import com.mfs.pp.dto.MoneyFilterRequestDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface MoneyFilterService {
	
	public Object moneyfilterService(MoneyFilterRequestDto request)throws Exception;

	  
}

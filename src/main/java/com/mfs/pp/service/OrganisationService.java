/**
 * 
 */
package com.mfs.pp.service;

import java.util.List;

import com.mfs.mdm.common.dto.CurrencyDto;
import com.mfs.mdm.common.dto.OrganisationProfileDto;
import com.mfs.mdm.common.dto.RolesDto;
import com.mfs.mdm.common.dto.UserDto;
import com.mfs.pp.dto.OrganizationListsResponse;
import com.mfs.pp.dto.OrganizationResponse;
import com.mfs.pp.dto.OrganizationResponseByName;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface OrganisationService {

	public List<UserDto> getAllUserByOrganisation(String organisationName) throws Exception;

	public List<RolesDto> getAllRoles() throws Exception;

	public int updateOrganizationDetails(OrganisationProfileDto organisationProfileDto) throws Exception;

	public boolean updateOrganizationUser(UserDto userDto) throws Exception;

	public List<CurrencyDto> getAllCurruncyDtos() throws Exception;

	public String getAPIKeyByOrganisationName(String organisationName) throws Exception;

	public OrganisationProfileDto getOrganisationDetailsByOrgName(String organisationName) throws Exception;

	public List<String> getAllReportTypesList() throws Exception;

	public List<OrganisationProfileDto> getAllOrganisationListForAdmin() throws Exception;

	public List<String> getAllStatusTypes() throws Exception;

	public CurrencyDto getCurrencyByCountryName(String countryName);
	
	public String UpdateOrganization(OrganisationProfileDto organisationProfileDto)throws Exception;
	
	public OrganizationResponse getOrgList()throws Exception;
	
	public OrganizationResponseByName getOrgDetails(String orgName)throws Exception;
	
	public OrganizationResponseByName getOrgDetail(String orgName)throws Exception;
	
	public List<OrganizationListsResponse> getOrgLists()throws Exception;
	
	
	

}

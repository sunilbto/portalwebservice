package com.mfs.pp.service;

import java.util.List;

import com.mfs.pp.dto.GetCountryRequestDto;
import com.mfs.pp.dto.GetCountryResponseDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface GetCountryService {

	public List<GetCountryResponseDto> getCountryByZone(GetCountryRequestDto request)throws Exception;
}

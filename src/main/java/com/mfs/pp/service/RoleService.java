package com.mfs.pp.service;

import java.util.List;

import com.mfs.pp.dto.MenuDTO;
import com.mfs.pp.dto.RoleAssignList;
import com.mfs.pp.dto.RoleAssignRequest;
import com.mfs.pp.dto.RoleDTO;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface RoleService {

	public List<RoleDTO> getListOfAdminRole () throws Exception;
	
	public List<RoleDTO> getListOfOrgRole (int orgId) throws Exception;
	
	public void createAdminRole(RoleDTO roleDTO) throws Exception;
	
	public void updateAdminRole(RoleDTO roleDTO) throws Exception;
	
	public void createOrgRole(RoleDTO roleDTO) throws Exception;
	
	public void updateOrgRole(RoleDTO roleDTO) throws Exception;
	
	public RoleDTO getAdminRoleDetail(int roleId) throws Exception;
	
	public RoleDTO getOrgRoleDetail(int roleId) throws Exception;
	
	public List<MenuDTO> getMenuList ();
	
	public boolean deleteOrgRole(int roleId);
	
	public boolean deleteAdminRole(int roleId);
	
	public RoleAssignList getRoleAssign(RoleAssignRequest roleAssignRequest);
	
}

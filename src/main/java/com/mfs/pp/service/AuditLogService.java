package com.mfs.pp.service;

import java.util.List;

import com.mfs.mdm.common.dto.AuditLogInputDto;
import com.mfs.mdm.common.dto.LoginAuditLogDTO;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface AuditLogService {

	public boolean saveAuditLog(LoginAuditLogDTO loginAuditLogDTO);
	
	public List<LoginAuditLogDTO> getAuditLogListByDates(AuditLogInputDto reportsInputDto);
	
	public LoginAuditLogDTO getLoginAuditDtoForLogin(String userEmail,String address,String event, String Description,String orgName);

	public List<LoginAuditLogDTO> getAdminAuditLogListByDates(AuditLogInputDto reportsInputDto);
	
}

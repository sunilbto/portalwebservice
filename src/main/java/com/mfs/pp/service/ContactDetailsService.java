package com.mfs.pp.service;

import java.util.List;

import com.mfs.pp.dto.ContactDto;
import com.mfs.pp.dto.ContactSaveResponseDto;
import com.mfs.pp.dto.QueryTypeDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface ContactDetailsService {
	public ContactSaveResponseDto saveConatctDetails( ContactDto contact) throws Exception;
	
	public List<QueryTypeDto> getQueryTypes();
}
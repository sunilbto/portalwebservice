package com.mfs.pp.service;

import org.springframework.web.bind.annotation.RequestBody;

import com.mfs.pp.dto.AllBalanceRequestDto;

/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */


public interface BalanceExcelService {
	
	public Object GenExcelForBalance(@RequestBody AllBalanceRequestDto request)throws Exception;

}

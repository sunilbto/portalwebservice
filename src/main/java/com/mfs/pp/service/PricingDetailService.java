package com.mfs.pp.service;

import com.mfs.pp.dto.PricingFinalResponseDto;
import com.mfs.pp.dto.PricingRequestDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface PricingDetailService {
	
	public PricingFinalResponseDto getPricingDetailsService(PricingRequestDto request)throws Exception; 

}

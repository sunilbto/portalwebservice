package com.mfs.pp.service;

import java.util.List;

import com.mfs.pp.dto.CategoryResponse;
import com.mfs.pp.dto.OrganizationTypes;
import com.mfs.pp.dto.PartnerTypeDto;
import com.mfs.pp.dto.StatusResponse;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface CommonService {
	
	List<OrganizationTypes> getOrgTypeList();
	
	List<CategoryResponse> getCategoryList();
	
	List<StatusResponse> getStatusList();
	
	List<PartnerTypeDto> getPartnerTypeList();
	
	

}

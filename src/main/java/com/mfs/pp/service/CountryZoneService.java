package com.mfs.pp.service;

import com.mfs.pp.dto.Zones;
import com.mfs.pp.dto.ZonesResponse;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface CountryZoneService {

	public String createZoneService(Zones zone) throws Exception;

	public ZonesResponse getAllZone() throws Exception;

	public String udpateByZone(Zones zone) throws Exception;

}

package com.mfs.pp.service;

import com.mfs.pp.dto.ComplianceRequestDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface ComplianceDetailService {
	public Object getComplianceDetails(ComplianceRequestDto request)throws Exception;

}

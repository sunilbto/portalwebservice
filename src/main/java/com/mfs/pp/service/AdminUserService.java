package com.mfs.pp.service;

import java.util.List;

import com.mfs.mdm.common.dto.PasswordDto;
import com.mfs.mdm.common.dto.UserDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface AdminUserService {

	public boolean updateAdminUser(UserDto userDTO) throws Exception;

	public boolean inviteAdminUser(UserDto userDto) throws Exception;

	public boolean changeAdminPassword(PasswordDto passwordDto) throws Exception;

	public boolean checkIfUserActive(String email) throws Exception;

	public List<UserDto> getAllSystemAdminUsers() throws Exception;
	
	public boolean deleteUserByEmail(String email) throws Exception; 
	
	public boolean checkPassword(String email,String currentPassword,String newPassword,String oldPassword);

}

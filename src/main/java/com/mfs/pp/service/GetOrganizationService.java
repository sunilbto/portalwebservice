package com.mfs.pp.service;

import java.util.List;

public interface GetOrganizationService {

	
	public List<Object> getOrganizationList() throws Exception;
}

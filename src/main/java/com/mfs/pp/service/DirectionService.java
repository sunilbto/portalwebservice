package com.mfs.pp.service;

import java.util.List;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface DirectionService {
	
	public List<Object> getDirectionList() throws Exception;

}

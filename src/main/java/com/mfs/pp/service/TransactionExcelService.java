package com.mfs.pp.service;

import org.springframework.web.bind.annotation.RequestBody;

import com.mfs.pp.dto.AllTransactionRequestDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface TransactionExcelService {

	public Object GenExcelForTransaction(@RequestBody AllTransactionRequestDto request)throws Exception;
	
}

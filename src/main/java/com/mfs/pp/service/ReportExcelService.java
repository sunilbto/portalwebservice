package com.mfs.pp.service;

import org.springframework.web.bind.annotation.RequestBody;

import com.mfs.pp.dto.AllDashBoardRequestDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface ReportExcelService {
	
	public Object GenExcelForBalance(@RequestBody AllDashBoardRequestDto request)throws Exception;

}

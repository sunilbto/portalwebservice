package com.mfs.pp.service;

import com.mfs.pp.dto.StatisticCounsumeRequestDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface StatisticCounsumeService {

	public Object statisticCounsume(StatisticCounsumeRequestDto request)throws Exception;
	
}
package com.mfs.pp.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.model.AdminUser;
import com.mfs.mdm.model.AdminUserRole;
import com.mfs.pp.dao.UserDao;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.exception.ServiceErrFactory;
import com.mfs.pp.security.userdetails.CustomUserDetails;
import com.mfs.pp.type.MFSErrorType;
import com.mfs.pp.util.Format;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Service
public class AdminUserDetailsService implements UserDetailsService {
  //  private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired

	UserDao userDao;

	@Autowired
	ServiceErrFactory serviceErrFactory;

	@Autowired
	private AuthenticationManager authenticationManager;
	/**
	 * Return user based on email
	 */

	@Override
	@Transactional
	public CustomUserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		//log.info("Inside loadUserByUsername method of CustomUserDetails");
		com.mfs.mdm.model.User user = null;
		CustomUserDetails customUserDetails = null;
		// Return user based on email
		user = userDao.getUserInfoByEmail(email);
		//check user
		if (null != user && Format.isCollectionNotEmptyAndNotNull(user.getAdminUsers())) {

			Set<AdminUser> adminUsers = user.getAdminUsers();
			AdminUser adminUser = adminUsers.stream().findFirst().get();
			Set<AdminUserRole> adminUserRoles = adminUser.getAdminUserRoles();
			Set<GrantedAuthority> authorities = buildUserAuthorityForAdminUser(adminUserRoles);
			customUserDetails = new CustomUserDetails();
			customUserDetails.setUser(buildUserForAuthenticationForAdminUser(user, authorities));
			customUserDetails.setAuthorities(authorities);
			customUserDetails.setUserId(user.getUserId());

		} else {
			
			throw new CustomException(MFSErrorType.USER_DOES_NOT_EXIST.getDescription());
		}
		//log.info("Exiting loadUserByUsername method of CustomUserDetails");
		return customUserDetails;
		
	}

	// check user authentication
	private User buildUserForAuthenticationForAdminUser(com.mfs.mdm.model.User user,
			Set<GrantedAuthority> authorities) {
		Set<AdminUser> adminUsers = user.getAdminUsers();
		AdminUser adminUser = adminUsers.stream().findFirst().get();
		return new User(user.getEmail(), adminUser.getPassword(), user.getIsEmailVerified(), user.getIsUserActive(),
				adminUser.getIsPasswordStatus(), !adminUser.getIsAccountLocked(), authorities);
	}
  // check user authentication
	private Set<GrantedAuthority> buildUserAuthorityForAdminUser(Set<AdminUserRole> adminUserRoles) {
		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
		for (AdminUserRole userRole : adminUserRoles) {
			setAuths.add(new SimpleGrantedAuthority(userRole.getAdminRole().getRoleName()));
		}
		return setAuths;
	}
	// check login details
	@Transactional
	public boolean login(String username, String password, String channel) {

		SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_GLOBAL);
		Authentication authenticate = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		if (authenticate.isAuthenticated()) {
			SecurityContextHolder.getContext().setAuthentication(authenticate);
			return true;
		}
		return false;
	}
	
	
 // check authentication credentials
	@Transactional
	public boolean authenticateCredentials(String username, String password) {
		Authentication authenticate = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		if (authenticate.isAuthenticated()) {
			return true;
		}
		return false;
	}

	
	//return current user
	@Transactional
	public CustomUserDetails getCurrentUser() throws Exception {
		CustomUserDetails activeUser = null;
		if (null != SecurityContextHolder.getContext()
				&& null != SecurityContextHolder.getContext().getAuthentication()) {
			activeUser = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		}
		return activeUser;
	}

}

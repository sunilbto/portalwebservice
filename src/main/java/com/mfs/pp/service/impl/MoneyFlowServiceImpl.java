package com.mfs.pp.service.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.pp.dao.TransactionDao;
import com.mfs.pp.dto.MoneyFlowDto;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.service.MoneyFlowService;
import com.mfs.pp.util.CallServices;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */

@Service
public class MoneyFlowServiceImpl implements MoneyFlowService {

	@Autowired
	TransactionDao transactionDao;
	private static final Logger log = LoggerFactory.getLogger(MoneyFlowServiceImpl.class);

	// return money flow details
	@Override
	public Object getMoneyFlows(MoneyFlowDto moneyFlowDto) {

		// log.info("Inside getMoneyFlows() method of
		// MoneyFlowServiceImpl-----"+moneyFlowDto);
		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();

		Gson gson = new Gson();

		String jsonResponse = CallServices.getResponseFromService(
				systemConfigDetailsMap.get(Constants.BASE_URl) + RestServiceURIConstant.MONEY_FLOWS_CURBI,
				gson.toJson(moneyFlowDto));

		if (jsonResponse == null) {
			log.error("Exception Inside getMoneyFlows() method of MoneyFlowServiceImpl");
			throw new CustomException("Getting No Response");
		}

		Object response = (Object) gson.fromJson(jsonResponse, Object.class);

		return response;

	}

}

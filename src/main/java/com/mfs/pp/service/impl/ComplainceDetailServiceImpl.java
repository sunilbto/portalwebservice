package com.mfs.pp.service.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.pp.dao.TransactionDao;
import com.mfs.pp.dto.ComplianceRequestDto;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.service.ComplianceDetailService;
import com.mfs.pp.util.CallServices;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Service("ComplianceDetailService")
public class ComplainceDetailServiceImpl implements ComplianceDetailService{
 
	@Autowired
	TransactionDao transactionDao;
	
	private static final Logger log = LoggerFactory.getLogger(ComplainceDetailServiceImpl.class);
	
	// returns compliance details
	@Override
	public Object getComplianceDetails(ComplianceRequestDto request)throws Exception
	{  //log.info("Inside getComplianceDetails() method of ComplainceDetailServiceImpl");
		
	    Object complianceFinalResponse=null;
	
		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();
		
		Gson gson = new Gson(); 
		String jsonResponse=null;
		
		jsonResponse=CallServices.getResponseFromService(systemConfigDetailsMap.get(Constants.BASE_URl)+RestServiceURIConstant.CURBI_REGULATOR_GET_COMPLIAINCE_DETAILS, gson.toJson(request));
		
		if(jsonResponse==null)
		{ log.error("Exception Inside getComplianceDetails() method of ComplainceDetailServiceImpl");
			throw new CustomException("Getting No Response");
		}
		
		complianceFinalResponse=gson.fromJson(jsonResponse, Object.class);
		
		return complianceFinalResponse;
	}

}

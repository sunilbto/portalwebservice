package com.mfs.pp.service.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.pp.dao.TransactionDao;
import com.mfs.pp.dto.AllDashBoardRequestDto;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.service.ReportExcelService;
import com.mfs.pp.util.CallServices;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */

@Service("ReportExcelServiceImpl")
public class ReportExcelServiceImpl implements ReportExcelService {

	@Autowired
	TransactionDao transactionDao;

	private static final Logger log = LoggerFactory.getLogger(ReportExcelServiceImpl.class);

	// genrate excel for balance
	@Override
	public Object GenExcelForBalance(AllDashBoardRequestDto request) throws Exception {
		// log.info("Inside GenExcelForBalance() method of ReportExcelServiceImpl");
		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();

		Gson gson = new Gson();
		String jsonResponse = null;

		jsonResponse = CallServices.getResponseFromService(
				systemConfigDetailsMap.get(Constants.BASE_URl) + RestServiceURIConstant.CURBI_REPORT_EXCEL_GEN,
				gson.toJson(request));

		if (jsonResponse.equals(null)) {
			log.error("Exception Inside GenExcelForBalance() method of ReportExcelServiceImpl");
			throw new CustomException("Get No Response");
		}
		Object response = (Object) gson.fromJson(jsonResponse, Object.class);

		return response;

	}

}

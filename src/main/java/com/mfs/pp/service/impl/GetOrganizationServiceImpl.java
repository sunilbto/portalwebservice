package com.mfs.pp.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.pp.dao.TransactionDao;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.service.GetOrganizationService;
import com.mfs.pp.util.CallServices;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;

@Service
public class GetOrganizationServiceImpl implements GetOrganizationService {

	private static final Logger log = LoggerFactory.getLogger(GetOrganizationServiceImpl.class);
	@Autowired
	TransactionDao transactionDao;
	
	// return organization list
	@Override
	public List<Object> getOrganizationList() {
		log.debug("Inside getOrganizationList() GetOrganizationServiceImpl");
		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();
		
		Gson gson=new Gson();
		String jsonResponse=null;
		 jsonResponse=CallServices.getResponseFromService(systemConfigDetailsMap.get(Constants.BASE_URl)+RestServiceURIConstant.CURBI_GET_ALLORGANIZATION_LIST);
		
		if(jsonResponse==null)
		{log.error("Exception Inside getOrganizationList() method of GetOrganizationServiceImpl");
			throw new CustomException("Getting No Response");
		}
		
		List<Object> response=(List<Object>) gson.fromJson(jsonResponse, Object.class);
		
		return response;
	}

	

}

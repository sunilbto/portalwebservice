package com.mfs.pp.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.mdm.model.ContactDetails;
import com.mfs.mdm.model.Organization;
import com.mfs.mdm.model.OrganizationUser;
import com.mfs.mdm.model.QueryType;
import com.mfs.mdm.model.User;
import com.mfs.pp.dao.ContactDetailsDao;
import com.mfs.pp.dao.OrganisationDao;
import com.mfs.pp.dao.TransactionDao;
import com.mfs.pp.dao.UserDao;
import com.mfs.pp.dto.ContactDto;
import com.mfs.pp.dto.ContactSaveResponseDto;
import com.mfs.pp.dto.QueryTypeDto;
import com.mfs.pp.service.ContactDetailsService;
import com.mfs.pp.service.UserSecurityService;
import com.mfs.pp.util.Constants;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */



@Service("ContactDetailsService")
public class ContactDetailsServiceImpl implements ContactDetailsService{
	@Autowired
	ContactDetailsDao contactDetailsDao;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	OrganisationDao organisationDao;
	
	@Autowired
	TransactionDao transactionDao;
	
//	private static final Logger log = LoggerFactory.getLogger(ContactDetailsServiceImpl.class);
	
	@Autowired
	UserSecurityService userSecurityService;
	
	// save contact details
	public ContactSaveResponseDto saveConatctDetails(ContactDto contact)throws Exception 
	{
		//log.info("Inside saveConatctDetails() method of ContactDetailsServiceImpl");
		ContactSaveResponseDto contactSaveResponse=null;
		boolean flag=false; 
		String userName=null;
		String userRole=null;
		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();
		if(systemConfigDetailsMap==null)
		{
			throw new Exception("No Config Details");
		}
		String partnerName=null;
		QueryType	queryType=contactDetailsDao.checkQueryTypeExits(contact.getQueryTypeDto().getQueryType());
		//check query type
		if(queryType != null)
		{

			ContactDetails contactDetails=new ContactDetails();
			contactDetails.setComment(contact.getComment());
			contactDetails.setEmail(contact.getEmail());
			if(contact.getPartnerName()!=null)
			{
			contactDetails.setPartnerName(contact.getPartnerName());
			partnerName=contact.getPartnerName();
			}
			else
			{
				partnerName="";
			}
			User user=null;
			contactDetails.setQueryType(queryType);
			//save contact
			contactDetailsDao.save(contactDetails);
			if(systemConfigDetailsMap.get(Constants.EMAIL)!=null)
			{
			 user=userDao.getUserInfoByEmail(systemConfigDetailsMap.get(Constants.EMAIL));
			}
			User userInfo=userDao.getUserInfoByEmail(contact.getEmail());
			OrganizationUser organizationUser=null;
			//check user Info
			if(userInfo!=null)
			{
			userName=userInfo.getFirstName();
			 organizationUser=organisationDao.getOrganizationUserById(userInfo.getUserId());
			
			}
			String partnerCode=null;
			//return organization by orgname
			Organization organization=organisationDao.getOrganizationByOrgName(contact.getPartnerName());
			if(organization!=null)
			{
			 partnerCode=organization.getOrgId();
			 if(partnerCode==null)
			 {
				 partnerCode="";
			 }
			}
			else {
				partnerCode="";
				
			}
			if(organizationUser!=null)
			{
				userRole=organizationUser.getOrganizationUserRoles().iterator().next().getOrganizationRole().getRoleName();
			}
			
			userSecurityService.organizationSendEmailQuery(user.getEmail(), contact.getComment(),partnerCode,partnerName,userName,userRole,contact.getEmail());
			flag=true;
			
		}
		if(flag==true)
		{   contactSaveResponse=new ContactSaveResponseDto();
			contactSaveResponse.setCode(Constants.SUCCESS_CODE);
			contactSaveResponse.setMessage("Contact Saved successfully");
			
		}
		return contactSaveResponse;
	}


	// return query types
	public List<QueryTypeDto> getQueryTypes()
	{
		//log.info("Inside getQueryTypes() method of ContactDetailsServiceImpl");
		
		List<QueryTypeDto> queryList = null;
		
		queryList = contactDetailsDao.getQueryType();
		
		return queryList;
	}

}
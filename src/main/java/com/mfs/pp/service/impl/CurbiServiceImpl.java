package com.mfs.pp.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.common.dto.OrganisationProfileDto;
import com.mfs.mdm.model.Address;
import com.mfs.mdm.model.Category;
import com.mfs.mdm.model.CountryMaster;
import com.mfs.mdm.model.CurrencyMaster;
import com.mfs.mdm.model.Organization;
import com.mfs.mdm.model.OrganizationType;
import com.mfs.mdm.model.User;
import com.mfs.pp.dao.CommonDao;
import com.mfs.pp.dao.CountryZoneDao;
import com.mfs.pp.dao.OrganisationDao;
import com.mfs.pp.dao.UserDao;
import com.mfs.pp.service.CurbiService;
import com.mfs.pp.service.UserSecurityService;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Service
public class CurbiServiceImpl implements CurbiService {

	@Autowired
	private UserDao userDao;

	@Autowired
	CountryZoneDao zoneDao;

	@Autowired
	CommonDao commonDao;
	
	@Autowired
	private OrganisationDao organisationDao;
	
	@Autowired
	UserSecurityService userSecurityService;
	
	//private static final Logger log = LoggerFactory.getLogger(CurbiServiceImpl.class);

	//update Organization 
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public String updateOrganizationDetails(OrganisationProfileDto organisationProfileDto) throws Exception {
		//log.info("Inside createZoneService() method of CurbiServiceImpl");
		String result = null;

		Organization organization = null;
	//	log.info("Before calling getOrganizationByOrgName in organisationDao");
		organization = organisationDao.getOrganizationByOrgName(organisationProfileDto.getOrganisationName());
	//	log.info("After calling getOrganizationByOrgName in organisationDao");
		if (null == organization) {
		//	log.info("Before calling getcategory in commonDao");
			Category category = commonDao.getcategory(organisationProfileDto.getCategory());
		//	log.info("After calling getcategory in commonDao");
			CurrencyMaster currency = commonDao
					.getCourencyDteail(organisationProfileDto.getCurrencyDto().getCurrencyName());
		//	log.info("Before calling getTimeZone in commonDao");
		//	TimeZone timeZone = commonDao.getTimeZone(organisationProfileDto.getTimezoneName());
		//	log.info("After calling getTimeZone in commonDao");
			OrganizationType orgtype = commonDao.getOrgType(organisationProfileDto.getOrganisationType());

			// Group group =
			// groupDao.getOrganizationByGroupName(organisationProfileDto.getGroups().getGroupName());
		//	log.info("Before calling getCountryDetails in zoneDao");
			CountryMaster country = zoneDao
					.getCountryDetails(organisationProfileDto.getAddressDtoList().get(0).getCountry().getCountryName());
		//	log.info("Before calling getCountryDetails in zoneDao");
			User user = commonDao.getUser(organisationProfileDto.getEmail());
		//	log.info("After calling getUser in commonDao");
			Organization org = new Organization();

			org.setBusinessRegNo(organisationProfileDto.getOrganisationName());
			org.setFinanceEmail(organisationProfileDto.getFinanceEmail());
			org.setOperationsEmail(organisationProfileDto.getOperationsEmail());
			org.setSupportEmail(organisationProfileDto.getSupportEmail());
			org.setDescription(organisationProfileDto.getDescription());
			org.setEmail(organisationProfileDto.getEmail());
			org.setFirstActivationDate(new Date());
			org.setMobileNumber(organisationProfileDto.getMobileNumber());
			org.setOrgApiKey(organisationProfileDto.getOrgApiKey());
			org.setOrgId(organisationProfileDto.getOrganisationID());
			org.setOrgName(organisationProfileDto.getOrganisationName());
			org.setRegDate(new Date());
			org.setStatus(user.getStatus());
			org.setCategories(category);
			org.setCurrencyMaster(currency);
	//		org.setZone(timeZone);
			org.setOrganizationType(orgtype);
		//	log.info("Before Saveing organization in userDao");
			userDao.saveOrganization(org);
		//	log.info("After Saveing organization in userDao");
			Address address = new Address();
			address.setAddress(organisationProfileDto.getAddressDtoList().get(0).getAddress());
			address.setAddress1(organisationProfileDto.getAddressDtoList().get(0).getAddress1());
			address.setAddress2(organisationProfileDto.getAddressDtoList().get(0).getAddress2());
			address.setCity(organisationProfileDto.getAddressDtoList().get(0).getCity());
			address.setPostalCode(organisationProfileDto.getAddressDtoList().get(0).getPostalCode());
			address.setStateRegion(organisationProfileDto.getAddressDtoList().get(0).getStateRegion());
			address.setCountryMaster(country);
			address.setOrganization(org.getOrganisationId());
		//	log.info("Before Saveing address in userDao");
			userDao.save(address);
		//	log.info("Before Saveing address in userDao");
			result = "Org Created";
			
			userSecurityService.newOrganizationSendEmail(organisationProfileDto.getEmail(), organisationProfileDto.getOrganisationName());

		}

		return result;
	}

}

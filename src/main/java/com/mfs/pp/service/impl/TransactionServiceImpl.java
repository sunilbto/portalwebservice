package com.mfs.pp.service.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.pp.dao.TransactionDao;
import com.mfs.pp.dto.AllTransactionRequestDto;
import com.mfs.pp.dto.AlltransactionByIdRequestDto;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.service.TransactionService;
import com.mfs.pp.util.CallServices;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */

@Service
public class TransactionServiceImpl implements TransactionService {

	private static final Logger log = LoggerFactory.getLogger(TransactionServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	// get All Transaction
	@Override
	public Object getAllTransaction(AllTransactionRequestDto request) throws Exception {
		// log.info("Inside getAllTransaction() method of TransactionServiceImpl");

		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();

		Gson gson = new Gson();
		String jsonResponse = null;
		jsonResponse = CallServices.getResponseFromService(
				systemConfigDetailsMap.get(Constants.BASE_URl) + RestServiceURIConstant.CURBI_GETALLTRANSACTION,
				gson.toJson(request));

		if (jsonResponse == null) {
			log.error("Exception Inside getAllTransaction() method of TransactionServiceImpl");
			throw new CustomException("Gettng No Response");
		}
		Object response = gson.fromJson(jsonResponse, Object.class);

		return response;
	}

	// get Transaction details by transaction ID
	@Override
	public Object getTransactionById(AlltransactionByIdRequestDto request) throws Exception {
		// log.info("Inside getTransactionById() method of TransactionServiceImpl");

		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();

		Gson gson = new Gson();
		String jsonResponse = null;
		jsonResponse = CallServices.getResponseFromService(
				systemConfigDetailsMap.get(Constants.BASE_URl) + RestServiceURIConstant.CURBI_GET_ORGTRANSACTION_BY_ID,
				gson.toJson(request));
		if (jsonResponse == null) {
			log.error("Exception Inside getTransactionById() method of TransactionServiceImpl");
			throw new CustomException("Get No Response");
		}

		Object response = gson.fromJson(jsonResponse, Object.class);

		return response;

	}

}
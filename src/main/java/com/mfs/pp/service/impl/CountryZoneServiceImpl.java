package com.mfs.pp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.mdm.common.dto.CountryDto;
import com.mfs.mdm.model.CountryMaster;
import com.mfs.mdm.model.CountryZone;
import com.mfs.mdm.model.Zone;
import com.mfs.pp.dao.CountryZoneDao;
import com.mfs.pp.dto.Countries;
import com.mfs.pp.dto.CountryDetails;
import com.mfs.pp.dto.ZoneDetails;
import com.mfs.pp.dto.ZoneDetailsResponse;
import com.mfs.pp.dto.ZoneListResponse;
import com.mfs.pp.dto.Zones;
import com.mfs.pp.dto.ZonesResponse;
import com.mfs.pp.service.CountryZoneService;
import com.mfs.pp.service.UserService;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Service("CountryZoneService")
public class CountryZoneServiceImpl implements CountryZoneService {

	@Autowired
	CountryZoneDao dao;

	@Autowired
	UserService userService;
	
	//private static final Logger log = LoggerFactory.getLogger(CountryZoneServiceImpl.class);

	// create zone
	public String createZoneService(Zones zones) throws Exception {
		//log.info("Inside createZoneService() method of CountryZoneServiceImpl");
		Zone zone = null;
		String result = null;
		CountryZone countryZone = new CountryZone();
		CountryMaster countryMaster = null;
		//log.info("Before calling checkZoneExist in CountryZoneDao");
		zone = dao.checkZoneExist(zones.getZoneName());
		//log.info("After calling checkZoneExist in CountryZoneDao");
		if (zone == null) {
			 
			zone = new Zone();
			zone.setCreateaDate(new Date());
			zone.setDescription(zones.getDescription());
			zone.setZoneName(zones.getZoneName());
			zone.setCreatedBy("mfs");
			//log.info("Before saveing Zone in CountryZoneDao");
			dao.save(zone);
			//log.info("After Saveing Zone in CountryZoneDao");
			if(!(zones.getCountry().isEmpty()))
			{
			List<Countries> Countries = zones.getCountry();
			for (Countries country : Countries) {
				countryMaster = dao.getCountryDetails(country.getCountryName());
				countryZone.setCountryMaster(countryMaster);
				countryZone.setZone(zone);
				//log.info("Before saveing countryZone in CountryZoneDao");
				dao.save(countryZone);
				//log.info("After saveing countryZone in CountryZoneDao");
			}
			}
			
			
			result = "Zone Created";
		}
		//log.info("Exting createZoneService() method of CountryZoneServiceImpl");
		return result;
	}
	

	// update zones
	public String udpateByZone(Zones zones) {
		//log.info("Inside udpateByZone() method of CountryZoneServiceImpl");
		boolean flag = false;
		String result = null;
		CountryMaster countryMaster = null;
		CountryZone countryZones = new CountryZone();
		//log.info("Before calling getCountryByzone in CountryZoneDao");
		List<CountryZone> countryZone = dao.getCountryByzone(zones.getZoneId());
		//log.info("After calling getCountryByzone in CountryZoneDao");
		if (!(countryZone.isEmpty())) {

			for (CountryZone co : countryZone) {
				if (flag) {
					break;
				}
				//log.info("Before calling getCountryDetails in CountryZoneDao");
				countryMaster = dao.getCountryDetails(co.getCountryMaster().getCountryName());
				//log.info("Before calling getCountryDetails in CountryZoneDao");
				co.setCountryMaster(countryMaster);
				if (co.getCountryMaster() != null) {
					//log.info("Before Deleteing country in CountryZoneDao");
					dao.delete(co);
					//log.info("After Deleteing country in CountryZoneDao");
					//log.info("Before calling getCountryDetails in CountryZoneDao");
					List<CountryZone> countryZone2 = dao.getCountryByzone(zones.getZoneId());
					//log.info("After calling getCountryDetails in CountryZoneDao");
					if (countryZone2.isEmpty()) {
						flag = true;
					}
				}

			}

		}
		//log.info("Before calling checkZoneExist in CountryZoneDao");
		Zone zone2 = dao.checkZoneExist(zones.getZoneId());
		//Zone zoness = dao.checkZoneExist(zones.getZoneName());
		//log.info("After calling checkZoneExist in CountryZoneDao");
		if (zone2 != null ) {
			zone2.setZoneName(zones.getZoneName());
			zone2.setDescription(zones.getDescription());
			//log.info("Before Updateing Zone in CountryZoneDao");
			dao.update(zone2);
			//log.info("After Updateing Zone in CountryZoneDao");
			for (Countries country : zones.getCountry()) {
			//	log.info("Before calling getCountryDetails in CountryZoneDao");
				countryMaster = dao.getCountryDetails(country.getCountryName());
			//	log.info("After calling getCountryDetails in CountryZoneDao");
				countryZones.setCountryMaster(countryMaster);
				countryZones.setZone(zone2);
				//log.info("Before Saveing  CountryZone in CountryZoneDao");
				dao.save(countryZones);
				//log.info("After Saveing  CountryZone in CountryZoneDao");

			}
			result = "Zone Updated";
		}
		//log.info("Exiting udpateByZone() method of CountryZoneServiceImpl");
		return result;

	}

	//returns all zone
	public ZonesResponse getAllZone() throws Exception {

		List<Zone> zoneList = null;
		ZoneDetails zoneDetails = null;
		//log.info("Before calling getallZones in CountryZoneDao");
		zoneList = dao.getallZones();
		//log.info("After calling getallZones in CountryZoneDao");
		ZonesResponse zonesResponse = new ZonesResponse();
		ZoneListResponse zoneListResponse = null;
		List<CountryZone> countryZoneList = null;
		ZoneDetailsResponse zoneDetailsResponse = null;

		CountryDetails countryDetails = null;
		List<CountryDetails> countryDetailsList = null;
		List<ZoneDetailsResponse> zoneDetailsResponseList = new ArrayList<ZoneDetailsResponse>();
		//log.info("Before calling getAllCountryDtoList in userService");
		List<CountryDto> countryList = userService.getAllCountryDtoList();
		//log.info("Before calling getAllCountryDtoList in userService");
		if(!(countryList.isEmpty()))
		{
		zonesResponse.setCountryList(countryList);
		}

		if (!(zoneList.isEmpty())) {
			for (Zone zone : zoneList) {
				zoneListResponse = new ZoneListResponse();
				zoneDetails = new ZoneDetails();
				zoneDetailsResponse = new ZoneDetailsResponse();
				countryDetails = new CountryDetails();
				zoneDetails.setCreateaDate(zone.getCreateaDate());
				zoneDetails.setCreatedBy(zone.getCreatedBy());
				zoneDetails.setDescription(zone.getDescription());
				zoneDetails.setZoneId(zone.getZoneId());
				zoneDetails.setZoneName(zone.getZoneName());
				zoneDetailsResponse.setZoneDetails(zoneDetails);
			//	log.info("Before calling getallZone in CountryZoneDao");
				countryZoneList = dao.getallZone(zone.getZoneId());
			//	log.info("Before calling getallZone in CountryZoneDao");
				if (!(countryZoneList.isEmpty())) {
					countryDetailsList = new ArrayList<CountryDetails>();
					for (CountryZone countryZone : countryZoneList) {

						countryDetails = new CountryDetails();
						countryDetails.setCountryCode(countryZone.getCountryMaster().getCountryCode());
						countryDetails.setCountryId(countryZone.getCountryMaster().getCountryId());
						countryDetails.setCountryName(countryZone.getCountryMaster().getCountryName());
						countryDetails.setNumericCode(countryZone.getCountryMaster().getNumericCode());
						countryDetails.setPhoneCode(countryZone.getCountryMaster().getPhoneCode());
						countryDetailsList.add(countryDetails);
						zoneDetailsResponse.setCountryDetails(countryDetailsList);

					}

				}

				zoneDetailsResponseList.add(zoneDetailsResponse);

			}
			zoneListResponse.setZoneList(zoneDetailsResponseList);

		}

		zonesResponse.setZoneListResponse(zoneListResponse);
	//	log.info("Exiting getAllZone() method of CountryZoneServiceImpl");
		return zonesResponse;
	}

}

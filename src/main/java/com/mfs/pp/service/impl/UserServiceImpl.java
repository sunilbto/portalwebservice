package com.mfs.pp.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.common.dto.CountryDto;
import com.mfs.mdm.common.dto.PasswordDto;
import com.mfs.mdm.common.dto.TimeZoneDto;
import com.mfs.mdm.common.dto.UserDto;
import com.mfs.mdm.common.type.StatusType;
import com.mfs.mdm.model.Address;
import com.mfs.mdm.model.AdminUser;
import com.mfs.mdm.model.CountryMaster;
import com.mfs.mdm.model.OrganizationUser;
import com.mfs.mdm.model.TimeZone;
import com.mfs.mdm.model.User;
import com.mfs.pp.dao.AccountDao;
import com.mfs.pp.dao.AdminDao;
import com.mfs.pp.dao.CommonDao;
import com.mfs.pp.dao.CountryZoneDao;
import com.mfs.pp.dao.OrganisationDao;
import com.mfs.pp.dao.UserDao;
import com.mfs.pp.exception.MPDataNotFoundException;
import com.mfs.pp.exception.MPDuplicateDataException;
import com.mfs.pp.mapper.OrganisationMapper;
import com.mfs.pp.mapper.UserMapper;
import com.mfs.pp.service.UserService;
import com.mfs.pp.type.MFSErrorType;
import com.mfs.pp.util.Format;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */

@Service
public class UserServiceImpl implements UserService {

	private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserDao userDao;

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private AccountDao accountDao;

	@Autowired
	private OrganisationMapper organisationMapper;

	@Autowired
	private OrganisationDao organisationDao;

	@Autowired
	private AdminDao adminDao;

	@Autowired
	BCryptPasswordEncoder bcryptPasswordEncoder;

	@Autowired
	CommonDao commonDao;

	@Autowired
	CountryZoneDao countryZoneDao;

	// register User
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public Boolean registerUser(UserDto userDTO) throws MPDuplicateDataException, Exception {
		// log.debug("user service registeruser");
		Boolean flag = Boolean.FALSE;
		User user = null;
		if (null != userDTO && null != userDTO.getProfileDto()) {
			user = userDao.getUserInfoByEmail(userDTO.getProfileDto().getEmail());
		}
		if (null == user) {
			user = userMapper.prepareUserModelFromUserDto(userDTO);
			flag = userDao.registerUser(user);
			Set<OrganizationUser> organizationUserSet = user.getOrganizationUsers();
			organizationUserSet = organisationMapper.prepareOrganisationUserModelFromUserDto(userDTO, user);
			user.setOrganizationUsers(organizationUserSet);
		} else if (null != user) {
			throw new MPDuplicateDataException("User already exists");
		}

		return flag;
	}

	// update User
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public boolean updateUser(UserDto userDTO) throws MPDataNotFoundException, Exception {
		// log.debug("user service updateUser");
		boolean flag = false;
		User user = null;
		Address address = null;
		CountryMaster countryMaster = null;

		if (null != userDTO && null != userDTO.getProfileDto()) {
			user = userDao.getUserInfoByEmail(userDTO.getProfileDto().getEmail());
		}
		if (null != user) {
			user = userMapper.prepareForUpdateUserModelFromUserDto(userDTO, user);

			flag = userDao.updateUser(user);
			address = commonDao.getUserAddress(user.getUserId());
			if (address != null) {

				

			} else if (null != userDTO.getProfileDto().getAddressDtoList()) {
				address = new Address();

				address.setAddress(userDTO.getProfileDto().getAddressDtoList().get(0).getAddress());
				address.setAddress1(userDTO.getProfileDto().getAddressDtoList().get(0).getAddress1());
				address.setAddress2(userDTO.getProfileDto().getAddressDtoList().get(0).getAddress2());
				address.setCity(userDTO.getProfileDto().getAddressDtoList().get(0).getCity());
				address.setPostalCode(userDTO.getProfileDto().getAddressDtoList().get(0).getPostalCode());
				address.setStateRegion(userDTO.getProfileDto().getAddressDtoList().get(0).getStateRegion());
				address.setUser(user.getUserId());
				countryMaster = countryZoneDao.getCountryDetails(
						userDTO.getProfileDto().getAddressDtoList().get(0).getCountry().getCountryName());
				if (countryMaster != null) {
					address.setCountryMaster(countryMaster);
				}
				userDao.save(address);
			}

		} else {
			throw new MPDataNotFoundException(MFSErrorType.USER_DOES_NOT_EXIST.getDescription());
		}

		return flag;
	}

	// get All Country List
	@Override
	@Transactional
	public List<CountryDto> getAllCountryDtoList() throws Exception {
		List<CountryDto> countryDtoList = null;
		List<CountryMaster> countryModelList = null;
		try {
			countryModelList = accountDao.getAllCountryList();
			countryDtoList = userMapper.getCountryDtoListFromCountryModelList(countryModelList);

		} catch (Exception e) {
			log.error("exception while ftechinbg user" + e);
		}

		return countryDtoList;
	}

	// get All Time Zone List
	@Override
	@Transactional
	public List<TimeZoneDto> getAllTimeZoneDtoList() throws Exception {
		List<TimeZoneDto> timezoneDtoList = null;
		List<TimeZone> timeZoneMasterModelList = null;
		try {
			timeZoneMasterModelList = accountDao.getAllTimeZoneList();
			timezoneDtoList = userMapper.getTimezoneDtoListFromTimeZoneModelList(timeZoneMasterModelList);
		} catch (Exception e) {
			log.error("exception while ftechinbg user" + e);
		}
		return timezoneDtoList;
	}

	// get User By User Email Id
	@Override
	@Transactional
	public UserDto getUserByUserEmailId(String email) throws Exception {
		// log.debug("user service getUserByUserEmailId");
		UserDto usrdto = null;
		User usr = userDao.getUserInfoByEmail(email);
		if (null != usr) {
			usrdto = userMapper.getUserDtoFromUserModel(usr);
		} else {
			throw new MPDataNotFoundException("User not found");
		}
		return usrdto;

	}

	// *********Login *******************

	// create Or Update Password
	@Override
	@Transactional
	public boolean createOrUpdatePassword(UserDto userDto) throws MPDataNotFoundException, Exception {
		User user = null;
		boolean isPasswordUpdated = false;

		String statusType = null;
		if (null != userDto.getProfileDto() && Format.isStringNotEmptyAndNotNull(userDto.getProfileDto().getEmail())) {
			user = userDao.getUserInfoByEmail(userDto.getProfileDto().getEmail());
			// check user status
			if (null != user.getStatus() && Format.isStringNotEmptyAndNotNull(user.getStatus())
					&& !user.getStatus().equals(StatusType.CLOSED.toString())) {
				if (null != user) {
					if (null != user.getStatus() && Format.isStringNotEmptyAndNotNull(user.getStatus())
							&& user.getStatus().equals(StatusType.PENDING.toString())) {
						statusType = StatusType.Active.toString();
					} else {
						statusType = StatusType.Active.toString();
					}
					user.setStatus(statusType);
					user.setIsUserActive(true);
					user.setIsEmailVerified(true);
					// check organization user
					if (Format.isCollectionNotEmptyAndNotNull(user.getOrganizationUsers())) {
						OrganizationUser organizationUser = organisationDao
								.getOrganizationUserByUserEmail(user.getEmail());
						if (null != organizationUser) {
							organizationUser.setPassword(bcryptPasswordEncoder.encode(userDto.getPassword()));
							organizationUser.setIsPasswordStatus(true);
							organizationUser.setResetPasswordCreatedOn(new Date());
						}
					}
					// check admin user
					if (Format.isCollectionNotEmptyAndNotNull(user.getAdminUsers())) {
						AdminUser adminUser = adminDao.getAdminUserByUserEmail(user.getEmail());
						if (null != adminUser) {
							adminUser.setPassword(bcryptPasswordEncoder.encode(userDto.getPassword()));
							adminUser.setIsPasswordStatus(true);
							adminUser.setResetPasswordCreatedOn(new Date());

						}
					}
					isPasswordUpdated = userDao.createOrUpdatePassword(user);
				}
			}
		}
		return isPasswordUpdated;
	}

	// invalidate Session
	@Override
	public boolean invalidateSession() throws Exception {
		SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_GLOBAL);

		SecurityContextHolder.clearContext();
		return true;

	}

	// check isAdminUser
	@Override
	@Transactional
	public boolean isAdminUser(String email) {
		User userModel = userDao.getUserInfoByEmail(email);
		if (Format.isCollectionNotEmptyAndNotNull(userModel.getAdminUsers())) {
			return true;
		}
		return false;
	}

	// change partner password
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public boolean changePartnerPassword(PasswordDto passwordDto) throws Exception {
		// log.debug("partner user service changePartnerPassword");
		Boolean isOrgUserSaved = Boolean.FALSE;
		User user = null;
		if (null != passwordDto) {
			user = userDao.getUserInfoByEmail(passwordDto.getEmail());
		}
		if (null != user) {
			user = userMapper.prepareMerchantUserForChangePassword(user, passwordDto);
			user.setOrganizationUsers(user.getOrganizationUsers());
			isOrgUserSaved = true;
		} else if (null == user) {
			throw new MPDataNotFoundException(MFSErrorType.USER_DOES_NOT_EXIST.getDescription());
		}
		return isOrgUserSaved;
	}

	// delete user by email
	@Override
	@Transactional
	public boolean deleteUserByEmail(String email) throws Exception {
		boolean isUserUpdated = false;
		User user = null;
		user = userDao.getUserInfoByEmail(email);
		for (OrganizationUser ou : user.getOrganizationUsers()) {
			ou.setIsAccountLocked(true);
		}
		user.setStatus(StatusType.CLOSED.toString());
		user.setIsUserActive(false);
		isUserUpdated = userDao.updateUser(user);

		return isUserUpdated;
	}

}

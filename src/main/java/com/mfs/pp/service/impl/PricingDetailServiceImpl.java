package com.mfs.pp.service.impl;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.mfs.pp.dao.ForexMarginDao;
import com.mfs.pp.dao.TransactionDao;
import com.mfs.pp.dto.PricingFinalResponseDto;
import com.mfs.pp.dto.PricingRequestDto;
import com.mfs.pp.service.PricingDetailService;
import com.mfs.pp.util.CallServices;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */

@Service("PricingDetailService")
public class PricingDetailServiceImpl implements PricingDetailService {

	// private static final Logger log =
	// LoggerFactory.getLogger(PricingDetailServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	ForexMarginDao forexMarginDao;

	// returns pricing details
	public PricingFinalResponseDto getPricingDetailsService(PricingRequestDto request)
			throws JsonParseException, JsonMappingException, IOException {
		// log.info("Inside getPricingDetailsService() method of
		// PricingDetailServiceImpl");

		PricingFinalResponseDto pricingResponse = new PricingFinalResponseDto();

		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();

		Gson gson = new Gson();

		String jsonResponse = null;
		double forexMargin = 0.0;
		forexMargin = forexMarginDao.getForexMargin();
		request.setForexMargin(forexMargin);

		jsonResponse = CallServices.getResponseFromService(systemConfigDetailsMap.get(Constants.BASE_URl)
				+ RestServiceURIConstant.CURBI_REGULATOR_GET_PRICING_DETAILS, gson.toJson(request));

		ObjectMapper mapper = new ObjectMapper();

		pricingResponse = mapper.readValue(jsonResponse, PricingFinalResponseDto.class);

		return pricingResponse;
	}

}

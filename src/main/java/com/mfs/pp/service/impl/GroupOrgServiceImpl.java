package com.mfs.pp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.mdm.model.Group;
import com.mfs.mdm.model.OrgGroup;
import com.mfs.mdm.model.Organization;
import com.mfs.pp.dao.GroupOrgDao;
import com.mfs.pp.dto.GroupDetailsResponse;
import com.mfs.pp.dto.GroupListReponse;
import com.mfs.pp.dto.Groups;
import com.mfs.pp.dto.GroupsResponse;
import com.mfs.pp.dto.OrganizationDetails;
import com.mfs.pp.dto.Organizations;
import com.mfs.pp.service.GroupOrgService;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Service
public class GroupOrgServiceImpl implements GroupOrgService {

	@Autowired
	GroupOrgDao dao;

	//private static final Logger log = LoggerFactory.getLogger(GroupOrgServiceImpl.class);
	//create group
	public String createGroup(Groups groups) throws Exception {
	//	log.info("Inside createGroup() method of GroupOrgServiceImpl");
		String result = null;
		Group groupes = null;
		OrgGroup orgGroup = null;
		Organization organization = null;
	//	log.info("Before calling check in GroupOrgDao");
		groupes = dao.check(groups.getGroupName());
	//	log.info("Before calling check in GroupOrgDao");

		if (groupes == null) {
			groupes = new Group();
			
			groupes.setCeationDate(new Date());
			groupes.setGroupName(groups.getGroupName());
			groupes.setDescription(groups.getGroupDescription());
		//	log.info("Before Saveing group in GroupOrgDao");
			dao.save(groupes);
		//	log.info("After Saveing group in GroupOrgDao");
			List<Organizations> orgList = groups.getOrganizations();
			// check organization list
			if(!(orgList.isEmpty()))
			{	
			for (Organizations organizations : orgList) {
				organization = dao.getOrg(organizations.getOrgId());
				if (organization != null) {
					orgGroup = new OrgGroup();
					orgGroup.setOrgId(organization.getOrganisationId());
					orgGroup.setGroup(groupes);
			//		log.info("Before Saveing orgGroup in GroupOrgDao");
					dao.save(orgGroup);
			//		log.info("Before Saveing orgGroup in GroupOrgDao");
				}
			}}

			result = "Group created";

		}
		//log.info("Exting createGroup() method of GroupOrgServiceImpl");
		return result;
	}

	// returns all group
	public GroupsResponse getAllGroup() {
		//log.info("Inside getAllGroup() method of GroupOrgServiceImpl");
		List<Group> GroupList = null;
	//	log.info("Before calling getAllGroup() method of GroupOrgDao");
		GroupList = dao.getAllGroup();
	//	log.info("After Calling getAllGroup() method of GroupOrgDao");
		GroupsResponse groupsResponse = new GroupsResponse();
		OrganizationDetails organizationDetails1 = null;
		OrganizationDetails organizationDetails2 = null;
		GroupDetailsResponse groupDetailsResponse = null;
		Organization organization = null;
		List<OrgGroup> orgGroupList = null;
		GroupListReponse groupListReponse = null;
		Groups group = null;
		List<GroupDetailsResponse> groupDetailsResponseList = new ArrayList<GroupDetailsResponse>();
		List<OrganizationDetails> organizationDetails = null;

		List<OrganizationDetails> OrganizationDetailsList = new ArrayList<OrganizationDetails>();
	//	log.info("Before calling getAllOrg() method of GroupOrgDao");
		List<Organization> organizationList = dao.getAllOrg();
	//	log.info("After calling getAllOrg() method of GroupOrgDao");
		// check organization list
		if (!(organizationList.isEmpty())) {
			for (Organization organization2 : organizationList) {
				organizationDetails2 = new OrganizationDetails();
				organizationDetails2.setOrgId(organization2.getOrganisationId());
				organizationDetails2.setOrgName(organization2.getOrgName());
				OrganizationDetailsList.add(organizationDetails2);

			}
		}

		groupsResponse.setOrganizationDetails(OrganizationDetailsList);
		// check group list
		if (!(GroupList.isEmpty())) {

			for (Group groups : GroupList) {
				groupListReponse = new GroupListReponse();
				groupDetailsResponse = new GroupDetailsResponse();
				group = new Groups();
				group.setCeationDate(groups.getCeationDate());
				group.setGroupDescription(groups.getDescription());
				group.setGroupId(groups.getGroupId());
				group.setGroupName(groups.getGroupName());
				groupDetailsResponse.setGroups(group);
		//		log.info("Before calling getOrgGroups() method of GroupOrgDao");
				orgGroupList = dao.getOrgGroups(groups.getGroupId());
		//		log.info("After calling getOrgGroups() method of GroupOrgDao");
				// check organization group list
				if (!(orgGroupList.isEmpty())) {
					organizationDetails = new ArrayList<OrganizationDetails>();
					for (OrgGroup orgGroup : orgGroupList) {

						organization = dao.getOrg(orgGroup.getOrgId());
						if (organization != null) {
							organizationDetails1 = new OrganizationDetails();
							organizationDetails1.setOrgId(organization.getOrganisationId());
							organizationDetails1.setOrgName(organization.getOrgName());
							organizationDetails.add(organizationDetails1);
						}
					}
					groupDetailsResponse.setOrganizationDetails(organizationDetails);
				}

				groupDetailsResponseList.add(groupDetailsResponse);

			}

			groupListReponse.setGroupDetailsResponse(groupDetailsResponseList);
		}
		groupsResponse.setGroupListReponse(groupListReponse);
	//	log.info("Exting getAllGroup() method of GroupOrgServiceImpl");
		return groupsResponse;
	}

	// update group
	public String updateByGroup(Groups groups) throws Exception {
	//	log.info("Inside updateByGroup() method of GroupOrgServiceImpl");
		boolean flag = false;
		String result = null;
		Organization organization = null;
		OrgGroup orgGroup = null;
	//	log.info("Before calling getOrgGroups() method of GroupOrgDao");
		List<OrgGroup> orgGroupList = dao.getOrgGroups(groups.getGroupId());
	//	log.info("After calling getOrgGroups() method of GroupOrgDao");
		// check organization group list
		if (!(orgGroupList.isEmpty())) {

			for (OrgGroup org : orgGroupList) {
				if (flag) {
					break;
				}
	//			log.info("Before calling getOrg() method of GroupOrgDao");
				organization = dao.getOrg(org.getOrgId());
	//			log.info("After calling getOrg() method of GroupOrgDao");
				org.setOrgId(organization.getOrganisationId());

				if (organization != null) {
		//			log.info("Before Delete org in GroupOrgDao");
					dao.delete(org);
		//			log.info("After Delete org in GroupOrgDao");
					List<OrgGroup> orgGroupLists = dao.getOrgGroups(groups.getGroupId());
					// check organization group list
					if (orgGroupLists.isEmpty()) {
						flag = true;
					}
				}

			}

		}
	//	log.info("Before calling getOrganizationByGroupId() method of GroupOrgDao");
		Group group = dao.getOrganizationByGroupId(groups.getGroupId());
	//	log.info("After calling check() method of GroupOrgDao");
		if (group != null) {
			group.setGroupName(groups.getGroupName());
			group.setDescription(groups.getGroupDescription());
		//	log.info("Before updateing group in GroupOrgDao");
			dao.update(group);
		//	log.info("After updateing group in GroupOrgDao");
			for (Organizations organizations : groups.getOrganizations()) {
				orgGroup = new OrgGroup();

				organization = dao.getOrg(organizations.getOrgId());
				if (organization != null) {
					orgGroup.setOrgId(organization.getOrganisationId());
					orgGroup.setGroup(group);
				//	log.info("Before save group in GroupOrgDao");
					dao.save(orgGroup);
				//	log.info("Before save group in GroupOrgDao");
				}

			}
			result = "Group Updated";
		}
	//	log.info("Exting updateByGroup() method of GroupOrgServiceImpl");
		return result;
	}

}
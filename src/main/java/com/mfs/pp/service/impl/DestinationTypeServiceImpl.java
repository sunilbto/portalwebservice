package com.mfs.pp.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.pp.dao.TransactionDao;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.service.DestinationTypeService;
import com.mfs.pp.util.CallServices;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Service
public class DestinationTypeServiceImpl  implements DestinationTypeService{

	@Autowired
	TransactionDao transactionDao;
	private static final Logger log = LoggerFactory.getLogger(DestinationTypeServiceImpl.class);
	//returns list of destination types
	@Override
	public List<Object> getDestinationTypeList() {
		//log.info("Inside getDestinationTypeList() method of DestinationTypeServiceImpl");
		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();
		Gson gson=new Gson();
		String jsonResponse=null;
		 jsonResponse=CallServices.getResponseFromService(systemConfigDetailsMap.get(Constants.BASE_URl)+RestServiceURIConstant.CURBI_GETDESTINATIONTYPE);

		if(jsonResponse==null) {
			log.error("Exception Inside getDestinationTypeList() method of DestinationTypeServiceImpl");
			throw new CustomException("Getting No Response");
		}
		List<Object> response=(List<Object>) gson.fromJson(jsonResponse, Object.class);
		return response;
	}

}

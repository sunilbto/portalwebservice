package com.mfs.pp.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.common.dto.PasswordDto;
import com.mfs.mdm.common.dto.UserDto;
import com.mfs.mdm.common.type.StatusType;
import com.mfs.mdm.model.AdminUser;
import com.mfs.mdm.model.AdminUserRole;
import com.mfs.mdm.model.User;
import com.mfs.pp.dao.AdminDao;
import com.mfs.pp.dao.UserDao;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.exception.MPDataNotFoundException;
import com.mfs.pp.exception.MPDuplicateDataException;
import com.mfs.pp.mapper.AdminUserMapper;
import com.mfs.pp.mapper.UserMapper;
import com.mfs.pp.service.AdminUserService;
import com.mfs.pp.type.MFSErrorType;
import com.mfs.pp.util.Format;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Service
public class AdminUserServiceImpl implements AdminUserService {

	//private static final Logger log = LoggerFactory.getLogger(AdminUserServiceImpl.class);

	@Autowired
	private UserDao userDao;

	@Autowired
	private AdminDao adminDao;

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private AdminUserMapper adminUserMapper;

	@Autowired
	BCryptPasswordEncoder bcryptPasswordEncoder;

	@Autowired
	CustomUserDetailsService service;

	// update admin user
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public boolean updateAdminUser(UserDto userDTO) throws MPDuplicateDataException, Exception {
		//log.debug("admin user service updateAdminUser");
		Boolean isAdminUserSaved = Boolean.FALSE;

		AdminUser aduser = null;
		User user = null;
		//check user 
		if (null != userDTO && null != userDTO.getProfileDto()) {
			aduser = adminUserMapper.prepareAdminUserInfo(userDTO);
		}
		
		if (null != aduser) {

			user = aduser.getUser();
			Boolean isAdminRoleSaved = false;
			user = userMapper.prepareForUpdateUserModelFromUserDto(userDTO, user);
			isAdminUserSaved = adminDao.saveOrUpdate(user);
			isAdminUserSaved = adminDao.saveOrUpdate(aduser);
			for (AdminUserRole role : aduser.getAdminUserRoles()) {
				isAdminRoleSaved = adminDao.saveOrUpdate(role);
			}

		} else if (null == user) {
			throw new MPDataNotFoundException(MFSErrorType.USER_DOES_NOT_EXIST.getDescription());
		}

		return isAdminUserSaved;
	}

	// change admin password
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public boolean changeAdminPassword(PasswordDto passwordDto) throws Exception {
		//log.debug("admin user service changeAdminPassword");
		Boolean isAdminUserSaved = Boolean.FALSE;
		User user = null;
		//check password
		if (null != passwordDto) {
			user = userDao.getUserInfoByEmail(passwordDto.getEmail());
		}
		if (null != user) {
			user = adminUserMapper.prepareAdminUserForChangePassword(user, passwordDto);
			user.setAdminUsers(user.getAdminUsers());
			isAdminUserSaved = true;
		} else if (null == user) {
			throw new MPDataNotFoundException(MFSErrorType.USER_DOES_NOT_EXIST.getDescription());
		}
		return isAdminUserSaved;
	}
	
    //invite admin user
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public boolean inviteAdminUser(UserDto userDTO) throws Exception {
		//log.debug("admin user service inviteAdminUser");
		boolean isSystemAdminUserSaved = false;
		boolean isAdminUserSaved = false;
		Set<AdminUser> adminUsersSet = null;
		Set<AdminUserRole> adminUserRoles = null;
		boolean isAdminUserRoleSaved = false;

		User user = null;
		//check user profile
		if (null != userDTO && null != userDTO.getProfileDto()) {
			user = userDao.getUserInfoByEmail(userDTO.getProfileDto().getEmail());
		}
		if (null == user) {
			user = adminUserMapper.prepareUserModelFromUserDtoForInviteSystemAdmin(userDTO);
			isSystemAdminUserSaved = userDao.registerUser(user);
			if (isSystemAdminUserSaved) {
				adminUsersSet = adminUserMapper.prepareAdminUserModelFromUserDtoForInviteSystemAdmin(userDTO);
				adminUserRoles = adminUserMapper.prepareAdminUserRoles(userDTO.getRoles());

				if (adminUsersSet != null) {
					for (AdminUser adminUser : adminUsersSet) {
						adminUser.setUser(user);
						isAdminUserSaved = adminDao.saveOrUpdate(adminUser);
						if (isAdminUserSaved) {
							if (adminUserRoles != null) {
								for (AdminUserRole adminUserRole : adminUserRoles) {
									adminUserRole.setAdminUser(adminUser);
									isAdminUserRoleSaved = adminDao.saveOrUpdate(adminUserRole);
									if (!isAdminUserRoleSaved) {
										//log.debug("Admin User Role Not saved");
									}
								}
							}
							adminUser.setAdminUserRoles(adminUserRoles);
						} else {
							//log.debug("Admin User Not Saved.");
						}
					}
				}
				user.setAdminUsers(adminUsersSet);
			}

		} else if (null != user) {
			throw new MPDuplicateDataException(MFSErrorType.USER_ALREADY_EXISTS.getDescription());
		}
		return isAdminUserSaved;
	}

	//check user active
	@Override
	@Transactional
	public boolean checkIfUserActive(String email) throws Exception {
		//log.debug("admin user service checkIfUserActive");
		boolean isUserActive = true;
		User user = userDao.getUserInfoByEmail(email);
		if (null != user) {
			if (user.getIsUserActive()) {
				return isUserActive;
			} else {
				throw new CustomException(MFSErrorType.INACTIVE_USER.getDescription());
			}
		} else {
			throw new CustomException(MFSErrorType.USER_DOES_NOT_EXIST.getDescription());
		}

	}

	//get all system admin users
	@Override
	@Transactional
	public List<UserDto> getAllSystemAdminUsers() throws Exception {
		//log.debug("user service getAllSystemAdminUsers");
		List<UserDto> userDtoList = null;
		UserDto userDto = null;
		User user = null;
		// get list of system admin users
		List<AdminUser> adminUserInfoList = adminDao.getAllSystemAdminUsers();
		if (Format.isCollectionNotEmptyAndNotNull(adminUserInfoList)) {
			userDtoList = new ArrayList<UserDto>();
			for (AdminUser adminUserInfo : adminUserInfoList) {
				if (null != adminUserInfo) {
					user = userDao.getUserInfoByEmail(adminUserInfo.getUser().getEmail());
					userDto = userMapper.getUserDtoFromUserModel(user);
					userDtoList.add(userDto);
				}
			}
		} else {
			throw new MPDataNotFoundException("No system admin users defined");
		}
		return userDtoList;

	}

	//delete user by email 
	@Override
	@Transactional
	public boolean deleteUserByEmail(String email) throws Exception {

		boolean isUserUpdated = false;
		User user = null;
		user = userDao.getUserInfoByEmail(email);
		for (AdminUser ou : user.getAdminUsers()) {
			ou.setIsAccountLocked(true);
		}
		user.setStatus(StatusType.CLOSED.toString());
		user.setIsUserActive(false);
		isUserUpdated = userDao.updateUser(user);

		return isUserUpdated;
	}

	//check password
	@Override
	public boolean checkPassword(String email, String currentPassword, String newPassword, String oldPassword) {

		boolean result = bcryptPasswordEncoder.matches(oldPassword, currentPassword);
		if (result) {
			boolean auth = service.authenticateCredentials(email, oldPassword);
			if (auth) {
				result = true;
			}
		}
		return result;
	}

}

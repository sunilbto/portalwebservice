/**
 * 
 */
package com.mfs.pp.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.common.dto.AuditLogInputDto;
import com.mfs.mdm.common.dto.LoginAuditLogDTO;
import com.mfs.mdm.model.LoginAudit;
import com.mfs.mdm.model.Organization;
import com.mfs.pp.dao.OrganisationDao;
import com.mfs.pp.mapper.AuditLogMapper;
import com.mfs.pp.service.AuditLogService;
import com.mfs.pp.util.Format;

/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Service
public class AuditLogServiceImpl implements AuditLogService {

	protected final Log log = LogFactory.getLog(getClass());
	@Autowired
	private OrganisationDao organisationDao;

	@Autowired
	private AuditLogMapper auditLogMapper;

	//prepare audit log for login
	@SuppressWarnings("null")
	@Transactional
	@Override
	public boolean saveAuditLog(LoginAuditLogDTO loginAuditLogDTO) {
		//log.debug("Start of saveAuditLog in AuditLogServiceImpl");
		boolean isSaved = false;
		LoginAudit loginAudit = null;

		try {
			if (Format.isObjectNotEmptyAndNotNull(loginAuditLogDTO)) {
				//log.debug("loginAuditLogDTO " + loginAuditLogDTO);
				loginAudit = auditLogMapper.prepareLoginAuditinfoFromDto(loginAuditLogDTO);
				loginAudit.setOrgName(loginAuditLogDTO.getOrgName());
				//log.debug("loginAudit saved =" + loginAudit);
				isSaved = organisationDao.save(loginAudit);
			}
		} catch (Exception e) {
			log.error("exception " + e.getMessage());
		}
		return isSaved;

	}

	// get audit log as per date
	@Override
	@Transactional
	public List<LoginAuditLogDTO> getAuditLogListByDates(AuditLogInputDto reportsInputDto) {
		List<LoginAuditLogDTO> auditLogDTOList = null;
		List<LoginAudit> auditLogInfoList = null;
		Organization organization = null;
		try {
			//log.debug("in getAudits log for organisation.....");
			//log.debug("from date :" + reportsInputDto.getFromDate() + ",to date:" + reportsInputDto.getToDate());
			organization = organisationDao.getOrganizationByOrgName(reportsInputDto.getOrgnisationName());
			if (Format.isObjectNotEmptyAndNotNull(organization)) {
				auditLogInfoList = organisationDao.getAuditLogListByDates(
						Format.convertStringFromdate(reportsInputDto.getFromDate()),
						Format.convertStringTodate(reportsInputDto.getToDate()), organization.getOrgName());
				auditLogDTOList = auditLogMapper.prepareLoginAuditDtoListFromModel(auditLogInfoList);
			}

		} catch (Exception e) {
			log.error("exception while getAuditLogListByDates" + e.getMessage());
		}
		return auditLogDTOList;
	}

	//get audit log for login
	@Override
	public LoginAuditLogDTO getLoginAuditDtoForLogin(String userEmail, String address, String event, String Description,
			String orgName) {
		LoginAuditLogDTO loginAuditLogDTO = new LoginAuditLogDTO();
		loginAuditLogDTO.setDateTime(new Date());
		loginAuditLogDTO.setEmail(userEmail);
		loginAuditLogDTO.setEventResult(event);
		loginAuditLogDTO.setIpAddress(address);
		loginAuditLogDTO.setDescription(Description);
		loginAuditLogDTO.setOrgName(orgName);

		return loginAuditLogDTO;
	}

	//get admin audit log
	@Override
	@Transactional
	public List<LoginAuditLogDTO> getAdminAuditLogListByDates(AuditLogInputDto reportsInputDto) {
		List<LoginAuditLogDTO> auditLogDTOList = null;
		List<LoginAudit> auditLogInfoList = null;
		try {
			if (null != reportsInputDto) {
				//log.debug("in  getAdminAuditLogListByDates.....");

				//log.debug("from date :" + reportsInputDto.getFromDate() + ",to date:" + reportsInputDto.getToDate());
				auditLogInfoList = organisationDao.getAdminAuditLogListByDates(
						Format.convertStringFromdate(reportsInputDto.getFromDate()),
						Format.convertStringTodate(reportsInputDto.getToDate()));
				auditLogDTOList = auditLogMapper.prepareLoginAuditDtoListFromModel(auditLogInfoList);
			}

		} catch (Exception e) {
			log.error("exception while getAdminAuditLogListByDates" + e.getMessage());
		}
		return auditLogDTOList;
	}
}

package com.mfs.pp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.model.AdminRole;
import com.mfs.mdm.model.AdminRoleAccess;
import com.mfs.mdm.model.AdminUser;
import com.mfs.mdm.model.AdminUserRole;
import com.mfs.mdm.model.Menu;
import com.mfs.mdm.model.MenuAccess;
import com.mfs.mdm.model.OrgRoleAccess;
import com.mfs.mdm.model.Organization;
import com.mfs.mdm.model.OrganizationRole;
import com.mfs.mdm.model.OrganizationUser;
import com.mfs.mdm.model.OrganizationUserRole;
import com.mfs.mdm.model.RoleType;
import com.mfs.mdm.model.User;
import com.mfs.pp.dao.OrganisationDao;
import com.mfs.pp.dao.RoleDao;
import com.mfs.pp.dao.UserDao;
import com.mfs.pp.dto.MenuActionDTO;
import com.mfs.pp.dto.MenuDTO;
import com.mfs.pp.dto.RoleAssignList;
import com.mfs.pp.dto.RoleAssignRequest;
import com.mfs.pp.dto.RoleDTO;
import com.mfs.pp.service.RoleService;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */

@Service
public class RoleServiceImpl implements RoleService {

	// private static final Logger log =
	// LoggerFactory.getLogger(RoleServiceImpl.class);

	@Autowired
	private RoleDao roleDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	OrganisationDao organisationDao;

	//get List Of Admin Role
	@Override
	public List<RoleDTO> getListOfAdminRole() throws Exception {
		// log.debug("Inside getListOfAdminRole of RoleServiceImpl");
		List<RoleDTO> roleDTOLsit = null;
		List<AdminRole> adminRoleList = roleDao.getDefaultAdminRoleList();
		// check admin role list
		if (adminRoleList != null && adminRoleList.size() > 0) {
			roleDTOLsit = new ArrayList<>();
			for (AdminRole adminRole : adminRoleList) {
				RoleDTO roleDTO = new RoleDTO();
				roleDTO.setRoleName(adminRole.getRoleName());
				roleDTO.setId(adminRole.getAdminRoleId());
				roleDTO.setCreatedBy(adminRole.getCreatedBy());
				roleDTO.setCreatedOn(adminRole.getCreatedOn());
				roleDTO.setRoleDescription(adminRole.getRoleDescription());
				roleDTO.setRoleType(adminRole.getRoleType() != null ? adminRole.getRoleType().getRoleTypeName() : "");
				if (adminRole.getAdminRoleAccesses() != null) {
					List<MenuActionDTO> actionDTOs = new ArrayList<>();
					for (AdminRoleAccess adminRoleAccess : adminRole.getAdminRoleAccesses()) {
						MenuActionDTO actionDTO = new MenuActionDTO();
						actionDTO.setMenuAcionName(adminRoleAccess.getMenuAccess().getManuAccessName());
						actionDTO.setMenuName(adminRoleAccess.getMenuAccess().getMenu().getManuName());
						actionDTO.setMenuActionId(adminRoleAccess.getMenuAccess().getMenuAccessId());

						actionDTOs.add(actionDTO);
					}
					roleDTO.setMenuActionDTOList(actionDTOs);
				}
				roleDTOLsit.add(roleDTO);
			}
		}

		List<AdminRole> adminCustRoleList = roleDao.getCustomAdminRoleList();
		//check admin custom role list
		if (adminCustRoleList != null && adminCustRoleList.size() > 0) {
			if (roleDTOLsit == null) {
				roleDTOLsit = new ArrayList<>();
			}
			for (AdminRole adminRole : adminCustRoleList) {
				RoleDTO roleDTO = new RoleDTO();
				roleDTO.setRoleName(adminRole.getRoleName());
				roleDTO.setId(adminRole.getAdminRoleId());
				roleDTO.setCreatedBy(adminRole.getCreatedBy());
				roleDTO.setCreatedOn(adminRole.getCreatedOn());
				roleDTO.setRoleDescription(adminRole.getRoleDescription());
				roleDTO.setRoleType(adminRole.getRoleType() != null ? adminRole.getRoleType().getRoleTypeName() : "");
				if (adminRole.getAdminRoleAccesses() != null) {
					List<MenuActionDTO> actionDTOs = new ArrayList<>();
					for (AdminRoleAccess adminRoleAccess : adminRole.getAdminRoleAccesses()) {
						MenuActionDTO actionDTO = new MenuActionDTO();
						actionDTO.setMenuAcionName(adminRoleAccess.getMenuAccess().getManuAccessName());
						actionDTO.setMenuName(adminRoleAccess.getMenuAccess().getMenu().getManuName());
						actionDTO.setMenuActionId(adminRoleAccess.getMenuAccess().getMenuAccessId());
						actionDTOs.add(actionDTO);
					}
					roleDTO.setMenuActionDTOList(actionDTOs);
				}
				roleDTOLsit.add(roleDTO);
			}
		}
		return roleDTOLsit;
	}

	// get List Of Organiztion Role by orgId
	@Override
	public List<RoleDTO> getListOfOrgRole(int orgId) throws Exception {
		// log.debug("Inside getListOfOrgRole of RoleServiceImpl");
		List<RoleDTO> roleDTOLsit = new ArrayList<>();
		

		
		List<OrganizationRole> organizationRoleList = null;

		Organization organization = organisationDao.getOrganizationByID(orgId);
		if(organization!=null)
		{
		organizationRoleList = roleDao.getDefaultOrgRoleList(organization.getOrganizationType().getOrgType());
		// check organization role list
		if (!(organizationRoleList.isEmpty())) {
			for (OrganizationRole organizationRoles : organizationRoleList) {
				RoleDTO roleDTO = new RoleDTO();

				roleDTO.setRoleName(organizationRoles.getRoleName());
				roleDTO.setId(organizationRoles.getOrganisationRoleId());
				roleDTO.setCreatedBy(organizationRoles.getCreatedBy());
				roleDTO.setCreatedOn(organizationRoles.getCreationDate());
				roleDTO.setRoleDescription(organizationRoles.getRoleDescription());
				roleDTO.setRoleType(
						organizationRoles.getRoleType() != null ? organizationRoles.getRoleType().getRoleTypeName()
								: "");
				if (organizationRoles.getOrgRoleAccesses() != null) {
					List<MenuActionDTO> actionDTOs = new ArrayList<>();
					for (OrgRoleAccess orgRoleAccess : organizationRoles.getOrgRoleAccesses()) {
						MenuActionDTO actionDTO = new MenuActionDTO();
						actionDTO.setMenuAcionName(orgRoleAccess.getMenuAccess().getManuAccessName());
						actionDTO.setMenuName(orgRoleAccess.getMenuAccess().getMenu().getManuName());
						actionDTO.setMenuActionId(orgRoleAccess.getMenuAccess().getMenuAccessId());
						actionDTOs.add(actionDTO);
					}
					roleDTO.setMenuActionDTOList(actionDTOs);
				}
				roleDTOLsit.add(roleDTO);

			}

		}

		}
		
		List<OrganizationRole> orgCustRoleList = roleDao.getCustomOrgRoleList(orgId);
        // check organization custom role list
		if (!(orgCustRoleList.isEmpty())) {
			for (OrganizationRole organizationRole1 : orgCustRoleList) {
				RoleDTO roleDTO = new RoleDTO();
				roleDTO.setRoleName(organizationRole1.getRoleName());
				roleDTO.setId(organizationRole1.getOrganisationRoleId());
				roleDTO.setCreatedBy(organizationRole1.getCreatedBy());
				roleDTO.setCreatedOn(organizationRole1.getCreationDate());
				roleDTO.setRoleDescription(organizationRole1.getRoleDescription());
				roleDTO.setRoleType(
						organizationRole1.getRoleType() != null ? organizationRole1.getRoleType().getRoleTypeName()
								: "");
				if (organizationRole1.getOrgRoleAccesses() != null) {
					List<MenuActionDTO> actionDTOs = new ArrayList<>();
					for (OrgRoleAccess orgRoleAccess : organizationRole1.getOrgRoleAccesses()) {
						MenuActionDTO actionDTO = new MenuActionDTO();
						actionDTO.setMenuAcionName(orgRoleAccess.getMenuAccess().getManuAccessName());
						actionDTO.setMenuName(orgRoleAccess.getMenuAccess().getMenu().getManuName());
						actionDTO.setMenuActionId(orgRoleAccess.getMenuAccess().getMenuAccessId());
						actionDTOs.add(actionDTO);
					}
					roleDTO.setMenuActionDTOList(actionDTOs);
				}
				roleDTOLsit.add(roleDTO);
			}
		}

		

		return roleDTOLsit;
	}

	//create Admin Role
	@Transactional(rollbackFor = { Exception.class })
	public void createAdminRole(RoleDTO roleDTO) throws Exception {
		// log.debug("Inside createAdminRole of RoleServiceImpl");
		if (roleDTO != null) {
			AdminUserRole adminUserRole = new AdminUserRole();
			AdminRole adminRole = new AdminRole();
			adminRole.setRoleName(roleDTO.getRoleName());
			adminRole.setRoleDescription(roleDTO.getRoleDescription());
			adminRole.setCreatedOn(new Date());
			RoleType roleType = roleDao.getRoleType(roleDTO.getRoleType());
			adminRole.setRoleType(roleType);

			boolean isSuccess = roleDao.save(adminRole);
			User user = userDao.getUserInfoByEmail(roleDTO.getEmail());
			if (user != null) {
				AdminUser adminUser = roleDao.getAdminUser(user.getUserId());
				if (adminUser != null) {
					adminUserRole.setAdminUser(adminUser);
					adminUserRole.setAdminRole(adminRole);
					adminUserRole.setIsRoleActive(true);
					roleDao.save(adminUserRole);
				}
			}
			if (isSuccess) {
				List<MenuActionDTO> menuActionDTOList = roleDTO.getMenuActionDTOList();
				if (menuActionDTOList != null) {
					for (MenuActionDTO actionDTO : menuActionDTOList) {
						AdminRoleAccess access = new AdminRoleAccess();
						access.setAdminRole(adminRole);
						MenuAccess menuAccess = (MenuAccess) roleDao.get(MenuAccess.class, actionDTO.getMenuActionId());
						access.setManuAccess(menuAccess);
						roleDao.save(access);
					}
				}
			}
		}
	}

	//update Admin Role
	@Override
	public void updateAdminRole(RoleDTO roleDTO) throws Exception {
		if (roleDTO != null) {
			// log.debug("Inside updateAdminRole of RoleServiceImpl");
			AdminRole adminRole = (AdminRole) roleDao.get(AdminRole.class, roleDTO.getId());
			if (adminRole != null) {
				adminRole.setRoleName(roleDTO.getRoleName());
				adminRole.setRoleDescription(roleDTO.getRoleDescription());
				RoleType roleType = roleDao.getRoleType(roleDTO.getRoleType());
				adminRole.setRoleType(roleType);

				boolean isSuccess = roleDao.update(adminRole);
				if (isSuccess) {
					Set<Integer> menuAccessIdSet = null;
					if (adminRole.getAdminRoleAccesses() != null && adminRole.getAdminRoleAccesses().size() > 0) {
						menuAccessIdSet = new HashSet<>();
						for (AdminRoleAccess adminRoleAccess : adminRole.getAdminRoleAccesses()) {
							menuAccessIdSet.add(adminRoleAccess.getMenuAccess().getMenuAccessId());
						}
					}

					List<MenuActionDTO> menuActionDTOList = roleDTO.getMenuActionDTOList();
					if (menuActionDTOList != null) {
						for (MenuActionDTO actionDTO : menuActionDTOList) {
							if (menuAccessIdSet != null && menuAccessIdSet.contains(actionDTO.getMenuActionId())) {
								menuAccessIdSet.remove(actionDTO.getMenuActionId());
							} else {
								AdminRoleAccess access = new AdminRoleAccess();
								access.setAdminRole(adminRole);
								MenuAccess menuAccess = (MenuAccess) roleDao.get(MenuAccess.class,
										actionDTO.getMenuActionId());
								access.setManuAccess(menuAccess);
								roleDao.save(access);
							}
						}
						if (menuAccessIdSet != null) {
							for (Integer accessId : menuAccessIdSet) {
								AdminRoleAccess adminRoleAccess = roleDao.getRoleAccess(adminRole.getAdminRoleId(),
										accessId);
								roleDao.delete(adminRoleAccess);
							}
						}
					}
				}
			}
		}

	}

	//create Organiztion Role
	@Transactional(rollbackFor = { Exception.class })
	public void createOrgRole(RoleDTO roleDTO) throws Exception {
		// log.debug("Inside createOrgRole of RoleServiceImpl");
		if (roleDTO != null) {
			OrganizationUserRole organizationUserRole = new OrganizationUserRole();
			OrganizationRole organizationRole = new OrganizationRole();
			organizationRole.setRoleName(roleDTO.getRoleName());
			organizationRole.setCreationDate(new Date());
			organizationRole.setRoleDescription(roleDTO.getRoleDescription());
			organizationRole.setOrganizationId(roleDTO.getOrgId());
			RoleType roleType = roleDao.getRoleType(roleDTO.getRoleType());
			organizationRole.setRoleType(roleType);

			boolean isSuccess = roleDao.save(organizationRole);
			User user = userDao.getUserInfoByEmail(roleDTO.getEmail());
			if (user != null) {
				OrganizationUser organizationUser = roleDao.getOrgUser(user.getUserId());
				if (organizationUser != null) {
					organizationUserRole.setOrganizationRole(organizationRole);
					organizationUserRole.setOrganizationUser(organizationUser);
					organizationUserRole.setIsRoleActive(true);
					roleDao.save(organizationUserRole);
				}
			}
			if (isSuccess) {
				List<MenuActionDTO> menuActionDTOList = roleDTO.getMenuActionDTOList();
				if (menuActionDTOList != null) {
					for (MenuActionDTO actionDTO : menuActionDTOList) {
						OrgRoleAccess access = new OrgRoleAccess();
						access.setOrganizationRole(organizationRole);
						MenuAccess menuAccess = (MenuAccess) roleDao.get(MenuAccess.class, actionDTO.getMenuActionId());
						access.setManuAccess(menuAccess);
						roleDao.save(access);
					}
				}
			}
		}

	}

	//update Organiztion Role
	@Override
	public void updateOrgRole(RoleDTO roleDTO) throws Exception {
		// log.debug("Inside updateOrgRole of RoleServiceImpl");
		if (roleDTO != null) {
			OrganizationRole organizationRole = (OrganizationRole) roleDao.get(OrganizationRole.class, roleDTO.getId());
			if (organizationRole != null) {
				organizationRole.setRoleName(roleDTO.getRoleName());
				organizationRole.setRoleDescription(roleDTO.getRoleDescription());
				RoleType roleType = roleDao.getRoleType(roleDTO.getRoleType());
				organizationRole.setRoleType(roleType);

				boolean isSuccess = roleDao.update(organizationRole);
				if (isSuccess) {
					Set<Integer> menuAccessIdSet = null;
					if (organizationRole.getOrgRoleAccesses() != null
							&& organizationRole.getOrgRoleAccesses().size() > 0) {
						menuAccessIdSet = new HashSet<>();
						for (OrgRoleAccess orgRoleAccess : organizationRole.getOrgRoleAccesses()) {
							menuAccessIdSet.add(orgRoleAccess.getMenuAccess().getMenuAccessId());
						}
					}

					List<MenuActionDTO> menuActionDTOList = roleDTO.getMenuActionDTOList();
					if (menuActionDTOList != null) {
						for (MenuActionDTO actionDTO : menuActionDTOList) {
							if (menuAccessIdSet != null && menuAccessIdSet.contains(actionDTO.getMenuActionId())) {
								menuAccessIdSet.remove(actionDTO.getMenuActionId());
							} else {
								OrgRoleAccess access = new OrgRoleAccess();
								access.setOrganizationRole(organizationRole);
								MenuAccess menuAccess = (MenuAccess) roleDao.get(MenuAccess.class,
										actionDTO.getMenuActionId());
								access.setManuAccess(menuAccess);
								roleDao.save(access);
							}
						}
						if (menuAccessIdSet != null) {
							for (Integer accessId : menuAccessIdSet) {

								OrgRoleAccess orgRoleAccess = roleDao
										.getOrgRoleAccess(organizationRole.getOrganisationRoleId(), accessId);
								roleDao.delete(orgRoleAccess);
							}
						}
					}
				}
			}
		}

	}

	//get Admin Role Detail
	@Override
	public RoleDTO getAdminRoleDetail(int roleId) throws Exception {
		// log.debug("Inside getAdminRoleDetail of RoleServiceImpl");
		RoleDTO roleDTO = null;
		AdminRole adminRole = roleDao.getAdminRoleDetail(roleId);

		if (adminRole != null) {
			roleDTO = new RoleDTO();
			roleDTO.setRoleName(adminRole.getRoleName());
			roleDTO.setId(adminRole.getAdminRoleId());
			roleDTO.setRoleDescription(adminRole.getRoleDescription());
			roleDTO.setCreatedBy(adminRole.getCreatedBy());
			roleDTO.setCreatedOn(adminRole.getCreatedOn());
			roleDTO.setRoleType(adminRole.getRoleType() != null ? adminRole.getRoleType().getRoleTypeName() : "");
			if (adminRole.getAdminRoleAccesses() != null) {
				List<MenuActionDTO> actionDTOs = new ArrayList<>();
				for (AdminRoleAccess adminRoleAccess : adminRole.getAdminRoleAccesses()) {
					MenuActionDTO actionDTO = new MenuActionDTO();
					actionDTO.setMenuAcionName(adminRoleAccess.getMenuAccess().getManuAccessName());
					actionDTO.setMenuName(adminRoleAccess.getMenuAccess().getMenu().getManuName());
					actionDTO.setMenuActionId(adminRoleAccess.getMenuAccess().getMenuAccessId());
					actionDTOs.add(actionDTO);
				}
				roleDTO.setMenuActionDTOList(actionDTOs);
			}
		}
		return roleDTO;
	}

	//get Organiztion Role Detail 
	@Override
	public RoleDTO getOrgRoleDetail(int roleId) throws Exception {
		// log.debug("Inside getOrgRoleDetail of RoleServiceImpl");
		OrganizationRole organizationRole = roleDao.getOrgRoleDetail(roleId);
		RoleDTO roleDTO = null;
		if (organizationRole != null) {
			roleDTO = new RoleDTO();
			roleDTO.setId(organizationRole.getOrganisationRoleId());
			roleDTO.setCreatedBy(organizationRole.getCreatedBy());
			roleDTO.setCreatedOn(organizationRole.getCreationDate());
			roleDTO.setRoleName(organizationRole.getRoleName());
			roleDTO.setRoleType(
					organizationRole.getRoleType() != null ? organizationRole.getRoleType().getRoleTypeName() : "");
			if (organizationRole.getOrgRoleAccesses() != null) {
				List<MenuActionDTO> actionDTOs = new ArrayList<>();
				for (OrgRoleAccess orgRoleAccess : organizationRole.getOrgRoleAccesses()) {
					MenuActionDTO actionDTO = new MenuActionDTO();
					actionDTO.setMenuAcionName(orgRoleAccess.getMenuAccess().getManuAccessName());
					actionDTO.setMenuName(orgRoleAccess.getMenuAccess().getMenu().getManuName());
					actionDTO.setMenuActionId(orgRoleAccess.getMenuAccess().getMenuAccessId());
					actionDTOs.add(actionDTO);
				}
				roleDTO.setMenuActionDTOList(actionDTOs);
			}
		}
		return roleDTO;
	}

	//get Menu List
	@Override
	public List<MenuDTO> getMenuList() {
		// log.debug("Inside getMenuList of RoleServiceImpl");
		List<MenuDTO> menuLsit = null;
		List<Menu> menuList = roleDao.getMenuList();

		if (menuList != null && menuList.size() > 0) {
			menuLsit = new ArrayList<>();
			for (Menu menu : menuList) {
				MenuDTO menuDTO = new MenuDTO();
				menuDTO.setMenuId(menu.getManuId() + "");
				menuDTO.setMenuName(menu.getManuName());
				if (menu.getMenuAccesses() != null) {
					List<MenuActionDTO> actionDTOs = new ArrayList<>();
					for (MenuAccess menuAccess : menu.getMenuAccesses()) {
						MenuActionDTO actionDTO = new MenuActionDTO();
						actionDTO.setMenuAcionName(menuAccess.getManuAccessName());
						actionDTO.setMenuName(menuAccess.getMenu().getManuName());
						actionDTO.setMenuActionId(menuAccess.getMenuAccessId());
						actionDTOs.add(actionDTO);
					}
					menuDTO.setMenuActionDTOList(actionDTOs);
				}
				menuLsit.add(menuDTO);
			}
		}
		return menuLsit;
	}

	//delete org role
	@Transactional(rollbackFor = { Exception.class })
	public boolean deleteOrgRole(int roleId) {
		boolean isDelete = false;
		OrganizationRole organizationRole = null;

		OrganizationUserRole organizationUserRole = null;

		organizationRole = roleDao.getOrgRoleDetail(roleId);
		if (organizationRole != null) {

			organizationUserRole = roleDao.getOrgUserRole(organizationRole.getOrganisationRoleId());
			if (organizationUserRole != null) {
				roleDao.delete(organizationUserRole);
			}

			if ((organizationRole.getOrgRoleAccesses() != null)) {
				for (OrgRoleAccess orgRoleAccess : organizationRole.getOrgRoleAccesses()) {
					roleDao.delete(orgRoleAccess);
				}
			}
			roleDao.delete(organizationRole);
			isDelete = true;
		} else {

		}
		return isDelete;
	}
    //delete Admin Role
	@Transactional(rollbackFor = { Exception.class })
	public boolean deleteAdminRole(int roleId) {
		boolean isDelete = false;
		AdminUserRole adminUserRole = null;
		AdminRole adminRole = null;

		adminRole = roleDao.getAdminRoleDetail(roleId);
		if (adminRole != null) {
			adminUserRole = roleDao.adminUserRole(adminRole.getAdminRoleId());
			roleDao.delete(adminUserRole);

			if (adminRole.getAdminRoleAccesses() != null) {
				for (AdminRoleAccess adminRoleAccess : adminRole.getAdminRoleAccesses()) {
					roleDao.delete(adminRoleAccess);
				}
			}
			roleDao.delete(adminRole);
			isDelete = true;
		}
		return isDelete;
	}

	//get Role Assign
	@Transactional(rollbackFor = { Exception.class })
	public RoleAssignList getRoleAssign(RoleAssignRequest roleAssignRequest) {
		User user = userDao.getUserInfoByEmail(roleAssignRequest.getEmail());
		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		List<OrganizationRole> organizationRole = null;
		RoleAssignList roleAssignList = new RoleAssignList();
		if (roleAssignRequest.getFlag() == 1) {
			List<AdminRole> adminRole = roleDao.adminRoleList(roleAssignRequest.getRoleName());
			if (!(adminRole.isEmpty())) {
				roleAssignList.setPersonList(adminRole.size());
			}
			roleAssignList.setFirstName(firstName);
			roleAssignList.setLastName(lastName);

		} else if (roleAssignRequest.getOrgId() == 0) {
			organizationRole = roleDao.orgRoleList(roleAssignRequest.getOrgId(), roleAssignRequest.getRoleName());
			if (!(organizationRole.isEmpty())) {
				roleAssignList.setPersonList(organizationRole.size());
				roleAssignList.setFirstName(firstName);
				roleAssignList.setLastName(lastName);
			}
		} else {
			organizationRole = roleDao.orgRoleList(roleAssignRequest.getOrgId(), roleAssignRequest.getRoleName());
			if (!(organizationRole.isEmpty())) {
				roleAssignList.setPersonList(organizationRole.size());
				roleAssignList.setFirstName(firstName);
				roleAssignList.setLastName(lastName);
			}
		}

		return roleAssignList;
	}

}

package com.mfs.pp.service.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.pp.dao.TransactionDao;
import com.mfs.pp.dto.AllDashBoardRequestDto;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.service.DashBoardService;
import com.mfs.pp.util.CallServices;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Service
public class DashBoardServiceImpl implements DashBoardService {

	@Autowired
	TransactionDao transactionDao;
	private static final Logger log = LoggerFactory.getLogger(DashBoardServiceImpl.class);
	//return dashboard details
	@Override
	public Object getDashBoard(AllDashBoardRequestDto request) {
	//	log.info("Inside getDashBoard() method of DashBoardServiceImpl-----"+request);
		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();

		Gson gson = new Gson();
		
		String jsonResponse = CallServices.getResponseFromService(
				 systemConfigDetailsMap.get(Constants.BASE_URl) + RestServiceURIConstant.CURBI_GET_DASHBOARD, gson.toJson(request));
		
		
		  if(jsonResponse==null) {
		  log.error("Exception Inside getDashBoard() method of DashBoardServiceImpl");
		  throw new CustomException("Getting No Response"); }
		 

		Object response = (Object) gson.fromJson(jsonResponse, Object.class);

		return response;

	}

}

package com.mfs.pp.service.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.pp.dao.CountryZoneDao;
import com.mfs.pp.dao.TransactionDao;
import com.mfs.pp.dto.MoneyFilterRequestDto;
import com.mfs.pp.dto.MoneyFilterResponseDto;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.service.MoneyFilterService;
import com.mfs.pp.util.CallServices;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */

@Service("MoneyFilterService")
public class MoneyFilterServiceImpl implements MoneyFilterService {

	@Autowired
	TransactionDao transactionDao;
	@Autowired
	CountryZoneDao countryZoneDao;

	private static final Logger log = LoggerFactory.getLogger(MoneyFilterServiceImpl.class);

	// money filter
	public Object moneyfilterService(MoneyFilterRequestDto request) throws Exception {
		// log.info("Inside moneyfilterService() method of MoneyFilterServiceImpl");

		Object moneyResponse = new MoneyFilterResponseDto();

		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();

		Gson gson = new Gson();

		String jsonResponse = null;

		jsonResponse = CallServices.getResponseFromService(
				systemConfigDetailsMap.get(Constants.BASE_URl) + RestServiceURIConstant.CURBI_REGULATOR_MONEY_FILTER,
				gson.toJson(request));

		if (jsonResponse == null) {
			log.error("Exception Inside moneyfilterService() method of MoneyFilterServiceImpl");
			throw new CustomException("Getting No Response");
		}

		moneyResponse = gson.fromJson(jsonResponse, Object.class);

		return moneyResponse;

	}

}

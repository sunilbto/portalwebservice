package com.mfs.pp.service.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.pp.dao.TransactionDao;
import com.mfs.pp.dto.StatisticTransactionRequestDto;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.service.StatisticTransactionService;
import com.mfs.pp.util.CallServices;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */

@Service("StatisticTransactionService")
public class StatisticTransactionServiceImpl implements StatisticTransactionService {

	@Autowired
	TransactionDao transactionDao;

	private static final Logger log = LoggerFactory.getLogger(StatisticTransactionServiceImpl.class);

	// get Statistic Transaction Detail
	@Override
	public Object getStatisticTransactionDetailService(StatisticTransactionRequestDto request) throws Exception {
		// log.info("Inside getStatisticTransactionDetailService() method of
		// StatisticTransactionServiceImpl");

		Object statisticTransactionResponse = null;

		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();

		Gson gson = new Gson();
		String jsonResponse = null;

		jsonResponse = CallServices.getResponseFromService(
				systemConfigDetailsMap.get(Constants.BASE_URl)
						+ RestServiceURIConstant.CURBI_REGULATOR_GET_STATICSTIC_TRANSACTION_DETAILS,
				gson.toJson(request));

		if (jsonResponse == null) {
			log.error(
					"Exception Inside getStatisticTransactionDetailService() method of StatisticTransactionServiceImpl");
			throw new CustomException("Getting No Response");
		}

		statisticTransactionResponse = gson.fromJson(jsonResponse, Object.class);

		return statisticTransactionResponse;

	}

}

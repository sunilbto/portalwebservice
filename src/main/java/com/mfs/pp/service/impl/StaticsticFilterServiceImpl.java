package com.mfs.pp.service.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.pp.dao.TransactionDao;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.service.StaticsticFilterService;
import com.mfs.pp.util.CallServices;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */

@Service("StaticsticFilterService")
public class StaticsticFilterServiceImpl implements StaticsticFilterService {

	@Autowired
	TransactionDao transactionDao;

	private static final Logger log = LoggerFactory.getLogger(StaticsticFilterServiceImpl.class);

	// get Staticstic
	@Override
	public Object getStaticsticFilter() throws Exception {
		// log.info("Inside getStaticsticFilter() method of
		// StaticsticFilterServiceImpl");

		Object staticsticFilterResponse = null;

		Gson gson = new Gson();

		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();

		String jsonResponse = null;

		jsonResponse = CallServices.getResponseFromService(systemConfigDetailsMap.get(Constants.BASE_URl)
				+ RestServiceURIConstant.CURBI_REGULATOR_GET_STATICSTIC_FILTER);

		if (jsonResponse == null) {
			log.error("Exception Inside getStaticsticFilter() method of StaticsticFilterServiceImpl");
			throw new CustomException("Getting No Response");
		}

		staticsticFilterResponse = gson.fromJson(jsonResponse, Object.class);

		return staticsticFilterResponse;
	}

}

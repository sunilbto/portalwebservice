package com.mfs.pp.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.pp.dao.TransactionDao;
import com.mfs.pp.dto.AllBalanceByIdRequestDto;
import com.mfs.pp.dto.AllBalanceRequestDto;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.service.BalanceService;
import com.mfs.pp.util.CallServices;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Service
public class BalanceServiceImpl implements BalanceService {

	private static final Logger log = LoggerFactory.getLogger(BalanceServiceImpl.class);
	@Autowired
	TransactionDao transactionDao;

	//get balance summary
	@Override
	public List<Object> getBalanceSummury() {
		//log.info("Inside getBalanceSummury() method of BalanceServiceImpl");
		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();
		Gson gson = new Gson();
		String jsonResponse=null;
		jsonResponse = CallServices.getResponseFromService(
				systemConfigDetailsMap.get(Constants.BASE_URl) + RestServiceURIConstant.CURBI_GETBALANCESUMMURY);

		if(jsonResponse==null)
		{ log.error("Exception Inside getBalanceSummury() method of BalanceServiceImpl");
			throw new CustomException("Getting No Response");
		}
		List<Object> response = (List<Object>) gson.fromJson(jsonResponse, Object.class);

		return response;
	}

	// get list of balance
	@Override
	public Object getAllBalance(AllBalanceRequestDto request) throws Exception {
		//log.info("Inside getAllBalance() method of BalanceServiceImpl");
		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();
		Gson gson = new Gson();
		String jsonResponse=null;
		 jsonResponse = CallServices.getResponseFromService(
				 systemConfigDetailsMap.get(Constants.BASE_URl) + RestServiceURIConstant.CURBI_GET_ALL_BALANCE,
				gson.toJson(request));

		if(jsonResponse==null)
		{log.error("Exception Inside getAllBalance() method of BalanceServiceImpl");
			throw new CustomException("Getting No Response");
		}
		Object response = gson.fromJson(jsonResponse, Object.class);

		return response;
	}

	// get all balance by id
	@Override
	public Object getAllBalanceById(AllBalanceByIdRequestDto request) throws Exception {
		//log.info("Inside getAllBalanceById() method of BalanceServiceImpl ");
		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();
		Gson gson = new Gson();
		String jsonResponse=null;
		 jsonResponse = CallServices.getResponseFromService(
				 systemConfigDetailsMap.get(Constants.BASE_URl) + RestServiceURIConstant.CURBI_GET_ALL_BALANCE_BY_ID,
				gson.toJson(request));

		if(jsonResponse==null)
		{ log.error("Exception Inside getAllBalanceById() method of BalanceServiceImpl");
			throw new CustomException("Getting No Response");
		}
		Object response = gson.fromJson(jsonResponse, Object.class);

		return response;

	}

}

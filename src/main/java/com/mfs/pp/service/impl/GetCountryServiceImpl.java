package com.mfs.pp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.mdm.model.CountryZone;
import com.mfs.pp.dao.CountryZoneDao;
import com.mfs.pp.dto.GetCountryRequestDto;
import com.mfs.pp.dto.GetCountryResponseDto;
import com.mfs.pp.service.GetCountryService;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Service("GetCountryService")
public class GetCountryServiceImpl implements GetCountryService {

	@Autowired
	CountryZoneDao countryZoneDao;
	//private static final Logger log = LoggerFactory.getLogger(GetCountryServiceImpl.class);
	
	
	//get country by zone
	@Override
	public List<GetCountryResponseDto> getCountryByZone(GetCountryRequestDto request)throws Exception
	{ 
	//	log.info("Inside getCountryByZone() method of GetCountryServiceImpl");
		
		int zoneId=1;
		
		List<GetCountryResponseDto> countryList=new ArrayList<>();
		List<CountryZone> countryZoneList=countryZoneDao.getCountryByzone(zoneId);
		for(CountryZone countryZone:countryZoneList)
		{
			GetCountryResponseDto countryResponse=new GetCountryResponseDto();	
			countryResponse.setCountryId(countryZone.getCountryMaster().getCountryId());
			countryResponse.setCountryName(countryZone.getCountryMaster().getCountryName());
			countryList.add(countryResponse);
		}
		return countryList;
	}

}

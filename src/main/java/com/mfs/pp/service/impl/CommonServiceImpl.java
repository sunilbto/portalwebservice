package com.mfs.pp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.mdm.model.Category;
import com.mfs.mdm.model.OrganizationType;
import com.mfs.mdm.model.PartnerType;
import com.mfs.mdm.model.Status;
import com.mfs.pp.dao.CommonDao;
import com.mfs.pp.dto.CategoryResponse;
import com.mfs.pp.dto.OrganizationTypes;
import com.mfs.pp.dto.PartnerTypeDto;
import com.mfs.pp.dto.StatusResponse;
import com.mfs.pp.service.CommonService;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */

@Service("CommonService")
public class CommonServiceImpl implements CommonService {

	@Autowired
	private CommonDao commonDao;

	// get organization type list
	@Override
	public List<OrganizationTypes> getOrgTypeList() {
		// log.info("Inside getOrgTypeList() method of CommonServiceImpl");
		OrganizationTypes orgTypeResponse = null;
		List<OrganizationTypes> orgTypeResponseList = new ArrayList<OrganizationTypes>();
		// log.info("Before calling getAllorgType in commondao");
		List<OrganizationType> orgTypeList = commonDao.getAllOrgType();
		// log.info("After calling getAllorgType in commondao");
		if(!(orgTypeList.isEmpty()))
		{
		for (OrganizationType orgType : orgTypeList) {
			orgTypeResponse = new OrganizationTypes();
			orgTypeResponse.setOrgTypeId(orgType.getOrgTypeId());
			orgTypeResponse.setOrgType(orgType.getOrgType());
			orgTypeResponseList.add(orgTypeResponse);
		}
		}
		// log.info("Exting getOrgTypeList() method of CommonServiceImpl");
		return orgTypeResponseList;
	}

	// get category list
	@Override
	public List<CategoryResponse> getCategoryList() {
		// log.info("Inside getCategoryList() method of CommonServiceImpl");
		CategoryResponse categoryResponse = null;
		List<CategoryResponse> categoryResponseList = new ArrayList<CategoryResponse>();
		// log.info("Before calling getAllCategory in commondao");
		List<Category> categoryList = commonDao.getAllCategory();
		// log.info("After calling getAllCategory in commondao");
		for (Category category : categoryList) {
			categoryResponse = new CategoryResponse();
			categoryResponse.setCategoryId(category.getCategoryId());
			categoryResponse.setCategoryName(category.getCategoryName());
			categoryResponseList.add(categoryResponse);
		}
		// log.info("Exting getAllCategory() method of CommonServiceImpl");
		return categoryResponseList;
	}

	// returns status list
	@Override
	public List<StatusResponse> getStatusList() {
		// log.info("Inside getStatusList() method of CommonServiceImpl");
		StatusResponse statusResponse = null;
		List<StatusResponse> statusResponseList = new ArrayList<StatusResponse>();
		// log.info("Before calling getAllStatus in commondao");
		// log.info("After calling getAllStatus in commondao");
		List<Status> statusList = commonDao.getAllStatus();
		for (Status status : statusList) {
			statusResponse = new StatusResponse();
			statusResponse.setStatusId(status.getStatusId());
			statusResponse.setStatusName(status.getStatusName());
			statusResponseList.add(statusResponse);
		}
		// log.info("Exting getStatusList() method of CommonServiceImpl");
		return statusResponseList;
	}

	//returns list of partner types
	
	public List<PartnerTypeDto> getPartnerTypeList() {
		
		     PartnerTypeDto partnerTypeDto=null;
				List<PartnerTypeDto> partnerTypeResponseList=new ArrayList<PartnerTypeDto>();
			
				List<PartnerType> partnerTypeList=commonDao.getPartnerTypeList();
				
				if(!(partnerTypeList.isEmpty()))
				{
				for(PartnerType partnerType:partnerTypeList)
				{
					 partnerTypeDto=new PartnerTypeDto();
					 partnerTypeDto.setPartnerTypeId(partnerType.getPartnerTypeId());
					 partnerTypeDto.setPartnerType(partnerType.getPartnerType());
					 partnerTypeResponseList.add(partnerTypeDto);
				}
				}
				return partnerTypeResponseList;
	}

}

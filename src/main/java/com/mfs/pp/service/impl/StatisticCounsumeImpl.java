package com.mfs.pp.service.impl;

import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;
import com.mfs.pp.dao.TransactionDao;
import com.mfs.pp.dto.StatisticCounsumeRequestDto;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.service.StatisticCounsumeService;
import com.mfs.pp.util.CallServices;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */

@Service("StatisticCounsume")
public class StatisticCounsumeImpl implements StatisticCounsumeService {

	@Autowired
	TransactionDao transactionDao;

	private static final Logger log = LoggerFactory.getLogger(StatisticCounsumeImpl.class);

	// get statistic Counsume
	@Override
	public Object statisticCounsume(StatisticCounsumeRequestDto request)
			throws JsonParseException, JsonMappingException, IOException {
		// log.info("Inside StatisticCounsumeService() method of
		// StatisticCounsumeImpl");

		Object statisticCounsumeResponse = null;

		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();

		Gson gson = new Gson();
		String jsonResponse = null;

		jsonResponse = CallServices.getResponseFromService(systemConfigDetailsMap.get(Constants.BASE_URl)
				+ RestServiceURIConstant.CURBI_REGULATOR_GET_STATICSTIC_CONSUME_DETAILS, gson.toJson(request));

		if (jsonResponse == null) {
			log.error("Exception Inside StatisticCounsumeService() method of StatisticCounsumeImpl");
			throw new CustomException("Getting No Response");
		}

		statisticCounsumeResponse = gson.fromJson(jsonResponse, Object.class);

		return statisticCounsumeResponse;

	}

}

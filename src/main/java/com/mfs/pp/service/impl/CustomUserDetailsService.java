package com.mfs.pp.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.common.type.StatusType;
import com.mfs.mdm.model.AdminUser;
import com.mfs.mdm.model.AdminUserRole;
import com.mfs.mdm.model.OrganizationUser;
import com.mfs.mdm.model.OrganizationUserRole;
import com.mfs.pp.dao.UserDao;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.exception.ServiceErrFactory;
import com.mfs.pp.security.userdetails.CustomUserDetails;
import com.mfs.pp.type.MFSErrorType;
import com.mfs.pp.util.Format;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	UserDao userDao;

	@Autowired
	ServiceErrFactory serviceErrFactory;

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	BCryptPasswordEncoder bcryptPasswordEncoder;
	

	

	// returns user based on email
	@Override
	@Transactional
	public CustomUserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		com.mfs.mdm.model.User user = null;
		CustomUserDetails customUserDetails = null;
		user = userDao.getUserInfoByEmail(email);
		if (null != user) {
			if (Format.isCollectionNotEmptyAndNotNull(user.getOrganizationUsers())) {
				Set<OrganizationUser> organizationUsers = user.getOrganizationUsers();
				OrganizationUser organizationUser = organizationUsers.stream().findFirst().get();
				Set<OrganizationUserRole> organizationUserRoles = organizationUser.getOrganizationUserRoles();
				Set<GrantedAuthority> authorities = buildUserAuthority(organizationUserRoles);
				customUserDetails = new CustomUserDetails();
				customUserDetails.setUser(buildUserForAuthentication(user, authorities));
				customUserDetails.setAuthorities(authorities);
				customUserDetails.setUserId(user.getUserId());
			} else {
				Set<AdminUser> adminUsers = user.getAdminUsers();
				AdminUser adminUser = adminUsers.stream().findFirst().get();
				Set<AdminUserRole> adminUserRoles = adminUser.getAdminUserRoles();
				Set<GrantedAuthority> authorities = buildUserAuthorityForAdminUser(adminUserRoles);
				customUserDetails = new CustomUserDetails();
				customUserDetails.setUser(buildUserForAuthenticationForAdminUser(user, authorities));
				customUserDetails.setAuthorities(authorities);
				customUserDetails.setUserId(user.getUserId());
			}
		} else {
			throw new CustomException(MFSErrorType.USER_DOES_NOT_EXIST.getDescription());
		}
		return customUserDetails;
	}

	// user for authentication 
	private User buildUserForAuthentication(com.mfs.mdm.model.User user, Set<GrantedAuthority> authorities) {
		Set<OrganizationUser> organizationUsers = user.getOrganizationUsers();
		OrganizationUser organizationUser = organizationUsers.stream().findFirst().get();
		if (user.getIsUserActive() && null != user.getStatus() && null != user.getStatus()
				&& user.getStatus().equalsIgnoreCase(StatusType.Active.toString())) {
			return new User(user.getEmail(), organizationUser.getPassword(), user.getIsEmailVerified(),
					user.getIsUserActive(), organizationUser.getIsPasswordStatus(),
					!organizationUser.getIsAccountLocked(), authorities);
		} else {
			throw new CustomException(MFSErrorType.INACTIVE_USER.getDescription());
		}

	}

	// user authentication for admin user
	private User buildUserForAuthenticationForAdminUser(com.mfs.mdm.model.User user,
			Set<GrantedAuthority> authorities) {
		Set<AdminUser> adminUsers = user.getAdminUsers();
		AdminUser adminUser = adminUsers.stream().findFirst().get();
		if (user.getIsUserActive() && null != user.getStatus() && null != user.getStatus()
				&& user.getStatus().equalsIgnoreCase(StatusType.Active.toString())) {
			return new User(user.getEmail(), adminUser.getPassword(), user.getIsEmailVerified(), user.getIsUserActive(),
					adminUser.getIsPasswordStatus(), !adminUser.getIsAccountLocked(), authorities);
		} else {
			throw new CustomException(MFSErrorType.INACTIVE_USER.getDescription());
		}
	}

	//user authority
	private Set<GrantedAuthority> buildUserAuthority(Set<OrganizationUserRole> organizationUserRoles) {
		Set<GrantedAuthority> setAuths = new HashSet<>();
		for (OrganizationUserRole userRole : organizationUserRoles) {
			setAuths.add(new SimpleGrantedAuthority(userRole.getOrganizationRole().getRoleName()));
		}
		return setAuths;
	}

	// user authority for admin user
	private Set<GrantedAuthority> buildUserAuthorityForAdminUser(Set<AdminUserRole> adminUserRoles) {
		Set<GrantedAuthority> setAuths = new HashSet<>();
		for (AdminUserRole userRole : adminUserRoles) {
			setAuths.add(new SimpleGrantedAuthority(userRole.getAdminRole().getRoleName()));
		}
		return setAuths;
	}

	//login 
	@Transactional
	public boolean login(String username, String password) {
		SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_GLOBAL);
		Authentication authenticate = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		if (authenticate.isAuthenticated()) {
			SecurityContextHolder.getContext().setAuthentication(authenticate);
			return true;
		}
		return false;
	}

	//check authenticate Credentials
	@Transactional
	public boolean authenticateCredentials(String username, String password) {
		boolean flag = false;

		try {
			
			Authentication authenticate = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(username, password));
			if (authenticate.isAuthenticated()) {
				flag = true;
			}

		} catch (BadCredentialsException e) {
			flag = false;

		}
		return flag;
	}

	//get current user
	@Transactional
	public CustomUserDetails getCurrentUser() throws Exception {
		CustomUserDetails activeUser = null;
		if (null != SecurityContextHolder.getContext()
				&& null != SecurityContextHolder.getContext().getAuthentication()) {
			activeUser = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		}
		return activeUser;
	}

}

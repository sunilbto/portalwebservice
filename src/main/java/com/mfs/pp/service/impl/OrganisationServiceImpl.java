package com.mfs.pp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.common.dto.CountryDto;
import com.mfs.mdm.common.dto.CurrencyDto;
import com.mfs.mdm.common.dto.OrganisationProfileDto;
import com.mfs.mdm.common.dto.ProfileDto;
import com.mfs.mdm.common.dto.RolesDto;
import com.mfs.mdm.common.dto.UserDto;
import com.mfs.mdm.common.type.ReportType;
import com.mfs.mdm.model.Address;
import com.mfs.mdm.model.Category;
import com.mfs.mdm.model.CountryMaster;
import com.mfs.mdm.model.CurrencyMaster;
import com.mfs.mdm.model.Group;
import com.mfs.mdm.model.OrgGroup;
import com.mfs.mdm.model.Organization;
import com.mfs.mdm.model.OrganizationRole;
import com.mfs.mdm.model.OrganizationType;
import com.mfs.mdm.model.OrganizationUser;
import com.mfs.mdm.model.PartnerType;
import com.mfs.mdm.model.StateRegion;
import com.mfs.mdm.model.Status;
import com.mfs.mdm.model.User;
import com.mfs.mdm.model.Zone;
import com.mfs.pp.dao.CommonDao;
import com.mfs.pp.dao.CountryZoneDao;
import com.mfs.pp.dao.GroupOrgDao;
import com.mfs.pp.dao.OrganisationDao;
import com.mfs.pp.dao.UserDao;
import com.mfs.pp.dto.Addres;
import com.mfs.pp.dto.CategoryResponse;
import com.mfs.pp.dto.Groups;
import com.mfs.pp.dto.OrganizationListResponse;
import com.mfs.pp.dto.OrganizationListsResponse;
import com.mfs.pp.dto.OrganizationResponse;
import com.mfs.pp.dto.OrganizationResponseByName;
import com.mfs.pp.dto.OrganizationTypes;
import com.mfs.pp.dto.PartnerTypeDto;
import com.mfs.pp.dto.StateRegionResponse;
import com.mfs.pp.dto.Zones;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.mapper.OrganisationMapper;
import com.mfs.pp.service.CommonService;
import com.mfs.pp.service.OrganisationService;
import com.mfs.pp.service.UserService;
import com.mfs.pp.type.MFSErrorType;
import com.mfs.pp.util.Format;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */

@Service
public class OrganisationServiceImpl implements OrganisationService {

	private static final Logger log = LoggerFactory.getLogger(OrganisationServiceImpl.class);

	@Autowired
	private OrganisationDao organisationDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private OrganisationMapper organisationMapper;

	@Autowired
	CountryZoneDao zoneDao;

	@Autowired
	CommonDao commonDao;

	@Autowired
	GroupOrgDao groupOrgDao;

	@Autowired
	UserService userService;

	@Autowired
	CommonService commonService;

	@Autowired
	GroupOrgDao dao;

	@Autowired
	CountryZoneDao countryZoneDao;

	// update organization
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public int updateOrganizationDetails(OrganisationProfileDto organisationProfileDto) throws Exception {

		int result = 0;

		OrgGroup orgGroup = null;

		Organization organization = null;

		CountryMaster regulatorCountry = null;

		Zone zone = null;

		Address address = null;
		organization = organisationDao.getOrganizationByOrgName(organisationProfileDto.getOrganisationName());

		// check organization
		if (null == organization) {

			Category category = commonDao.getcategory(organisationProfileDto.getCategory());

			CurrencyMaster currency = commonDao
					.getCourencyDteail(organisationProfileDto.getCurrencyDto().getCurrencyName());

			// check zone name is null or not

			if (Format.isStringNotEmptyAndNotNull(organisationProfileDto.getTimezoneName())) {

				zone = countryZoneDao.checkZoneExist(organisationProfileDto.getTimezoneName());

			}

			OrganizationType orgtype = commonDao.getOrgType(organisationProfileDto.getOrganisationType());

			CountryMaster country = zoneDao
					.getCountryDetails(organisationProfileDto.getAddressDtoList().get(0).getCountry().getCountryName());

			// check regulator country null or not
			if (Format.isStringNotEmptyAndNotNull(organisationProfileDto.getRegulatorCountry())) {

				regulatorCountry = zoneDao.getCountryDetails(organisationProfileDto.getRegulatorCountry());
			}

			User user = commonDao.getUser(organisationProfileDto.getEmail());

			PartnerType partnerType = commonDao.getPartnerType(organisationProfileDto.getPartnerType());

			Organization org = new Organization();
			// check regulator country
			if (regulatorCountry != null) {
				org.setRegulatorCountryId(regulatorCountry);
			}
			org.setBusinessRegNo(organisationProfileDto.getBusinessRegistrationNumber());
			org.setComplianceEmail(organisationProfileDto.getComplianceEmail());
			org.setComplianceOfficer(organisationProfileDto.getComplianceOfficer());
			org.setCompliancePhone(organisationProfileDto.getCompliancePhone());
			org.setFinanceEmail(organisationProfileDto.getFinanceEmail());
			org.setOperationsEmail(organisationProfileDto.getOperationsEmail());
			org.setSupportEmail(organisationProfileDto.getSupportEmail());
			org.setDescription(organisationProfileDto.getDescription());
			org.setEmail(organisationProfileDto.getEmail());
			org.setFirstActivationDate(new Date());
			org.setMobileNumber(organisationProfileDto.getMobileNumber());
			org.setOrgApiKey(organisationProfileDto.getOrgApiKey());
			org.setOrgId(organisationProfileDto.getOrganisationID());
			org.setOrgName(organisationProfileDto.getOrganisationName());
			org.setRegDate(new Date());
			org.setFinanceEmail(organisationProfileDto.getFinanceEmail());
			org.setOperationsEmail(organisationProfileDto.getOperationsEmail());
			org.setSupportEmail(organisationProfileDto.getSupportEmail());
			org.setStatus(user.getStatus());
			if (category != null) {
				org.setCategories(category);
			}
			if (currency != null) {
				org.setCurrencyMaster(currency);
			}
			if (zone != null) {
				org.setZone(zone);
			}
			if (orgtype != null) {
				org.setOrganizationType(orgtype);
			}
			if (partnerType != null) {
				org.setPartnerType(partnerType);
			}

			int orgId = userDao.saveOrganization(org);

			// check address
			if (!(organisationProfileDto.getAddressDtoList().isEmpty())) {
				address = new Address();
				address.setAddress(organisationProfileDto.getAddressDtoList().get(0).getAddress());
				address.setAddress1(organisationProfileDto.getAddressDtoList().get(0).getAddress1());
				address.setAddress2(organisationProfileDto.getAddressDtoList().get(0).getAddress2());
				address.setCity(organisationProfileDto.getAddressDtoList().get(0).getCity());
				address.setPostalCode(organisationProfileDto.getAddressDtoList().get(0).getPostalCode());
				address.setStateRegion(organisationProfileDto.getAddressDtoList().get(0).getStateRegion());
				address.setCountryMaster(country);
				address.setOrganization(org.getOrganisationId());

				userDao.save(address);
			}
			// check group
			if (organisationProfileDto.getGroups().getGroupName() != null) {
				Group group = groupOrgDao.getOrganizationByGroupName(organisationProfileDto.getGroups().getGroupName());
				if (group != null) {
					orgGroup = new OrgGroup();
					orgGroup.setGroup(group);
					orgGroup.setOrgId(orgId);
					userDao.save(orgGroup);
				}

			}

			result = orgId;

		}

		return result;
	}

	// get user by organization
	@Override
	@Transactional
	public List<UserDto> getAllUserByOrganisation(String organisationName) throws Exception {
		List<UserDto> organisationUserDtoList = new ArrayList<UserDto>();
		UserDto userDto = null;
		ProfileDto profileDto = null;
		RolesDto roles = null;
		List<OrganizationUser> organizationUserInfolList = commonDao.getOrgUser(organisationName);
		// check organization user list
		if (!(organizationUserInfolList.isEmpty())) {
			for (OrganizationUser org : organizationUserInfolList) {
				userDto = new UserDto();
				profileDto = new ProfileDto();
				roles = new RolesDto();
				profileDto.setFirstName(org.getUser().getFirstName());
				profileDto.setLastName(org.getUser().getLastName());
				profileDto.setEmail(org.getUser().getEmail());
				profileDto.setStatus(org.getUser().getStatus());
				roles.setRoleName(org.getOrganizationUserRoles().iterator().next().getOrganizationRole().getRoleName());
				userDto.setRoles(roles);
				userDto.setProfileDto(profileDto);
				organisationUserDtoList.add(userDto);

			}

		}

		return organisationUserDtoList;
	}

	// returns all roles
	@Override
	@Transactional
	public List<RolesDto> getAllRoles() throws Exception {
		List<RolesDto> roleList = null;
		List<OrganizationRole> roleInfoList = null;
		roleInfoList = organisationDao.getAllRoles();
		// check organization role list
		if (Format.isCollectionNotEmptyAndNotNull(roleInfoList)) {
			roleList = organisationMapper.getRoleDtoListFromOrganizationRolesModel(roleInfoList);
		} else {
			throw new CustomException("Get roles failed");
		}

		return roleList;
	}

	// get All Currency
	@Override
	@Transactional
	public List<CurrencyDto> getAllCurruncyDtos() throws Exception {
		List<CurrencyDto> currencyDtoList = null;
		List<CurrencyMaster> currencyMasterList = null;

		currencyMasterList = organisationDao.getAllCurrencyMastersList();
		// check currency list
		if (Format.isCollectionNotEmptyAndNotNull(currencyMasterList)) {
			currencyDtoList = organisationMapper.prepareCurruncyDtoListFromModelList(currencyMasterList);
		} else {
			throw new CustomException("Get curruncy list failed");
		}

		return currencyDtoList;

	}

	// return API Key of organization
	@Override
	@Transactional
	public String getAPIKeyByOrganisationName(String organisationName) throws Exception {
		String apiKey = null;
		Organization organization = null;
		organization = organisationDao.getOrganizationByOrgName(organisationName);
		if (Format.isObjectNotEmptyAndNotNull(organization)) {
			apiKey = organization.getOrgApiKey();
		}
		if (null == apiKey) {
			throw new CustomException(MFSErrorType.NO_API_KEY.getDescription());
		}
		return apiKey;
	}

	// return organization details by organization name
	@Override
	@Transactional
	public OrganisationProfileDto getOrganisationDetailsByOrgName(String organisationName) throws Exception {
		Organization organization = null;
		OrganisationProfileDto organisationProfileDto = null;

		organization = organisationDao.getOrganizationByOrgName(organisationName);
		if (Format.isObjectNotEmptyAndNotNull(organization)) {
			organisationProfileDto = organisationMapper.getOrganisationProfileDtoFromModel(organization);
		} else {
			throw new CustomException("Get organisation details failed");
		}
		return organisationProfileDto;
	}

	// update organization user
	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean updateOrganizationUser(UserDto userDto) throws Exception {
		boolean flag = false;
		OrganizationUser organizationUser = null;
		User user = null;

		if (null != userDto && null != userDto.getProfileDto()) {
			user = userDao.getUserInfoByEmail(userDto.getProfileDto().getEmail());
			organizationUser = organisationMapper.prepareOrganizationUserInfo(userDto, user);
			if (null != organizationUser) {
				flag = true;
				return flag;
			} else {
				throw new CustomException("Organization User update failed");
			}
		}
		return false;
	}

	// get all report types
	@Override
	public List<String> getAllReportTypesList() throws Exception {
		List<String> reportTypeList = null;
		try {
			reportTypeList = new ArrayList<String>();
			reportTypeList.add(ReportType.ANALYTICS.getCode());
			reportTypeList.add(ReportType.TRANSACTIONS.getCode());
			reportTypeList.add(ReportType.TRANSACTION_DETAILS.getCode());
		} catch (Exception e) {
			log.error("exception " + e);
		}

		return reportTypeList;
	}

	// returns status types
	@Override
	@Transactional
	public List<String> getAllStatusTypes() throws Exception {
		List<String> statusTypesList = null;
		List<Status> statusInfoList = organisationDao.getAllStatusInfoList();
		if (Format.isCollectionNotEmptyAndNotNull(statusInfoList)) {
			statusTypesList = new ArrayList<String>();
			for (Status status : statusInfoList) {
				if (null != status) {
					statusTypesList.add(status.getStatusName());
				}
			}
		} else {
			throw new CustomException("There are no StatusTypes present.");
		}

		return statusTypesList;
	}

	// get Currency By CountryName
	@Override
	@Transactional
	public CurrencyDto getCurrencyByCountryName(String countryName) {
		CurrencyDto currencyDto = null;
		CurrencyMaster currencyMaster = null;
		if (Format.isStringNotEmptyAndNotNull(countryName)) {
			currencyMaster = organisationDao.getCurrencyByCountryName(countryName);
			currencyDto = organisationMapper.getCurrencyDtoFromCurrencyModel(currencyMaster);
		} else {
			throw new CustomException("There are no StatusTypes present.");
		}

		return currencyDto;
	}

	// update organization
	@Override
	@Transactional(rollbackFor = Exception.class)
	public String UpdateOrganization(OrganisationProfileDto organisationProfileDto) throws Exception {

		Organization org = organisationDao.getOrganizationByOrgName(organisationProfileDto.getOrganisationName());

		String result = null;

		Address address = null;

		Zone zone = null;

		CountryMaster regulatorCountry = null;
		// check organization
		if (org != null) {

			org.setBusinessRegNo(organisationProfileDto.getBusinessRegistrationNumber());

			Category category = commonDao.getcategory(organisationProfileDto.getCategory());
			org.setCategories(category);

			CurrencyMaster currency = commonDao
					.getCourencyDteail(organisationProfileDto.getCurrencyDto().getCurrencyName());
			// check zone name is null or not
			if (Format.isStringEmptyOrNull(organisationProfileDto.getTimezoneName())) {

				zone = countryZoneDao.checkZoneExist(organisationProfileDto.getTimezoneName());

			}

			CountryMaster country = zoneDao
					.getCountryDetails(organisationProfileDto.getAddressDtoList().get(0).getCountry().getCountryName());

			address = commonDao.getAddress(org.getOrganisationId());
			// check regulator country is null or not

			if (Format.isStringEmptyOrNull(organisationProfileDto.getRegulatorCountry())) {

				regulatorCountry = zoneDao.getCountryDetails(organisationProfileDto.getRegulatorCountry());

			}

			OrganizationType orgtype = commonDao.getOrgType(organisationProfileDto.getOrganisationType());

			PartnerType partnerType = commonDao.getPartnerType(organisationProfileDto.getPartnerType());
			if (currency != null) {
				org.setCurrencyMaster(currency);
			}
			if (regulatorCountry != null) {
				org.setRegulatorCountryId(regulatorCountry);
			}
			org.setComplianceEmail(organisationProfileDto.getComplianceEmail());
			org.setComplianceOfficer(organisationProfileDto.getComplianceOfficer());
			org.setCompliancePhone(organisationProfileDto.getCompliancePhone());
			if (zone != null) {
				org.setZone(zone);
			}
			org.setDescription(organisationProfileDto.getDescription());
			org.setEmail(organisationProfileDto.getEmail());
			org.setFinanceEmail(organisationProfileDto.getFinanceEmail());
			org.setOperationsEmail(organisationProfileDto.getOperationsEmail());
			org.setSupportEmail(organisationProfileDto.getSupportEmail());
			org.setFirstActivationDate(new Date());
			org.setMobileNumber(organisationProfileDto.getMobileNumber());
			org.setFinanceEmail(organisationProfileDto.getFinanceEmail());
			org.setOperationsEmail(organisationProfileDto.getOperationsEmail());
			org.setSupportEmail(organisationProfileDto.getSupportEmail());
			org.setOrganizationType(orgtype);
			org.setOrgApiKey(organisationProfileDto.getOrgApiKey());

			org.setOrgName(organisationProfileDto.getOrganisationName());
			org.setStatus(organisationProfileDto.getStatus());
			if (partnerType != null) {
				org.setPartnerType(partnerType);
			}
			userDao.update(org);
			// check address
			if (!(organisationProfileDto.getAddressDtoList().isEmpty())) {
				if (address != null) {

					address.setAddress(organisationProfileDto.getAddressDtoList().get(0).getAddress());
					address.setAddress1(organisationProfileDto.getAddressDtoList().get(0).getAddress1());
					address.setAddress2(organisationProfileDto.getAddressDtoList().get(0).getAddress2());
					address.setCity(organisationProfileDto.getAddressDtoList().get(0).getCity());
					address.setPostalCode(organisationProfileDto.getAddressDtoList().get(0).getPostalCode());
					address.setStateRegion(organisationProfileDto.getAddressDtoList().get(0).getStateRegion());
					address.setCountryMaster(country);
					address.setOrganization(org.getOrganisationId());

					userDao.update(address);
				} else {
					address = new Address();
					address.setAddress(organisationProfileDto.getAddressDtoList().get(0).getAddress());
					address.setAddress1(organisationProfileDto.getAddressDtoList().get(0).getAddress1());
					address.setAddress2(organisationProfileDto.getAddressDtoList().get(0).getAddress2());
					address.setCity(organisationProfileDto.getAddressDtoList().get(0).getCity());
					address.setPostalCode(organisationProfileDto.getAddressDtoList().get(0).getPostalCode());
					address.setStateRegion(organisationProfileDto.getAddressDtoList().get(0).getStateRegion());
					address.setCountryMaster(country);
					address.setOrganization(org.getOrganisationId());
					userDao.save(address);

				}
			}
			result = "org Updated";

		}

		return result;
	}

	// get organization list for admin
	@Override
	@Transactional
	public List<OrganisationProfileDto> getAllOrganisationListForAdmin() throws Exception {
		List<OrganisationProfileDto> orgDtoList = null;
		List<Organization> orgInfoList = organisationDao.getOrganisationList();
		if (Format.isCollectionNotEmptyAndNotNull(orgInfoList)) {
			orgDtoList = organisationMapper.getOrgProfileListFromModelList(orgInfoList);
		} else {
			throw new CustomException("There are no organizations present.");
		}
		return orgDtoList;
	}

	// return list of organization
	@Override
	public OrganizationResponse getOrgList() throws Exception {

		Address address = null;
		Addres addres = null;
		Groups groups = null;
		Zones zones = null;
		List<Zones> zonesList = new ArrayList<Zones>();
		StateRegionResponse stateRegionResponse = null;
		CountryDto countryDto = new CountryDto();
		List<Groups> groupsList = new ArrayList<Groups>();
		List<StateRegionResponse> stateRegionResponseList = new ArrayList<StateRegionResponse>();
		List<OrganizationListResponse> organizationList = new ArrayList<OrganizationListResponse>();
		OrganizationResponse OrganizationResponse = new OrganizationResponse();
		OrganizationListResponse organizationListResponse = null;
		List<CountryDto> countryList = userService.getAllCountryDtoList();
		List<CurrencyDto> currencyList = getAllCurruncyDtos();
		List<OrganizationTypes> orgTypeList = commonService.getOrgTypeList();
		List<Organization> orgList = organisationDao.getOrganisationList();
		List<PartnerTypeDto> partnerTypeList = commonService.getPartnerTypeList();
		List<StateRegion> stateList = commonDao.getStateList();
		// check state list
		if (!(stateList.isEmpty())) {
			for (StateRegion stateRegion : stateList) {
				stateRegionResponse = new StateRegionResponse();
				stateRegionResponse.setCountryId(stateRegion.getCountryMaster().getCountryId());
				stateRegionResponse.setStateId(stateRegion.getStateRegionId());
				stateRegionResponse.setStateName(stateRegion.getStateRegionName());
				stateRegionResponseList.add(stateRegionResponse);
			}
			OrganizationResponse.setStateList(stateRegionResponseList);
		}
		// check group list
		List<Group> groupList = dao.getAllGroup();
		if (!(groupList.isEmpty())) {
			for (Group group : groupList) {
				groups = new Groups();
				groups.setCeationDate(group.getCeationDate());
				groups.setGroupDescription(group.getDescription());
				groups.setGroupId(group.getGroupId());
				groups.setGroupName(group.getGroupName());
				groupsList.add(groups);

			}

			OrganizationResponse.setGroupList(groupsList);

		}
		// check zone list
		List<Zone> zoneList = countryZoneDao.getallZones();
		if (!(zoneList).isEmpty()) {
			for (Zone zone : zoneList) {
				zones = new Zones();
				zones.setDescription(zone.getDescription());
				zones.setZoneId(zone.getZoneId());
				zones.setZoneName(zone.getZoneName());
				zonesList.add(zones);

			}
			OrganizationResponse.setZonesList(zonesList);

		}
		if (!(partnerTypeList.isEmpty())) {
			OrganizationResponse.setPartnerTypeList(partnerTypeList);
		}

		List<CategoryResponse> categoryList = commonService.getCategoryList();
		if (!(categoryList.isEmpty())) {
			OrganizationResponse.setCategoryList(categoryList);
		}

		if (!(countryList.isEmpty())) {
			OrganizationResponse.setCountryList(countryList);
		}
		if (!(currencyList.isEmpty())) {
			OrganizationResponse.setCurrencyDtos(currencyList);
		}
		if (!(orgTypeList.isEmpty())) {
			OrganizationResponse.setOrgTypeList(orgTypeList);
		}
		if (!(orgList.isEmpty())) {
			for (Organization org : orgList) {

				organizationListResponse = new OrganizationListResponse();
				organizationListResponse.setOrgId(org.getOrganisationId());
				organizationListResponse.setBussniessRegNo(org.getBusinessRegNo());
				organizationListResponse.setCreationDate(org.getRegDate() + "");
				organizationListResponse.setDescription(org.getDescription());
				organizationListResponse.setFinanceEmail(org.getFinanceEmail());
				organizationListResponse.setMobileNo(org.getMobileNumber());
				organizationListResponse.setOperationsEmail(org.getOperationsEmail());
				organizationListResponse.setOrgName(org.getOrgName());
				organizationListResponse.setStatus(org.getStatus());
				organizationListResponse.setSupportEamil(org.getSupportEmail());
				organizationListResponse.setOrgType(org.getOrganizationType().getOrgType());
				organizationListResponse.setComplianceEmail(org.getComplianceEmail());
				organizationListResponse.setComplianceOfficer(org.getComplianceOfficer());
				organizationListResponse.setCompliancePhone(org.getCompliancePhone());
				if (org.getPartnerType() != null) {
					organizationListResponse.setPartnerType(org.getPartnerType().getPartnerType());

				}
				// check organization type
				if (org.getOrganizationType().getOrgType().equals("Regulator")) {
					if (org.getZone() != null) {
						organizationListResponse.setTimeZoneName(org.getZone().getZoneName());
					}
					if (org.getRegulatorCountryId() != null) {
						organizationListResponse.setRegulatorCountryName(org.getRegulatorCountryId().getCountryName());
					}
				}
				organizationListResponse.setCategory(org.getCategories().getCategoryName());
				organizationListResponse.setSupportEamil(org.getSupportEmail());
				organizationListResponse.setFinanceEmail(org.getFinanceEmail());
				organizationListResponse.setOperationsEmail(org.getOperationsEmail());
				organizationListResponse.setPartnerCode(org.getOrgId());
				organizationListResponse.setCurrencyName(org.getCurrencyMaster().getCurrencyCode());

				OrgGroup orgGroup = commonDao.getOrg(org.getOrganisationId());
				if (orgGroup != null) {
					organizationListResponse.setGroupName(orgGroup.getGroup().getGroupName());
				}

				address = commonDao.getAddress(org.getOrganisationId());
				if (address != null) {
					addres = new Addres();
					CountryMaster countryMaster = commonDao
							.getCountryDteails(address.getCountryMaster().getCountryId());
					if (countryMaster != null) {
						countryDto = new CountryDto();
						countryDto.setCountryCode(countryMaster.getCountryCode());
						countryDto.setCountryName(countryMaster.getCountryName());
						countryDto.setNumericCode(countryMaster.getNumericCode());
						countryDto.setPhoneCode(countryMaster.getPhoneCode());
						addres.setCountryDto(countryDto);

					}
					addres.setAddress(address.getAddress());
					addres.setAddress1(address.getAddress1());
					addres.setAddress2(address.getAddress2());
					addres.setPostalCode(address.getPostalCode());
					addres.setStateRegion(address.getStateRegion());

					organizationListResponse.setAddress(addres);

				}
				organizationList.add(organizationListResponse);

			}

			OrganizationResponse.setOrgList(organizationList);
		}
		return OrganizationResponse;
	}

	// returns details of organization
	@Override
	public OrganizationResponseByName getOrgDetails(String orgName) throws Exception {

		Address address = null;
		Addres addres = null;

		CountryDto countryDto = new CountryDto();
		OrganizationResponseByName organizationResponseByName = new OrganizationResponseByName();

		OrganizationListResponse organizationListResponse = null;

		List<CountryDto> countryList = userService.getAllCountryDtoList();
		List<CurrencyDto> currencyList = getAllCurruncyDtos();
		List<OrganizationTypes> orgTypeList = commonService.getOrgTypeList();
		Organization org = organisationDao.getOrganizationByOrgName(orgName);
		List<PartnerTypeDto> partnerTypeList = commonService.getPartnerTypeList();
		// check partner type list
		if (!(partnerTypeList.isEmpty())) {
			organizationResponseByName.setPartnerTypeList(partnerTypeList);
		}
		StateRegionResponse stateRegionResponse = null;
		List<StateRegionResponse> stateRegionResponseList = new ArrayList<StateRegionResponse>();
		Zones zones = null;
		List<Zones> zonesList = new ArrayList<Zones>();
		List<Zone> zoneList = countryZoneDao.getallZones();
		// check zone list
		if (!(zoneList).isEmpty()) {
			for (Zone zone : zoneList) {
				zones = new Zones();
				zones.setDescription(zone.getDescription());
				zones.setZoneId(zone.getZoneId());
				zones.setZoneName(zone.getZoneName());
				zonesList.add(zones);

			}
			organizationResponseByName.setZonesList(zonesList);

		}
		List<StateRegion> stateList = commonDao.getStateList();
		// check state list
		if (!(stateList.isEmpty())) {
			for (StateRegion stateRegion : stateList) {
				stateRegionResponse = new StateRegionResponse();
				stateRegionResponse.setCountryId(stateRegion.getCountryMaster().getCountryId());
				stateRegionResponse.setStateId(stateRegion.getStateRegionId());
				stateRegionResponse.setStateName(stateRegion.getStateRegionName());
				stateRegionResponseList.add(stateRegionResponse);
			}
			organizationResponseByName.setStateList(stateRegionResponseList);
		}

		List<CategoryResponse> categoryList = commonService.getCategoryList();
		if (!(categoryList.isEmpty())) {
			organizationResponseByName.setCategoryList(categoryList);
		}

		if (!(countryList.isEmpty())) {
			organizationResponseByName.setCountryList(countryList);
		}
		if (!(currencyList.isEmpty())) {
			organizationResponseByName.setCurrencyDtos(currencyList);
		}
		if (!(orgTypeList.isEmpty())) {
			organizationResponseByName.setOrgTypeList(orgTypeList);
		}
		if (org != null) {

			organizationListResponse = new OrganizationListResponse();
			// check organization type
			if (org.getOrganizationType().getOrgType().equals("Regulator")) {
				if (org.getZone() != null) {
					organizationListResponse.setTimeZoneName(org.getZone().getZoneName());
				}
				if (org.getRegulatorCountryId() != null) {
					organizationListResponse.setRegulatorCountryName(org.getRegulatorCountryId().getCountryName());
				}
			}
			organizationListResponse.setOrgId(org.getOrganisationId());
			organizationListResponse.setComplianceEmail(org.getComplianceEmail());
			organizationListResponse.setComplianceOfficer(org.getComplianceOfficer());
			organizationListResponse.setCompliancePhone(org.getCompliancePhone());
			organizationListResponse.setBussniessRegNo(org.getBusinessRegNo());
			organizationListResponse.setCreationDate(org.getRegDate() + "");
			organizationListResponse.setDescription(org.getDescription());
			organizationListResponse.setFinanceEmail(org.getFinanceEmail());
			organizationListResponse.setMobileNo(org.getMobileNumber());
			organizationListResponse.setOperationsEmail(org.getOperationsEmail());
			organizationListResponse.setOrgName(org.getOrgName());
			organizationListResponse.setStatus(org.getStatus());
			organizationListResponse.setSupportEamil(org.getSupportEmail());
			organizationListResponse.setOrgType(org.getOrganizationType().getOrgType());
			organizationListResponse.setCategory(org.getCategories().getCategoryName());
			organizationListResponse.setSupportEamil(org.getSupportEmail());
			organizationListResponse.setCurrencyName(org.getCurrencyMaster().getCurrencyName());
			organizationListResponse.setFinanceEmail(org.getFinanceEmail());
			organizationListResponse.setOperationsEmail(org.getOperationsEmail());
			organizationListResponse.setPartnerCode(org.getOrgId());
			if (org.getPartnerType() != null) {
				organizationListResponse.setPartnerType(org.getPartnerType().getPartnerType());

			}

			address = commonDao.getAddress(org.getOrganisationId());
			if (address != null) {
				addres = new Addres();
				CountryMaster countryMaster = commonDao.getCountryDteails(address.getCountryMaster().getCountryId());
				if (countryMaster != null) {
					countryDto.setCountryCode(countryMaster.getCountryCode());
					countryDto.setCountryName(countryMaster.getCountryName());
					countryDto.setNumericCode(countryMaster.getNumericCode());
					countryDto.setPhoneCode(countryMaster.getPhoneCode());
					addres.setCountryDto(countryDto);

				}
				addres.setAddress(address.getAddress());
				addres.setAddress1(address.getAddress1());
				addres.setAddress2(address.getAddress2());
				addres.setPostalCode(address.getPostalCode());
				addres.setStateRegion(address.getStateRegion());

				organizationListResponse.setAddress(addres);

			}

			organizationResponseByName.setOrg(organizationListResponse);

		}
		return organizationResponseByName;
	}

	// get Orgnization Detail bu orgname
	@Override
	public OrganizationResponseByName getOrgDetail(String orgName) throws Exception {
		OrganizationResponseByName organizationResponseByName = new OrganizationResponseByName();
		OrganizationListResponse organizationListResponse = null;
		Address address = null;
		Addres addres = null;
		Organization org = organisationDao.getOrganizationByOrgName(orgName);
		if (org != null) {
			organizationListResponse = new OrganizationListResponse();
			organizationListResponse.setComplianceEmail(org.getComplianceEmail());
			organizationListResponse.setComplianceOfficer(org.getComplianceOfficer());
			organizationListResponse.setCompliancePhone(org.getCompliancePhone());
			organizationListResponse.setOrgName(org.getOrgName());
			organizationListResponse.setMobileNo(org.getMobileNumber());
			address = commonDao.getAddress(org.getOrganisationId());
			if (address != null) {
				addres = new Addres();
				addres.setAddress(address.getAddress());
				organizationListResponse.setAddress(addres);

			}
			organizationResponseByName.setOrg(organizationListResponse);

		}
		return organizationResponseByName;
	}

	// get Organization List
	@Override
	public List<OrganizationListsResponse> getOrgLists() throws Exception {
		List<OrganizationListsResponse> organizationList = new ArrayList<OrganizationListsResponse>();
		OrganizationListsResponse organizationListResponse = null;
		List<Organization> orgList = organisationDao.getOrganisationList();

		// check org list is empty or not
		if (!(orgList.isEmpty())) {
			for (Organization org : orgList) {

				organizationListResponse = new OrganizationListsResponse();
				organizationListResponse.setOrgName(org.getOrgName());
				organizationListResponse.setOrgType(org.getOrganizationType().getOrgType());
				organizationListResponse.setPartnerCode(org.getOrgId());
				organizationList.add(organizationListResponse);

			}

		}

		return organizationList;
	}

}

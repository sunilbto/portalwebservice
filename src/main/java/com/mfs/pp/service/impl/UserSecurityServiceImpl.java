package com.mfs.pp.service.impl;

import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.common.dto.UserDto;
import com.mfs.mdm.common.type.StatusType;
import com.mfs.mdm.model.AdminUser;
import com.mfs.mdm.model.OrganizationUser;
import com.mfs.mdm.model.User;
import com.mfs.pp.dao.AdminDao;
import com.mfs.pp.dao.OrganisationDao;
import com.mfs.pp.dao.TransactionDao;
import com.mfs.pp.dao.UserDao;
import com.mfs.pp.dto.MailDetails;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.mapper.UserSecurityMapper;
import com.mfs.pp.service.OrganisationService;
import com.mfs.pp.service.UserSecurityService;
import com.mfs.pp.service.UserService;
import com.mfs.pp.type.MFSErrorType;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.Format;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Service
public class UserSecurityServiceImpl implements UserSecurityService {

	@Autowired
	UserService userService;

	@Autowired
	OrganisationService organisationService;

	@Autowired
	UserSecurityMapper userSecurityMapper;

	@Autowired
	UserDao userDao;

	@Autowired
	AdminDao adminDao;

	@Autowired
	OrganisationDao organisationDao;
	
	@Autowired
	TransactionDao transactionDao;

	//construct Reset Token And SendEmail
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public boolean constructResetTokenAndSendEmail(String email, String emailUrl, String userCreationType)
			throws Exception {
		boolean isUserUpdated = false;
		UserDto userDto = null;
		boolean isMailSent = false;
		String url = null;
		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();
		userDto = userService.getUserByUserEmailId(email);
		// check user creation type
		if (null != userCreationType && userCreationType.equals("reset")) {
			if (null != userDto && userDto.isActiveUser()
					&& (null != userDto.getProfileDto() && userDto.isPasswordStatus()
							&& userDto.getProfileDto().getStatus().equals(StatusType.Active.toString()))) {
				String token = UUID.randomUUID().toString();
				userDto.setResetPassword(token);
				userDto.setResetPasswordCreateDate(new Date());
				userDto.setPasswordStatus(false);

				OrganizationUser organizationUser = organisationDao
						.getOrganizationUserByUserEmail(userDto.getProfileDto().getEmail());
				// check organization user
				if (Format.isObjectNotEmptyAndNotNull(organizationUser)) {
					organizationUser = userSecurityMapper.prepareOrgUserForReset(userDto, organizationUser);
				}
				
				AdminUser adUser = adminDao.getAdminUserByUserEmail(userDto.getProfileDto().getEmail());
				// check admin user
				if (Format.isObjectNotEmptyAndNotNull(adUser)) {
					adUser = userSecurityMapper.prepareAdminUserForReset(userDto, adUser);
				}

				// Forgot Password
				url = systemConfigDetailsMap.get(Constants.HOST)+"?id=" + email + "&token=" + token + "&type="
						+ userCreationType;

				if (Format.isObjectNotEmptyAndNotNull(organizationUser)) {
					isUserUpdated = userDao.saveOrUpdate(organizationUser);
				} else if (Format.isObjectNotEmptyAndNotNull(adUser)) {
					isUserUpdated = adminDao.saveOrUpdate(adUser);
				}
				// check  user update or not
				if (isUserUpdated) {
					MailDetails mailDetails = userSecurityMapper.prepareMailDetailsDto(
							userDto.getProfileDto().getEmail(), url, userCreationType, "Administrator", null,null,null,null);
					isMailSent = sendMail(mailDetails);
				}
			} else {
				// Password or Email or User status is inactive.
				throw new CustomException("Either password is not set or account is inactive");
			}
		} else {
			if (null != userDto) {
				String token = UUID.randomUUID().toString();
				userDto.setResetPassword(token);
				userDto.setResetPasswordCreateDate(new Date());
				if (userCreationType.equalsIgnoreCase("register")) {
					// Register User
					url = systemConfigDetailsMap.get(Constants.HOST) + "?id=" + email + "&token=" + token + "&type="
							+ userCreationType;
				} else {
					// Invite User
					// will be set to false at the time of register
					url = systemConfigDetailsMap.get(Constants.HOST)+"?id=" + email + "&token=" + token + "&type="
							+ userCreationType;
				}
				isUserUpdated = organisationService.updateOrganizationUser(userDto);
				// check user update or not
				if (isUserUpdated) {
					MailDetails mailDetails = userSecurityMapper.prepareMailDetailsDto(
							userDto.getProfileDto().getEmail(), url, userCreationType,
							userDto.getProfileDto().getFirstName() + " " + userDto.getProfileDto().getLastName(),
							userDto.getOrgProfileDto().getOrganisationName(),null,null,null);
					isMailSent = sendMail(mailDetails);
				}
			}

		}
		// check mail sent or not
		if (!isMailSent) {
			throw new CustomException(MFSErrorType.EMAIL_NOT_TRIGGERED.getDescription());
		}
		return isMailSent;
	}

	//construct Reset Token For AdminUser
	@Override
	@Transactional
	public String constructResetTokenForAdminUser(UserDto userDto) throws Exception {
		User user = null;
		String token = null;
		boolean isAdminUserForInviteUserSaved = false;
		if (null != userDto && null != userDto.getProfileDto()) {
			user = userDao.getUserInfoByEmail(userDto.getProfileDto().getEmail());
			if (null != user) {
				user = userSecurityMapper.prepareUserFromUserDtoForResetToken(user, userDto);
				for (AdminUser adminUser : user.getAdminUsers()) {
					isAdminUserForInviteUserSaved = userDao.saveOrUpdate(adminUser);
					// check user save or not
					if (!isAdminUserForInviteUserSaved) {
						throw new CustomException("Cannot save user");
					} else {
						token = adminUser.getResetPassword();
					}
				}
				user.setAdminUsers(user.getAdminUsers());
			}
		}
		return token;
	}

	// new Organization Send Email
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public boolean newOrganizationSendEmail(String email, String orgName) throws Exception {

		boolean isMailSent = false;

		MailDetails mailDetails = userSecurityMapper.prepareMailDetailsDto(email, null, "newOrganization", null,
				orgName,null,null,null);
		isMailSent = sendMail(mailDetails);
		// check mail sent or not
		if (!isMailSent) {
			throw new CustomException(MFSErrorType.EMAIL_NOT_TRIGGERED.getDescription());
		}
		return isMailSent;
	}

	private boolean sendMail(MailDetails mailDetails) throws AddressException, MessagingException {
		Properties props = System.getProperties();
		props.put("mail.smtp.starttls.enable", mailDetails.getTlsEnable());
		props.put("mail.smtp.host", mailDetails.getHost());
		props.put("mail.smtp.user", mailDetails.getFromUserName());
		props.put("mail.smtp.password", mailDetails.getPassword());
		props.put("mail.smtp.port", mailDetails.getPort());
		props.put("mail.smtp.auth", mailDetails.getAuth());

		props.put("mail.smtp.ssl.trust", mailDetails.getHost());

		Session session = Session.getDefaultInstance(props);
		MimeMessage message = new MimeMessage(session);
		Transport transport = null;
		try {
			message.setFrom(new InternetAddress(mailDetails.getFromUserName()));
			InternetAddress[] toAddress = new InternetAddress[mailDetails.getRecipient().length];

			for (int i = 0; i < mailDetails.getRecipient().length; i++) {
				toAddress[i] = new InternetAddress(mailDetails.getRecipient()[i]);
			}

			for (int i = 0; i < toAddress.length; i++) {
				message.addRecipient(Message.RecipientType.TO, toAddress[i]);
			}

			message.setSubject(mailDetails.getSubject());

			message.setContent(mailDetails.getBody(), "text/html");
			transport = session.getTransport("smtp");

			transport.connect(mailDetails.getHost(), mailDetails.getFromUserName(), mailDetails.getPassword());
			transport.sendMessage(message, message.getAllRecipients());
			return true;
		} finally {
			if (transport != null) {
				try {
					transport.close();
				} catch (MessagingException e) {
				}
			}

		}
	}

	//validate User Token
	@Override
	public boolean validateUserToken(String userName, String token, String type) throws Exception {
		boolean isValidUser = false;
		UserDto userDto = userService.getUserByUserEmailId(userName);
		Date currentDate = new Date();
		if (null != userDto && null != userDto.getResetPasswordCreateDate()) {
			long sec = (currentDate.getTime() - userDto.getResetPasswordCreateDate().getTime()) / 1000;
			long hrs = sec / 3600;
			long limit = 2;
			if (hrs < limit ) {
				isValidUser = true;
			} else if (hrs > limit && token.equals(userDto.getResetPassword())) {
				isValidUser = false;
				throw new CustomException(MFSErrorType.EXPIRED_LINK.getDescription());
			} else {
				isValidUser = false;
				throw new CustomException("Invalid token");
			}
		}
		return isValidUser;
	}

	//udpate New Create Password
	@Override
	public boolean udpateNewCreatePassword(String userName, String password, String type) {
		return false;
	}

	//activate User By Email Verification
	@Override
	@Transactional
	public boolean activateUserByEmailVerification(String email) throws Exception {
		UserDto userDto = userService.getUserByUserEmailId(email);
		userDto = userSecurityMapper.prepareUserDtoForEmailVerification(userDto);
		boolean isUserUpdated = userService.updateUser(userDto);
		return isUserUpdated;
	}

	//send Email
	@Override
	public boolean sendEmail(String email, String emailUrl, String token, String userCreationType)
			throws AddressException, MessagingException, Exception {
		Map<String, String> systemConfigDetailsMap = transactionDao.getConfigDetailsMap();
		String url = systemConfigDetailsMap.get(Constants.HOST)+"?id=" + email + "&token=" + token + "&type="
				+ userCreationType;
		MailDetails mailDetails = userSecurityMapper.prepareMailDetailsDto(email, url, userCreationType,
				"Administrator", null,null,null,null);
		boolean isMailSent = sendMail(mailDetails);
		return isMailSent;
	}
	//organization Send Email Query
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public boolean organizationSendEmailQuery(String email, String comments,String orgId,String orgName,String userName,String userRole,String userEmail) throws Exception {

		boolean isMailSent = false;

		MailDetails mailDetails = userSecurityMapper.prepareMailDetailsDto(email, comments, "organisationQuery", userRole,
				orgName,userEmail,userName,orgId);
		isMailSent = sendMail(mailDetails);
		if (!isMailSent) {
			throw new CustomException(MFSErrorType.EMAIL_NOT_TRIGGERED.getDescription());
		}
		return isMailSent;
	}


}

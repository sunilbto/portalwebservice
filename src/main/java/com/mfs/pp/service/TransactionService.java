package com.mfs.pp.service;

import com.mfs.pp.dto.AllTransactionRequestDto;
import com.mfs.pp.dto.AlltransactionByIdRequestDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface TransactionService {


    public Object getAllTransaction(AllTransactionRequestDto request) throws Exception;   
	
	public Object getTransactionById(AlltransactionByIdRequestDto request)throws Exception;
	


}

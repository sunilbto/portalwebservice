package com.mfs.pp.service;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import com.mfs.mdm.common.dto.UserDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface UserSecurityService {

	public boolean constructResetTokenAndSendEmail(String email, String emailUrl, String userCreationType) throws Exception;

	public boolean validateUserToken(String userName, String token,String type) throws Exception;

	public boolean udpateNewCreatePassword(String userName, String password, String type) throws Exception;

	public boolean activateUserByEmailVerification( String email) throws Exception;

	public String constructResetTokenForAdminUser(UserDto userDto) throws Exception;

	public boolean sendEmail(String email, String emailUrl,String token, String userCreationType) throws AddressException, MessagingException,Exception ;
	
	public boolean newOrganizationSendEmail(String email,String orgName)
			throws Exception ;
	
	public boolean organizationSendEmailQuery(String email, String comments,String orgId,String orgName,String userName,String userRole,String userEmail) throws Exception;

}

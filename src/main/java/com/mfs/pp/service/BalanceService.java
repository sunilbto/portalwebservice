package com.mfs.pp.service;

import java.util.List;

import com.mfs.pp.dto.AllBalanceByIdRequestDto;
import com.mfs.pp.dto.AllBalanceRequestDto;


/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface BalanceService {
	
	public List<Object> getBalanceSummury() throws Exception;
	
	public Object getAllBalance(AllBalanceRequestDto request)throws Exception;
	
	public Object getAllBalanceById(AllBalanceByIdRequestDto request)throws Exception;

}

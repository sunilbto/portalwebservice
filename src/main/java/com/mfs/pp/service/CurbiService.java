package com.mfs.pp.service;

import com.mfs.mdm.common.dto.OrganisationProfileDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface CurbiService {
	public String updateOrganizationDetails(OrganisationProfileDto organisationProfileDto) throws Exception;
}

package com.mfs.pp.service;

import com.mfs.pp.dto.Groups;
import com.mfs.pp.dto.GroupsResponse;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public interface GroupOrgService {

	public String createGroup(Groups groups) throws Exception;

	public GroupsResponse getAllGroup() throws Exception;

	public String updateByGroup(Groups groups) throws Exception;

}

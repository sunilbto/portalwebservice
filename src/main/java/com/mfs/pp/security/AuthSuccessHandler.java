package com.mfs.pp.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class AuthSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
    @Override
    protected String determineTargetUrl(HttpServletRequest request, HttpServletResponse response) {
        String targetUrl="";
        if (null!=SecurityContextHolder.getContext()) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (null!=auth && null!=auth.getAuthorities()) {
				String role = auth.getAuthorities().toString();
				if (null!=role && role.contains("Administrator")) {
					targetUrl = "/pp/login";
				} 
			} 
		}
		return targetUrl;
    }
}

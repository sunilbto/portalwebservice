package com.mfs.pp.dto;

import java.util.Date;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class ZoneDetails {
	private int zoneId;
	private String zoneName;
	private String description;
	private Date createaDate;
	private String createdBy;

	public int getZoneId() {
		return zoneId;
	}

	public void setZoneId(int zoneId) {
		this.zoneId = zoneId;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateaDate() {
		return createaDate;
	}

	public void setCreateaDate(Date createaDate) {
		this.createaDate = createaDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public String toString() {
		return "ZoneDetails [zoneId=" + zoneId + ", zoneName=" + zoneName + ", description=" + description
				+ ", createaDate=" + createaDate + ", createdBy=" + createdBy + "]";
	}

}

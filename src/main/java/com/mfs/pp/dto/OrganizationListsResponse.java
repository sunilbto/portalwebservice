package com.mfs.pp.dto;

public class OrganizationListsResponse {

	private String orgName;
	private String partnerCode;
	private String orgType;

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getOrgType() {
		return orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	@Override
	public String toString() {
		return "OrganizationListsResponse [orgName=" + orgName + ", partnerCode=" + partnerCode + ", orgType=" + orgType
				+ "]";
	}

}

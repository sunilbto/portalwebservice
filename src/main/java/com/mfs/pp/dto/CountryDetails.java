package com.mfs.pp.dto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class CountryDetails {
	private int countryId;
	private String countryName;
	private String countryCode;
	private String numericCode;
	private int phoneCode;
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getNumericCode() {
		return numericCode;
	}
	public void setNumericCode(String numericCode) {
		this.numericCode = numericCode;
	}
	public int getPhoneCode() {
		return phoneCode;
	}
	public void setPhoneCode(int phoneCode) {
		this.phoneCode = phoneCode;
	}
	@Override
	public String toString() {
		return "CountryDetails [countryId=" + countryId + ", countryName=" + countryName + ", countryCode="
				+ countryCode + ", numericCode=" + numericCode + ", phoneCode=" + phoneCode + "]";
	}
	
}

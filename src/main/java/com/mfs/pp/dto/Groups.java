package com.mfs.pp.dto;

import java.util.Date;
import java.util.List;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class Groups {
	private int groupId;
	private String groupName;
	private Date ceationDate;
	private String groupDescription;
	private List<Organizations> organizations;
	

	public List<Organizations> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(List<Organizations> organizations) {
		this.organizations = organizations;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Date getCeationDate() {
		return ceationDate;
	}

	public void setCeationDate(Date ceationDate) {
		this.ceationDate = ceationDate;
	}

	public String getGroupDescription() {
		return groupDescription;
	}

	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	@Override
	public String toString() {
		return "Groups [groupId=" + groupId + ", groupName=" + groupName + ", ceationDate=" + ceationDate
				+ ", groupDescription=" + groupDescription + ", organizations=" + organizations + "]";
	}

}

package com.mfs.pp.dto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class RoleAssignList {

	private String firstName;
	private String lastName;
	private int personList;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getPersonList() {
		return personList;
	}

	public void setPersonList(int personList) {
		this.personList = personList;
	}

	@Override
	public String toString() {
		return "RoleAssignList [firstName=" + firstName + ", lastName=" + lastName + ", personList=" + personList + "]";
	}

}

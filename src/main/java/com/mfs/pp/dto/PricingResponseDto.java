package com.mfs.pp.dto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class PricingResponseDto {

	private String partnerName;
	
	private double averageSendFee;
	
	private double averageTotalCost;
	
	private double averageTotalCostPercent;
	private String partnerCurrency;
 


	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPartnerCurrency() {
		return partnerCurrency;
	}

	public void setPartnerCurrency(String partnerCurrency) {
		this.partnerCurrency = partnerCurrency;
	}

	public double getAverageSendFee() {
		return averageSendFee;
	}

	public void setAverageSendFee(double averageSendFee) {
		this.averageSendFee = averageSendFee;
	}

	public double getAverageTotalCost() {
		return averageTotalCost;
	}

	public void setAverageTotalCost(double averageTotalCost) {
		this.averageTotalCost = averageTotalCost;
	}

	public double getAverageTotalCostPercent() {
		return averageTotalCostPercent;
	}

	public void setAverageTotalCostPercent(double averageTotalCostPercent) {
		this.averageTotalCostPercent = averageTotalCostPercent;
	}

	@Override
	public String toString() {
		return "PricingResponseDto [partnerName=" + partnerName + ", averageSendFee=" + averageSendFee
				+ ", averageTotalCost=" + averageTotalCost + ", averageTotalCostPercent=" + averageTotalCostPercent
				+ ", partnerCurrency=" + partnerCurrency + "]";
	}

	
	
	
}           

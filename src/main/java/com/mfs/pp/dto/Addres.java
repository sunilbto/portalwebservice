package com.mfs.pp.dto;

import com.mfs.mdm.common.dto.CountryDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class Addres {

	private String address;

	private String address1;

	private String address2;

	private String postalCode;

	private String stateRegion;

	private CountryDto countryDto;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getStateRegion() {
		return stateRegion;
	}

	public void setStateRegion(String stateRegion) {
		this.stateRegion = stateRegion;
	}

	public CountryDto getCountryDto() {
		return countryDto;
	}

	public void setCountryDto(CountryDto countryDto) {
		this.countryDto = countryDto;
	}

	@Override
	public String toString() {
		return "Addres [address=" + address + ", address1=" + address1 + ", address2=" + address2 + ", postalCode="
				+ postalCode + ", stateRegion=" + stateRegion + ", countryDto=" + countryDto + "]";
	}

}

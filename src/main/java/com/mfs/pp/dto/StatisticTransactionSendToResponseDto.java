package com.mfs.pp.dto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class StatisticTransactionSendToResponseDto {

	
    private String partnerName;
	
	private String partnerDirection;
	
	private double avgAmount;

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPartnerDirection() {
		return partnerDirection;
	}

	public void setPartnerDirection(String partnerDirection) {
		this.partnerDirection = partnerDirection;
	}

	public double getAvgAmount() {
		return avgAmount;
	}

	public void setAvgAmount(double avgAmount) {
		this.avgAmount = avgAmount;
	}

	@Override
	public String toString() {
		return "StatisticTransactionSendToResponseDto [partnerName=" + partnerName + ", partnerDirection="
				+ partnerDirection + ", avgAmount=" + avgAmount + "]";
	}
	
	
	
}

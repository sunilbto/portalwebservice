package com.mfs.pp.dto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class ContactDto {
	private int contactDetailsId;
	private String email;
	private String partnerName;
	private String comment;

	private QueryTypeDto queryTypeDto;

	public int getContactDetailsId() {
		return contactDetailsId;
	}

	public void setContactDetailsId(int contactDetailsId) {
		this.contactDetailsId = contactDetailsId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public QueryTypeDto getQueryTypeDto() {
		return queryTypeDto;
	}

	public void setQueryTypeDto(QueryTypeDto queryTypeDto) {
		this.queryTypeDto = queryTypeDto;
	}

	@Override
	public String toString() {
		return "ContactDto [contactDetailsId=" + contactDetailsId + ", email=" + email + ", partnerName=" + partnerName
				+ ", comment=" + comment + ", queryTypeDto=" + queryTypeDto + "]";
	}

}
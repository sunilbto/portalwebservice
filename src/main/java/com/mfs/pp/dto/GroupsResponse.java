package com.mfs.pp.dto;

import java.util.List;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class GroupsResponse {
	private List<OrganizationDetails> organizationDetails;
	private GroupListReponse groupListReponse;

	public List<OrganizationDetails> getOrganizationDetails() {
		return organizationDetails;
	}

	public void setOrganizationDetails(List<OrganizationDetails> organizationDetails) {
		this.organizationDetails = organizationDetails;
	}

	public GroupListReponse getGroupListReponse() {
		return groupListReponse;
	}

	public void setGroupListReponse(GroupListReponse groupListReponse) {
		this.groupListReponse = groupListReponse;
	}

	@Override
	public String toString() {
		return "GroupsResponse [organizationDetails=" + organizationDetails + ", groupListReponse=" + groupListReponse
				+ "]";
	}

}

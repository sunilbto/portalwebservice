package com.mfs.pp.dto;

import java.util.List;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class ZonesDetails {
private List<Zones> zoneList;

public List<Zones> getZoneList() {
	return zoneList;
}

public void setZoneList(List<Zones> zoneList) {
	this.zoneList = zoneList;
}

@Override
public String toString() {
	return "ZonesDetails [zoneList=" + zoneList + "]";
}





}

package com.mfs.pp.dto;

import java.util.List;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class ZoneDetailsResponse {

	private ZoneDetails zoneDetails;
	private List<CountryDetails> countryDetails;

	public ZoneDetails getZoneDetails() {
		return zoneDetails;
	}

	public void setZoneDetails(ZoneDetails zoneDetails) {
		this.zoneDetails = zoneDetails;
	}

	public List<CountryDetails> getCountryDetails() {
		return countryDetails;
	}

	public void setCountryDetails(List<CountryDetails> countryDetails) {
		this.countryDetails = countryDetails;
	}

	@Override
	public String toString() {
		return "ZoneDetailsResponse [zoneDetails=" + zoneDetails + ", countryDetails=" + countryDetails + "]";
	}

}

/**
 * 
 */
package com.mfs.pp.dto;

import java.util.Arrays;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class MailDetails {

	private String host;
	private String fromUserName;
	private String password;
	private String[] recipient;
	private String tlsEnable;
	private String port;
	private String auth;
	private String subject;
	private String body;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String[] getRecipient() {
		return recipient;
	}

	public void setRecipient(String[] recipient) {
		this.recipient = recipient;
	}

	public String getTlsEnable() {
		return tlsEnable;
	}

	public void setTlsEnable(String tlsEnable) {
		this.tlsEnable = tlsEnable;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "MailDetails [host=" + host + ", fromUserName=" + fromUserName
				+ ", password=" + password + ", recipient="
				+ Arrays.toString(recipient) + ", tlsEnable=" + tlsEnable
				+ ", port=" + port + ", auth=" + auth + ", subject=" + subject
				+ ", body=" + body + "]";
	}

}

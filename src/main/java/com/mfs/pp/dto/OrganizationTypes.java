package com.mfs.pp.dto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class OrganizationTypes {
	private String orgType;
	private int orgTypeId;

	public int getOrgTypeId() {
		return orgTypeId;
	}

	public void setOrgTypeId(int orgTypeId) {
		this.orgTypeId = orgTypeId;
	}

	public String getOrgType() {
		return orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	@Override
	public String toString() {
		return "OrganizationTypes [orgType=" + orgType + ", orgTypeId=" + orgTypeId + "]";
	}

}

package com.mfs.pp.dto;

import java.util.List;

import com.mfs.mdm.common.dto.CountryDto;
import com.mfs.mdm.common.dto.CurrencyDto;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */

public class OrganizationResponse {
	private List<OrganizationTypes> orgTypeList;
	private List<CountryDto> countryList;
	private List<CurrencyDto> currencyDtos;
	private List<OrganizationListResponse> orgList;
	private List<Groups> groupList;
	private List<CategoryResponse> categoryList;
	private List<StateRegionResponse> stateList;
	private List<Zones> zonesList;
	private List<PartnerTypeDto> partnerTypeList;

	public List<OrganizationTypes> getOrgTypeList() {
		return orgTypeList;
	}

	public void setOrgTypeList(List<OrganizationTypes> orgTypeList) {
		this.orgTypeList = orgTypeList;
	}

	public List<CountryDto> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<CountryDto> countryList) {
		this.countryList = countryList;
	}

	public List<CurrencyDto> getCurrencyDtos() {
		return currencyDtos;
	}

	public void setCurrencyDtos(List<CurrencyDto> currencyDtos) {
		this.currencyDtos = currencyDtos;
	}

	public List<OrganizationListResponse> getOrgList() {
		return orgList;
	}

	public void setOrgList(List<OrganizationListResponse> orgList) {
		this.orgList = orgList;
	}

	public List<Groups> getGroupList() {
		return groupList;
	}

	public void setGroupList(List<Groups> groupList) {
		this.groupList = groupList;
	}

	public List<CategoryResponse> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<CategoryResponse> categoryList) {
		this.categoryList = categoryList;
	}

	public List<StateRegionResponse> getStateList() {
		return stateList;
	}

	public void setStateList(List<StateRegionResponse> stateList) {
		this.stateList = stateList;
	}

	public List<Zones> getZonesList() {
		return zonesList;
	}

	public void setZonesList(List<Zones> zonesList) {
		this.zonesList = zonesList;
	}

	public List<PartnerTypeDto> getPartnerTypeList() {
		return partnerTypeList;
	}

	public void setPartnerTypeList(List<PartnerTypeDto> partnerTypeList) {
		this.partnerTypeList = partnerTypeList;
	}

	@Override
	public String toString() {
		return "OrganizationResponse [orgTypeList=" + orgTypeList + ", countryList=" + countryList + ", currencyDtos="
				+ currencyDtos + ", orgList=" + orgList + ", groupList=" + groupList + ", categoryList=" + categoryList
				+ ", stateList=" + stateList + ", zonesList=" + zonesList + ", partnerTypeList=" + partnerTypeList
				+ "]";
	}

}

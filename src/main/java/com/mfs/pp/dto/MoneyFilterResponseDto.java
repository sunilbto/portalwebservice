package com.mfs.pp.dto;


import java.util.List;


/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */



public class MoneyFilterResponseDto {
	
	 private List<MdmProductResponseDto> productList;

	 private List<MdmDirectionResponseDto> directionList;
	 
	 private List<MdmDestinationTypeResponseDto> destinationTypeList;
	 
	 private List<MdmCountryResponseDto> countryList;
	 
	 private List<MdmSendPartnerResponseDto> mdmSendPartnerList;
	 
	 private List<MdmSendPartnerResponseDto> mdmRecievePartnerList;
	 
	 private List<MdmSendCountryDto> mdmSendCountryList;
	 
	 private List<MdmSendCountryDto> mdmReceiveCountryList;

	public List<MdmProductResponseDto> getProductList() {
		return productList;
	}

	public void setProductList(List<MdmProductResponseDto> productList) {
		this.productList = productList;
	}

	public List<MdmDirectionResponseDto> getDirectionList() {
		return directionList;
	}

	public void setDirectionList(List<MdmDirectionResponseDto> directionList) {
		this.directionList = directionList;
	}

	public List<MdmDestinationTypeResponseDto> getDestinationTypeList() {
		return destinationTypeList;
	}

	public void setDestinationTypeList(List<MdmDestinationTypeResponseDto> destinationTypeList) {
		this.destinationTypeList = destinationTypeList;
	}

	public List<MdmCountryResponseDto> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<MdmCountryResponseDto> countryList) {
		this.countryList = countryList;
	}

	public List<MdmSendPartnerResponseDto> getMdmSendPartnerList() {
		return mdmSendPartnerList;
	}

	public void setMdmSendPartnerList(List<MdmSendPartnerResponseDto> mdmSendPartnerList) {
		this.mdmSendPartnerList = mdmSendPartnerList;
	}

	public List<MdmSendPartnerResponseDto> getMdmRecievePartnerList() {
		return mdmRecievePartnerList;
	}

	public void setMdmRecievePartnerList(List<MdmSendPartnerResponseDto> mdmRecievePartnerList) {
		this.mdmRecievePartnerList = mdmRecievePartnerList;
	}

	public List<MdmSendCountryDto> getMdmSendCountryList() {
		return mdmSendCountryList;
	}

	public void setMdmSendCountryList(List<MdmSendCountryDto> mdmSendCountryList) {
		this.mdmSendCountryList = mdmSendCountryList;
	}

	public List<MdmSendCountryDto> getMdmReceiveCountryList() {
		return mdmReceiveCountryList;
	}

	public void setMdmReceiveCountryList(List<MdmSendCountryDto> mdmReceiveCountryList) {
		this.mdmReceiveCountryList = mdmReceiveCountryList;
	}

	@Override
	public String toString() {
		return "MoneyFilterResponseDto [productList=" + productList + ", directionList=" + directionList
				+ ", destinationTypeList=" + destinationTypeList + ", countryList=" + countryList
				+ ", mdmSendPartnerList=" + mdmSendPartnerList + ", mdmRecievePartnerList=" + mdmRecievePartnerList
				+ ", mdmSendCountryList=" + mdmSendCountryList + ", mdmReceiveCountryList=" + mdmReceiveCountryList
				+ "]";
	}

	

	
	 

}

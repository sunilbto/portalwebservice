package com.mfs.pp.dto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class MdmDestinationTypeResponseDto {
	
	private String destinationType;

	public String getDestinationType() {
		return destinationType;
	}

	public void setDestinationType(String destinationType) {
		this.destinationType = destinationType;
	}

	@Override
	public String toString() {
		return "MdmDestinationTypeResponseDto [destinationType=" + destinationType + "]";
	}
	
	

}

package com.mfs.pp.dto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class StatisticTransactionSendFromResponseDto {
	
	 private String partnerName;
		
	 private String partnerDirection;
		
	 private double avgAmount;

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPartnerDirection() {
		return partnerDirection;
	}

	public void setPartnerDirection(String partnerDirection) {
		this.partnerDirection = partnerDirection;
	}

	public double getAvgAmount() {
		return avgAmount;
	}

	public void setAvgAmount(double avgAmount) {
		this.avgAmount = avgAmount;
	}

	@Override
	public String toString() {
		return "StatisticTransactionSendFromResponseDto [partnerName=" + partnerName + ", partnerDirection="
				+ partnerDirection + ", avgAmount=" + avgAmount + "]";
	}
	 
	 

}

package com.mfs.pp.dto;

import java.util.List;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class GroupDetailsResponse {

	private Groups groups;
	private List<OrganizationDetails> organizationDetails;

	public Groups getGroups() {
		return groups;
	}

	public void setGroups(Groups groups) {
		this.groups = groups;
	}

	public List<OrganizationDetails> getOrganizationDetails() {
		return organizationDetails;
	}

	public void setOrganizationDetails(List<OrganizationDetails> organizationDetails) {
		this.organizationDetails = organizationDetails;
	}

	@Override
	public String toString() {
		return "GroupDetailsResponse [groups=" + groups + ", organizationDetails=" + organizationDetails + "]";
	}

}

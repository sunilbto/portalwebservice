package com.mfs.pp.dto;

import java.util.Date;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class AllTransactionDetailsResponseDto {
	
	private String mfsId;
	
    private String status;
	
	private Date creationDate;
	
	private Date proccessDate;
	
	private String product;
	
	private String direction;
	
	private String country;
	
	private double amount;
	
	private double fee;
	
	private String currency;
	
	private double balanceBefore;
	
	private double principle;
	
	private double commision;
	
	private double feeRevShare;
	
	private double forexRevShare;
	
	private double balanceAfter;

	public String getMfsId() {
		return mfsId;
	}

	public void setMfsId(String mfsId) {
		this.mfsId = mfsId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getProccessDate() {
		return proccessDate;
	}

	public void setProccessDate(Date proccessDate) {
		this.proccessDate = proccessDate;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public double getBalanceBefore() {
		return balanceBefore;
	}

	public void setBalanceBefore(double balanceBefore) {
		this.balanceBefore = balanceBefore;
	}

	public double getPrinciple() {
		return principle;
	}

	public void setPrinciple(double principle) {
		this.principle = principle;
	}

	public double getCommision() {
		return commision;
	}

	public void setCommision(double commision) {
		this.commision = commision;
	}

	public double getFeeRevShare() {
		return feeRevShare;
	}

	public void setFeeRevShare(double feeRevShare) {
		this.feeRevShare = feeRevShare;
	}

	public double getForexRevShare() {
		return forexRevShare;
	}

	public void setForexRevShare(double forexRevShare) {
		this.forexRevShare = forexRevShare;
	}

	public double getBalanceAfter() {
		return balanceAfter;
	}

	public void setBalanceAfter(double balanceAfter) {
		this.balanceAfter = balanceAfter;
	}

	@Override
	public String toString() {
		return "AllTransactionDetailsResponseDto [mfsId=" + mfsId + ", status=" + status + ", creationDate="
				+ creationDate + ", proccessDate=" + proccessDate + ", product=" + product + ", direction=" + direction
				+ ", country=" + country + ", amount=" + amount + ", fee=" + fee + ", currency=" + currency
				+ ", balanceBefore=" + balanceBefore + ", principle=" + principle + ", commision=" + commision
				+ ", feeRevShare=" + feeRevShare + ", forexRevShare=" + forexRevShare + ", balanceAfter=" + balanceAfter
				+ "]";
	}
	
	

	

	
	
	/*
	 * private int thirdPartyTransactionId;
	 * 
	 * private Date valueDate;
	 * 
	 * private String destinationType;
	 * 
	 * private String partner;
	 * 
	 * private int fx_rate;
	 * 
	 * 
	 */
	
	//financial details
	//settlementCurrency
	


	
	 

}

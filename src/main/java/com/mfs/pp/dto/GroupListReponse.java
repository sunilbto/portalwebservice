package com.mfs.pp.dto;

import java.util.List;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class GroupListReponse {

	private List<GroupDetailsResponse> groupDetailsResponse;

	public List<GroupDetailsResponse> getGroupDetailsResponse() {
		return groupDetailsResponse;
	}

	public void setGroupDetailsResponse(List<GroupDetailsResponse> groupDetailsResponse) {
		this.groupDetailsResponse = groupDetailsResponse;
	}

	@Override
	public String toString() {
		return "GroupListReponse [groupDetailsResponse=" + groupDetailsResponse + "]";
	}

}
package com.mfs.pp.dto;

import java.util.List;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class ZoneListResponse {

	private List<ZoneDetailsResponse> zoneList;

	public List<ZoneDetailsResponse> getZoneList() {
		return zoneList;
	}

	public void setZoneList(List<ZoneDetailsResponse> zoneList) {
		this.zoneList = zoneList;
	}

	@Override
	public String toString() {
		return "ZoneListResponse [zoneList=" + zoneList + "]";
	}

}

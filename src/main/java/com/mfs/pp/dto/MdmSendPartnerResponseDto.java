package com.mfs.pp.dto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class MdmSendPartnerResponseDto {
	
	
	private int partnerId;
	
	private String partnerName;
	
	private String partnerCode;
	
	private String partnerStatus;
	
	private int partnerType;
	
	private String officialPartnerName;

	public int getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(int partnerId) {
		this.partnerId = partnerId;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getPartnerStatus() {
		return partnerStatus;
	}

	public void setPartnerStatus(String partnerStatus) {
		this.partnerStatus = partnerStatus;
	}

	public int getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(int partnerType) {
		this.partnerType = partnerType;
	}

	public String getOfficialPartnerName() {
		return officialPartnerName;
	}

	public void setOfficialPartnerName(String officialPartnerName) {
		this.officialPartnerName = officialPartnerName;
	}

	@Override
	public String toString() {
		return "MdmSendPartnerResponseDto [partnerId=" + partnerId + ", partnerName=" + partnerName + ", partnerCode="
				+ partnerCode + ", partnerStatus=" + partnerStatus + ", partnerType=" + partnerType
				+ ", officialPartnerName=" + officialPartnerName + "]";
	}
	
	
}

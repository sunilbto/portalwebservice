package com.mfs.pp.dto;

import java.io.Serializable;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class MenuActionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7077310181450983989L;

	private String menuAcionName;
	private String menuName;
	private int menuActionId;
	
	public int getMenuActionId() {
		return menuActionId;
	}
	public void setMenuActionId(int menuActionId) {
		this.menuActionId = menuActionId;
	}
	public String getMenuAcionName() {
		return menuAcionName;
	}
	public void setMenuAcionName(String menuAcionName) {
		this.menuAcionName = menuAcionName;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	@Override
	public String toString() {
		return "MenuActionDTO [menuAcionName=" + menuAcionName + ", menuName=" + menuName + ", menuActionId="
				+ menuActionId + "]";
	}
	
}

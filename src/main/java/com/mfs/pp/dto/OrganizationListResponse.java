package com.mfs.pp.dto;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */

public class OrganizationListResponse {

	private String orgName;
	private String bussniessRegNo;
	private String description;
	private String category;
	private String timeZoneName;
	private String currencyName;
	private String groupName;
	private int orgId;
	private String mobileNo;
	private String financeEmail;
	private String supportEamil;
	private String operationsEmail;
	private Addres address;
	private String status;
	private String creationDate;
	private String orgType;
	private String partnerCode;
	private String complianceOfficer;
	private String complianceEmail;
	private String compliancePhone;
	private String regulatorCountryName;
	private String partnerType;

	public String getRegulatorCountryName() {
		return regulatorCountryName;
	}

	public void setRegulatorCountryName(String regulatorCountryName) {
		this.regulatorCountryName = regulatorCountryName;
	}

	public String getComplianceOfficer() {
		return complianceOfficer;
	}

	public void setComplianceOfficer(String complianceOfficer) {
		this.complianceOfficer = complianceOfficer;
	}

	public String getComplianceEmail() {
		return complianceEmail;
	}

	public void setComplianceEmail(String complianceEmail) {
		this.complianceEmail = complianceEmail;
	}

	public String getCompliancePhone() {
		return compliancePhone;
	}

	public void setCompliancePhone(String compliancePhone) {
		this.compliancePhone = compliancePhone;
	}

	public String getOrgType() {
		return orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getBussniessRegNo() {
		return bussniessRegNo;
	}

	public void setBussniessRegNo(String bussniessRegNo) {
		this.bussniessRegNo = bussniessRegNo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getFinanceEmail() {
		return financeEmail;
	}

	public void setFinanceEmail(String financeEmail) {
		this.financeEmail = financeEmail;
	}

	public String getSupportEamil() {
		return supportEamil;
	}

	public void setSupportEamil(String supportEamil) {
		this.supportEamil = supportEamil;
	}

	public String getOperationsEmail() {
		return operationsEmail;
	}

	public void setOperationsEmail(String operationsEmail) {
		this.operationsEmail = operationsEmail;
	}

	public Addres getAddress() {
		return address;
	}

	public void setAddress(Addres address) {
		this.address = address;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getOrgId() {
		return orgId;
	}

	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}

	public String getTimeZoneName() {
		return timeZoneName;
	}

	public void setTimeZoneName(String timeZoneName) {
		this.timeZoneName = timeZoneName;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	@Override
	public String toString() {
		return "OrganizationListResponse [orgName=" + orgName + ", bussniessRegNo=" + bussniessRegNo + ", description="
				+ description + ", category=" + category + ", timeZoneName=" + timeZoneName + ", currencyName="
				+ currencyName + ", groupName=" + groupName + ", orgId=" + orgId + ", mobileNo=" + mobileNo
				+ ", financeEmail=" + financeEmail + ", supportEamil=" + supportEamil + ", operationsEmail="
				+ operationsEmail + ", address=" + address + ", status=" + status + ", creationDate=" + creationDate
				+ ", orgType=" + orgType + ", partnerCode=" + partnerCode + ", complianceOfficer=" + complianceOfficer
				+ ", complianceEmail=" + complianceEmail + ", compliancePhone=" + compliancePhone
				+ ", regulatorCountryName=" + regulatorCountryName + ", partnerType=" + partnerType + "]";
	}

}

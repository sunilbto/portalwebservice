package com.mfs.pp.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class RoleDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7077310181450983989L;

	private int id;
	private int orgId;
	private String roleName;
	private String roleDescription;
	private String roleType;
	private Date createdOn;
	private String createdBy;
    private String email;
	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	private List<MenuActionDTO> menuActionDTOList;

	public List<MenuActionDTO> getMenuActionDTOList() {
		return menuActionDTOList;
	}

	public void setMenuActionDTOList(List<MenuActionDTO> menuActionDTOList) {
		this.menuActionDTOList = menuActionDTOList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public int getOrgId() {
		return orgId;
	}

	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}

	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "RoleDTO [id=" + id + ", orgId=" + orgId + ", roleName=" + roleName + ", roleDescription="
				+ roleDescription + ", roleType=" + roleType + ", createdOn=" + createdOn + ", createdBy=" + createdBy
				+ ", email=" + email + ", menuActionDTOList=" + menuActionDTOList + "]";
	}

}

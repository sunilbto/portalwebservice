package com.mfs.pp.dto;

import java.io.Serializable;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class UserResponse<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6281645488339949102L;

	private String responseCode;
	private String responseMessage;
	private OutputDTO<T> result;
	private Exception exception;
	private String token;
	int orgId;
	
	
	public int getOrgId() {
		return orgId;
	}

	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}

	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode
	 *            the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * @return the responseMessage
	 */
	public String getResponseMessage() {
		return responseMessage;
	}

	/**
	 * @param responseMessage
	 *            the responseMessage to set
	 */
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	/**
	 * @return the result
	 */
	public OutputDTO<T> getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(OutputDTO<T> result) {
		this.result = result;
	}

	/**
	 * @return the exception
	 */
	public Exception getException() {
		return exception;
	}

	/**
	 * @param exception the exception to set
	 */
	public void setException(Exception exception) {
		this.exception = exception;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		return "UserResponse [responseCode=" + responseCode + ", responseMessage=" + responseMessage + ", result="
				+ result + ", exception=" + exception + ", token=" + token + ", orgId=" + orgId + "]";
	}

	
	

}


package com.mfs.pp.dto;

import java.util.List;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class Zones {

	private String zoneName;
	private String description;
	private List<Countries> country;
	private int zoneId;

	public int getZoneId() {
		return zoneId;
	}

	public void setZoneId(int zoneId) {
		this.zoneId = zoneId;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	public List<Countries> getCountry() {
		return country;
	}

	public void setCountry(List<Countries> country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Zones [zoneName=" + zoneName + ", description=" + description + ", country=" + country + ", zoneId="
				+ zoneId + "]";
	}

}

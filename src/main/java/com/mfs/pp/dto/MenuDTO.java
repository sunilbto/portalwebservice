package com.mfs.pp.dto;

import java.io.Serializable;
import java.util.List;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class MenuDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7077310181450983989L;

	private String menuId;
	private String menuName;
	private List<MenuActionDTO> menuActionDTOList;

	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public List<MenuActionDTO> getMenuActionDTOList() {
		return menuActionDTOList;
	}
	public void setMenuActionDTOList(List<MenuActionDTO> menuActionDTOList) {
		this.menuActionDTOList = menuActionDTOList;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	
}

package com.mfs.pp.dto;

import java.util.List;

import com.mfs.mdm.common.dto.CountryDto;
import com.mfs.mdm.common.dto.CurrencyDto;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */

public class OrganizationResponseByName {

	private List<OrganizationTypes> orgTypeList;
	private List<CountryDto> countryList;
	private List<CurrencyDto> currencyDtos;
	private OrganizationListResponse org;
	private List<CategoryResponse> categoryList;
	private List<StateRegionResponse> stateList;
	private List<Zones> zonesList;
	private List<PartnerTypeDto> partnerTypeList;

	public List<PartnerTypeDto> getPartnerTypeList() {
		return partnerTypeList;
	}

	public void setPartnerTypeList(List<PartnerTypeDto> partnerTypeList) {
		this.partnerTypeList = partnerTypeList;
	}

	public List<Zones> getZonesList() {
		return zonesList;
	}

	public void setZonesList(List<Zones> zonesList) {
		this.zonesList = zonesList;
	}

	public List<OrganizationTypes> getOrgTypeList() {
		return orgTypeList;
	}

	public void setOrgTypeList(List<OrganizationTypes> orgTypeList) {
		this.orgTypeList = orgTypeList;
	}

	public List<CountryDto> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<CountryDto> countryList) {
		this.countryList = countryList;
	}

	public List<CurrencyDto> getCurrencyDtos() {
		return currencyDtos;
	}

	public void setCurrencyDtos(List<CurrencyDto> currencyDtos) {
		this.currencyDtos = currencyDtos;
	}

	public OrganizationListResponse getOrg() {
		return org;
	}

	public void setOrg(OrganizationListResponse org) {
		this.org = org;
	}

	public List<CategoryResponse> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<CategoryResponse> categoryList) {
		this.categoryList = categoryList;
	}

	public List<StateRegionResponse> getStateList() {
		return stateList;
	}

	public void setStateList(List<StateRegionResponse> stateList) {
		this.stateList = stateList;
	}

	@Override
	public String toString() {
		return "OrganizationResponseByName [orgTypeList=" + orgTypeList + ", countryList=" + countryList
				+ ", currencyDtos=" + currencyDtos + ", org=" + org + ", categoryList=" + categoryList + ", stateList="
				+ stateList + ", zonesList=" + zonesList + ", partnerTypeList=" + partnerTypeList + "]";
	}

}

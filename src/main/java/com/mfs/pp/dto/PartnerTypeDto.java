package com.mfs.pp.dto;

public class PartnerTypeDto {

	private int partnerTypeId;
	private String partnerType;

	public int getPartnerTypeId() {
		return partnerTypeId;
	}

	public void setPartnerTypeId(int partnerTypeId) {
		this.partnerTypeId = partnerTypeId;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	@Override
	public String toString() {
		return "PartnerTypeDto [partnerTypeId=" + partnerTypeId + ", partnerType=" + partnerType + "]";
	}

}

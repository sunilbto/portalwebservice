package com.mfs.pp.dto;

import java.util.List;

import com.mfs.mdm.common.dto.CountryDto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class ZonesResponse {

	private ZoneListResponse zoneListResponse;
	private List<CountryDto> countryList;

	public ZoneListResponse getZoneListResponse() {
		return zoneListResponse;
	}

	public void setZoneListResponse(ZoneListResponse zoneListResponse) {
		this.zoneListResponse = zoneListResponse;
	}

	public List<CountryDto> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<CountryDto> countryList) {
		this.countryList = countryList;
	}

	@Override
	public String toString() {
		return "ZonesResponse [zoneListResponse=" + zoneListResponse + ", countryList=" + countryList + "]";
	}

}

package com.mfs.pp.dto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class AlltransactionByIdRequestDto {

     private int transactionId;
	


	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	@Override
	public String toString() {
		return "AllTransactionRequestDto [transactionId=" + transactionId + "]";
	}
	
}

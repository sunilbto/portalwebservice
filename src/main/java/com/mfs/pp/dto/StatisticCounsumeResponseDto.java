package com.mfs.pp.dto;

import java.sql.Timestamp;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class StatisticCounsumeResponseDto {
 
	public Timestamp statisticDate;
	
	public double sendCount;
	
	public double receiveCount;
	
	public String direction;


	public Timestamp getStatisticDate() {
		return statisticDate;
	}

	public void setStatisticDate(Timestamp statisticDate) {
		this.statisticDate = statisticDate;
	}

	public double getSendCount() {
		return sendCount;
	}

	public void setSendCount(double sendCount) {
		this.sendCount = sendCount;
	}

	public double getReceiveCount() {
		return receiveCount;
	}

	public void setReceiveCount(double receiveCount) {
		this.receiveCount = receiveCount;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	@Override
	public String toString() {
		return "StatisticResponseDto [statisticDate=" + statisticDate + ", sendCount=" + sendCount + ", receiveCount="
				+ receiveCount + ", direction=" + direction + "]";
	}

	

	

	

	
	
	
	
	
}

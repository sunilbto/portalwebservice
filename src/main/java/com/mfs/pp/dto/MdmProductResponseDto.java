package com.mfs.pp.dto;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */


public class MdmProductResponseDto {
	
	private int mdmProductId;

	private String productName;

	public int getMdmProductId() {
		return mdmProductId;
	}

	public void setMdmProductId(int mdmProductId) {
		this.mdmProductId = mdmProductId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Override
	public String toString() {
		return "MdmProductResponseDto [mdmProductId=" + mdmProductId + ", productName=" + productName + "]";
	}
	
	

}

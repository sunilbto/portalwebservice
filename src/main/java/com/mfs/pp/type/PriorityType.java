package com.mfs.pp.type;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public enum PriorityType {
	
	/** The e. */
	E,
	CRITICAL,
	IMPORTANT
}

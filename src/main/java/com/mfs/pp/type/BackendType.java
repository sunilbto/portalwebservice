package com.mfs.pp.type;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public enum BackendType {
	SL;
	
	private String completeName;

	
	private BackendType(String completeName) {
		this.completeName = completeName;
	}

	
	private BackendType() {
		this.completeName = null;
	}
}
package com.mfs.pp.type;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public enum MFSErrorType {
	/** The startup. */
	// Fatal errors
	STARTUP(PriorityType.E, "MFS Service startup error"),
	/** The internal. */
	INTERNAL(PriorityType.E, "MFS Service Internal Error"),
	/** The backend sys. */
	BACKEND_SYS(PriorityType.E, "Backend system error"),
	// Non-Fatal errors

	/** The invalid input. */
	INVALID_INPUT(PriorityType.CRITICAL, "Invalid input"),
	/** The backend app. */
	BACKEND_APP(PriorityType.CRITICAL, "Backend application error"),
	
	// API specific exceptions
	// Self Registration
	INVALID_INPUT_FORMAT(PriorityType.CRITICAL, "Incorrect Password Format"),
	WRONG_RECAPTCHA_INPUT(PriorityType.CRITICAL, "Please submit correct reCaptcha input"),
	USER_ALREADY_EXISTS(PriorityType.CRITICAL, "User already exists"),
	EMAIL_NOT_TRIGGERED(PriorityType.CRITICAL, "Email has not been triggered"),
	
	// Email Verification
	EXPIRED_LINK(PriorityType.CRITICAL, "Verification link has expired"),
	
	// Login
	INACTIVE_USER(PriorityType.CRITICAL, "Account status does not allow login.User is Inactive"),
	EMAIL_STATUS(PriorityType.CRITICAL, "Email verified status is False."),
	PASSWORD_STATUS(PriorityType.CRITICAL, "Password status is False"),
	USER_DOES_NOT_EXIST(PriorityType.CRITICAL,"User does not exist"),
	INVALID_USERNAME_PASSWORD(PriorityType.CRITICAL,"Incorrect Username or Password"),
	
	//Forgot Password
	EMAIL_NOT_FOUND(PriorityType.CRITICAL, "Email does not exist"),
	ACCOUNT_STATUS(PriorityType.CRITICAL, "Email verified status is False"),
	USER_ID_STATUS(PriorityType.CRITICAL, "User ID status is False"),

	//Invite User
	INVALID_EMAIL_FORMAT(PriorityType.CRITICAL, "Invalid email input format"),
	
	// Accept Invitation
	//Invalid_Input_Format
	
	//Manage Profile
	//Invalid_Input
	
	//Change Password
	INVALID_CURRENT_PASSWORD(PriorityType.CRITICAL, "Invalid current password"),
	INVALID_NEW_PASSWORD_FORMAT(PriorityType.CRITICAL, "Invalid new password format"),
	PASSWORD_NOT_MATCHING(PriorityType.CRITICAL, "Invalid new and repeat password matching"),
	
	//Manage Org Profile
	//Invalid_Input
	
	//View API Key
	NO_API_KEY(PriorityType.IMPORTANT, "No merchant account has been created, thus no API key exists"),
	
	//Analytics
	NO_ANALYTICS_DATA(PriorityType.IMPORTANT, "No analytics data available"),
	
	
	
	//Transactions Report
	NO_TRANSACTION_DATA(PriorityType.IMPORTANT, "No transactions available");
	
	/** The severity. */
	private PriorityType priority;
	/** The description. */
	private String description;

	/**
	 * Construct the SLErrorType enum.
	 * 
	 * @param severity
	 *            type of severity
	 * @param description
	 *            description of service error
	 */
	private MFSErrorType(PriorityType severity, String description) {
		this.priority = severity;
		this.description = description;
	}

	/**
	 * getSeverity.
	 * 
	 * @return type of severity
	 */
	public PriorityType getPriority() {
		return priority;
	}

	/**
	 * getDescription.
	 * 
	 * @return description of service error
	 */
	public String getDescription() {
		return description;
	}
}

package com.mfs.pp.exception;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class GWCommonException extends BaseException {

    /**
     * 
     */
    private static final long serialVersionUID = -6758211952532650529L;

       public GWCommonException(Throwable e) {
           super(e);
       }

       public GWCommonException(String msg) {
           super(msg);
       }

       public GWCommonException(String msg, Throwable e) {
           super(msg, e);
       }
}

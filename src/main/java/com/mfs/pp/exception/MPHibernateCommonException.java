/**
 * 
 */
package com.mfs.pp.exception;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class MPHibernateCommonException extends BaseException {

    /**
     * 
     */
    private static final long serialVersionUID = -6758211952532650529L;

       public MPHibernateCommonException(Throwable e) {
           super(e);
       }

       public MPHibernateCommonException(String msg) {
           super(msg);
       }

       public MPHibernateCommonException(String msg, Throwable e) {
           super(msg, e);
       }
}

package com.mfs.pp.exception;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class MFSRuntimeException extends RuntimeException {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * constructor ServiceRuntimeException.
	 */
	public MFSRuntimeException() {
	}

	/**
	 * constructor ServiceRuntimeException.
	 * 
	 * @param message
	 *            String
	 */
	public MFSRuntimeException(String message) {
		super(message);
	}

	/**
	 * constructor ServiceRuntimeException.
	 * 
	 * @param cause
	 *            Throwable
	 */
	public MFSRuntimeException(Throwable cause) {
		super(cause);
	}

	/**
	 * constructor ServiceRuntimeException.
	 * 
	 * @param message
	 *            String
	 * @param cause
	 *            Throwable
	 */
	public MFSRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}
}
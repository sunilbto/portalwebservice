package com.mfs.pp.exception;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class MFSServiceException extends Exception {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The err. */
	private MFSErr err;
	
	/** The cause stack trace. */
	private String causeStackTrace;

	/**
	 * constructor ServiceException.
	 * 
	 * @param err
	 *            ServiceErr
	 */
	public MFSServiceException(MFSErr err) {
		this.err = err;
	}

	/**
	 * constructor ServiceException.
	 * 
	 * @param err
	 *            ServiceErr
	 * @param slMessage
	 *            String
	 */
	public MFSServiceException(MFSErr err, String slMessage) {
		super(slMessage);
		this.err = err;
	}

	/**
	 * constructor ServiceException.
	 * 
	 * @param err
	 *            ServiceErr
	 * @param slMessage
	 *            String
	 * @param cause
	 *            Throwable
	 */
	public MFSServiceException(MFSErr err, String slMessage, Throwable cause) {
		super(slMessage);
		this.err = err;
		this.causeStackTrace = getStackTrace(cause);
	}

	/**
	 * getErr.
	 * 
	 * @return ServiceErr
	 */
	public MFSErr getErr() {
		return err;
	}

	/**
	 * setErr.
	 * 
	 * @param err
	 *            ServiceErr
	 */
	public void setErr(MFSErr err) {
		this.err = err;
	}

	/**
	 * getStackTrace.
	 * 
	 * @param t
	 *            Throwable
	 * @return String
	 */
	private String getStackTrace(Throwable t) {
		if (t == null) {
			return null;
		}
		StringWriter sw = new StringWriter(2048);
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		pw.flush();
		pw.close();
		String st = sw.toString();
		if (st == null || st.length() == 0) {
			st = t.toString();
		}
		return st;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		StringBuilder sb = new StringBuilder(1024);
		sb.append(err.getMessage());
		MFSErr cause = err.getCause();
		if (cause != null) {
			sb.append(" (Caused by: ").append(cause.getMessage()).append(")");
		}
		String msg = super.getMessage();
		if (msg != null && msg.trim().length() > 0) {
			sb.append(":").append(msg);
		}
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#printStackTrace(java.io.PrintStream)
	 */
	@Override
	public void printStackTrace(PrintStream s) {
		super.printStackTrace(s);
		if (causeStackTrace != null) {
			s.print("Caused by: ");
			s.println(causeStackTrace);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#printStackTrace(java.io.PrintWriter)
	 */
	@Override
	public void printStackTrace(PrintWriter s) {
		super.printStackTrace(s);
		if (causeStackTrace != null) {
			s.print("Caused by: ");
			s.println(causeStackTrace);
		}
	}
}

/**
 * 
 */
package com.mfs.pp.exception;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class MPDuplicateDataException extends BaseException {

    /**
     * 
     */
    private static final long serialVersionUID = 7392368768755542840L;


    public MPDuplicateDataException(Throwable e) {
        super(e);
    }

    public MPDuplicateDataException(String msg) {
        super(msg);
    }

    public MPDuplicateDataException(String msg, Throwable e) {
        super(msg, e);
    }
}

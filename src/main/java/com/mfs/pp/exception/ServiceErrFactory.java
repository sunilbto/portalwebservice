package com.mfs.pp.exception;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.mfs.pp.type.BackendType;
import com.mfs.pp.type.MFSErrorType;
import com.mfs.pp.type.PriorityType;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */


@Component
public class ServiceErrFactory {
	
	/**
	 * createServiceErr.
	 * 
	 * @param mfsErrorType
	 *            service error type
	 * @return MFSErr appropriate service error
	 */
	public MFSErr createServiceErr(MFSErrorType mfsErrorType) {
		MFSErr serviceErr = new MFSErr(BackendType.SL.toString(), mfsErrorType.getPriority(), mfsErrorType.name(), mfsErrorType.getDescription());
		return serviceErr;
	}

	/**
	 * This method creates service error.
	 * 
	 * @param mfsErrorType
	 *            the sl error type
	 * @param description
	 *            description of error
	 * @return MFSErr service error
	 */
	public MFSErr createServiceErr(MFSErrorType mfsErrorType, String description) {
		MFSErr serviceErr = new MFSErr(BackendType.SL.toString(), mfsErrorType.getPriority(), mfsErrorType.name(),
				description != null ? description : mfsErrorType.getDescription());
		return serviceErr;
	}

	/**
	 * This method creates service error.
	 * 
	 * @param backend
	 *            backend type
	 * @param mfsErrorType
	 *            service error type
	 * @return MFSErr service error
	 */
	public MFSErr createServiceErr(BackendType backend, MFSErrorType mfsErrorType) {
		MFSErr serviceErr = new MFSErr(backend.toString(), mfsErrorType.getPriority(), mfsErrorType.name(), mfsErrorType.getDescription());
		return serviceErr;
	}

	/**
	 * This method creates service error.
	 * 
	 * @param topSlErrorType
	 *            service error type
	 * @param causeBackend
	 *            backend type
	 * @param causeSlErrorType
	 *            cause of service error type
	 * @return service error
	 */
	public MFSErr createServiceErr(MFSErrorType topSlErrorType, BackendType causeBackend, MFSErrorType causeSlErrorType) {
		return createServiceErr(topSlErrorType, createServiceErr(causeBackend, causeSlErrorType));
	}

	/**
	 * This method creates service error.
	 * 
	 * @param backend
	 *            BackendType
	 * @param severityType
	 *            service errror type
	 * @param errCode
	 *            error code
	 * @param errMessage
	 *            error message
	 * @return service error
	 */
	public MFSErr createServiceErr(BackendType backend, PriorityType severityType, String errCode, String errMessage) {
		MFSErr serviceErr = new MFSErr(backend.toString(), severityType, errCode, errMessage);
		return serviceErr;
	}
	
	/**
	 * This method creates service error.
	 * 
	 * @param backend
	 *            BackendType
	 * @param severityType
	 *            service errror type
	 * @param errCode
	 *            error code
	 * @param errMessage
	 *            error message
	 * @param errExtraInfoMap
	 *            error extra info
	 * @return service error
	 */
	public MFSErr createServiceErr(BackendType backend, PriorityType severityType, String errCode, String errMessage, Map<String, String> errExtraInfoMap) {
		MFSErr serviceErr = new MFSErr(backend.toString(), severityType, errCode, errMessage, errExtraInfoMap);
		return serviceErr;
	}

	/**
	 * This method creates service error.
	 * 
	 * @param backend
	 *            BackendType
	 * @param severityType
	 *            PriorityType
	 * @param errCode
	 *            error code
	 * @param errMessage
	 *            error message
	 * @return service error
	 */
	public MFSErr createServiceErr(String backend, PriorityType severityType, String errCode, String errMessage) {
		MFSErr serviceErr = new MFSErr(backend, severityType, errCode, errMessage);
		return serviceErr;
	}

	/**
	 * This method creates service error.
	 * 
	 * @param mfsErrorType
	 *            service error type
	 * @param cause
	 *            cauese fo error
	 * @return service error
	 */
	public MFSErr createServiceErr(MFSErrorType mfsErrorType, MFSErr cause) {
		MFSErr serviceErr = new MFSErr(BackendType.SL.toString(), mfsErrorType.getPriority(), mfsErrorType.name(),
				mfsErrorType.getDescription(), cause);
		return serviceErr;
	}

	/**
	 * This method creates service error.
	 * 
	 * @param mfsErrorType
	 *            service error type
	 * @param causeList
	 *            error cause list
	 * @return service error
	 */
	public MFSErr createServiceErr(MFSErrorType mfsErrorType, List<MFSErr> causeList) {
		MFSErr serviceErr = new MFSErr(BackendType.SL.toString(), mfsErrorType.getPriority(), mfsErrorType.name(),
				mfsErrorType.getDescription(), causeList);
		return serviceErr;
	}

	/**
	 * Added a method to create the object of MFSErr based on the MFSErrorType , BackendType and CauseList value.
	 * 
	 * @param mfsErrorType
	 *            service error type
	 * @param causeBackend
	 *            backend type
	 * @param causeList
	 *            error cause list
	 * @return service error
	 */
	public MFSErr createServiceErr(MFSErrorType mfsErrorType, BackendType causeBackend, List<MFSErr> causeList) {
		MFSErr serviceErr = new MFSErr(causeBackend.toString(), mfsErrorType.getPriority(), mfsErrorType.name(), mfsErrorType.getDescription(),
				causeList);
		return serviceErr;
	}

	/**
	 * This method create service error.
	 * 
	 * @param topSlErrorType
	 *            error type
	 * @param causeBackend
	 *            backend type
	 * @param causePriorityType
	 *            severity type
	 * @param causeErrCode
	 *            error cause code
	 * @param causeErrMessage
	 *            error cause message
	 * @return service error
	 */
	public MFSErr createServiceErr(MFSErrorType topSlErrorType, BackendType causeBackend, PriorityType causePriorityType, String causeErrCode,
			String causeErrMessage) {
		MFSErr cause = createServiceErr(causeBackend, causePriorityType, causeErrCode, causeErrMessage);
		return createServiceErr(topSlErrorType, cause);
	}
	
	/**
	 * This method create service error.
	 * 
	 * @param topSlErrorType
	 *            error type
	 * @param causeBackend
	 *            backend type
	 * @param causePriorityType
	 *            severity type
	 * @param causeErrCode
	 *            error cause code
	 * @param causeErrMessage
	 *            error cause message
	 * @param errExtraInfoMap
	 *            error extraInfo
	 * @return service error
	 */
	public MFSErr createServiceErr(MFSErrorType topSlErrorType, BackendType causeBackend, PriorityType causePriorityType, String causeErrCode,
			String causeErrMessage, Map<String, String> errExtraInfoMap) {
		MFSErr cause = createServiceErr(causeBackend, causePriorityType, causeErrCode, causeErrMessage, errExtraInfoMap);
		return createServiceErr(topSlErrorType, cause);
	}

	/**
	 * This method create service error.
	 * 
	 * @param causeBackend
	 *            backend type
	 * @param causePriorityType
	 *            severity type
	 * @param causeErrCode
	 *            error cause code
	 * @param causeErrMessage
	 *            error cause message
	 * @param causeList
	 *            error cause list
	 * @return service error
	 */
	public MFSErr createServiceErr(String causeBackend, PriorityType causePriorityType, String causeErrCode, String causeErrMessage,
			List<MFSErr> causeList) {
		MFSErr serviceErr = new MFSErr(causeBackend, causePriorityType, causeErrCode, causeErrMessage, causeList);
		return serviceErr;
	}

	/**
	 * This method create service error.
	 * 
	 * @param causeBackend
	 *            backend type
	 * @param causePriorityType
	 *            severity type
	 * @param causeErrCode
	 *            error cause code
	 * @param causeErrMessage
	 *            error cause message
	 * @param causeList
	 *            error cause list
	 * @return service error
	 */
	public MFSErr createServiceErr(BackendType causeBackend, PriorityType causePriorityType, String causeErrCode, String causeErrMessage,
			List<MFSErr> causeList) {
		MFSErr serviceErr = new MFSErr(causeBackend.toString(), causePriorityType, causeErrCode, causeErrMessage, causeList);
		return serviceErr;
	}

	/**
	 * createServiceErr.
	 * 
	 * @param mfsErrorType
	 *            service error type
	 * @param causeBackend
	 *            backend type
	 * @param cause
	 *            error cause
	 * @return service error
	 */
	public MFSErr createServiceErr(MFSErrorType mfsErrorType, BackendType causeBackend, MFSErr cause) {
		MFSErr serviceErr = new MFSErr(causeBackend.toString(), mfsErrorType.getPriority(), mfsErrorType.name(), mfsErrorType.getDescription(),
				cause);
		return serviceErr;
	}
}

package com.mfs.pp.exception;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.mfs.pp.type.PriorityType;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class MFSErr implements Serializable {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** The source. */
	private String source;
	/** The severity. */
	private PriorityType severity;
	/** The code. */
	private String code;
	/** The description. */
	private String description;
	/** The cause list. */
	private List<MFSErr> causeList;
	/** The extra info map. */
	private Map<String, String> extraInfoMap;

	/**
	 * Instantiates a new service err.
	 */
	public MFSErr() {
	}

	/**
	 * constructor ServiceErr.
	 * 
	 * @param source
	 *            String
	 * @param severity
	 *            SeverityType
	 * @param code
	 *            String
	 * @param description
	 *            String
	 */
	public MFSErr(String source, PriorityType severity, String code, String description) {
		this.source = source;
		this.severity = severity;
		this.code = code;
		this.description = description;
		this.causeList = new ArrayList<MFSErr>();
	}

	/**
	 * constructor ServiceErr.
	 * 
	 * @param source
	 *            String
	 * @param severity
	 *            SeverityType
	 * @param code
	 *            String
	 * @param description
	 *            String
	 * @param extraInfoMap
	 *            Map
	 */
	public MFSErr(String source, PriorityType severity, String code, String description, Map extraInfoMap) {
		this.source = source;
		this.severity = severity;
		this.code = code;
		this.description = description;
		this.causeList = new ArrayList<MFSErr>();
		this.extraInfoMap = extraInfoMap;
	}

	/**
	 * constructor ServiceErr.
	 * 
	 * @param source
	 *            String
	 * @param severity
	 *            SeverityType
	 * @param code
	 *            String
	 * @param description
	 *            String
	 * @param cause
	 *            ServiceErr
	 */
	public MFSErr(String source, PriorityType severity, String code, String description, MFSErr cause) {
		this(source, severity, code, description);
		this.causeList.add(cause);
	}

	/**
	 * constructor ServiceErr.
	 * 
	 * @param source
	 *            source
	 * @param severity
	 *            SeverityType
	 * @param code
	 *            source
	 * @param description
	 *            source
	 * @param causeList
	 *            List<ServiceErr>
	 */
	public MFSErr(String source, PriorityType severity, String code, String description, List<MFSErr> causeList) {
		this(source, severity, code, description);
		this.causeList = causeList;
	}

	/**
	 * getSeverity.
	 * 
	 * @return SeverityType
	 */
	public PriorityType getSeverity() {
		return severity;
	}

	/**
	 * getSource String.
	 * 
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * getCode.
	 * 
	 * @return String
	 */
	public String getCode() {
		return code;
	}

	
	/**
	 * getErrorCode.
	 * 
	 * @return String
	 */
	public String getErrorCode() {
		return source + severity.toString() + "_" + code;
	}

	/**
	 * getDescription.
	 * 
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * getCause.
	 * 
	 * @return ServiceErr
	 */
	public MFSErr getCause() {
		MFSErr serviceErr = null;
		if ((causeList != null) && (causeList.size() > 0)) {
			return causeList.get(0);
		}
		return serviceErr;
	}

	/**
	 * getCauseList.
	 * 
	 * @return List<ServiceErr>
	 */
	public List<MFSErr> getCauseList() {
		if ((causeList != null) && (causeList.size() > 0)) {
			return causeList;
		} else {
			return new ArrayList<MFSErr>();
		}
	}

	/**
	 * getMessage.
	 * 
	 * @return String
	 */
	public String getMessage() {
		return source + severity.toString() + "_" + code + ":" + description;
	}

	/**
	 * Gets the extra info map.
	 * 
	 * @return the extraInfoMap
	 */
	public Map<String, String> getExtraInfoMap() {
		return extraInfoMap;
	}

	/**
	 * Sets the extra info map.
	 * 
	 * @param extraInfoMap
	 *            the extraInfoMap to set
	 */
	public void setExtraInfoMap(Map<String, String> extraInfoMap) {
		this.extraInfoMap = extraInfoMap;
	}

	/**
	 * Sets the source.
	 * 
	 * @param source
	 *            the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * Sets the severity.
	 * 
	 * @param severity
	 *            the severity to set
	 */
	public void setSeverity(PriorityType severity) {
		this.severity = severity;
	}

	/**
	 * Sets the code.
	 * 
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets the cause list.
	 * 
	 * @param causeList
	 *            the causeList to set
	 */
	public void setCauseList(List<MFSErr> causeList) {
		this.causeList = causeList;
	}
}


package com.mfs.pp.exception;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class MPDataNotFoundException extends BaseException{

    /**
     * 
     */
    private static final long serialVersionUID = -7870816789799229910L;

    public MPDataNotFoundException(Throwable e) {
        super(e);
    }

    public MPDataNotFoundException(String msg) {
        super(msg);
    }

    public MPDataNotFoundException(String msg, Throwable e) {
        super(msg, e);
    }
}

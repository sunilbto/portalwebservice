/**
 * 
 */
package com.mfs.pp.exception;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class BaseException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -68785173490703156L;

    public BaseException() {
        super();
    }

    public BaseException(Throwable thwx) {
        super(thwx);
    }

    public BaseException(String msg) {
        super(msg);
    }

    public BaseException(String msg, Throwable thx) {
        super(msg, thx);
    }
    
}

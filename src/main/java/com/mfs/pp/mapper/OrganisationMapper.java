/**
 * 
 */
package com.mfs.pp.mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.common.dto.AddressDto;
import com.mfs.mdm.common.dto.CountryDto;
import com.mfs.mdm.common.dto.CurrencyDto;
import com.mfs.mdm.common.dto.OrganisationProfileDto;
import com.mfs.mdm.common.dto.RolesDto;
import com.mfs.mdm.common.dto.UserDto;
import com.mfs.mdm.common.type.StatusType;
import com.mfs.mdm.model.Address;
import com.mfs.mdm.model.Category;
import com.mfs.mdm.model.CountryZone;
import com.mfs.mdm.model.CurrencyMaster;
import com.mfs.mdm.model.Organization;
import com.mfs.mdm.model.OrganizationRole;
import com.mfs.mdm.model.OrganizationUser;
import com.mfs.mdm.model.OrganizationUserRole;
import com.mfs.mdm.model.User;
import com.mfs.pp.dao.CountryZoneDao;
import com.mfs.pp.dao.OrganisationDao;
import com.mfs.pp.dao.RoleDao;
import com.mfs.pp.dao.UserDao;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.exception.MPDuplicateDataException;
import com.mfs.pp.exception.MPHibernateCommonException;
import com.mfs.pp.service.UserService;
import com.mfs.pp.service.impl.CustomUserDetailsService;
import com.mfs.pp.util.FieldValidator;
import com.mfs.pp.util.Format;
import com.mfs.pp.util.RandomGeneratorUtil;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Component
public class OrganisationMapper {

	//private static final Logger log = LoggerFactory.getLogger(OrganisationMapper.class);

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private OrganisationDao organisationDao;

	@Autowired
	BCryptPasswordEncoder bcryptPasswordEncoder;

	@Autowired
	UserDao userDao;

	@Autowired
	CustomUserDetailsService customUserDetailsService;

	@Autowired
	UserService userService;

	@Autowired
	FieldValidator fieldValidator;

	@Autowired
	CountryZoneDao countryZoneDao;
	
	@Autowired
	RoleDao roleDao;

	//validate organization profile 
	public void validateOrganisationProfileDtoRequest(OrganisationProfileDto organisationProfileDto) throws Exception {
		if (fieldValidator.validateFieldForNotNull("organisationProfileDto", organisationProfileDto)) {
			fieldValidator.validateFieldForNotNull("organisationProfileDto.organisationName",
					organisationProfileDto.getOrganisationName());
		}
		fieldValidator.throwValidationExceptionIfRequired();
	}

	/**
	 * @param OrganizationUser
	 * get user from organization
	 * @return UserDto
	 */
	public UserDto getUserDtoFromOrganisationalUserModel(OrganizationUser organizationUser) throws Exception {
		UserDto userDTO = null;
		List<RolesDto> rulesDtoList = null;
		if (Format.isObjectNotEmptyAndNotNull(organizationUser)) {
			User user = organizationUser.getUser();
			if (Format.isObjectNotEmptyAndNotNull(user)) {
				userDTO = userMapper.getUserDtoFromUserModel(user);
			}
			if (Format.isCollectionNotEmptyAndNotNull(organizationUser.getOrganizationUserRoles())) {
				rulesDtoList = getRoleDtoListFromOrganizationUserRolesModel(
						organizationUser.getOrganizationUserRoles());
				if (Format.isCollectionNotEmptyAndNotNull(rulesDtoList)) {
					userDTO.setRoles(rulesDtoList.get(0));
				}
			}
		}
		return userDTO;

	}

	/**
	 * @param organizationUserModelList
	 * get all user from organization
	 * @return List<UserDto>
	 */
	public List<UserDto> getAllUserDtoListFromOrganizationUserModelList(
			List<OrganizationUser> organizationUserModelList) throws Exception {
		List<UserDto> userDtoList = new ArrayList<UserDto>();
		if (Format.isCollectionNotEmptyAndNotNull(organizationUserModelList)) {
			organizationUserModelList.forEach((organizationUserModel) -> {
				try {
					userDtoList.add(getUserDtoFromOrganisationalUserModel(organizationUserModel));
				} catch (Exception e) {
					throw new CustomException("Exception in Backend");
				}
			});
		}
		return userDtoList;
	}

	/**
	 * @param organizationRole
	 * get Role From Organization Role Model
	 * @return RolesDto
	 */
	public RolesDto getRoleDtoFromOrganizationRoleModel(OrganizationRole organizationRole) {
		RolesDto rolesDto = null;
		if (Format.isObjectNotEmptyAndNotNull(organizationRole)) {
			rolesDto = new RolesDto();

			if (Format.isStringNotEmptyAndNotNull(organizationRole.getRoleName())) {

				rolesDto.setRoleName(organizationRole.getRoleName());
			}
			rolesDto.setRoleDescription(organizationRole.getRoleDescription());
			rolesDto.setCreatedBy(organizationRole.getCreatedBy());
			rolesDto.setRoleCreateDate(organizationRole.getCreationDate());
		}
		return rolesDto;
	}

	/**
	 * @param organizationUserRole
	 * get Role  From Organization User Role Model
	 * @return RolesDto
	 */

	public RolesDto getRoleDtoFromOrganizationUseRoleModel(OrganizationUserRole organizationUserRole) {
		RolesDto rolesDto = null;
		OrganizationRole organizationRole = null;
		if (Format.isObjectNotEmptyAndNotNull(organizationUserRole)) {
			rolesDto = new RolesDto();
			organizationRole = organizationUserRole.getOrganizationRole();
			if (Format.isObjectNotEmptyAndNotNull(organizationRole)) {
				rolesDto = getRoleDtoFromOrganizationRoleModel(organizationRole);
			}
		}
		return rolesDto;
	}

	/**
	 * @param List <OrganizationUserRole>
	 * get Role List From Organization User Roles Model
	 * @return List<RolesDto>
	 */
	public List<RolesDto> getRoleDtoListFromOrganizationUserRolesModel(Set<OrganizationUserRole> set) {
		List<RolesDto> ruleDtoList = null;
		RolesDto rolesDto = null;
		if (Format.isCollectionNotEmptyAndNotNull(set)) {
			ruleDtoList = new ArrayList<RolesDto>();
			for (OrganizationUserRole organizationUserRole : set) {

				if (Format.isObjectNotEmptyAndNotNull(organizationUserRole)) {
					rolesDto = getRoleDtoFromOrganizationUseRoleModel(organizationUserRole);
					ruleDtoList.add(rolesDto);
				}
			}
		}
		return ruleDtoList;

	}

	/**
	 * @param List <OrganizationRole>
	 * get Role List From Organization Roles Model
	 * @return List<RolesDto>
	 */
	public List<RolesDto> getRoleDtoListFromOrganizationRolesModel(List<OrganizationRole> organizationRoleList) {
		List<RolesDto> ruleDtoList = null;
		RolesDto rolesDto = null;
		if (Format.isCollectionNotEmptyAndNotNull(organizationRoleList)) {
			ruleDtoList = new ArrayList<RolesDto>();
			for (OrganizationRole organizationRole : organizationRoleList) {

				if (Format.isObjectNotEmptyAndNotNull(organizationRole)) {
					rolesDto = getRoleDtoFromOrganizationRoleModel(organizationRole);
					ruleDtoList.add(rolesDto);
				}
			}
		}
		return ruleDtoList;

	}

	//prepare Organisation User Model From User 
	public HashSet<OrganizationUser> prepareOrganisationUserModelFromUserDto(UserDto userDTO, User userInfo)
			throws Exception {
		HashSet<OrganizationUser> organizationUsers = new HashSet<OrganizationUser>();
		OrganizationUser organizationUser = prepareOrganizationUserInfo(userDTO, userInfo);
		organizationUsers.add(organizationUser);
		return organizationUsers;
	}

	//prepare Organization User Info
	@Transactional(rollbackFor = Exception.class)
	public OrganizationUser prepareOrganizationUserInfo(UserDto userDTO, User userInfo) throws Exception {
		OrganizationUser organizationUserInfo = null;
		Organization organization = null;
		Set<OrganizationUserRole> organizationUserRoles = null;

		if (null != userInfo) {
			organizationUserInfo = organisationDao.getOrganizationUserByUserEmail(userInfo.getEmail());
			if (null != organizationUserInfo) {
				if (userDTO.isPasswordStatus()) {
					if (Format.isStringEmptyOrNull(organizationUserInfo.getPassword())) {
						organizationUserInfo.setIsPasswordStatus(false);
					}
				}
			}
		}
		if (null == organizationUserInfo) {
			organizationUserInfo = new OrganizationUser();
			if (!userDTO.isPasswordStatus() && null != userDTO.getPassword()) {
				organizationUserInfo.setPassword(bcryptPasswordEncoder.encode(userDTO.getPassword()));
				organizationUserInfo.setIsPasswordStatus(true);
			}
		}
		organizationUserInfo.setIsPasswordStatus(userDTO.isPasswordStatus());
		if (Format.isStringNotEmptyAndNotNull(userDTO.getResetPassword())) {
			organizationUserInfo.setResetPassword(userDTO.getResetPassword());
		}
		if (Format.isDateNotEmtyAndNotNull(userDTO.getResetPasswordCreateDate())) {
			organizationUserInfo.setResetPasswordCreatedOn(userDTO.getResetPasswordCreateDate());
		}
		if (Format.isNotNull(userDTO.isAccountLock())) {
			organizationUserInfo.setIsAccountLocked(userDTO.isAccountLock());
		}
		if (null != userDTO.getOrgProfileDto()) {
			Organization organizationTemp = organisationDao
					.getOrganizationByOrgName(userDTO.getOrgProfileDto().getOrganisationName());

			organization = organisationDao.getOrganizationByOrgName(userDTO.getOrgProfileDto().getOrganisationName());
			if (null == organization) {
				organization = prepareOrganizationModelFromOrgProfileDto(userDTO.getOrgProfileDto());
			}
			if (null == organizationTemp && null == organization.getOrgApiKey()) {
				organization.setOrgApiKey("ApiKey");
			}

			// }
			organizationUserInfo.setOrganization(organization);
		}

		organizationUserInfo.setUser(userInfo);
		boolean organizationUserFlag = organisationDao.saveOrUpdate(organizationUserInfo);
		if (organizationUserFlag) {
			organizationUserRoles = prepareOrganizationUserRoles(userDTO, organizationUserInfo);
			organizationUserInfo.setOrganizationUserRoles(organizationUserRoles);
		} else {
			throw new CustomException("Exception while saving OrganizationUser");
		}
		return organizationUserInfo;
	}

	/**
	 * Methoda to create and update Organization
	 * 
	 * @param orgProfileDto
	 * @return organization
	 */
	public Organization prepareOrganizationModelFromOrgProfileDto(OrganisationProfileDto orgProfileDto)
			throws Exception {
		// TODO need to rewrite this method
		Organization organization = null;
		Category category = null;
		CurrencyMaster currencyMaster = null;
		String statusType = null;

		if (null != orgProfileDto) {
			organization = organisationDao.getOrganizationByOrgName(orgProfileDto.getOrganisationName());
			if (null == organization) {
				organization = new Organization();
				organization.setOrgName(orgProfileDto.getOrganisationName());
				organization.setOrgId("O" + RandomGeneratorUtil.getNextRandomId());
				organization.setRegDate(new Date());
				if (null != orgProfileDto.getStatus()) {
					statusType = orgProfileDto.getStatus();
				} else {
					statusType = StatusType.PENDING.toString();
				}
				organization.setStatus(statusType);
			} else {
				organization.setStatus(orgProfileDto.getStatus().toString());
			}
			organization.setDescription(orgProfileDto.getDescription());
			organization.setBusinessRegNo(orgProfileDto.getBusinessRegistrationNumber());
			organization.setEmail(orgProfileDto.getEmail());

			if (null != orgProfileDto.getCategory()) {
				category = organisationDao.getOrganizationCategoryByCategoryType(orgProfileDto.getCategory());
			}
			organization.setCategories(category);
			if (null != orgProfileDto.getMobileNumber()) {
				organization.setMobileNumber(orgProfileDto.getMobileNumber());
			}

			if (null != orgProfileDto.getCurrencyDto()
					&& Format.isStringNotEmptyAndNotNull(orgProfileDto.getCurrencyDto().getCurrencyName())) {
				currencyMaster = organisationDao
						.getCurrencyMasterByCurrencyName(orgProfileDto.getCurrencyDto().getCurrencyName());
				organization.setCurrencyMaster(currencyMaster);
			}

			boolean organizationUpdated = organisationDao.saveOrUpdate(organization);

			if (!organizationUpdated) {
				throw new CustomException("Organization Profile updation failed.");
			}
		}
		return null;
	}

	/**
	 * @param userDTO
	 * @param organizationUserInfo
	 * prepare Organization User Roles
	 * @return
	 */
	private Set<OrganizationUserRole> prepareOrganizationUserRoles(UserDto userDTO,
			OrganizationUser organizationUserInfo) {
		Set<OrganizationUserRole> organizationUserRoles = null;
		OrganizationUserRole organizationUserRole = null;
		List<OrganizationUserRole> organizationUserRolesList = organisationDao
				.getOrganizationUserRolesListByOrganizationUserId(organizationUserInfo.getOrganisationUserId());
		if (Format.isCollectionNotEmptyAndNotNull(organizationUserRolesList)) {
			organizationUserRoles = new HashSet<>(organizationUserRolesList);
		} else {
			organizationUserRoles = new HashSet<OrganizationUserRole>();
		}
		organizationUserRole = organisationDao
				.getOrganizationUserRoleByOrgUserId(organizationUserInfo.getOrganisationUserId());
		if (null == organizationUserRole) {
			organizationUserRole = new OrganizationUserRole();
		}

		OrganizationRole organizationRole = null;
		try {
			if (Format.isObjectNotEmptyAndNotNull(userDTO.getRoles())
					&& Format.isObjectNotEmptyAndNotNull(userDTO.getRoles().getRoleId())) {
				organizationRole = roleDao.getOrgRoleById(userDTO.getRoles().getRoleId());
			}
			organizationUserRole.setOrganizationRole(organizationRole);
			organizationUserRole.setIsRoleActive(true);
			organizationUserRole.setOrganizationUser(organizationUserInfo);
			boolean organizationUserRoleFlag = organisationDao.saveOrUpdate(organizationUserRole);
			organizationUserRoles.add(organizationUserRole);

		} catch (MPDuplicateDataException e) {
			e.printStackTrace();
		} catch (MPHibernateCommonException e) {
			e.printStackTrace();
		}
		return organizationUserRoles;
	}

	/**
	 * @param currencyMaster
	 * get Currency From Currency Model
	 * @return
	 */
	public CurrencyDto getCurrencyDtoFromCurrencyModel(CurrencyMaster currencyMaster) {
		CurrencyDto currencyDto = null;

		if (Format.isObjectNotEmptyAndNotNull(currencyMaster)) {
			currencyDto = new CurrencyDto();
			currencyDto.setCurrencyCode(currencyMaster.getCurrencyCode());
			currencyDto.setCurrencyName(currencyMaster.getCurrencyName());
		}
		return currencyDto;
	}

	/**
	 * @param currencyMastersList
	 * prepare Curruncy List From Model List
	 * @return
	 */
	public List<CurrencyDto> prepareCurruncyDtoListFromModelList(List<CurrencyMaster> currencyMastersList) {
		List<CurrencyDto> currencyDtoList = null;
		if (Format.isCollectionNotEmptyAndNotNull(currencyMastersList)) {
			currencyDtoList = new ArrayList<CurrencyDto>();

			for (CurrencyMaster currencyMaster : currencyMastersList) {
				currencyDtoList.add(getCurrencyDtoFromCurrencyModel(currencyMaster));
			}
		}
		return currencyDtoList;

	}

	//get Organisation Profile From Model
	public OrganisationProfileDto getOrganisationProfileDtoFromModel(Organization organization) throws Exception {
		OrganisationProfileDto organisationProfileDto = null;
		CountryDto countries = null;
		List<CountryDto> counList = new ArrayList<CountryDto>();

		if (Format.isObjectNotEmptyAndNotNull(organization)) {
			organisationProfileDto = new OrganisationProfileDto();
			organisationProfileDto.setOrganisationID(organization.getOrganisationId()+"");
			organisationProfileDto.setOrganisationName(organization.getOrgName());
			organisationProfileDto.setOrgApiKey(organization.getOrgApiKey());
			organisationProfileDto.setOrganisationType(organization.getOrganizationType().getOrgType());
			organisationProfileDto.setBusinessRegistrationNumber(organization.getBusinessRegNo());
			if(organization.getOrgId()!=null)
			{
			organisationProfileDto.setPartnerCode(organization.getOrgId());
			}
			if (organization.getOrganizationType().getOrgType().equals("Regulator")) {
				if (organization.getZone()!=null) {
					organisationProfileDto.setTypeUser("zoneUser");
					organisationProfileDto.setZoneName(organization.getZone().getZoneName());
					List<CountryZone> countryZoneList = countryZoneDao
							.getCountryByzone(organization.getZone().getZoneId());
					if (!(countryZoneList.isEmpty())) {
						for (CountryZone countryZone : countryZoneList) {
							countries = new CountryDto();
							countries.setCountryName(countryZone.getCountryMaster().getCountryName());
							counList.add(countries);

						}

					}

				}
				else
				{
				
				organisationProfileDto.setTypeUser("CountryUser");
				countries = new CountryDto();
				countries.setCountryName(organization.getRegulatorCountryId().getCountryName());
				counList.add(countries);
				}

			}
			organisationProfileDto.setCountryList(counList);
			organisationProfileDto.setEmail(organization.getEmail());
			if (organization.getMobileNumber() != null) {
				organisationProfileDto.setMobileNumber(organization.getMobileNumber());
			}

			if (Format.isObjectNotEmptyAndNotNull(organization.getCurrencyMaster())) {
				organisationProfileDto
						.setCurrencyDto(getCurrencyDtoFromCurrencyModel(organization.getCurrencyMaster()));
			}

			if (null != organization.getStatus()) {
				organisationProfileDto.setStatus(organization.getStatus());
			}
		}
		return organisationProfileDto;

	}

	//get Address List From Address Model List
	public List<AddressDto> getAddressDtoListFromAddressModelList(Set<Address> addressInfoSet) {
		List<AddressDto> addressDtoList = null;
		if (Format.isCollectionNotEmptyAndNotNull(addressInfoSet)) {
			addressDtoList = new ArrayList<AddressDto>();
			/*
			 * addressList.forEach(address->{
			 * addressDtoList.add(getAddressDtoFromModel(address)); });
			 */
			for (Address address : addressInfoSet) {
				addressDtoList.add(getAddressDtoFromModel(address));
			}
		}
		return addressDtoList;

	}
	//get Address From Model

	public AddressDto getAddressDtoFromModel(Address address) {
		AddressDto addressDto = null;
		if (Format.isObjectNotEmptyAndNotNull(address)) {
			addressDto = new AddressDto();
			addressDto.setAddress(address.getAddress());
			addressDto.setAddress1(address.getAddress1());
			addressDto.setAddress2(address.getAddress2());
			if (Format.isObjectNotEmptyAndNotNull(address.getCountryMaster()))
				addressDto.setCountry(userMapper.getCountryDtoFromCountryModel(address.getCountryMaster()));
			addressDto.setCity((address.getCity()));
			addressDto.setPostalCode(address.getPostalCode());
			addressDto.setStateRegion(address.getStateRegion());
		}
		return addressDto;

	}

	//prepare Address Model List From Dto List
	public List<Address> prepareAddressModelListFromDtoList(String orgName) {
		List<Address> addressInfoList = null;
		if (Format.isStringNotEmptyAndNotNull(orgName)) {
			addressInfoList = organisationDao.getAddressSetByOrganisationName(orgName);
		}

		return addressInfoList;
	}

	//get OrgProfile List From Model List
	public List<OrganisationProfileDto> getOrgProfileListFromModelList(List<Organization> orgInfoList)
			throws Exception {
		List<OrganisationProfileDto> orgDtoList = null;
		OrganisationProfileDto organisationProfileDto = null;
		if (Format.isCollectionNotEmptyAndNotNull(orgInfoList)) {
			orgDtoList = new ArrayList<OrganisationProfileDto>();
			for (Organization organisation : orgInfoList) {
				organisationProfileDto = getOrganisationProfileDtoFromModel(organisation);
				orgDtoList.add(organisationProfileDto);
			}
		}
		return orgDtoList;
	}
}

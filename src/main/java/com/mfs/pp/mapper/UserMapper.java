package com.mfs.pp.mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.common.dto.AddressDto;
import com.mfs.mdm.common.dto.CountryDto;
import com.mfs.mdm.common.dto.IdDto;
import com.mfs.mdm.common.dto.OrganisationProfileDto;
import com.mfs.mdm.common.dto.PasswordDto;
import com.mfs.mdm.common.dto.ProfileDto;
import com.mfs.mdm.common.dto.RolesDto;
import com.mfs.mdm.common.dto.TimeZoneDto;
import com.mfs.mdm.common.dto.UserDto;
import com.mfs.mdm.common.type.StatusType;
import com.mfs.mdm.model.Address;
import com.mfs.mdm.model.AdminRole;
import com.mfs.mdm.model.AdminUser;
import com.mfs.mdm.model.AdminUserRole;
import com.mfs.mdm.model.CountryMaster;
import com.mfs.mdm.model.IdType;
import com.mfs.mdm.model.OrganizationRole;
import com.mfs.mdm.model.OrganizationUser;
import com.mfs.mdm.model.OrganizationUserRole;
import com.mfs.mdm.model.Status;
import com.mfs.mdm.model.TimeZone;
import com.mfs.mdm.model.User;
import com.mfs.pp.dao.CommonDao;
import com.mfs.pp.dao.CountryZoneDao;
import com.mfs.pp.dao.OrganisationDao;
import com.mfs.pp.dao.UserDao;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.util.Format;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */

@Component
public class UserMapper {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserMapper.class);

	@Autowired
	UserDao userDao;

	@Autowired
	OrganisationDao organisationDao;

	@Autowired
	OrganisationMapper organisationMapper;

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	CommonDao commonDao;

	@Autowired
	CountryZoneDao countryZoneDao;

	// prepare User Model From User
	@Transactional
	public User prepareUserModelFromUserDto(UserDto userDTO) throws Exception {
		User userInfo = null;

		Date date = null;
		TimeZone timeZoneMaster = null;
		try {
			if (Format.isObjectNotEmptyAndNotNull(userDTO)) {
				userInfo = new User();
				date = new Date();
				ProfileDto profileDto = userDTO.getProfileDto();
				if (Format.isObjectNotEmptyAndNotNull(profileDto)) {
					userInfo.setFirstName(profileDto.getFirstName());
					userInfo.setMobile(profileDto.getMobileNumber());
					userInfo.setLastName(profileDto.getLastName());
					userInfo.setEmail(profileDto.getEmail());
					userInfo.setStatus("pending");
					userInfo.setDob(profileDto.getDateOfBirth());

					userInfo.setIsUserActive(true);
					userInfo.setIsEmailVerified(true);
					IdDto idDto = profileDto.getIdDto();
					if (Format.isObjectNotEmptyAndNotNull(idDto)) {
						userInfo.setIdNumber(idDto.getIdNumber());
						userInfo.setIdExpiryDate(idDto.getIdExpiryDate());

					}

					if (Format.isObjectNotEmptyAndNotNull(profileDto.getTimeZoneDto())) {
						timeZoneMaster = userDao
								.getTimeZoneMasterByTimeZoneName(profileDto.getTimeZoneDto().getTimeZoneName());
					}
					userInfo.setTimeZone(timeZoneMaster);
				}
				// userInfo.setIsUserActive(userDTO.isActiveUser());
				userInfo.setFirstActivationDate(new Date());
			}
		} catch (Exception e) {
			throw new CustomException(e.getMessage());
		}
		return userInfo;
	}

	/**
	 * @param userDTO
	 * @param userInfo
	 *            prepare For Update User Model From User
	 * @return User
	 */
	public User prepareForUpdateUserModelFromUserDto(UserDto userDTO, User userInfo) {

		Date date = null;
		TimeZone timeZoneMaster = null;
		CountryMaster countryMaster = null;
		IdType idType = null;
		if (Format.isObjectNotEmptyAndNotNull(userDTO)) {
			date = new Date();
			ProfileDto profileDto = userDTO.getProfileDto();
			if (Format.isObjectNotEmptyAndNotNull(profileDto)) {
				userInfo.setFirstName(profileDto.getFirstName());
				userInfo.setLastName(profileDto.getLastName());
				userInfo.setIsEmailVerified(true);
				userInfo.setMobile(profileDto.getMobileNumber());
				userInfo.setFirstActivationDate(new Date());
				userInfo.setDob(profileDto.getDateOfBirth());
				userInfo.setStatus(userInfo.getStatus());
				// check user
				if (userDTO.getProfileDto() != null) {
					if (userDTO.getProfileDto().getCountryCode() != null) {
						countryMaster = countryZoneDao.getCountryDetails(userDTO.getProfileDto().getCountryCode());
						userInfo.setCountryId(countryMaster);
					}

					else if (userDTO.getProfileDto().getAddressDtoList()!=null) {
						if ((userDTO.getProfileDto().getAddressDtoList().get(0).getCountry()
								.getCountryName() != null)) {
							countryMaster = countryZoneDao.getCountryDetails(
									userDTO.getProfileDto().getAddressDtoList().get(0).getCountry().getCountryName());
							userInfo.setCountryId(countryMaster);
						}
					}

				}
				// userInfo.setMobile(userDTO.getProfileDto().getMobileNumber());
				IdDto idDto = profileDto.getIdDto();
				if (Format.isObjectNotEmptyAndNotNull(idDto)) {
					idType = commonDao.getIdType(idDto.getIdType());
					if (idType != null) {
						userInfo.setIdType(idType);
					}
					userInfo.setIdNumber(idDto.getIdNumber());
					userInfo.setIdExpiryDate(idDto.getIdExpiryDate());
				}
				if (null != profileDto.getTimeZoneDto()) {

					timeZoneMaster = userDao.getTimeZoneMasterByTimeZoneName(
							userDTO.getProfileDto().getTimeZoneDto().getTimeZoneName());
				}
				userInfo.setTimeZone(timeZoneMaster);
			}
			// userInfo.setIsUserActive(userDTO.isActiveUser());

		}
		return userInfo;

	}

	/**
	 * @param addressDtoList
	 * @param emailOrOrgName
	 *            get Address Info Set From Address List
	 * @return
	 */
	public Set<Address> getAddressInfoSetFromAddressDtoList(List<AddressDto> addressDtoList, String emailOrOrgName,
			boolean isOrgOrUser) {
		Set<Address> addressInfoSet = new HashSet<Address>();
		addressDtoList.forEach((address) -> {
			addressInfoSet.add(prepareAddressInfoFromAddressDto(address, emailOrOrgName, isOrgOrUser));
		});
		return addressInfoSet;
	}

	/**
	 * @param addressDto
	 *            prepare Address Info From Address Dto
	 * @return Address
	 */
	@Transactional
	private Address prepareAddressInfoFromAddressDto(AddressDto addressDto, String emailOrOrgName,
			boolean isOrgOrUser) {
		Address addressInfo = null;
		// check organiztion or user
		if (!isOrgOrUser) {
			// If it is an address of User

			addressInfo = userDao.getAddressByUser(emailOrOrgName);
		} else {
			// If it is an address of Organization
			if (Format.isCollectionNotEmptyAndNotNull(organisationDao.getAddressSetByOrganisationName(emailOrOrgName))
					&& null != organisationDao.getAddressSetByOrganisationName(emailOrOrgName).stream()) {
				addressInfo = organisationDao.getAddressSetByOrganisationName(emailOrOrgName).stream().findFirst()
						.get();
			}
		}
		// check address
		if (null == addressInfo) {
			addressInfo = new Address();
		}
		addressInfo.setAddress(addressDto.getAddress());
		addressInfo.setAddress1(addressDto.getAddress1());
		addressInfo.setAddress2(addressDto.getAddress2());
		if (null != addressDto.getPostalCode()) {
			addressInfo.setPostalCode(addressDto.getPostalCode());
		}
		addressInfo.setCity((addressDto.getCity()));
		addressInfo.setStateRegion(addressDto.getStateRegion());
		CountryMaster countryMaster = null;
		if (null != addressDto.getCountry()) {
			countryMaster = userDao.getCountryMasterByCountryName(addressDto.getCountry().getCountryName());
		}
		addressInfo.setCountryMaster(countryMaster);

		return addressInfo;
	}

	// **************************To DTOs From Models**************************
	/**
	 * @param user
	 * @return UserDto get User Dto From User Model
	 */
	public UserDto getUserDtoFromUserModel(User user) throws Exception {
		UserDto userDTO = null;
		ProfileDto profileDto = null;
		OrganisationProfileDto orgProfileDto = null;
		RolesDto rolesDto = null;
		Set<OrganizationUserRole> organizationUserRoleSet = null;
		OrganizationRole organizationRole = null;
		Set<AdminUserRole> adminUserRoleSet = null;
		AdminRole adminRole = null;
		try {
			if (Format.isObjectNotEmptyAndNotNull(user)) {
				userDTO = new UserDto();
				profileDto = getProfileDtoFromUserModel(user);
				userDTO.setProfileDto(profileDto);

				// TODO write a queries to get admin users and org users
				Set<OrganizationUser> organizationUserSet = user.getOrganizationUsers();
				Set<AdminUser> adminUserSet = user.getAdminUsers();
				if (Format.isCollectionNotEmptyAndNotNull(organizationUserSet)
						&& Format.isCollectionEmtyOrNull(adminUserSet)) {
					for (OrganizationUser organizationUser : organizationUserSet) {
						userDTO.setPasswordStatus(organizationUser.getIsPasswordStatus());
						userDTO.setPassword(organizationUser.getPassword());
						userDTO.setResetPassword(organizationUser.getResetPassword());
						userDTO.setResetPasswordCreateDate(new Date());
						userDTO.setAccountLock(organizationUser.getIsAccountLocked());
						orgProfileDto = organisationMapper
								.getOrganisationProfileDtoFromModel(organizationUser.getOrganization());
						organizationUserRoleSet = organizationUser.getOrganizationUserRoles();
						for (OrganizationUserRole organizationUserRole : organizationUserRoleSet) {
							organizationRole = organizationUserRole.getOrganizationRole();
							rolesDto = getRoleDtoFromOrganizationRoleModel(organizationRole);
							userDTO.setRoles(rolesDto);
						}

					}
				} else {
					for (AdminUser adminUser : adminUserSet) {
						userDTO.setPasswordStatus(adminUser.getIsPasswordStatus());
						userDTO.setPassword(adminUser.getPassword());
						userDTO.setResetPassword(adminUser.getResetPassword());
						userDTO.setResetPasswordCreateDate(new Date());
						userDTO.setAccountLock(adminUser.getIsAccountLocked());
						orgProfileDto = new OrganisationProfileDto();
						orgProfileDto.setOrganisationName("MFS");
						adminUserRoleSet = adminUser.getAdminUserRoles();
						for (AdminUserRole adminUserRole : adminUserRoleSet) {
							adminRole = adminUserRole.getAdminRole();
							rolesDto = getRoleDtoFromAdminRoleModel(adminRole);
							userDTO.setRoles(rolesDto);
						}

					}
				}
				userDTO.setOrgProfileDto(orgProfileDto);

				userDTO.setActiveUser(user.getIsUserActive());
			}

		} catch (Exception e) {
			LOGGER.error("exception" + e);
			throw new CustomException("Exception in Backend");
		}
		return userDTO;
	}

	/**
	 * @param user
	 * @return ProfileDto get Profile Dto From User Model
	 */
	private ProfileDto getProfileDtoFromUserModel(User user) {
		ProfileDto profileDto = new ProfileDto();
		IdDto idDto = null;
		IdDto idDtos = null;
		List<IdType> idList = null;
		List<AddressDto> addressDtoList = new ArrayList<AddressDto>();
		AddressDto addressDto = null;
		CountryDto country = null;
		Address address = null;
		List<IdDto> idTypeList = new ArrayList<IdDto>();
		profileDto.setFirstName(user.getFirstName());
		profileDto.setLastName(user.getLastName());
		profileDto.setEmail(user.getEmail());
		profileDto.setMobileNumber(user.getMobile());
		profileDto.setDateOfBirth(user.getDob());
		if (user.getCountryId() != null) {
			profileDto.setCountryCode(user.getCountryId().getCountryCode());
		}
		profileDto.setStatus(user.getStatus());
		if (user.getIdType() != null) {
			idDto = getIdDtoFromUserModel(user);
			profileDto.setIdDto(idDto);
		}
		idList = commonDao.getIdType();
		if (!(idList).isEmpty()) {
			for (IdType idType : idList) {
				idDtos = new IdDto();
				idDtos.setIdType(idType.getIdType());
				idTypeList.add(idDtos);
			}

		}
		profileDto.setIdTypeList(idTypeList);
		address = commonDao.getUserAddress(user.getUserId());
		if (address != null) {
			country = new CountryDto();
			addressDto = new AddressDto();
			addressDto.setAddress(address.getAddress());
			addressDto.setAddress1(address.getAddress1());
			addressDto.setAddress2(address.getAddress2());
			addressDto.setCity(address.getCity());
			addressDto.setPostalCode(address.getPostalCode());
			addressDto.setStateRegion(address.getStateRegion());
			country.setCountryCode(address.getCountryMaster().getCountryCode());
			country.setCountryName(address.getCountryMaster().getCountryName());
			country.setNumericCode(address.getCountryMaster().getNumericCode());
			country.setPhoneCode(address.getCountryMaster().getPhoneCode());
			addressDto.setCountry(country);

		}
		addressDtoList.add(addressDto);

		profileDto.setAddressDtoList(addressDtoList);
		profileDto.setTimeZoneDto(getTimezoneDtoFromTimeZoneModel(user.getTimeZone()));
		profileDto.setEmailVerified(user.getIsEmailVerified());

		return profileDto;
	}

	// get Status Type Enum From Model
	public StatusType getStatusTypeEnumFromModel(Status statusInfo) {
		StatusType statusType = null;
		// check status info
		if (null != statusInfo && Format.isStringNotEmptyAndNotNull(statusInfo.getStatusName())) {
			if (statusInfo.getStatusName().equals(StatusType.Active.toString())) {
				statusType = StatusType.Active;
			} else if (statusInfo.getStatusName().equals(StatusType.PENDING.toString())) {
				statusType = StatusType.PENDING;
			} else if (statusInfo.getStatusName().equals(StatusType.BLOCKED.toString())) {
				statusType = StatusType.BLOCKED;
			} else if (statusInfo.getStatusName().equals(StatusType.SUSPENDED.toString())) {
				statusType = StatusType.SUSPENDED;
			} else if (statusInfo.getStatusName().equals(StatusType.CLOSED.toString())) {
				statusType = StatusType.CLOSED;
			}
		}
		return statusType;
	}

	/**
	 * @param user
	 * @return IdDto get Id Dto From User Model
	 */
	private IdDto getIdDtoFromUserModel(User user) {
		IdDto idDto = new IdDto();
		idDto.setIdType(user.getIdType().getIdType());
		idDto.setIdNumber(user.getIdNumber());
		idDto.setIdExpiryDate(user.getIdExpiryDate());
		return idDto;
	}

	/**
	 * @param addressInfoSet
	 * @return AddressDto get Address Dto List From User Model
	 */
	private List<AddressDto> getAddressDtoListFromUserModel(Set<Address> addressInfoSet) {
		List<AddressDto> addressDtoList = new ArrayList<AddressDto>();
		addressInfoSet.forEach((address) -> {
			addressDtoList.add(getAddressDtoFromAddressModel(address));
		});
		return addressDtoList;
	}

	/**
	 * @param address
	 * @return AddressDto get Address Dto From Address Model
	 */
	private AddressDto getAddressDtoFromAddressModel(Address address) {
		AddressDto addressDto = null;
		CountryDto countryDto = null;
		if (null != address) {
			addressDto = new AddressDto();
			addressDto.setAddress(address.getAddress());
			addressDto.setAddress1(address.getAddress1());
			addressDto.setAddress2(address.getAddress2());
			countryDto = getCountryDtoFromCountryModel(address.getCountryMaster());
			addressDto.setCountry(countryDto);
			addressDto.setPostalCode(address.getPostalCode());
			addressDto.setStateRegion(address.getStateRegion());
		}
		return addressDto;
	}

	/**
	 * @param organizationRole
	 * @return RolesDto get Role Dto From Organization Role Model
	 */
	public RolesDto getRoleDtoFromOrganizationRoleModel(OrganizationRole organizationRole) {
		RolesDto rolesDto = null;
		if (Format.isObjectNotEmptyAndNotNull(organizationRole)) {
			rolesDto = new RolesDto();

			if (null != organizationRole.getRoleName()
					&& Format.isStringNotEmptyAndNotNull(organizationRole.getRoleName())) {
				rolesDto.setRoleName(organizationRole.getRoleName());
			}
			rolesDto.setRoleId(organizationRole.getOrganisationRoleId());
			rolesDto.setRoleDescription(organizationRole.getRoleDescription());
			rolesDto.setCreatedBy(organizationRole.getCreatedBy());
			rolesDto.setRoleCreateDate(organizationRole.getCreationDate());
		}
		return rolesDto;
	}

	/**
	 * @param organizationRole
	 * @return RolesDto get Role Dto From Admin Role Model
	 */
	public RolesDto getRoleDtoFromAdminRoleModel(AdminRole adminRole) {
		RolesDto rolesDto = null;
		if (Format.isObjectNotEmptyAndNotNull(adminRole)) {
			rolesDto = new RolesDto();

			if (null != adminRole.getRoleName() && Format.isStringNotEmptyAndNotNull(adminRole.getRoleName())) {

				rolesDto.setRoleName(adminRole.getRoleName());
			}
			rolesDto.setRoleDescription(adminRole.getRoleDescription());
			rolesDto.setCreatedBy(adminRole.getCreatedBy());
			rolesDto.setRoleCreateDate(adminRole.getCreatedOn());
		}
		return rolesDto;
	}

	/**
	 * @param organizationUserRole
	 * @return RolesDto get Role Dto From Organization User Role Model
	 */

	public RolesDto getRoleDtoFromOrganizationUseRoleModel(OrganizationUserRole organizationUserRole) {
		RolesDto rolesDto = null;
		OrganizationRole organizationRole = null;
		if (Format.isObjectNotEmptyAndNotNull(organizationUserRole)) {
			rolesDto = new RolesDto();
			organizationRole = organizationUserRole.getOrganizationRole();
			if (Format.isObjectNotEmptyAndNotNull(organizationRole)) {
				rolesDto = getRoleDtoFromOrganizationRoleModel(organizationRole);
			}
		}
		return rolesDto;
	}

	/**
	 * @param List
	 *            <OrganizationUserRole>
	 * @return List<RolesDto> get Role Dto List From Organization User Roles Model
	 */
	public List<RolesDto> getRoleDtoListFromOrganizationUserRolesModel(Set<OrganizationUserRole> set) {
		List<RolesDto> ruleDtoList = null;
		RolesDto rolesDto = null;
		if (Format.isCollectionNotEmptyAndNotNull(set)) {
			ruleDtoList = new ArrayList<RolesDto>();
			for (OrganizationUserRole organizationUserRole : set) {

				if (Format.isObjectNotEmptyAndNotNull(organizationUserRole)) {
					rolesDto = getRoleDtoFromOrganizationUseRoleModel(organizationUserRole);
					ruleDtoList.add(rolesDto);
				}
			}
		}

		return ruleDtoList;

	}

	/**
	 * @param timeZoneMasterModel
	 * @return TimeZoneDto get Time zone Dto From Time Zone Model
	 */
	public TimeZoneDto getTimezoneDtoFromTimeZoneModel(TimeZone timeZoneMasterModel) {
		TimeZoneDto timeZoneDto = null;
		if (Format.isObjectNotEmptyAndNotNull(timeZoneMasterModel)) {
			timeZoneDto = new TimeZoneDto();
			timeZoneDto.setTimeZoneName(timeZoneMasterModel.getTimeZoneName());
			timeZoneDto.setTimeZone(timeZoneMasterModel.getTimeZone());
		}
		return timeZoneDto;

	}

	/**
	 * @param timeZoneMasterModelList
	 * @return timeZoneDtoList get Time zone Dto List From Time Zone Model List
	 */
	public List<TimeZoneDto> getTimezoneDtoListFromTimeZoneModelList(List<TimeZone> timeZoneMasterModelList) {
		List<TimeZoneDto> timeZoneDtoList = null;
		TimeZoneDto timeZoneDto = null;
		if (!Format.isCollectionEmtyOrNull(timeZoneMasterModelList)) {
			timeZoneDtoList = new ArrayList<TimeZoneDto>();
			for (TimeZone timeZoneMaster : timeZoneMasterModelList) {
				timeZoneDto = getTimezoneDtoFromTimeZoneModel(timeZoneMaster);
				timeZoneDtoList.add(timeZoneDto);
			}

		}
		return timeZoneDtoList;

	}

	/**
	 * @param countryMasterModelList
	 * @return List<CountryDto> get Country Dto List From Country Model List
	 */
	public List<CountryDto> getCountryDtoListFromCountryModelList(List<CountryMaster> countryMasterModelList) {
		List<CountryDto> countryDtoList = null;
		if (!Format.isCollectionEmtyOrNull(countryMasterModelList)) {
			countryDtoList = new ArrayList<CountryDto>();
			for (CountryMaster countryMasterModel : countryMasterModelList) {
				countryDtoList.add(getCountryDtoFromCountryModel(countryMasterModel));
			}
		}
		return countryDtoList;

	}

	/**
	 * @param countryMasterModel
	 * @return CountryDto get Country Dto From Country Model
	 */
	public CountryDto getCountryDtoFromCountryModel(CountryMaster countryMasterModel) {
		CountryDto countryDto = null;
		if (Format.isObjectNotEmptyAndNotNull(countryMasterModel)) {
			countryDto = new CountryDto();
			countryDto.setCountryCode(countryMasterModel.getCountryCode());
			countryDto.setCountryName(countryMasterModel.getCountryName());
			countryDto.setNumericCode(countryMasterModel.getNumericCode());
			countryDto.setPhoneCode(countryMasterModel.getPhoneCode());
		}
		return countryDto;
	}

	// prepare Merchant User For Change Password
	@Transactional(rollbackFor = Exception.class)
	public User prepareMerchantUserForChangePassword(User user, PasswordDto passwordDto) {

		Set<OrganizationUser> orgUsersSet = user.getOrganizationUsers();
		// TODO a query to get all org users
		//
		boolean isOrgUserUpdated = false;
		if (Format.isCollectionNotEmptyAndNotNull(orgUsersSet)) {
			for (OrganizationUser organisationUser : orgUsersSet) {
				organisationUser.setPassword(bCryptPasswordEncoder.encode(passwordDto.getNewPassword()));
				organisationUser.setIsPasswordStatus(true);
				organisationUser.setUser(user);
				isOrgUserUpdated = userDao.update(organisationUser);
				if (!isOrgUserUpdated) {
					LOGGER.error("Error while updating merchant User");
				}
			}
		} else {
			throw new CustomException("The User is not merchant user.");
		}
		return user;
	}

}

/**
 * 
 */
package com.mfs.pp.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.mfs.mdm.common.dto.LoginAuditLogDTO;
import com.mfs.mdm.model.LoginAudit;
import com.mfs.pp.util.Format;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Component
public class AuditLogMapper {

	/**
	 * @param loginAuditLogDTO 
	 * prepare login audit 
	 * @return
	 */
	public LoginAudit prepareLoginAuditinfoFromDto(
			LoginAuditLogDTO loginAuditLogDTO) {
		LoginAudit loginAudit = null;
		if (Format.isObjectNotEmptyAndNotNull(loginAuditLogDTO)) {
			loginAudit = new LoginAudit();
			loginAudit.setEventResult(loginAuditLogDTO.getEventResult());
			loginAudit.setDateTime(loginAuditLogDTO.getDateTime());
			loginAudit.setDescription(loginAuditLogDTO.getDescription());
			loginAudit.setIpAddress(loginAuditLogDTO.getIpAddress());
		}
		return loginAudit;

	}
	
	/**
	 * @param LoginAuditInfoList
	 * prepare Login Audit List
	 * @return
	 */
	public List<LoginAuditLogDTO> prepareLoginAuditDtoListFromModel(List <LoginAudit> LoginAuditInfoList)
	{
		List<LoginAuditLogDTO> loginAuditLogDTOList=null;
	
		if(Format.isCollectionNotEmptyAndNotNull(LoginAuditInfoList))
		{
			loginAuditLogDTOList=new ArrayList<LoginAuditLogDTO>();	
			for (LoginAudit loginAudit : LoginAuditInfoList) {
				loginAuditLogDTOList.add(prepareLoginAuditDtoFromModel(loginAudit));
			}				
		}
		return loginAuditLogDTOList;
		
	}
	
	/**
	 * @param loginAudit
	 * prepare Login Audit From Model
	 * @return
	 */
	public LoginAuditLogDTO prepareLoginAuditDtoFromModel(LoginAudit loginAudit)
	{
		LoginAuditLogDTO loginAuditLogDTO=null;
		if (Format.isObjectNotEmptyAndNotNull(loginAudit)) {
			loginAuditLogDTO=new LoginAuditLogDTO();
			loginAuditLogDTO.setDateTime(loginAudit.getDateTime());
			loginAuditLogDTO.setDescription(loginAudit.getDescription());
			loginAuditLogDTO.setEventResult(loginAudit.getEventResult());
			loginAuditLogDTO.setIpAddress(loginAudit.getIpAddress());
			if(Format.isObjectNotEmptyAndNotNull(loginAudit.getOrgName())) {
				loginAuditLogDTO.setOrgName(loginAudit.getOrgName());
			}
		}
		return loginAuditLogDTO;		
	}

	
}

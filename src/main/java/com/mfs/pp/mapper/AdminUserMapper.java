package com.mfs.pp.mapper;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.mdm.common.dto.IdDto;
import com.mfs.mdm.common.dto.PasswordDto;
import com.mfs.mdm.common.dto.ProfileDto;
import com.mfs.mdm.common.dto.RolesDto;
import com.mfs.mdm.common.dto.UserDto;
import com.mfs.mdm.model.AdminRole;
import com.mfs.mdm.model.AdminUser;
import com.mfs.mdm.model.AdminUserRole;
import com.mfs.mdm.model.TimeZone;
import com.mfs.mdm.model.User;
import com.mfs.pp.dao.AdminDao;
import com.mfs.pp.dao.UserDao;
import com.mfs.pp.exception.CustomException;
import com.mfs.pp.util.Format;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */

@Component
public class AdminUserMapper {
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminUserMapper.class);

	@Autowired
	AdminDao adminDao;

	@Autowired
	UserDao userDao;

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;

	// prepare admin user
	@Transactional(rollbackFor = Exception.class)
	public Set<AdminUser> prepareAdminUserModelFromUserDtoForInviteSystemAdmin(UserDto userDTO) throws Exception {
		Set<AdminUser> adminUsers = new HashSet<AdminUser>();
		AdminUser adminUser = prepareAdminUserInfo(userDTO);
		adminUsers.add(adminUser);
		return adminUsers;
	}

	// prepare admin user info
	@Transactional(rollbackFor = Exception.class)
	public AdminUser prepareAdminUserInfo(UserDto userDTO) throws Exception {
		AdminUser adminUserInfo = null;
		Date date = null;
		boolean isAccountLocked = false;
		if (null != userDTO) {
			adminUserInfo = adminDao.getAdminUserByUserEmail(userDTO.getProfileDto().getEmail());
			if (null == adminUserInfo) {
				adminUserInfo = new AdminUser();
				date = new Date();
				// check user password
				if (Format.isStringNotEmptyAndNotNull(userDTO.getPassword())) {
					adminUserInfo.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
					adminUserInfo.setIsPasswordStatus(true);
					// adminUserInfo.setIsAccountLocked(isAccountLocked);
				}
				adminUserInfo.setIsPasswordStatus(userDTO.isPasswordStatus());
				adminUserInfo.setResetPassword(userDTO.getResetPassword());

				adminUserInfo.setResetPasswordCreatedOn(date);

			} else {
				// check user role
				if (Format.isObjectNotEmptyAndNotNull(userDTO.getRoles())) {
					Set<AdminUserRole> adminRoles = adminUserInfo.getAdminUserRoles();

					for (AdminUserRole role : adminRoles) {
						AdminRole adrole = adminDao.getAdminRoleByRoleId(userDTO.getRoles().getRoleId());
						role.setAdminRole(adrole);
						role.setAdminUser(adminUserInfo);
					}
					adminUserInfo.setAdminUserRoles(adminRoles);

				}
				if (Format.isStringNotEmptyAndNotNull(userDTO.getPassword())) {
					adminUserInfo.setPassword(userDTO.getPassword());

				}
				if (Format.isNotNull(userDTO.isPasswordStatus())) {
					adminUserInfo.setIsPasswordStatus(true);
				}
				if (Format.isNotNull(userDTO.isAccountLock())) {
					// adminUserInfo.setIsAccountLocked(true);
				}
				if (Format.isStringNotEmptyAndNotNull(userDTO.getResetPassword())) {
					adminUserInfo.setResetPassword(userDTO.getResetPassword());
				}
				if (Format.isDateNotEmtyAndNotNull(userDTO.getResetPasswordCreateDate())) {
					adminUserInfo.setResetPasswordCreatedOn(userDTO.getResetPasswordCreateDate());
				}
			}
		}
		return adminUserInfo;
	}

	// prepare admin user roles
	@Transactional(rollbackFor = Exception.class)
	public Set<AdminUserRole> prepareAdminUserRoles(RolesDto rolesDto) throws Exception {
		Set<AdminUserRole> adminUserRolesSet = new HashSet<AdminUserRole>();
		AdminUserRole adminUserRole = null;
		AdminRole adminRole = null;
		if (null != rolesDto) {
			adminUserRole = new AdminUserRole();
			adminUserRole.setIsRoleActive(true);
			if (0 != rolesDto.getRoleId()) {
				adminRole = adminDao.getAdminRoleByRoleId(rolesDto.getRoleId());
			}

			adminUserRole.setAdminRole(adminRole);
		}
		adminUserRolesSet.add(adminUserRole);
		return adminUserRolesSet;
	}

	// prepare admin user for change passowrd
	@Transactional(rollbackFor = Exception.class)
	public User prepareAdminUserForChangePassword(User user, PasswordDto passwordDto) throws Exception {
		Set<AdminUser> adminUsersSet = user.getAdminUsers();
		;// user.getAdminUsers();
		boolean isAdminUserUpdated = false;
		if (Format.isCollectionNotEmptyAndNotNull(adminUsersSet)) {
			for (AdminUser adminUser : adminUsersSet) {
				adminUser.setPassword(bCryptPasswordEncoder.encode(passwordDto.getNewPassword()));
				adminUser.setIsPasswordStatus(true);
				adminUser.setUser(user);
				isAdminUserUpdated = adminDao.update(adminUser);
				if (!isAdminUserUpdated) {
					LOGGER.error("Error while updating Admin User");
				}
			}
		} else {
			throw new CustomException("The User is not System Admin.");
		}

		return user;
	}

	// prepare user for invite by system admin
	public User prepareUserModelFromUserDtoForInviteSystemAdmin(UserDto userDTO) {
		User user = null;
		ProfileDto profileDto = null;
		TimeZone timeZoneMaster = null;
		if (null != userDTO) {
			user = new User();
			profileDto = userDTO.getProfileDto();
			if (null != profileDto) {
				user.setFirstName(profileDto.getFirstName());
				user.setLastName(profileDto.getLastName());
				user.setEmail(profileDto.getEmail());
				user.setFirstActivationDate(new Date());
				user.setMobile(userDTO.getProfileDto().getMobileNumber() + "");
				user.setStatus("pending");
				user.setIsUserActive(true);
				user.setIsEmailVerified(true);
				IdDto idDto = profileDto.getIdDto();
				if (Format.isObjectNotEmptyAndNotNull(idDto)) {
					user.setIdNumber(idDto.getIdNumber());
					user.setIdExpiryDate(idDto.getIdExpiryDate());

				}
				if (Format.isObjectNotEmptyAndNotNull(profileDto.getTimeZoneDto())) {
					timeZoneMaster = userDao
							.getTimeZoneMasterByTimeZoneName(profileDto.getTimeZoneDto().getTimeZoneName());
				}
				user.setTimeZone(timeZoneMaster);

			}
		}
		return user;
	}
}

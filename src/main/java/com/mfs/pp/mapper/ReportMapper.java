package com.mfs.pp.mapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.mfs.mdm.common.dto.ResponseTransactionSummaryByDateDto;
import com.mfs.mdm.common.dto.TransactionDateSummaryDto;
import com.mfs.mdm.common.dto.TransactionDetailsDto;
import com.mfs.mdm.common.dto.TransactionSummaryByDateDto;
import com.mfs.mdm.common.dto.TransactionSummaryDto;
import com.mfs.pp.util.Format;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Component
public class ReportMapper {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportMapper.class);


	//get Transaction Bill Id Map From List
	public Map<String, TransactionSummaryDto> getTransactionBillIdMapFromList(
			List<TransactionSummaryDto> transactionSummaryDtoList) {
		Map<String, TransactionSummaryDto> transactionSummaryMap = null;
		if (Format.isCollectionNotEmptyAndNotNull(transactionSummaryDtoList)) {
			transactionSummaryMap = new HashMap<String, TransactionSummaryDto>();
			for (TransactionSummaryDto transactionSummaryDto : transactionSummaryDtoList) {
				transactionSummaryMap.put(transactionSummaryDto.getMfsBillId(), transactionSummaryDto);
			}
		}
		return transactionSummaryMap;
	}

	//get Transaction Details From Map
	public TransactionDetailsDto getTransactionDetailsFromMap(TransactionSummaryDto transactionSummaryDto,
			TransactionDetailsDto transactionDetailsDto) {
		TransactionDetailsDto transactionDetailsDtoTemp = transactionDetailsDto;
		transactionDetailsDtoTemp.setMfsBillId(transactionSummaryDto.getMfsBillId());
		transactionDetailsDtoTemp.setBillCreatedDate(transactionSummaryDto.getBillCreatedDate());
		transactionDetailsDtoTemp.setBillAmount(transactionSummaryDto.getBillAmount());
		transactionDetailsDtoTemp.setBillCurrency(transactionSummaryDto.getBillCurrency());
		transactionDetailsDtoTemp.setBillId(transactionSummaryDto.getBillId());
		transactionDetailsDtoTemp.setBillPayDate(transactionSummaryDto.getBillPayDate());
		transactionDetailsDtoTemp.setMerchantCode(transactionSummaryDto.getMerchantCode());
		transactionDetailsDtoTemp.setMerchantMsisdn(transactionSummaryDto.getMerchantMsisdn());
		transactionDetailsDtoTemp.setRpCode(transactionSummaryDto.getRpCode());
		transactionDetailsDtoTemp.setSpCode(transactionSummaryDto.getSpCode());
		transactionDetailsDtoTemp.setStatus(transactionSummaryDto.getStatus());
		return transactionDetailsDtoTemp;
	}

	//filter Response By Date
	public ResponseTransactionSummaryByDateDto filterResponseByDate(
			List<TransactionSummaryDto> transactionSummaryDtoList) {

		List<TransactionDateSummaryDto> listOfTransactionDateSummaryDto = new ArrayList<TransactionDateSummaryDto>();
		List<TransactionSummaryByDateDto> listTransactionSummaryByDateDto = new ArrayList<TransactionSummaryByDateDto>();
		TransactionDateSummaryDto transactionDateSummaryDto = null;

		ResponseTransactionSummaryByDateDto responseTransactionSummaryByDateDto = new ResponseTransactionSummaryByDateDto();
		
		TransactionSummaryByDateDto transactionSummaryByDateDto = new TransactionSummaryByDateDto();
		
		Comparator<TransactionSummaryDto> dateCompare = (TransactionSummaryDto o1, TransactionSummaryDto o2) -> o1
				.getBillPayDate().compareTo(o2.getBillPayDate());
		Collections.sort(transactionSummaryDtoList, dateCompare);
		String dateStr_pre = "";
		String dateStr_post = "";
		Double amount = 0.0;
		Integer count = 0;
		Integer totalCount = 0;
		Double totalAmount = 0.0;
		String currency = "";
		for (TransactionSummaryDto transactionSummaryDto : transactionSummaryDtoList) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

			dateStr_post = sdf.format(transactionSummaryDto.getBillPayDate());
			if (!dateStr_pre.equals(dateStr_post)) {
				
				if (count != 0) {
					transactionDateSummaryDto = getTransactionDateSummaryDto(dateStr_pre,amount,count);
					listOfTransactionDateSummaryDto.add(transactionDateSummaryDto);
				}
				amount = 0.0;
				count = 0;
			}
			dateStr_pre = dateStr_post;
			amount = amount + transactionSummaryDto.getBillAmount();
			totalAmount = totalAmount + transactionSummaryDto.getBillAmount();
			count = count + 1;
			totalCount = totalCount + 1;
			currency = transactionSummaryDto.getBillCurrency();
		}

		/** only for last entry **/
		
		transactionDateSummaryDto=getTransactionDateSummaryDto(dateStr_pre, amount, count);
		listOfTransactionDateSummaryDto.add(transactionDateSummaryDto);
		transactionSummaryByDateDto.setTotalTransactionAmount(totalAmount);
		transactionSummaryByDateDto.setTotalTransactionCount(totalCount);
		transactionSummaryByDateDto.setCurrency(currency);
		transactionSummaryByDateDto.setListTransactionDateSummaryDto(listOfTransactionDateSummaryDto);
		listTransactionSummaryByDateDto.add(transactionSummaryByDateDto);
		responseTransactionSummaryByDateDto.setTransactionList(listTransactionSummaryByDateDto);
		return responseTransactionSummaryByDateDto;
	}

	//get Transaction Date Summary 
	private TransactionDateSummaryDto getTransactionDateSummaryDto(String dateStr_pre, Double amount, Integer count) {
		TransactionDateSummaryDto transactionDateSummaryDto = new TransactionDateSummaryDto();
		try {
			transactionDateSummaryDto.setBillPayDate(new SimpleDateFormat("yyyyMMdd").parse(dateStr_pre));
		} catch (ParseException e) {
			LOGGER.debug("Error while parsing date in ReportMapper.filterResponseByDate method");
		}
		transactionDateSummaryDto.setTransactionAmount(amount);
		transactionDateSummaryDto.setTransactionCount(count);
		return transactionDateSummaryDto;
	}

}

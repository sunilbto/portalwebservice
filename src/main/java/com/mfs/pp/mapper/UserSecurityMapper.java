package com.mfs.pp.mapper;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mfs.mdm.common.dto.ProfileDto;
import com.mfs.mdm.common.dto.UserDto;
import com.mfs.mdm.common.type.StatusType;
import com.mfs.mdm.model.AdminUser;
import com.mfs.mdm.model.OrganizationUser;
import com.mfs.mdm.model.User;
import com.mfs.pp.dto.MailDetails;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Component
public class UserSecurityMapper {

	@Autowired
	private Properties mfsSchoolMailProperties;

	//prepare Mail Details 
	public MailDetails prepareMailDetailsDto(String email, String url, String createOrReset, String userRole,
			String orgName,String userEmail,String userName,String partnerCode) throws Exception {
		MailDetails mailDetails = new MailDetails();
		String tempmessage = null;
		String message = null;
		//check string
		if (createOrReset.equals("reset")) {
			tempmessage = mfsSchoolMailProperties.getProperty("mail.reset.password.content");
			message = MessageFormat.format(tempmessage, url);
			mailDetails.setSubject(mfsSchoolMailProperties.getProperty("mail.reset.subject"));
		} else if (createOrReset.equals("register")) {
			tempmessage = mfsSchoolMailProperties.getProperty("mail.create.user.content");
			message = MessageFormat.format(tempmessage, email, url);
			mailDetails.setSubject(mfsSchoolMailProperties.getProperty("mail.create.subject"));
		} else if (createOrReset.equals("invite")) {
			tempmessage = mfsSchoolMailProperties.getProperty("mail.invite.password.content");
			message = MessageFormat.format(tempmessage, userRole, url, orgName);
			mailDetails.setSubject(mfsSchoolMailProperties.getProperty("mail.invite.subject"));
		} else if (createOrReset.equals("inviteAdminUser")) {
			tempmessage = mfsSchoolMailProperties.getProperty("mail.adminuser.invite.password.content");
			message = MessageFormat.format(tempmessage, userRole, url);
			mailDetails.setSubject(mfsSchoolMailProperties.getProperty("mail.adminuser.invite.subject"));
		} else if (createOrReset.equals("newOrganization")) {
			tempmessage = mfsSchoolMailProperties.getProperty("mail.create.oranization.content");
			message = MessageFormat.format(tempmessage, orgName);
			mailDetails.setSubject(mfsSchoolMailProperties.getProperty("mail.create.oranization.subject"));
		} else if (createOrReset.equals("inviteOrganisationUser")) {
			tempmessage = mfsSchoolMailProperties.getProperty("mail.organisationuser.invite.password.content");
			message = MessageFormat.format(tempmessage, url);
			mailDetails.setSubject(mfsSchoolMailProperties.getProperty("mail.invite.subject"));
		
	}
	else if (createOrReset.equals("organisationQuery")) {
		tempmessage = mfsSchoolMailProperties.getProperty("mail.organisation.request.content");
		message = MessageFormat.format(tempmessage,partnerCode,orgName,userName,userRole,userEmail,url);
		mailDetails.setSubject(mfsSchoolMailProperties.getProperty("mail.oranization.query.subject"));
	}

		String[] recipient = new String[1];
		recipient[0] = email;
		mailDetails.setHost(mfsSchoolMailProperties.getProperty("mail.smtp.host"));
		mailDetails.setFromUserName(mfsSchoolMailProperties.getProperty("mail.smtp.user"));
		mailDetails.setPassword(mfsSchoolMailProperties.getProperty("mail.smtp.password"));
		mailDetails.setPort(mfsSchoolMailProperties.getProperty("mail.smtp.port"));
		mailDetails.setAuth(mfsSchoolMailProperties.getProperty("mail.smtp.auth"));
		mailDetails.setRecipient(recipient);
		mailDetails.setTlsEnable(mfsSchoolMailProperties.getProperty("mail.smtp.starttls.enable"));
		mailDetails.setBody(message);
		return mailDetails;

	}

	//prepare User For Email Verification
	public UserDto prepareUserDtoForEmailVerification(UserDto userDto) throws Exception {
		UserDto userDTO = userDto;
		userDTO.setActiveUser(true);
		userDTO.setPasswordStatus(false);
		ProfileDto profileDto = userDTO.getProfileDto();
		profileDto.setEmailVerified(true);
		profileDto.setStatus(StatusType.Active.toString());
		userDTO.setProfileDto(profileDto);
		return userDTO;
	}

	//prepare User From User For Reset Token
	public User prepareUserFromUserDtoForResetToken(User user, UserDto userDto) {
		String token = UUID.randomUUID().toString();
		Date todayDate = new Date();
		Set<AdminUser> adminUserset = user.getAdminUsers();
		for (AdminUser adminUser : adminUserset) {
			//check admin user
			if (null != adminUser) {
				adminUser.setResetPassword(token);
				adminUser.setResetPasswordCreatedOn(todayDate);
				if (userDto.isPasswordStatus() == false) {
					adminUser.setIsPasswordStatus(userDto.isPasswordStatus());
				} else {
					adminUser.setIsPasswordStatus(false);
				}
				adminUser.setUser(user);

			}
		}
		return user;
	}

	//prepare Org User For Reset
	public OrganizationUser prepareOrgUserForReset(UserDto userDto, OrganizationUser organizationUser)
			throws Exception {

		organizationUser.setIsPasswordStatus(userDto.isPasswordStatus());
		organizationUser.setResetPassword(userDto.getResetPassword());
		organizationUser.setResetPasswordCreatedOn(userDto.getResetPasswordCreateDate());
		return organizationUser;
	}

	//prepare Admin User For Reset
	public AdminUser prepareAdminUserForReset(UserDto userDto, AdminUser adUser) throws Exception {

		adUser.setIsPasswordStatus(userDto.isPasswordStatus());
		adUser.setResetPassword(userDto.getResetPassword());
		adUser.setResetPasswordCreatedOn(userDto.getResetPasswordCreateDate());
		return adUser;
	}
}

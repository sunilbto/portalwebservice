/**
 * 
 */
package com.mfs.pp.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.mdm.common.dto.AuditLogInputDto;
import com.mfs.mdm.common.dto.CurrencyDto;
import com.mfs.mdm.common.dto.LoginAuditLogDTO;
import com.mfs.mdm.common.dto.OrganisationProfileDto;
import com.mfs.mdm.common.dto.RolesDto;
import com.mfs.mdm.common.dto.UserDto;
import com.mfs.pp.dto.OrganizationListsResponse;
import com.mfs.pp.dto.OrganizationResponse;
import com.mfs.pp.dto.OrganizationResponseByName;
import com.mfs.pp.dto.OutputDTO;
import com.mfs.pp.dto.UserResponse;
import com.mfs.pp.service.AuditLogService;
import com.mfs.pp.service.OrganisationService;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.Format;
import com.mfs.pp.util.RestServiceURIConstant;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */
@RestController
public class OrganisationController extends BaseController {
	@Autowired
	private OrganisationService organisationService;

	@Autowired
	private AuditLogService auditLogService;

	protected final Log log = LogFactory.getLog(getClass());

	// get All Organisation Roles
	@RequestMapping(value = RestServiceURIConstant.GET_ALL_ORGANISATION_ROLES, method = RequestMethod.GET)
	public UserResponse<List<RolesDto>> getAllOrganisationRoles() {
		// log.info("Inside getAllOrganisationRoles() method of
		// ---->"+Constants.ORGANISATION_CONTROLLER);
		UserResponse<List<RolesDto>> response = null;
		OutputDTO<List<RolesDto>> outputDTO = null;
		response = new UserResponse<List<RolesDto>>();
		try {
			// log.info("Inside getAllOrganisationRoles() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getAllOrganisationRoles() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			List<RolesDto> roleDtoList = organisationService.getAllRoles();
			if (Format.isCollectionNotEmptyAndNotNull(roleDtoList)) {
				outputDTO = new OutputDTO<List<RolesDto>>();
				outputDTO.setResponseData(roleDtoList);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("service executed  Successfully");
			}
		} catch (Exception e) {
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Failed while retriving organiation role ");

			log.error("exception in getAllOrganisationRoles " + e.getMessage());
		}
		// log.info("Exiting getAllOrganisationRoles() method of
		// --->"+Constants.ORGANISATION_CONTROLLER);
		return response;
	}

	// get All Curruncy List
	@RequestMapping(value = RestServiceURIConstant.GET_ALL_CURRENCY_LIST, method = RequestMethod.GET)
	public UserResponse<List<CurrencyDto>> getAllCurruncyDtoList() {
		// log.info("Inside getAllCurruncyDtoList() method of
		// ---->"+Constants.ORGANISATION_CONTROLLER);
		UserResponse<List<CurrencyDto>> response = null;
		OutputDTO<List<CurrencyDto>> outputDTO = null;
		response = new UserResponse<List<CurrencyDto>>();
		try {
			// log.info("Inside getAllCurruncyDtoList() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getAllCurruncyDtoList() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			List<CurrencyDto> curruncyDtoList = organisationService.getAllCurruncyDtos();
			if (Format.isCollectionNotEmptyAndNotNull(curruncyDtoList)) {
				outputDTO = new OutputDTO<List<CurrencyDto>>();
				outputDTO.setResponseData(curruncyDtoList);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("service executed  Successfully");
			}
		} catch (Exception e) {
			log.error("exception in getAllCurruncyDtoList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Failed while retriving currency list.");

		}
		// log.info("Exiting getAllCurruncyDtoList() method of
		// --->"+Constants.ORGANISATION_CONTROLLER);
		return response;
	}

	// get All Status Types
	@RequestMapping(value = RestServiceURIConstant.GET_ALL_STATUS_TYPES, method = RequestMethod.GET)
	public UserResponse<List<String>> getAllStatusTypes() {
		// log.info("Inside getAllStatusTypes() method of
		// ---->"+Constants.ORGANISATION_CONTROLLER);
		UserResponse<List<String>> response = null;
		OutputDTO<List<String>> outputDTO = null;
		response = new UserResponse<List<String>>();
		try {
			// log.info("Inside getAllStatusTypes() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getAllStatusTypes() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			List<String> statusTypesList = organisationService.getAllStatusTypes();
			if (Format.isCollectionNotEmptyAndNotNull(statusTypesList)) {
				outputDTO = new OutputDTO<List<String>>();
				outputDTO.setResponseData(statusTypesList);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("service executed  Successfully");
			}
		} catch (Exception e) {
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Failed while retriving StatusTypes list.");
			log.error("exception in getAllStatusTypes " + e.getMessage());
		}
		// log.info("Exiting getAllStatusTypes() method of
		// --->"+Constants.ORGANISATION_CONTROLLER);
		return response;
	}

	// get Api Key
	@RequestMapping(value = RestServiceURIConstant.GET_API_KEY_BY_ORGANIZATION_NAME, method = RequestMethod.GET)
	public UserResponse<String> getApiKey(@PathVariable(value = "orgName") String orgName) {
		// log.info("Inside getApiKey() method of
		// ---->"+Constants.ORGANISATION_CONTROLLER+Constants.BEGIN_REQUEST+"--->"+orgName);
		String apiKey = null;
		UserResponse<String> response = null;
		OutputDTO<String> outputDTO = null;
		response = new UserResponse<String>();
		try {
			// log.info("Inside getApiKey() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getApiKey() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			apiKey = organisationService.getAPIKeyByOrganisationName(orgName);
			if (Format.isObjectNotEmptyAndNotNull(apiKey)) {
				outputDTO = new OutputDTO<String>();
				outputDTO.setResponseData(apiKey);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("service executed  Successfully");
			}
		} catch (Exception e) {
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Failed while retriving api key ");
			log.error("exception in getApiKey " + e.getMessage());
		}
		// log.info("Exiting getApiKey() method of
		// --->"+Constants.ORGANISATION_CONTROLLER);
		return response;
	}

	// get Organisation Details By OrgName
	@RequestMapping(value = RestServiceURIConstant.GET_ORGANIZATION_DETAILS_BY_ORGANIZATION_NAME, method = RequestMethod.GET)
	public UserResponse<OrganisationProfileDto> getOrganisationDetailsByOrgName(
			@PathVariable(value = "orgName") String orgName) {
		// log.info("Inside getOrganisationDetailsByOrgName() method of
		// ---->"+Constants.ORGANISATION_CONTROLLER+Constants.BEGIN_REQUEST+"--->"+orgName);
		OrganisationProfileDto organisationProfileDto = null;
		UserResponse<OrganisationProfileDto> response = null;
		OutputDTO<OrganisationProfileDto> outputDTO = null;
		response = new UserResponse<OrganisationProfileDto>();
		try {
			// log.info("Inside getOrganisationDetailsByOrgName() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getOrganisationDetailsByOrgName() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			organisationProfileDto = organisationService.getOrganisationDetailsByOrgName(orgName);
			if (Format.isObjectNotEmptyAndNotNull(organisationProfileDto)) {
				outputDTO = new OutputDTO<OrganisationProfileDto>();
				outputDTO.setResponseData(organisationProfileDto);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("service executed  Successfully");
			}
		} catch (Exception e) {
			log.error("exception in getOrganisationDetailsByOrgName " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Failed while retriving organisation details.");

		}
		// log.info("Exiting getOrganisationDetailsByOrgName() method of
		// --->"+Constants.ORGANISATION_CONTROLLER);
		return response;
	}

	// get Audit Log List By Dates
	@RequestMapping(value = RestServiceURIConstant.GET_LOGIN_AUDIT_BY_DATES, method = RequestMethod.POST)
	public UserResponse<List<LoginAuditLogDTO>> getAuditLogListByDates(@RequestBody AuditLogInputDto reportsInputDto) {
		// log.info("Inside getAuditLogListByDates() method of
		// ---->"+Constants.ORGANISATION_CONTROLLER+Constants.BEGIN_REQUEST+"--->"+reportsInputDto);
		UserResponse<List<LoginAuditLogDTO>> response = null;
		OutputDTO<List<LoginAuditLogDTO>> outputDTO = null;
		response = new UserResponse<List<LoginAuditLogDTO>>();
		try {
			// log.info("Inside getAuditLogListByDates() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getAuditLogListByDates() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			List<LoginAuditLogDTO> LoginAuditDtoList = auditLogService.getAuditLogListByDates(reportsInputDto);
			if (Format.isCollectionNotEmptyAndNotNull(LoginAuditDtoList)) {
				outputDTO = new OutputDTO<List<LoginAuditLogDTO>>();
				outputDTO.setResponseData(LoginAuditDtoList);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("service executed  Successfully");
			}
		} catch (Exception e) {

			log.error("exception in getAuditLogListByDates " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Failed while retriving login audits!");

		}
		// log.info("Exiting getAuditLogListByDates() method of
		// --->"+Constants.ORGANISATION_CONTROLLER);
		return response;
	}

	// get All Report Types List
	@RequestMapping(value = RestServiceURIConstant.GET_ALL_REPORT_TYPE, method = RequestMethod.GET)
	public UserResponse<List<String>> getAllReportTypesList() {
		// log.info("Inside getAllReportTypesList() method of
		// ---->"+Constants.ORGANISATION_CONTROLLER);
		UserResponse<List<String>> response = null;
		OutputDTO<List<String>> outputDTO = null;
		response = new UserResponse<List<String>>();
		try {
			// log.info("Inside getAllReportTypesList() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getAllReportTypesList() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			List<String> reportTypesList = organisationService.getAllReportTypesList();
			if (Format.isCollectionNotEmptyAndNotNull(reportTypesList)) {
				outputDTO = new OutputDTO<List<String>>();
				outputDTO.setResponseData(reportTypesList);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("service executed  Successfully");
			}
		} catch (Exception e) {
			log.error("exception in getAllReportTypesList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Failed while retriving all report types.");
		}
		// log.info("Exiting getAllReportTypesList() method of
		// --->"+Constants.ORGANISATION_CONTROLLER);
		return response;

	}

	// get All Organisations For Admin
	@RequestMapping(value = RestServiceURIConstant.GET_ALL_ORGANISATIONS_FOR_ADMIN, method = RequestMethod.GET)
	public UserResponse<List<OrganisationProfileDto>> getAllOrganisationsForAdmin() {
		// log.info("Inside getAllOrganisationsForAdmin() method of
		// ---->"+Constants.ORGANISATION_CONTROLLER);
		UserResponse<List<OrganisationProfileDto>> response = null;
		OutputDTO<List<OrganisationProfileDto>> outputDTO = null;
		response = new UserResponse<List<OrganisationProfileDto>>();
		try {
			// log.info("Inside getAllOrganisationsForAdmin() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getAllOrganisationsForAdmin() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			List<OrganisationProfileDto> organisationList = organisationService.getAllOrganisationListForAdmin();
			if (Format.isCollectionNotEmptyAndNotNull(organisationList)) {
				outputDTO = new OutputDTO<List<OrganisationProfileDto>>();
				outputDTO.setResponseData(organisationList);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("service executed  Successfully");
			}
		} catch (Exception e) {
			log.error("exception in getAllOrganisations " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Failed while retriving all organizations.");
		}
		// log.info("Exiting getAllOrganisationsForAdmin() method of
		// --->"+Constants.ORGANISATION_CONTROLLER);
		return response;
	}

	// get Admin Audit Log List By Dates
	@RequestMapping(value = RestServiceURIConstant.GET_ADMIN_LOGIN_AUDIT_BY_DATES, method = RequestMethod.POST)
	public UserResponse<List<LoginAuditLogDTO>> getAdminAuditLogListByDates(
			@RequestBody AuditLogInputDto reportsInputDto) {
		// log.info("Inside getAdminAuditLogListByDates() method of
		// ---->"+Constants.ORGANISATION_CONTROLLER+Constants.BEGIN_REQUEST+"--->"+reportsInputDto);
		UserResponse<List<LoginAuditLogDTO>> response = null;
		OutputDTO<List<LoginAuditLogDTO>> outputDTO = null;
		response = new UserResponse<List<LoginAuditLogDTO>>();
		try {
			// log.info("Inside getAdminAuditLogListByDates() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getAdminAuditLogListByDates() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			List<LoginAuditLogDTO> LoginAuditDtoList = auditLogService.getAdminAuditLogListByDates(reportsInputDto);
			if (Format.isCollectionNotEmptyAndNotNull(LoginAuditDtoList)) {
				outputDTO = new OutputDTO<List<LoginAuditLogDTO>>();
				outputDTO.setResponseData(LoginAuditDtoList);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("service executed  Successfully");
			}
		} catch (Exception e) {
			log.error("exception in getAdminAuditLogListByDates " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Failed while retriving login audits!");
		}
		// log.info("Exiting getAdminAuditLogListByDates() method of
		// --->"+Constants.ORGANISATION_CONTROLLER);
		return response;
	}

	// create Organization
	@RequestMapping(value = RestServiceURIConstant.CREATE_ORGANIZATION, method = RequestMethod.POST)
	public UserResponse<Boolean> createOrganization(@RequestBody OrganisationProfileDto organisationProfileDto) {
		// log.info("Inside createOrganization() method of
		// ---->"+Constants.ORGANISATION_CONTROLLER+Constants.BEGIN_REQUEST+"--->"+organisationProfileDto);
		UserResponse<Boolean> response = null;
		response = new UserResponse<>();
		OutputDTO<Boolean> results = null;

		try {
			// log.info("Inside createOrganization() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside createOrganization() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			int result = organisationService.updateOrganizationDetails(organisationProfileDto);
			if (result != 0) {
				results = new OutputDTO<>();
				// log.info("Organization Created");
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("The Organization Created.");
				response.setOrgId(result);
				response.setResult(results);
			} else {
				results = new OutputDTO<>();
				// log.info("Duplicate Organization");
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage("Duplicate Organization.");
				response.setResult(results);
			}

		} catch (Exception e) {
			results = new OutputDTO<>();
			log.error("CreateOrganization: Exception occurred");
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Exception while createing Organization");
			response.setResult(results);
		}
		// log.info("Exiting createOrganization() method of
		// --->"+Constants.ORGANISATION_CONTROLLER);
		return response;

	}

	// update Organization
	@RequestMapping(value = RestServiceURIConstant.UPDATE_ORGANIZATION, method = RequestMethod.POST)
	public UserResponse<Boolean> updateOrganization(@RequestBody OrganisationProfileDto organisationProfileDto) {
		// log.info("Inside updateOrganization() method of
		// ---->"+Constants.ORGANISATION_CONTROLLER+Constants.BEGIN_REQUEST+"--->"+organisationProfileDto);
		UserResponse<Boolean> response = null;
		response = new UserResponse<>();
		OutputDTO<Boolean> results = null;

		try {
			// log.info("Inside updateOrganization() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside updateOrganization() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			String result = organisationService.UpdateOrganization(organisationProfileDto);
			if (result != null && result.equals("org Updated")) {
				results = new OutputDTO<>();
				// log.info("Organization Updated");
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("The Organization Updated.");
				response.setResult(results);
			} else {
				results = new OutputDTO<>();
				// log.info("Duplicate Organization");
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage("Duplicate Organization.");
				response.setResult(results);
			}

		} catch (Exception e) {
			results = new OutputDTO<>();
			log.error("UpdateOrganization: Exception occurred" + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Exception while Update Organization");
			response.setResult(results);
		}
		// log.info("Exiting updateOrganization() method of
		// --->"+Constants.ORGANISATION_CONTROLLER);
		return response;

	}

	// get Organization List
	@RequestMapping(value = RestServiceURIConstant.GET_ORGANIZATIONLIST, method = RequestMethod.GET)
	public UserResponse<OrganizationResponse> getOrgList() {
		// log.info("Inside getOrgList() method of
		// ---->"+Constants.ORGANISATION_CONTROLLER);
		UserResponse<OrganizationResponse> response = null;
		OutputDTO<OrganizationResponse> result = null;
		response = new UserResponse<>();
		OrganizationResponse orgList = new OrganizationResponse();
		try {
			// log.info("Inside getOrgList() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getOrgList() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			orgList = organisationService.getOrgList();
			if (orgList != null) {
				result = new OutputDTO<>();
				result.setResponseData(orgList);
				response.setResponseCode("200");
				response.setResponseMessage("Org List Success");
				response.setResult(result);
			}
		} catch (Exception e) {
			log.error("exception in orgList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		}
		// log.info("Exiting getOrgList() method of
		// --->"+Constants.ORGANISATION_CONTROLLER);
		return response;
	}

	// get Organization Details
	@RequestMapping(value = RestServiceURIConstant.GET_ORGANIZATION_DETAILS, method = RequestMethod.POST)
	public UserResponse<OrganizationResponseByName> getOrgDetails(@RequestParam String orgName) {
		// log.info("Inside getOrgDetails() method of
		// ---->"+Constants.ORGANISATION_CONTROLLER+Constants.BEGIN_REQUEST+"--->"+orgName);
		UserResponse<OrganizationResponseByName> response = null;
		OutputDTO<OrganizationResponseByName> result = null;
		response = new UserResponse<>();
		OrganizationResponseByName orgDetails = new OrganizationResponseByName();
		try {
			// log.info("Inside getOrgDetails() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getOrgDetails() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			orgDetails = organisationService.getOrgDetails(orgName);
			if (orgDetails != null) {
				result = new OutputDTO<>();
				result.setResponseData(orgDetails);
				response.setResponseCode("200");
				response.setResponseMessage("Org Details Success");
				response.setResult(result);
			}
		} catch (Exception e) {
			log.error("exception in OrgDetails " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);

		}
		// log.info("Exiting getOrgDetails() method of
		// --->"+Constants.ORGANISATION_CONTROLLER);
		return response;
	}

	// get All User By Organisation
	@RequestMapping(value = RestServiceURIConstant.GET_All_USERS_BY_ORGANIZATION_NAME, method = RequestMethod.GET)
	public UserResponse<List<UserDto>> getAllUserByOrganisation(@PathVariable(value = "orgName") String orgName) {
		// log.info("Inside getAllUserByOrganisation() method of
		// ---->"+Constants.ORGANISATION_CONTROLLER+Constants.BEGIN_REQUEST+"--->"+orgName);
		UserResponse<List<UserDto>> response = null;
		OutputDTO<List<UserDto>> outputDTO = null;
		response = new UserResponse<List<UserDto>>();
		try {
			// log.info("Inside getAllUserByOrganisation() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getAllUserByOrganisation() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			List<UserDto> userDtoList = organisationService.getAllUserByOrganisation(orgName);
			if (Format.isCollectionNotEmptyAndNotNull(userDtoList)) {
				outputDTO = new OutputDTO<List<UserDto>>();
				outputDTO.setResponseData(userDtoList);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("service executed  Successfully");
			}
		} catch (Exception e) {
			log.error("exception in getAllUserByOrganisation " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Failed while retriving all organisation users");

		}
		// log.info("Exiting getAllUserByOrganisation() method of
		// --->"+Constants.ORGANISATION_CONTROLLER);
		return response;
	}

	// get Organization Detail
	@RequestMapping(value = RestServiceURIConstant.GET_ORGANIZATION_DETAIL, method = RequestMethod.POST)
	public UserResponse<OrganizationResponseByName> getOrgDetail(@RequestParam String orgName) {
		// log.info("Inside getOrgDetails() method of
		// ---->"+Constants.ORGANISATION_CONTROLLER+Constants.BEGIN_REQUEST+"--->"+orgName);

		UserResponse<OrganizationResponseByName> response = null;
		OutputDTO<OrganizationResponseByName> result = null;
		response = new UserResponse<>();
		OrganizationResponseByName orgDetails = new OrganizationResponseByName();
		try {
			// log.info("Inside getOrgDetails() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getOrgDetails() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			orgDetails = organisationService.getOrgDetail(orgName);
			// check Organization details
			if (orgDetails != null) {
				result = new OutputDTO<>();
				result.setResponseData(orgDetails);
				response.setResponseCode("200");
				response.setResponseMessage("Org Details Success");
				response.setResult(result);
			}
		} catch (Exception e) {
			log.error("exception in OrgDetails " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);

		}
		// log.info("Exiting getOrgDetails() method of
		// --->"+Constants.ORGANISATION_CONTROLLER);
		return response;
	}

	// get Organization List
	@RequestMapping(value = RestServiceURIConstant.GET_ORGANIZATIONLISTS, method = RequestMethod.GET)
	public UserResponse<List<OrganizationListsResponse>> getOrgLists() {
		// log.info("Inside getOrgLists() method of
		// ---->"+Constants.ORGANISATION_CONTROLLER);

		UserResponse<List<OrganizationListsResponse>> response = null;
		OutputDTO<List<OrganizationListsResponse>> outputDTO = null;
		response = new UserResponse<List<OrganizationListsResponse>>();
		List<OrganizationListsResponse> orgList = null;
		try {
			// log.info("Inside getOrgLists() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getOrgLists() method of
			// "+Constants.ORGANISATION_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			orgList = organisationService.getOrgLists();
			if (orgList != null) {
				outputDTO = new OutputDTO<List<OrganizationListsResponse>>();
				outputDTO.setResponseData(orgList);
				response.setResponseCode("200");
				response.setResponseMessage("Org List Success");
				response.setResult(outputDTO);
			}
		} catch (Exception e) {
			log.error("exception in orgList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		}
		// log.info("Exiting getOrgLists() method of
		// --->"+Constants.ORGANISATION_CONTROLLER);
		return response;
	}

}

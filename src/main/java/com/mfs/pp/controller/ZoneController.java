package com.mfs.pp.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.mdm.common.dto.TimeZoneDto;
import com.mfs.pp.dto.OutputDTO;
import com.mfs.pp.dto.UserResponse;
import com.mfs.pp.dto.Zones;
import com.mfs.pp.dto.ZonesResponse;
import com.mfs.pp.service.CountryZoneService;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.Format;
import com.mfs.pp.util.RestServiceURIConstant;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */
@RestController
public class ZoneController extends BaseController {

	@Autowired
	CountryZoneService countryZoneservice;

	protected final Log log = LogFactory.getLog(getClass());

	//create Zone
	@RequestMapping(value = RestServiceURIConstant.CREATE_ZONE, method = RequestMethod.POST)
	public UserResponse<Boolean> createZone(@RequestBody Zones zones) {
		//log.info("Inside createZone() method of ---->"+Constants.ZONE_CONTROLLER+Constants.BEGIN_REQUEST+"--->"+zones);
		UserResponse<Boolean> response = null;
		response = new UserResponse<>();
		OutputDTO<Boolean> results = null;

		try {
			//log.info("Inside createZone() method of "+Constants.ZONE_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside createZone() method of "+Constants.ZONE_CONTROLLER+"--->"+Constants.AFTER_TOKEN);

			String result = countryZoneservice.createZoneService(zones);
			if (result != null && result.equals(Constants.ZONE_CREATION)) {
				results = new OutputDTO<>();
				//log.info(Constants.ZONE_CREATION);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage(Constants.ZONE_CREATION);
				response.setResult(results);
			} else {
				results = new OutputDTO<>();
				//log.info(Constants.ZONE_DUPLICATE);
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage(Constants.ZONE_DUPLICATE);
				response.setResult(results);
			}

		} catch (Exception e) {
			results = new OutputDTO<>();
			log.error("CreateZone: Exception occurred"+e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Exception while createing Zone");
			response.setResult(results);
		}
		//log.info("Exiting createZone() method of --->"+Constants.ZONE_CONTROLLER);
		return response;
	}

	//update Zone
	@RequestMapping(value = RestServiceURIConstant.UPDATE_ZONE, method = RequestMethod.POST)
	public UserResponse<Boolean> updateZone(@RequestBody Zones zones) {

		//log.info("Inside updateZone() method of ---->"+Constants.ZONE_CONTROLLER+Constants.BEGIN_REQUEST+"--->"+zones);
		UserResponse<Boolean> response = null;
		response = new UserResponse<>();
		OutputDTO<Boolean> results = null;

		try {
			//log.info("Inside updateZone() method of "+Constants.ZONE_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside updateZone() method of "+Constants.ZONE_CONTROLLER+"--->"+Constants.AFTER_TOKEN);

			String result = countryZoneservice.udpateByZone(zones);
			if (result != null && result.equals(Constants.ZONE_UPDATE)) {
				results = new OutputDTO<>();
				//log.info(Constants.ZONE_UPDATE);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage(Constants.ZONE_UPDATE);
				response.setResult(results);
			} else {
				results = new OutputDTO<>();
				//log.info(Constants.ZONE_DUPLICATE);
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage(Constants.ZONE_DUPLICATE);
				response.setResult(results);
			}

		} catch (Exception e) {
			results = new OutputDTO<>();
			log.error("updateZone: Exception occurred"+e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Exception while Updateing Zone");
			response.setResult(results);
		}
		//log.info("Exiting updateZone() method of --->"+Constants.ZONE_CONTROLLER);
		return response;
	}

	//get Zone List
	@RequestMapping(value = RestServiceURIConstant.ZONE_LIST, method = RequestMethod.GET)
	public UserResponse<ZonesResponse> getZoneList() {

		//log.info("Inside getZoneList() method of ---->"+Constants.ZONE_CONTROLLER);

		UserResponse<ZonesResponse> response = null;
		OutputDTO<ZonesResponse> result = null;
		response = new UserResponse<>();
		ZonesResponse zoneList = new ZonesResponse();

		try {
			//log.info("Inside getZoneList() method of "+Constants.ZONE_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside getZoneList() method of "+Constants.ZONE_CONTROLLER+"--->"+Constants.AFTER_TOKEN);

			zoneList = countryZoneservice.getAllZone();
			//check zone list
			if (zoneList != null) {
				result = new OutputDTO<>();
				result.setResponseData(zoneList);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage(Constants.ZONE_LIST);
				response.setResult(result);

			}

		} catch (Exception e) {
			log.error("exception in getZoneList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		}
		//log.info("Exiting getZoneList() method of --->"+Constants.ZONE_CONTROLLER);

		return response;
	}

	//get All Time Zone Dto List
	@RequestMapping(value = RestServiceURIConstant.GET_All_TIMEZONELIST, method = RequestMethod.GET)
	public UserResponse<List<TimeZoneDto>> getAllTimeZoneDtoList() {
		//log.info("Inside getAllTimeZoneDtoList() method of ---->"+Constants.ZONE_CONTROLLER);
		UserResponse<List<TimeZoneDto>> response = null;
		response = new UserResponse<List<TimeZoneDto>>();
		try {
			//log.info("Inside getAllTimeZoneDtoList() method of "+Constants.ZONE_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
//			log.info("Inside getAllTimeZoneDtoList() method of "+Constants.ZONE_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			List<TimeZoneDto> timeZoneDtoList = userService.getAllTimeZoneDtoList();
			OutputDTO<List<TimeZoneDto>> outputDTO = new OutputDTO<List<TimeZoneDto>>();
			outputDTO.setResponseData(timeZoneDtoList);
			response.setResult(outputDTO);
			if (Format.isCollectionNotEmptyAndNotNull(timeZoneDtoList)) {
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("service executed  Successfully");
			} else {
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage("Fail");
			}
		} catch (Exception e) {
			log.error("exception in getAllUserByOrganisation " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Fail!");
		}
		//log.info("Exiting getAllTimeZoneDtoList() method of --->"+Constants.ZONE_CONTROLLER);
		return response;

	}

}

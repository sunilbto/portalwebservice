package com.mfs.pp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.pp.dto.MoneyFlowDto;
import com.mfs.pp.dto.OutputDTO;
import com.mfs.pp.dto.UserResponse;
import com.mfs.pp.service.MoneyFlowService;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */
@RestController
public class MoneyFlowController extends BaseController{

	private static final Logger log = LoggerFactory.getLogger(MoneyFlowController.class);
	
	@Autowired
	MoneyFlowService moneyFlowService;
	
	//get Money Flow Info
	@RequestMapping(value = RestServiceURIConstant.MONEY_FLOWS, method = RequestMethod.POST)
	public UserResponse<Object> getMoneyFlowInfo(@RequestBody MoneyFlowDto moneyFlowDto) {
		//log.info("Inside getMoneyFlowInfo() method of ---->"+Constants.MONEYFLOW_CONTROLLER+"--->"+Constants.BEGIN_REQUEST+"--->"+moneyFlowDto);
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();
		Object responses = null;
		try {
			//log.info("Inside getMoneyFlowInfo() method of "+Constants.MONEYFLOW_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside getMoneyFlowInfo() method of "+Constants.MONEYFLOW_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			responses = moneyFlowService.getMoneyFlows(moneyFlowDto);
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("MoneyFlowInfo Information");
		} catch (Exception e) {
			log.error("exception in geting MoneyFlow Information " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		
		}
		//log.info("Exiting getMoneyFlowInfo() method of -->"+Constants.MONEYFLOW_CONTROLLER);
		return response;
	}

}

package com.mfs.pp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.mfs.mdm.common.dto.UserDto;
import com.mfs.pp.auth.jwt.JwtTokenUtil;
import com.mfs.pp.service.UserService;
import com.mfs.pp.service.impl.CustomUserDetailsService;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Component
public class BaseController {

	@Autowired
	protected UserService userService;
	
	@Autowired
	protected CustomUserDetailsService customUserDetailsService;
	
	 @Autowired
	 protected JwtTokenUtil jwtTokenUtil;
	 
	//protected final Log log = LogFactory.getLog(getClass());
	/**
	 * @return userDto
	 * @throws Exception
	 * get Logged In Users 
	 */
	public UserDto getLoggedInUsersDto() throws Exception {
		//log.info("Inside getLoggedInUsersDto() method of BaseController");
		UserDetails activeUser = customUserDetailsService.getCurrentUser();
		UserDto userDto = null;
		if (null != activeUser && null != activeUser.getUsername()) {
			userDto = userService.getUserByUserEmailId(activeUser.getUsername());
		}
		//log.info("Exiting getLoggedInUsersDto() method of BaseController");
		return userDto;
	}
}

package com.mfs.pp.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.pp.dto.MenuDTO;
import com.mfs.pp.dto.OutputDTO;
import com.mfs.pp.dto.Role;
import com.mfs.pp.dto.RoleAssignList;
import com.mfs.pp.dto.RoleAssignRequest;
import com.mfs.pp.dto.RoleDTO;
import com.mfs.pp.dto.UserResponse;
import com.mfs.pp.service.RoleService;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */
@RestController
public class RoleController extends BaseController {

	@Autowired
	private RoleService roleService;

	protected final Log log = LogFactory.getLog(getClass());

	//get Menu List
	@RequestMapping(value = RestServiceURIConstant.GET_MENU_LIST, method = RequestMethod.GET)
	public UserResponse<List<MenuDTO>> getMenuList() {
		//log.info("Inside getMenuList() method of ---->" + Constants.ROLE_CONTROLLER);
		UserResponse<List<MenuDTO>> response = new UserResponse<>();
		OutputDTO<List<MenuDTO>> outputDTO = new OutputDTO<>();
		try {
			//log.info("Inside getMenuList() method of " + Constants.ROLE_CONTROLLER + "--->" + Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside getMenuList() method of " + Constants.ROLE_CONTROLLER + "--->" + Constants.AFTER_TOKEN);
			List<MenuDTO> menuDTOList = roleService.getMenuList();
			outputDTO.setResponseData(menuDTOList);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Menu List Success");
		} catch (Exception e) {
			log.error("exception in getMenuList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting getMenuList() method of --->" + Constants.ROLE_CONTROLLER);
		return response;
	}

	//get Admin Role Detail
	@RequestMapping(value = RestServiceURIConstant.GET_ADMIN_ROLE_DETAIL, method = RequestMethod.POST)
	public UserResponse<RoleDTO> getAdminRoleDetail(@RequestBody Role role) {
		//log.info("Inside getAdminRoleDetail() method of ---->" + Constants.ROLE_CONTROLLER + Constants.BEGIN_REQUEST+ "--->" + role);
		UserResponse<RoleDTO> response = new UserResponse<>();
		OutputDTO<RoleDTO> outputDTO = new OutputDTO<>();
		try {
			//log.info("Inside getAdminRoleDetail() method of " + Constants.ROLE_CONTROLLER + "--->"+ Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside getAdminRoleDetail() method of " + Constants.ROLE_CONTROLLER + "--->"+ Constants.AFTER_TOKEN);
			RoleDTO roleDTO = roleService.getAdminRoleDetail(role.getRoleId());
			outputDTO.setResponseData(roleDTO);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Admin Role Detail Success");
		} catch (Exception e) {
			log.error("exception in getAdminRoleDetail " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		
		}
		//log.info("Exiting getAdminRoleDetail() method of --->" + Constants.ROLE_CONTROLLER);
		return response;
	}

	//get Organization Role Detail
	@RequestMapping(value = RestServiceURIConstant.GET_ORG_ROLE_DETAIL, method = RequestMethod.POST)
	public UserResponse<RoleDTO> getOrgRoleDetail(@RequestBody Role role) {
		//log.info("Inside getOrgRoleDetail() method of ---->" + Constants.ROLE_CONTROLLER + Constants.BEGIN_REQUEST+ "--->" + role);
		UserResponse<RoleDTO> response = new UserResponse<>();
		OutputDTO<RoleDTO> outputDTO = new OutputDTO<>();
		try {
			//log.info("Inside getOrgRoleDetail() method of " + Constants.ROLE_CONTROLLER + "--->"+ Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside getOrgRoleDetail() method of " + Constants.ROLE_CONTROLLER + "--->"+ Constants.AFTER_TOKEN);
			RoleDTO roleDTO = roleService.getOrgRoleDetail(role.getRoleId());
			outputDTO.setResponseData(roleDTO);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Org Role Detail Success");
		} catch (Exception e) {
			log.error("exception in getOrgRoleDetail " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting getOrgRoleDetail() method of --->" + Constants.ROLE_CONTROLLER);
		return response;
	}

	//admin Role List
	@RequestMapping(value = RestServiceURIConstant.GET_ADMIN_ROLE_LIST, method = RequestMethod.GET)
	public UserResponse<List<RoleDTO>> adminRoleList() {
		//log.info("Inside adminRoleList() method of ---->" + Constants.ROLE_CONTROLLER);
		UserResponse<List<RoleDTO>> response = new UserResponse<>();
		OutputDTO<List<RoleDTO>> outputDTO = new OutputDTO<>();
		try {
			
			//log.info("Inside adminRoleList() method of " + Constants.ROLE_CONTROLLER + "--->" + Constants.AFTER_TOKEN);
			List<RoleDTO> roleDTOList = roleService.getListOfAdminRole();
			outputDTO.setResponseData(roleDTOList);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Admin Role List Success");
		} catch (Exception e) {
			log.error("exception in adminRoleList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting adminRoleList() method of --->" + Constants.ROLE_CONTROLLER);
		return response;
	}

	//organization Role List
	@RequestMapping(value = RestServiceURIConstant.GET_ORG_ROLE_LIST, method = RequestMethod.GET)
	public UserResponse<List<RoleDTO>> orgRoleList(@RequestParam int orgId) {
		//log.info("Inside orgRoleList() method of ---->" + Constants.ROLE_CONTROLLER);
		UserResponse<List<RoleDTO>> response = new UserResponse<>();

		OutputDTO<List<RoleDTO>> outputDTO = new OutputDTO<>();
		try {
			//log.info("Inside orgRoleList() method of " + Constants.ROLE_CONTROLLER + "--->" + Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside orgRoleList() method of " + Constants.ROLE_CONTROLLER + "--->" + Constants.AFTER_TOKEN);
			List<RoleDTO> roleDTOList = roleService.getListOfOrgRole(orgId);
			outputDTO.setResponseData(roleDTOList);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Org Role List Success");
		} catch (Exception e) {
			log.error("exception in orgRoleList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		
		}
		//log.info("Exiting orgRoleList() method of --->" + Constants.ROLE_CONTROLLER);
		return response;
	}

	//create Admin Role
	@RequestMapping(value = RestServiceURIConstant.CREATE_ADMIN_ROLE, method = RequestMethod.POST)
	public UserResponse<List<RoleDTO>> createAdminRole(@RequestBody RoleDTO roleDTO) {
		//log.info("Inside createAdminRole() method of ---->" + Constants.ROLE_CONTROLLER + Constants.BEGIN_REQUEST+ "--->" + roleDTO);
		UserResponse<List<RoleDTO>> response = new UserResponse<>();
		OutputDTO<List<RoleDTO>> outputDTO = new OutputDTO<>();
		try {
			//log.info("Inside createAdminRole() method of " + Constants.ROLE_CONTROLLER + "--->"+ Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside createAdminRole() method of " + Constants.ROLE_CONTROLLER + "--->" + Constants.AFTER_TOKEN);
			roleService.createAdminRole(roleDTO);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Admin Role Creation Success");
		} catch (Exception e) {
			log.error("exception in createAdminRole " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		
		}
		//log.info("Exiting createAdminRole() method of --->" + Constants.ROLE_CONTROLLER);
		return response;
	}

	//create Organization Role
	@RequestMapping(value = RestServiceURIConstant.CREATE_ORG_ROLE, method = RequestMethod.POST)
	public UserResponse<List<RoleDTO>> createOrgRole(@RequestBody RoleDTO roleDTO) {
		//log.info("Inside createOrgRole() method of ---->" + Constants.ROLE_CONTROLLER + Constants.BEGIN_REQUEST + "--->"+ roleDTO);
		UserResponse<List<RoleDTO>> response = new UserResponse<>();
		OutputDTO<List<RoleDTO>> outputDTO = new OutputDTO<>();
		try {
			//log.info("Inside createOrgRole() method of " + Constants.ROLE_CONTROLLER + "--->" + Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside createOrgRole() method of " + Constants.ROLE_CONTROLLER + "--->" + Constants.AFTER_TOKEN);
			roleService.createOrgRole(roleDTO);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Org Role Creation Success");
		} catch (Exception e) {
			log.error("exception in createAdminRole " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting createOrgRole() method of --->" + Constants.ROLE_CONTROLLER);
		return response;
	}

	//update Organization Role
	@RequestMapping(value = RestServiceURIConstant.UPDATE_ORG_ROLE, method = RequestMethod.POST)
	public UserResponse<List<RoleDTO>> updateOrgRole(@RequestBody RoleDTO roleDTO) {
		//log.info("Inside updateOrgRole() method of ---->" + Constants.ROLE_CONTROLLER + Constants.BEGIN_REQUEST + "--->"+ roleDTO);
		UserResponse<List<RoleDTO>> response = new UserResponse<>();
		OutputDTO<List<RoleDTO>> outputDTO = new OutputDTO<>();
		try {
			//log.info("Inside updateOrgRole() method of " + Constants.ROLE_CONTROLLER + "--->" + Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside updateOrgRole() method of " + Constants.ROLE_CONTROLLER + "--->" + Constants.AFTER_TOKEN);
			roleService.updateOrgRole(roleDTO);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Org Role Update Success");
		} catch (Exception e) {
			log.error("exception in createAdminRole " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting updateOrgRole() method of --->" + Constants.ROLE_CONTROLLER);
		return response;
	}

	//update Admin Role
	@RequestMapping(value = RestServiceURIConstant.UPDATE_ADMIN_ROLE, method = RequestMethod.POST)
	public UserResponse<List<RoleDTO>> updateAdminRole(@RequestBody RoleDTO roleDTO) {
		//log.info("Inside updateAdminRole() method of ---->" + Constants.ROLE_CONTROLLER + Constants.BEGIN_REQUEST+ "--->" + roleDTO);
		UserResponse<List<RoleDTO>> response = new UserResponse<>();
		OutputDTO<List<RoleDTO>> outputDTO = new OutputDTO<>();
		try {
			//log.info("Inside updateAdminRole() method of " + Constants.ROLE_CONTROLLER + "--->"+ Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside updateAdminRole() method of " + Constants.ROLE_CONTROLLER + "--->" + Constants.AFTER_TOKEN);
			roleService.updateAdminRole(roleDTO);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Admin Role Update Success");
		} catch (Exception e) {
			log.error("exception in updateAdminRole " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			}
		//log.info("Exiting updateAdminRole() method of --->" + Constants.ROLE_CONTROLLER);
		return response;
	}

	//delete Organization Role
	@RequestMapping(value = RestServiceURIConstant.DELETE_ORG_ROLE, method = RequestMethod.GET)
	public UserResponse<Boolean> deleteOrgRole(@RequestParam int roleId) {
		//log.info("Inside deleteRole() method of ---->" + Constants.ROLE_CONTROLLER);
		UserResponse<Boolean> response = new UserResponse<>();
		OutputDTO<Boolean> outputDTO = new OutputDTO<>();
		try {
			//log.info("Inside deleteRole() method of " + Constants.ROLE_CONTROLLER + "--->" + Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside deleteRole() method of " + Constants.ROLE_CONTROLLER + "--->" + Constants.AFTER_TOKEN);
			boolean flag = roleService.deleteOrgRole(roleId);
			if (flag) {
				outputDTO.setResponseData(flag);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("Delete Org Role Success");
			} else {
				outputDTO.setResponseData(flag);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage("Delete Org Role Fail");
			}
		} catch (Exception e) {
			log.error("exception in deleteRole " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		
		}
		//log.info("Exiting deleteRole() method of --->" + Constants.ROLE_CONTROLLER);
		return response;
	}

	//delete Role Admin
	@RequestMapping(value = RestServiceURIConstant.DELETE_ADMIN_ROLE, method = RequestMethod.GET)
	public UserResponse<Boolean> deleteRoleAdmin(@RequestParam int roleId) {
		//log.info("Inside deleteRoleAdmin() method of ---->" + Constants.ROLE_CONTROLLER);
		UserResponse<Boolean> response = new UserResponse<>();
		OutputDTO<Boolean> outputDTO = new OutputDTO<>();
		try {
			//log.info("Inside deleteRoleAdmin() method of " + Constants.ROLE_CONTROLLER + "--->"+ Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside deleteRoleAdmin() method of " + Constants.ROLE_CONTROLLER + "--->" + Constants.AFTER_TOKEN);
			boolean flag = roleService.deleteAdminRole(roleId);
			//check flag
			if (flag) {
				outputDTO.setResponseData(flag);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("Delete Admin Role Success");
			} else {
				outputDTO.setResponseData(flag);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage("Delete Admin Role Fail");
			}
		} catch (Exception e) {
			log.error("exception in deleteRole " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting deleteRoleAdmin() method of --->" + Constants.ROLE_CONTROLLER);
		return response;
	}

	//get Role Assign
	@RequestMapping(value = RestServiceURIConstant.ROLE_ASSIGN, method = RequestMethod.POST)
	public UserResponse<RoleAssignList> getRoleAssign(@RequestBody RoleAssignRequest roleAssignRequest) {
		//log.info("Inside getRoleAssign() method of ---->" + Constants.ROLE_CONTROLLER);
		UserResponse<RoleAssignList> response = new UserResponse<>();
		OutputDTO<RoleAssignList> outputDTO = new OutputDTO<>();
		RoleAssignList roleAssignList = null;
		try {
			//log.info("Inside getRoleAssign() method of " + Constants.ROLE_CONTROLLER + "--->" + Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside getRoleAssign() method of " + Constants.ROLE_CONTROLLER + "--->" + Constants.AFTER_TOKEN);
			roleAssignList = roleService.getRoleAssign(roleAssignRequest);
			//check role assign list
			if (roleAssignList != null) {
				outputDTO.setResponseData(roleAssignList);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("getAssign Role Success");
			} else {
				outputDTO.setResponseData(roleAssignList);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage("getAssign Role Fail");
			}
		} catch (Exception e) {
			log.error("exception in getRoleAssign " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting getRoleAssign() method of --->" + Constants.ROLE_CONTROLLER);
		return response;
	}

}

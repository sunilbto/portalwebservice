package com.mfs.pp.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.pp.dto.Groups;
import com.mfs.pp.dto.GroupsResponse;
import com.mfs.pp.dto.OutputDTO;
import com.mfs.pp.dto.UserResponse;
import com.mfs.pp.service.GroupOrgService;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */
@RestController
public class GroupController extends BaseController {

	@Autowired
	GroupOrgService groupOrgService;

	protected final Log log = LogFactory.getLog(getClass());

	//create Group
	@RequestMapping(value = RestServiceURIConstant.CREATE_GROUP, method = RequestMethod.POST)
	public UserResponse<Boolean> createGroup(@RequestBody Groups groups) {
		//log.info("Inside createGroup() method of ---->" + Constants.GROUP_CONTROLLER + Constants.BEGIN_REQUEST + "--->"+ groups);
		UserResponse<Boolean> response = null;
		response = new UserResponse<>();
		OutputDTO<Boolean> results = null;

		try {
			//log.info("Inside createGroup() method of " + Constants.GROUP_CONTROLLER + "--->" + Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside createGroup() method of " + Constants.GROUP_CONTROLLER + "--->" + Constants.AFTER_TOKEN);

			String result = groupOrgService.createGroup(groups);
			if (result != null && result.equals(Constants.GROUP_CREATION)) {
				results = new OutputDTO<>();
				//log.info(Constants.GROUP_CREATION);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage(Constants.GROUP_CREATION);
				response.setResult(results);
			} else {
				results = new OutputDTO<>();
				//log.info(Constants.GROUP_DUPLICATE);
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage(Constants.GROUP_DUPLICATE);
				response.setResult(results);

			}
		} catch (Exception e) {
			results = new OutputDTO<>();
			log.error("CreateGroup: Exception occurred");
			
			response.setResponseCode(Constants.ERROR_CODE);
			
			response.setResult(results);
		}
		//log.info("Exiting createGroup() method of --->" + Constants.GROUP_CONTROLLER);
		return response;
	}

	//update Group
	@RequestMapping(value = RestServiceURIConstant.UPDATE_GROUP, method = RequestMethod.POST)
	public UserResponse<Boolean> updateGroup(@RequestBody Groups groups) {

		//log.info("Inside updateGroup() method of ---->" + Constants.GROUP_CONTROLLER + Constants.BEGIN_REQUEST + "--->"+ groups);
		UserResponse<Boolean> response = null;
		response = new UserResponse<>();
		OutputDTO<Boolean> results = null;

		try {
			//log.info("Inside updateGroup() method of " + Constants.GROUP_CONTROLLER + "--->" + Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside updateGroup() method of " + Constants.GROUP_CONTROLLER + "--->" + Constants.AFTER_TOKEN);

			String result = groupOrgService.updateByGroup(groups);
			if (result != null && result.equals(Constants.GROUP_UPDATE)) {
				results = new OutputDTO<>();
				//log.info(Constants.GROUP_UPDATE);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage(Constants.GROUP_UPDATE);
				response.setResult(results);
			} else {
				results = new OutputDTO<>();
				//log.info(Constants.GROUP_DUPLICATE);
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage(Constants.GROUP_DUPLICATE);
				response.setResult(results);
			}

		} catch (Exception e) {
			results = new OutputDTO<>();
			log.error("updateGroup: Exception occurred");
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResult(results);
		}
		//log.info("Exiting updateGroup() method of --->" + Constants.GROUP_CONTROLLER);
		return response;
	}

	//get Group List
	@RequestMapping(value = RestServiceURIConstant.GROUP_LIST, method = RequestMethod.GET)
	public UserResponse<GroupsResponse> getGroupList() {

		//log.info("Inside getGroupList() method of ---->" + Constants.GROUP_CONTROLLER);
		UserResponse<GroupsResponse> response = null;
		OutputDTO<GroupsResponse> result = null;
		response = new UserResponse<>();
		GroupsResponse groupList = new GroupsResponse();

		try {
			//log.info("Inside getGroupList() method of " + Constants.GROUP_CONTROLLER + "--->" + Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside getGroupList() method of " + Constants.GROUP_CONTROLLER + "--->" + Constants.AFTER_TOKEN);

			groupList = groupOrgService.getAllGroup();
			//check list of group
			if (groupList != null) {
				result = new OutputDTO<>();
				result.setResponseData(groupList);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage(Constants.GROUP_LIST);
				response.setResult(result);

			}

		} catch (Exception e) {
			log.error("exception in getGroupList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		
		}
		//log.info("Exiting getGroupList() method of --->" + Constants.GROUP_CONTROLLER);

		return response;
	}

}
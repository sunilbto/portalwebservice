package com.mfs.pp.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.pp.dto.CategoryResponse;
import com.mfs.pp.dto.OutputDTO;
import com.mfs.pp.dto.StatusResponse;
import com.mfs.pp.dto.UserResponse;
import com.mfs.pp.service.CommonService;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */
@RestController
public class CommonController extends BaseController {

	protected final Log log = LogFactory.getLog(getClass());

	@Autowired
	private CommonService commonService;

	//get Category List
	@RequestMapping(value = RestServiceURIConstant.GET_ALL_CATEGORY_LIST, method = RequestMethod.GET)
	public UserResponse<List<CategoryResponse>> getCategoryList() {
		//log.info("Inside getCategoryList() method of ---->"+Constants.COMMON_CONTROLLER+"--->");
		UserResponse<List<CategoryResponse>> response = new UserResponse<>();
		OutputDTO<List<CategoryResponse>> outputDTO = new OutputDTO<>();
		try {
			//log.info("Inside getCategoryList() method of "+Constants.COMMON_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside getCategoryList() method of "+Constants.COMMON_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			List<CategoryResponse> categoryDTOList = commonService.getCategoryList();
			outputDTO.setResponseData(categoryDTOList);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("category List Success");
		} catch (Exception e) {
			log.error("exception in categoryList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting getCategoryList() method of -->"+Constants.COMMON_CONTROLLER);
		return response;
	}

	//get Status List
	@RequestMapping(value = RestServiceURIConstant.GET_ALL_STATUS_LIST, method = RequestMethod.GET)
	public UserResponse<List<StatusResponse>> getStatusList() {
		//log.info("Inside getStatusList() method of ---->"+Constants.COMMON_CONTROLLER+"--->");
		UserResponse<List<StatusResponse>> response = new UserResponse<>();
		OutputDTO<List<StatusResponse>> outputDTO = new OutputDTO<>();
		try {
			//log.info("Inside getStatusList() method of "+Constants.COMMON_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside getStatusList() method of "+Constants.COMMON_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			List<StatusResponse> StatusResponseList = commonService.getStatusList();
			outputDTO.setResponseData(StatusResponseList);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Status List Success");
		} catch (Exception e) {
			log.error("exception in StatusList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting getStatusList() method of -->"+Constants.COMMON_CONTROLLER);
		return response;
	}


}

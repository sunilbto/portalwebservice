package com.mfs.pp.controller;

import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.mdm.common.dto.PasswordDto;
import com.mfs.mdm.common.dto.ProfileDto;
import com.mfs.mdm.common.dto.UserDto;
import com.mfs.pp.dto.OutputDTO;
import com.mfs.pp.dto.UserResponse;
import com.mfs.pp.exception.MPDataNotFoundException;
import com.mfs.pp.exception.MPDuplicateDataException;
import com.mfs.pp.exception.MPHibernateCommonException;
import com.mfs.pp.security.userdetails.CustomUserDetails;
import com.mfs.pp.service.AdminUserService;
import com.mfs.pp.service.UserSecurityService;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.Format;
import com.mfs.pp.util.RestServiceURIConstant;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */
@RestController
public class AdminUserController extends BaseController {

	@Autowired
	private AdminUserService adminUserService;

	@Autowired
	private UserSecurityService userSecurityService;
	

    protected final Log log = LogFactory.getLog(getClass());
   
    

    //update Admin User
	@RequestMapping(value = RestServiceURIConstant.UPDATE_ADMIN_USER, method = RequestMethod.POST)
	public UserResponse<Boolean> updateAdminUser(@RequestBody UserDto userDto) {		
		//log.info("Inside updateAdminUser() method of ---->"+Constants.ADMINCONTROLLER+Constants.BEGIN_REQUEST+"--->"+userDto);
		UserResponse<Boolean> response = null;
		OutputDTO<Boolean> result = null;
		response = new UserResponse<>();
		try {
			//log.info("Inside updateAdminUser() method of "+Constants.ADMINCONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(), response);
			//log.info("Inside updateAdminUser() method of "+Constants.ADMINCONTROLLER+"--->"+Constants.AFTER_TOKEN);
			Boolean flag = adminUserService.updateAdminUser(userDto);
			//check flag
			if (flag) {
				result = new OutputDTO<>();
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("User updated Successfully.");
				result.setResponseData(flag);
				response.setResult(result);
			}
		} catch (MPDataNotFoundException e) {
//			log.warn("exception in update user" + e.getMessage());
		
			response.setResponseCode(Constants.ERROR_CODE);
			
		} catch (Exception e) {
			log.error("exception occurred while updateing user" + e.getMessage());
			if (e instanceof MPHibernateCommonException) {
			
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage("User updation unsuccessful");
			}
			
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting updateAdminUser() method of --->"+Constants.ADMINCONTROLLER);
		return response;
	}

	//change Admin Password
	@RequestMapping(value = RestServiceURIConstant.CHANGE_ADMIN_PASSWORD, method = RequestMethod.POST)
	public UserResponse<Boolean> changeAdminPassword(@RequestBody PasswordDto passwordDto) {
		
		//log.info("Inside changeAdminPassword() method of---->"+Constants.ADMINCONTROLLER+Constants.BEGIN_REQUEST+"--->"+passwordDto);	
		UserResponse<Boolean> response = null;
		OutputDTO<Boolean> result = null;
		boolean isCorrectCurrentPassword = false;
		CustomUserDetails customUserDetails = null;
		response = new UserResponse<>();

		try {
		//	log.info("Inside changeAdminPassword() method of---> "+Constants.ADMINCONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(), response);
			//log.info("Inside changeAdminPassword() method of---> "+Constants.ADMINCONTROLLER+"--->"+Constants.AFTER_TOKEN);
			customUserDetails =  customUserDetailsService.loadUserByUsername(passwordDto.getEmail());
			if (null != customUserDetails) {
				isCorrectCurrentPassword = adminUserService.checkPassword(passwordDto.getEmail(),
						passwordDto.getCurrentPassword(),passwordDto.getNewPassword(),passwordDto.getOldPassword());
				if (isCorrectCurrentPassword) {
					Boolean flag = adminUserService.changeAdminPassword(passwordDto);
					if (flag) {
						result = new OutputDTO<>();
						response.setResponseCode(Constants.SUCCESS_CODE);
						response.setResponseMessage("Admin Password updated Successfully.");
						result.setResponseData(flag);
						response.setResult(result);
					}
				} else {
					response.setResponseCode(Constants.ERROR_CODE);				}
			} else {
				response.setResponseCode(Constants.ERROR_CODE);
			}

		} catch (MPDataNotFoundException e) {
//			log.warn("exception in change password for Admin user" + e.getMessage());
			
			response.setResponseCode(Constants.ERROR_CODE);
			
		} catch (Exception e) {
			log.error("exception occurred whlile Change password for admin user" + e.getMessage());
			if (e instanceof MPHibernateCommonException) {
			
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage("Password updation unsuccessful");
			}
		
			response.setResponseCode(Constants.ERROR_CODE);
	
		}
		//log.info("Exiting changeAdminPassword method of -->"+Constants.ADMINCONTROLLER);
		return response;
	}

	//check  isUserActive
	@RequestMapping(value = RestServiceURIConstant.IS_USER_ACTIVE, method = RequestMethod.GET)
	public UserResponse<Boolean> isUserActive(@PathVariable(value = "email") String email) {
		//log.info("Inside isUserActive() method of ---->"+Constants.ADMINCONTROLLER+"--->"+Constants.BEGIN_REQUEST+email);	
		UserResponse<Boolean> response = null;
		OutputDTO<Boolean> outputDTO = null;
		boolean isUserActive = false;
		response = new UserResponse<>();
		try {
			//log.info("Inside changeAdminPassword() method of "+Constants.ADMINCONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(), response);
			//log.info("Inside changeAdminPassword() method of "+Constants.ADMINCONTROLLER+"--->"+Constants.AFTER_TOKEN);
			isUserActive = adminUserService.checkIfUserActive(email);
			if (isUserActive) {
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("User is active");
				outputDTO = new OutputDTO<>();
				outputDTO.setResponseData(isUserActive);
				response.setResult(outputDTO);
			}
		} catch (Exception e) {
			log.error("exception in isUserActive " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting changeAdminPassword method of -->"+Constants.ADMINCONTROLLER);
		return response;

	}

	//get All System Admin Users
	@RequestMapping(value = RestServiceURIConstant.GET_ALL_SYSTEM_ADMIN_USERS, method = RequestMethod.GET)
	public UserResponse<List<UserDto>> getAllSystemAdminUsers() {
		//log.info("Inside getAllSystemAdminUsers() method of ---->"+Constants.ADMINCONTROLLER+"--->");
		UserResponse<List<UserDto>> response = null;
		OutputDTO<List<UserDto>> outputDTO = null;
		List<UserDto> adminUsersList = null;
		response = new UserResponse<>();
		try {
			//log.info("Inside getAllSystemAdminUsers() method of "+Constants.ADMINCONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(), response);
			//log.info("Inside getAllSystemAdminUsers() method of "+Constants.ADMINCONTROLLER+"--->"+Constants.AFTER_TOKEN);
			adminUsersList = adminUserService.getAllSystemAdminUsers();
			if (null != adminUsersList) {
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("User is active");
				outputDTO = new OutputDTO<>();
				outputDTO.setResponseData(adminUsersList);
				response.setResult(outputDTO);
			}
		} catch (Exception e) {
			log.error("exception in getAllSystemAdminUsers " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		
		}
		//log.info("Exiting getAllSystemAdminUsers() method of -->"+Constants.ADMINCONTROLLER);
		return response;

	}

	//check inviteAdminUser
	@RequestMapping(value = RestServiceURIConstant.INVITE_ADMIN_USER, method = RequestMethod.POST)
	public UserResponse<Boolean> inviteAdminUser(@RequestBody UserDto userDto) {
		//log.info("Inside inviteAdminUser() method of ---->"+Constants.ADMINCONTROLLER+"--->"+userDto);
		UserResponse<Boolean> response = null;
		OutputDTO<Boolean> result = null;
		boolean isEmailSent=false;
		response = new UserResponse<>();
		try {
			//log.info("Inside inviteAdminUser() method of "+Constants.ADMINCONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(), response);
			//log.info("Inside inviteAdminUser() method of "+Constants.ADMINCONTROLLER+"--->"+Constants.AFTER_TOKEN);
			if(null!=userDto && null!=userDto.getProfileDto() && Format.isStringEmptyOrNull(userDto.getProfileDto().getEmail())) {
				
			}
			Boolean flag = adminUserService.inviteAdminUser(userDto);
			if (flag) {
				String token = userSecurityService.constructResetTokenForAdminUser(userDto);
				if (null!=token) {
					isEmailSent = userSecurityService.sendEmail(userDto.getProfileDto().getEmail(),
							userDto.getEmailUrl(),token, "inviteAdminUser");
				}
				if (isEmailSent) {
					result = new OutputDTO<>();
					response.setResponseCode(Constants.SUCCESS_CODE);
					response.setResponseMessage("User Registered Successfully.Email sent for User Verification");
					result.setResponseData(isEmailSent);
					response.setResult(result);
				}
			}
		} catch (MPDuplicateDataException e) {
			log.error("exception in invite AdminUser" + e.getMessage());
		
			response.setResponseCode(Constants.ERROR_CODE);
			
		} catch (MessagingException e) {
			log.error("exception in invite AdminUser" + e.getMessage());
			
			response.setResponseCode(Constants.ERROR_CODE);
			
		} catch (Exception e) {
			log.error("inviteAdminUser, exception occurred" + e.getMessage());
			if (e instanceof MPHibernateCommonException) {
				
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage("User registration unsuccessful");
			}
		
			response.setResponseCode(Constants.ERROR_CODE);
		
		}
		//log.info("Exiting inviteAdminUser() method of -->"+Constants.ADMINCONTROLLER);
		return response;
	}

	
	//delete User
	@RequestMapping(value = RestServiceURIConstant.DELETE_ADMINUSER, method = RequestMethod.POST)
	public UserResponse<Boolean> deleteUser(@RequestBody ProfileDto prof) {
		//log.info("Inside deleteUser() method of ---->"+Constants.ADMINCONTROLLER+"--->"+prof);
		UserResponse<Boolean> response = null;
		OutputDTO<Boolean> result = null;
		

		response = new UserResponse<Boolean>();
		try {
			//log.info("Inside deleteUser() method of "+Constants.ADMINCONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside deleteUser() method of "+Constants.ADMINCONTROLLER+"--->"+Constants.AFTER_TOKEN);
			if (Format.isObjectNotEmptyAndNotNull(prof)) {
				if (Format.isStringNotEmptyAndNotNull(prof.getEmail())) {
					//log.info("DeleteAdminUser: User email being deleted: " + prof.getEmail());

					boolean flag = adminUserService.deleteUserByEmail(prof.getEmail());

					if (flag) {
						//log.info("DeleteAdminUser: User deleted Successfully");
						result = new OutputDTO<Boolean>();
						response.setResponseCode(Constants.SUCCESS_CODE);
						response.setResponseMessage("AdminUser deleted Successfully");
						result.setResponseData(flag);
						response.setResult(result);
					}

				}
			} else {
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage("AdminUser could not be deleted");
				//log.info("DeleteAdminUser: AdminUser email not found");
				
			}

		} catch (Exception e) {
			log.error("DeleteAdminUser: Exception while deleting AdminUser");
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("AdminUser could not be deleted");
			
		}
		//log.info("Exiting deleteUser() method of -->"+Constants.ADMINCONTROLLER);
		return response;
	}
	
	
	
}

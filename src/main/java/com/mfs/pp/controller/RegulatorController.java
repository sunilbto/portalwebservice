package com.mfs.pp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.pp.dto.ComplianceRequestDto;
import com.mfs.pp.dto.ContactDto;
import com.mfs.pp.dto.GetCountryRequestDto;
import com.mfs.pp.dto.MoneyFilterRequestDto;
import com.mfs.pp.dto.OutputDTO;
import com.mfs.pp.dto.PricingRequestDto;
import com.mfs.pp.dto.StatisticCounsumeRequestDto;
import com.mfs.pp.dto.StatisticTransactionRequestDto;
import com.mfs.pp.dto.UserResponse;
import com.mfs.pp.service.ComplianceDetailService;
import com.mfs.pp.service.ContactDetailsService;
import com.mfs.pp.service.GetCountryService;
import com.mfs.pp.service.MoneyFilterService;
import com.mfs.pp.service.PricingDetailService;
import com.mfs.pp.service.StaticsticFilterService;
import com.mfs.pp.service.StatisticCounsumeService;
import com.mfs.pp.service.StatisticTransactionService;
import com.mfs.pp.util.Constants;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */
@RestController
public class RegulatorController extends BaseController {
	
	
	@Autowired
	PricingDetailService pricingDetailService;
	@Autowired
	ContactDetailsService contactDetailsService;
	@Autowired
	MoneyFilterService moneyFilterService;
	@Autowired
	GetCountryService getCountryService;
	@Autowired
	ComplianceDetailService complianceDetailsService;
	@Autowired
	StatisticCounsumeService statisticCounsumeService;
	@Autowired
	StatisticTransactionService statisticTransactionService;
	@Autowired
	StaticsticFilterService staticsticFilterService;
	
	
	private static final Logger log = LoggerFactory.getLogger(RegulatorController.class);

	//get Country By Zone
	@RequestMapping(value="/pp/getCountryByZone",method=RequestMethod.POST)
	public UserResponse<Object> getCountryByZone(@RequestBody GetCountryRequestDto request)
	{ //log.info("Inside getCountryByZone() method of ---->"+Constants.REGULATOR_CONTROLLER+"--->"+Constants.BEGIN_REQUEST+"--->"+request);
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();
		Object responses = null;
		try {
			//log.info("Inside getCountryByZone() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside getCountryByZone() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			
			responses=getCountryService.getCountryByZone(request);//contactDetailsService.saveConatctDetails(request);
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Country List");
		
		} catch (Exception e) {
			log.error("exception while getting countryList" + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting getCountryByZone() method of -->"+Constants.REGULATOR_CONTROLLER);
		return response;
	
	}
	
	//get Pricing Details
	@RequestMapping(value ="/pp/getPricing",method = RequestMethod.POST)
	public UserResponse<Object> getPricingDetails(@RequestBody PricingRequestDto request) 
	{
		
		//log.info("Inside getPricingDetails() method of ---->"+Constants.REGULATOR_CONTROLLER+"--->"+Constants.BEGIN_REQUEST+"--->"+request);
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();
		Object responses = null;
		try {
			//log.info("Inside getPricingDetails() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside getPricingDetails() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			
			responses=pricingDetailService.getPricingDetailsService(request);
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage(Constants.PRICING_SUCCESS_MESSAGE);
		
		} catch (Exception e) {
			log.error("exception while getting Pricing Details" + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting getPricingDetails() method of -->"+Constants.REGULATOR_CONTROLLER);
		return response;
	}
	
	//money Filter
	@RequestMapping(value="/pp/getMoneyFilter",method=RequestMethod.POST)
	public UserResponse<Object> moneyFilter(@RequestBody MoneyFilterRequestDto request)
	{
		//log.info("Inside moneyFilter() method of ---->"+Constants.REGULATOR_CONTROLLER+"--->"+Constants.BEGIN_REQUEST+"--->"+request);
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();
		Object responses = null;
		try {
			//log.info("Inside moneyFilter() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside moneyFilter() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			
			responses=moneyFilterService.moneyfilterService(request);
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage(Constants.MONEY_FILTER_SUCCESS);
		
		} catch (Exception e) {
			log.error("exception while getting Filters" + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		
		}
		//log.info("Exiting moneyFilter() method of -->"+Constants.REGULATOR_CONTROLLER);
		return response;
		
	}
	//get Query Types
	@RequestMapping(value="/pp/getQueryType")
	public UserResponse<Object> getQueryTypes()
	{
		//log.info("Inside getQueryTypes() method of ---->"+Constants.REGULATOR_CONTROLLER+"--->");
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();
		Object responses = null;
		try {
			//log.info("Inside getQueryTypes() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside getQueryTypes() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			
			responses=contactDetailsService.getQueryTypes();
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
		
		} catch (Exception e) {
			log.error("exception while getting Query List details" + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting getQueryTypes() method of -->"+Constants.REGULATOR_CONTROLLER);
		return response;
		
		
	}
	
	//save Contact Details
	@RequestMapping(value = "/pp/saveContact" ,method = RequestMethod.POST)
	public UserResponse<Object> saveContactDetails(@RequestBody ContactDto request) {
	
		//log.info("Inside saveContactDetails() method of ---->"+Constants.REGULATOR_CONTROLLER+"--->"+Constants.BEGIN_REQUEST+"--->"+request);
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();
		Object responses = null;
		try {
			//log.info("Inside saveContactDetails() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside saveContactDetails() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			
			responses=contactDetailsService.saveConatctDetails(request);
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
		
		} catch (Exception e) {
			log.error("exception while Save contact details" + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting saveContactDetails() method of -->"+Constants.REGULATOR_CONTROLLER);
		return response;
		
		
	}
	
	//get Compliance Details
	@RequestMapping(value = "/pp/getCompliance",method = RequestMethod.POST)
	public UserResponse<Object> getComplianceDetails(@RequestBody ComplianceRequestDto request) {
		
		//log.info("Inside getComplianceDetails() method of ---->"+Constants.REGULATOR_CONTROLLER+"--->"+Constants.BEGIN_REQUEST+"--->"+request);
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();
		Object responses = null;
		try {
			//log.info("Inside getComplianceDetails() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside getComplianceDetails() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			
			responses=complianceDetailsService.getComplianceDetails(request);//contactDetailsService.saveConatctDetails(request);
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage(Constants.COMPLIANCE_SUCCESS_MESSAGE);
		
		} catch (Exception e) {
			log.error("exception while getting compliance details" + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		
		}
		//log.info("Exiting getComplianceDetails() method of -->"+Constants.REGULATOR_CONTROLLER);
		return response;
		
	}
	
	//get Statistic Counsume Details
    @RequestMapping(value="/pp/StatisticCounsumeDetails",method=RequestMethod.POST)
	public UserResponse<Object> getStatisticCounsumeDetails(@RequestBody StatisticCounsumeRequestDto request)
	{
     //log.info("Inside getStatisticCounsumeDetails() method of ---->"+Constants.REGULATOR_CONTROLLER+"--->"+Constants.BEGIN_REQUEST+"--->"+request);
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();
		Object responses = null;
		try {
			//log.info("Inside getStatisticCounsumeDetails() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside getStatisticCounsumeDetails() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			
			responses=statisticCounsumeService.statisticCounsume(request);
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage(Constants.STATICSTICS_CONSUME_SUCCESS_MESSAGE);
		
		} catch (Exception e) {
			log.error("exception while Staticstic Consume Details" + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting getStatisticCounsumeDetails() method of -->"+Constants.REGULATOR_CONTROLLER);
		return response;
		
	}
    
    //get Statistic Transaction Details
    @RequestMapping(value="/pp/getStatisticTransaction",method=RequestMethod.POST)
    public UserResponse<Object> getStatisticTransactionDetails(@RequestBody StatisticTransactionRequestDto request)
	{
    	//log.info("Inside getStatisticTransactionDetails() method of ---->"+Constants.REGULATOR_CONTROLLER+"--->"+Constants.BEGIN_REQUEST+"--->"+request);
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();
		Object responses = null;
		try {
			//log.info("Inside getStatisticTransactionDetails() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside getStatisticTransactionDetails() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			
			responses=statisticTransactionService.getStatisticTransactionDetailService(request);
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage(Constants.STATICSTICS_TRANSACTION_SUCCESS_MESSAGE);
		
		} catch (Exception e) {
			log.error("exception while Staticstic Transaction Details" + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
//		log.info("Exiting getStatisticTransactionDetails() method of -->"+Constants.REGULATOR_CONTROLLER);
		return response;
    }

    //get Staticstic Filter
    @RequestMapping(value="/pp/getStaticsticFilter",method=RequestMethod.GET)
    public UserResponse<Object> getStaticsticFilter()
    {
       //log.info("Inside getStaticsticFilter() method of ---->"+Constants.REGULATOR_CONTROLLER+"--->");
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();
		Object responses = null;
		try {
			//log.info("Inside getStaticsticFilter() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
		//log.info("Inside getStaticsticFilter() method of "+Constants.REGULATOR_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			
			responses=staticsticFilterService.getStaticsticFilter();
	    	
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage(Constants.STATICSTICS_TRANSACTION_FILTER_SUCCESS_MESSAGE);
		
		} catch (Exception e) {
			log.error("exception while Staticstic Filter" + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting getStaticsticFilter() method of -->"+Constants.REGULATOR_CONTROLLER);
		return response;
    }
}
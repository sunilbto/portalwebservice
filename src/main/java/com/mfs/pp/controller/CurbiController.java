package com.mfs.pp.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.mdm.common.dto.OrganisationProfileDto;
import com.mfs.pp.dto.AllBalanceByIdRequestDto;
import com.mfs.pp.dto.AllBalanceRequestDto;
import com.mfs.pp.dto.AllDashBoardRequestDto;
import com.mfs.pp.dto.AllTransactionRequestDto;
import com.mfs.pp.dto.AlltransactionByIdRequestDto;
import com.mfs.pp.dto.OutputDTO;
import com.mfs.pp.dto.UserResponse;
import com.mfs.pp.service.BalanceExcelService;
import com.mfs.pp.service.BalanceService;
import com.mfs.pp.service.CountryService;
import com.mfs.pp.service.CurbiService;
import com.mfs.pp.service.DashBoardService;
import com.mfs.pp.service.DestinationTypeService;
import com.mfs.pp.service.DirectionService;
import com.mfs.pp.service.ProductService;
import com.mfs.pp.service.ReportExcelService;
import com.mfs.pp.service.TransactionExcelService;
import com.mfs.pp.service.TransactionService;
import com.mfs.pp.service.TransactionStatusService;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.mahind@beautosys.com
 *
 */
@RestController
public class CurbiController extends BaseController {

	@Autowired
	TransactionExcelService transactionExcelService;
	@Autowired
	BalanceExcelService balanceExcelService;
	@Autowired
	ReportExcelService reportExcelService;
	@Autowired
	TransactionService transService;
	@Autowired
	DirectionService directionService;
	@Autowired
	CountryService countryService;
	@Autowired
	ProductService productService;
	@Autowired
	DestinationTypeService destinationTypeService;
	@Autowired
	TransactionStatusService transStatusService;
	@Autowired
	BalanceService balanceService;
	
	@Autowired
	private CurbiService curbiService;
	@Autowired
	DashBoardService dashService;

	private static int flag = 0;

	private static final Logger log = LoggerFactory.getLogger(CurbiController.class);

	//get DashBoard Info
	@RequestMapping(value = RestServiceURIConstant.GET_DASHBOARD, method = RequestMethod.POST)
	public UserResponse<Object> getDashBoardInfo(@RequestBody AllDashBoardRequestDto request) {
		// log.info("Inside getDashBoardInfo() method of
		// ---->"+Constants.CURBI_CONTROLLER+"--->"+Constants.BEGIN_REQUEST+"--->"+request);
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();
		Object responses = null;
		try {
			// log.info("Inside getDashBoardInfo() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getDashBoardInfo() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			responses = dashService.getDashBoard(request);
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("DashBorad Information");
		} catch (Exception e) {
			log.error("exception in geting DashBorad Information " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		// log.info("Exiting getDashBoardInfo() method of
		// -->"+Constants.CURBI_CONTROLLER);
		return response;
	}

	/*//get Organization List
	@RequestMapping(value = RestServiceURIConstant.GET_ALLORGANIZATION_LIST)
	public UserResponse<List<Object>> getOrganizationList() {
		// log.info("Inside getOrganizationList() method of
		// ---->"+Constants.CURBI_CONTROLLER+"--->");
		UserResponse<List<Object>> response = new UserResponse<>();
		OutputDTO<List<Object>> outputDTO = new OutputDTO<>();
		List<Object> responses = null;
		try {
			// log.info("Inside getOrganizationList() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getOrganizationList() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			responses = getOrganizationService.getOrganizationList();
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Organization List Success");
		} catch (Exception e) {
			log.error("exception in getOrganizationList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		

		}
		// log.info("Exiting getOrganizationList() method of
		// -->"+Constants.CURBI_CONTROLLER);
		return response;

	}*/

	//get All Transaction
	@RequestMapping(value = RestServiceURIConstant.GET_ALLTRANSACTION, method = RequestMethod.POST)
	public UserResponse<Object> getAllTransaction(@RequestBody AllTransactionRequestDto request) {
		CurbiController.flag++;
		
//		log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Testing : " + CurbiController.flag);
		// log.info("Inside getAllTransaction() method of
		// ---->"+Constants.CURBI_CONTROLLER+"--->"+Constants.BEGIN_REQUEST+"--->"+request);
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();
		Object responses = null;

		try {
			// log.info("Inside getAllTransaction() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getAllTransaction() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			responses = transService.getAllTransaction(request);
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Transaction Information");

		} catch (Exception e) {
			log.error("exception in geting Transaction Information " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		// log.info("Exiting getAllTransaction() method of
		// -->"+Constants.CURBI_CONTROLLER);
		return response;
	}

	//get Transaction By Id
	@RequestMapping(value = RestServiceURIConstant.GET_TRANSACTION_BY_ID, method = RequestMethod.POST)
	public UserResponse<Object> getTransactionById(@RequestBody AlltransactionByIdRequestDto request) {
		// log.info("Inside getTransactionById() method of
		// ---->"+Constants.CURBI_CONTROLLER+"--->"+Constants.BEGIN_REQUEST+"--->"+request);
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();
		Object responses = null;
		try {
			// log.info("Inside getTransactionById() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getTransactionById() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			responses = transService.getTransactionById(request);
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Transaction Information");

		} catch (Exception e) {
			log.error("exception in geting Transaction Information " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		
		}
		// log.info("Exiting getTransactionById() method of
		// -->"+Constants.CURBI_CONTROLLER);
		return response;
	}

	//get All Balance
	@RequestMapping(value = RestServiceURIConstant.GET_ALL_BALANCE, method = RequestMethod.POST)
	public UserResponse<Object> getAllBalance(@RequestBody AllBalanceRequestDto request) {
		// log.info("Inside getAllBalance() method of
		// ---->"+Constants.CURBI_CONTROLLER+"--->"+Constants.BEGIN_REQUEST+"--->"+request);
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();

		Object responses = null;
		try {
			// log.info("Inside getAllBalance() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getAllBalance() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			responses = balanceService.getAllBalance(request);

			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Balance Information");
		} catch (Exception e) {
			log.error("exception in geting Balance Information " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		// log.info("Exiting getAllBalance() method of -->"+Constants.CURBI_CONTROLLER);

		return response;
	}

	//get All Balance By Id
	@RequestMapping(value = RestServiceURIConstant.GET_ALL_BALANCE_BY_ID, method = RequestMethod.POST)
	public UserResponse<Object> getAllBalanceById(@RequestBody AllBalanceByIdRequestDto request) {
		// log.info("Inside getAllBalanceById() method of
		// ---->"+Constants.CURBI_CONTROLLER+"--->"+Constants.BEGIN_REQUEST+"--->"+request);
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();
		Object responses = null;

		try {
			// log.info("Inside getAllBalanceById() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getAllBalanceById() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			responses = balanceService.getAllBalanceById(request);
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Balance Information");
		} catch (Exception e) {
			log.error("exception in geting Balance Information " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		
		}
		// log.info("Exiting getAllBalanceById() method of
		// -->"+Constants.CURBI_CONTROLLER);
		return response;
	}

	//get Direction List
	@RequestMapping(value = RestServiceURIConstant.GET_DIRECTIONLIST)
	public UserResponse<List<Object>> getDirectionList() {
		// log.info("Inside getDirectionList() method of
		// ---->"+Constants.CURBI_CONTROLLER+"--->");
		UserResponse<List<Object>> response = new UserResponse<>();
		OutputDTO<List<Object>> outputDTO = new OutputDTO<>();
		List<Object> directionList = null;
		try {
			// log.info("Inside getDirectionList() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getDirectionList() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			directionList = directionService.getDirectionList();
			outputDTO.setResponseData(directionList);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Direction List Success");
		} catch (Exception e) {
			log.error("exception in getDirectionList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			

		}
		// log.info("Exiting getDirectionList() method of
		// -->"+Constants.CURBI_CONTROLLER);
		return response;

	}

	//get All Countries
	@RequestMapping(value = RestServiceURIConstant.GET_ALLCOUNTRIES)
	public UserResponse<List<Object>> getAllCountries() {
		// log.info("Inside getAllCountries() method of
		// ---->"+Constants.CURBI_CONTROLLER+"--->");
		UserResponse<List<Object>> response = new UserResponse<>();
		OutputDTO<List<Object>> outputDTO = new OutputDTO<>();
		List<Object> countryList = null;

		try {
			// log.info("Inside getAllCountries() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getAllCountries() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			countryList = countryService.getAllCountries();
			outputDTO.setResponseData(countryList);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Country List Success");
		} catch (Exception e) {
			log.error("exception in getCountryList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			

		}
		// log.info("Exiting getAllCountries() method of
		// -->"+Constants.CURBI_CONTROLLER);
		return response;
	}

	//get All Product
	@RequestMapping(value = RestServiceURIConstant.GET_ALLPRODUCT)
	public UserResponse<List<Object>> getAllProduct() {
		// log.info("Inside getAllProduct() method of
		// ---->"+Constants.CURBI_CONTROLLER+"--->");
		UserResponse<List<Object>> response = new UserResponse<>();
		OutputDTO<List<Object>> outputDTO = new OutputDTO<>();
		List<Object> productList = null;

		try {
			// log.info("Inside getAllProduct() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getAllProduct() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			productList = productService.getAllProduct();
			outputDTO.setResponseData(productList);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Product List Success");
		} catch (Exception e) {
			log.error("exception in getProductList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		// log.info("Exiting getAllProduct() method of -->"+Constants.CURBI_CONTROLLER);
		return response;
	}

	//get Destination Type List
	@RequestMapping(value = RestServiceURIConstant.GET_DESTINATIONTYPE)
	public UserResponse<List<Object>> getDestinationTypeList() {
		// log.info("Inside getDestinationTypeList() method of
		// ---->"+Constants.CURBI_CONTROLLER+"--->");
		UserResponse<List<Object>> response = new UserResponse<>();
		OutputDTO<List<Object>> outputDTO = new OutputDTO<>();
		List<Object> destinationType = null;

		try {
			// log.info("Inside getDestinationTypeList() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getDestinationTypeList() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			destinationType = destinationTypeService.getDestinationTypeList();
			outputDTO.setResponseData(destinationType);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Destination List Success");
		} catch (Exception e) {
			log.error("exception in getDestinationList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			

		}
		// log.info("Exiting getDestinationTypeList() method of
		// -->"+Constants.CURBI_CONTROLLER);
		return response;
	}

	//get Balance Summury
	@RequestMapping(value = RestServiceURIConstant.GET_BALANCESUMMURY)
	public UserResponse<List<Object>> getBalanceSummury() {
		// log.info("Inside getBalanceSummury() method of
		// ---->"+Constants.CURBI_CONTROLLER+"--->");
		List<Object> balance = null;
		UserResponse<List<Object>> response = new UserResponse<>();
		OutputDTO<List<Object>> outputDTO = new OutputDTO<>();

		try {
			// log.info("Inside getBalanceSummury() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getBalanceSummury() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			balance = balanceService.getBalanceSummury();
			outputDTO.setResponseData(balance);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Balance Summury List Success");
		} catch (Exception e) {
			log.error("exception in getBalanceSummuryList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		// log.info("Exiting getBalanceSummury() method of
		// -->"+Constants.CURBI_CONTROLLER);
		return response;

	}

	//get Transaction Status
	@RequestMapping(value = RestServiceURIConstant.GET_TRANSACTIONSTATUS)
	public UserResponse<List<Object>> getTransactionStatus() {
		// log.info("Inside getTransactionStatus() method of
		// ---->"+Constants.CURBI_CONTROLLER+"--->");
		UserResponse<List<Object>> response = new UserResponse<>();
		OutputDTO<List<Object>> outputDTO = new OutputDTO<>();
		List<Object> status = null;

		try {
			// log.info("Inside getTransactionStatus() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside getTransactionStatus() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			status = transStatusService.getTransactionStatus();
			outputDTO.setResponseData(status);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Transaction Status List Success");
		} catch (Exception e) {
			log.error("exception in getTransactionStatusList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		// log.info("Exiting getTransactionStatus() method of
		// -->"+Constants.CURBI_CONTROLLER);
		return response;
	}

	//create Organization
	@RequestMapping(value = RestServiceURIConstant.CREATE_ORGANIZATION_CURBI, method = RequestMethod.POST)
	public UserResponse<Boolean> createOrganization(@RequestBody OrganisationProfileDto organisationProfileDto) {
		// log.info("Inside createOrganization() method of
		// ---->"+Constants.CURBI_CONTROLLER+Constants.BEGIN_REQUEST+"--->"+organisationProfileDto);
		UserResponse<Boolean> response = null;
		response = new UserResponse<>();
		OutputDTO<Boolean> results = null;

		try {
			// log.info("Inside createOrganization() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside createOrganization() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			String result = curbiService.updateOrganizationDetails(organisationProfileDto);
			if (result != null && result.equals("Org Created")) {
				results = new OutputDTO<>();
				// log.info("Organization Created");
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("The Organization Created.");
				response.setResult(results);
			} else {
				results = new OutputDTO<>();
				// log.info("Duplicate Organization");
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage("Duplicate Organization.");
				response.setResult(results);
			}

		} catch (Exception e) {
			results = new OutputDTO<>();
			log.error("CreateOrganization: Exception occurred");
		
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Exception while createing Organization");
			response.setResult(results);
		}
		// log.info("Exiting createOrganization() method of
		// --->"+Constants.CURBI_CONTROLLER);
		return response;

	}

	//generate Excel For Transaction
	@RequestMapping(value = RestServiceURIConstant.GEN_TRANSACTION_EXCEL, method = RequestMethod.POST)
	public UserResponse<Object> genExcelForTransaction(@RequestBody AllTransactionRequestDto request) {

		// log.info("Inside genExcelForTransaction() method of
		// ---->"+Constants.CURBI_CONTROLLER+"--->"+Constants.BEGIN_REQUEST+"--->"+request);
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();
		Object responses = null;
		try {
			// log.info("Inside genExcelForTransaction() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside genExcelForTransaction() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			responses = transactionExcelService.GenExcelForTransaction(request);
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Excel saved successfully");
		} catch (Exception e) {
			log.error("exception while generating Excel For Transaction Information " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		// log.info("Exiting getDashBoardInfo() method of
		// -->"+Constants.CURBI_CONTROLLER);
		return response;

	}

	//generate Excel For Balance
	@RequestMapping(value = RestServiceURIConstant.GEN_BALANCE_EXCEL, method = RequestMethod.POST)
	public UserResponse<Object> genExcelForBalance(@RequestBody AllBalanceRequestDto request) {
		// log.info("Inside genExcelForBalance() method of
		// ---->"+Constants.CURBI_CONTROLLER+"--->"+Constants.BEGIN_REQUEST+"--->"+request);
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();
		Object responses = null;
		try {
			// log.info("Inside genExcelForBalance() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside genExcelForBalance() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			responses = balanceExcelService.GenExcelForBalance(request);
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Excel saved successfully");
		} catch (Exception e) {
			log.error("exception while generating Excel For Balance Information " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		// log.info("Exiting genExcelForBalance() method of
		// -->"+Constants.CURBI_CONTROLLER);
		return response;

	}

	//generate Excel For Report
	@RequestMapping(value = RestServiceURIConstant.GEN_REPORT_EXCEL, method = RequestMethod.POST)
	public UserResponse<Object> genExcelForReport(@RequestBody AllDashBoardRequestDto request) {
		// log.info("Inside genExcelForReport() method of
		// ---->"+Constants.CURBI_CONTROLLER+"--->"+Constants.BEGIN_REQUEST+"--->"+request);
		UserResponse<Object> response = new UserResponse<>();
		OutputDTO<Object> outputDTO = new OutputDTO<>();
		Object responses = null;
		try {
			// log.info("Inside genExcelForReport() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			// log.info("Inside genExcelForReport() method of
			// "+Constants.CURBI_CONTROLLER+"--->"+Constants.AFTER_TOKEN);
			responses = reportExcelService.GenExcelForBalance(request);
			outputDTO.setResponseData(responses);
			response.setResult(outputDTO);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseMessage("Excel saved successfully");
		} catch (Exception e) {
			log.error("exception while generating Excel For Report Information " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		// log.info("Exiting genExcelForReport() method of
		// -->"+Constants.CURBI_CONTROLLER);
		return response;

	}

}

package com.mfs.pp.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.mdm.common.dto.CountryDto;
import com.mfs.mdm.common.dto.PasswordDto;
import com.mfs.mdm.common.dto.ProfileDto;
import com.mfs.mdm.common.dto.UserDto;
import com.mfs.pp.dto.OutputDTO;
import com.mfs.pp.dto.UserResponse;
import com.mfs.pp.exception.MPDataNotFoundException;
import com.mfs.pp.exception.MPDuplicateDataException;
import com.mfs.pp.exception.MPHibernateCommonException;
import com.mfs.pp.security.userdetails.CustomUserDetails;
import com.mfs.pp.service.AdminUserService;
import com.mfs.pp.service.UserSecurityService;
import com.mfs.pp.service.UserService;
import com.mfs.pp.type.MFSErrorType;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.Format;
import com.mfs.pp.util.RestServiceURIConstant;

/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */
@RestController
public class UserController extends BaseController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private AdminUserService adminUserService;

	@Autowired
	private UserSecurityService userSecurityService;

	protected final Log log = LogFactory.getLog(UserController.class);

	//register User
	@RequestMapping(value = RestServiceURIConstant.REGISTER_USER, method = RequestMethod.POST)
	public UserResponse<Boolean> registerUser(@RequestBody UserDto userDto) {
		//log.info("Inside registerUser() method of ---->" + Constants.USER_CONTROLLER + Constants.BEGIN_REQUEST + "--->"+ userDto);
		UserResponse<Boolean> response = null;
		String userCreationType = null;
		OutputDTO<Boolean> result = null;

		response = new UserResponse<Boolean>();
		try {

			Boolean flag = userService.registerUser(userDto);
			if (flag) {
				if (null != userDto && Format.isStringNotEmptyAndNotNull(userDto.getPassword())) {
					userCreationType = "register";
				} else {
					userCreationType = "invite";
				}
				boolean isEmailSent = userSecurityService.constructResetTokenAndSendEmail(
						userDto.getProfileDto().getEmail(), userDto.getEmailUrl(), userCreationType);
				if (isEmailSent) {
					result = new OutputDTO<Boolean>();
					response.setResponseCode(Constants.SUCCESS_CODE);
					response.setResponseMessage("User Registered Successfully.Email sent for User Verification");
					result.setResponseData(isEmailSent);
					response.setResult(result);
				}
			}
		} catch (MPDuplicateDataException e) {
			log.error("exception in register user"+e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		} catch (Exception e) {
			log.error("exception occurred in invite user"+e.getMessage());
			if (e instanceof MPHibernateCommonException) {
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage("User registration unsuccessful");
			}
			response.setResponseCode(Constants.ERROR_CODE);

		}
		//log.info("Exiting registerUser() method of --->" + Constants.USER_CONTROLLER);
		return response;
	}

	//update User
	@RequestMapping(value = RestServiceURIConstant.UPDATE_USER, method = RequestMethod.POST)
	public UserResponse<Boolean> updateUser(@RequestBody UserDto userDto) {
		//log.info("Inside updateUser() method of ---->" + Constants.USER_CONTROLLER + Constants.BEGIN_REQUEST + "--->"+ userDto);
		UserResponse<Boolean> response = null;
		OutputDTO<Boolean> result = null;
		response = new UserResponse<Boolean>();
		try {
			//log.info("Inside updateUser() method of " + Constants.USER_CONTROLLER + "--->" + Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside updateUser() method of " + Constants.USER_CONTROLLER + "--->" + Constants.AFTER_TOKEN);
			boolean flag = userService.updateUser(userDto);
			if (flag) {
				result = new OutputDTO<Boolean>();
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("User updated Successfully");
				result.setResponseData(flag);
				response.setResult(result);
			}
		} catch (Exception e) {
			log.error("exception occurred in update user"+e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("User details not updated");
		}
		//log.info("Exiting updateUser() method of --->" + Constants.USER_CONTROLLER);
		return response;
	}

	//get All Country List
	@RequestMapping(value = RestServiceURIConstant.GET_All_COUNTRYlIST, method = RequestMethod.GET)
	public UserResponse<List<CountryDto>> getAllCountryDtoList() {
		//log.info("Inside getAllCountryDtoList() method of ---->" + Constants.USER_CONTROLLER);
		UserResponse<List<CountryDto>> response = null;
		response = new UserResponse<List<CountryDto>>();
		try {
			//log.info("Inside getAllCountryDtoList() method of " + Constants.USER_CONTROLLER + "--->"+ Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside getAllCountryDtoList() method of " + Constants.USER_CONTROLLER + "--->"+ Constants.AFTER_TOKEN);
			List<CountryDto> countryDtoList = userService.getAllCountryDtoList();
			OutputDTO<List<CountryDto>> outputDTO = new OutputDTO<List<CountryDto>>();
			outputDTO.setResponseData(countryDtoList);
			response.setResult(outputDTO);
			
			if (Format.isCollectionNotEmptyAndNotNull(countryDtoList)) {
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("service executed  Successfully");
			} else {
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage("Fail");
			}
		} catch (Exception e) {
			log.error("exception in getAllCountryDtoList " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Fail!");
		}
		//log.info("Exiting getAllCountryDtoList() method of --->" + Constants.USER_CONTROLLER);
		return response;

	}

	//get User By Email Id
	@RequestMapping(value = RestServiceURIConstant.GET_USER_BY_EMAIL, method = RequestMethod.POST)
	public UserResponse<UserDto> getUserByEmailId(@RequestParam String email) {
		//log.info("Inside getUserByEmailId() method of ---->" + Constants.USER_CONTROLLER + Constants.BEGIN_REQUEST+ "--->" + email);
		UserResponse<UserDto> response = null;
		response = new UserResponse<UserDto>();
		try {
			//log.info("Inside getUserByEmailId() method of " + Constants.USER_CONTROLLER + "--->"+ Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside getUserByEmailId() method of " + Constants.USER_CONTROLLER + "--->"+ Constants.AFTER_TOKEN);
			UserDto userDTO = userService.getUserByUserEmailId(email);
			OutputDTO<UserDto> outputDTO = new OutputDTO<UserDto>();
			outputDTO.setResponseData(userDTO);
			response.setResult(outputDTO);
			if (Format.isObjectNotEmptyAndNotNull(userDTO)) {
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("service executed  Successfully");
			} else {
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage("Fail!");
			}
		} catch (Exception e) {
			log.error("exception in getUserByEmailId " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage(e.getMessage());
		}
		//log.info("Exiting getUserByEmailId() method of --->" + Constants.USER_CONTROLLER);
		return response;
	}

	//change Organization Password
	@RequestMapping(value = RestServiceURIConstant.CHANGE_ORG_PASSWORD, method = RequestMethod.POST)
	public UserResponse<Boolean> changeOrgPassword(@RequestBody PasswordDto passwordDto) {
		//log.info("Inside changeOrgPassword() method of ---->" + Constants.USER_CONTROLLER + Constants.BEGIN_REQUEST+ "--->" + passwordDto);
		UserResponse<Boolean> response = null;
		OutputDTO<Boolean> result = null;
		boolean isCorrectCurrentPassword = false;
		CustomUserDetails customUserDetails = null;
		response = new UserResponse<>();

		try {
			//log.info("Inside changeOrgPassword() method of " + Constants.USER_CONTROLLER + "--->"+ Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside changeOrgPassword() method of " + Constants.USER_CONTROLLER + "--->"+ Constants.AFTER_TOKEN);
			customUserDetails = customUserDetailsService.loadUserByUsername(passwordDto.getEmail());
			//check customUser Details
			if (null != customUserDetails) {
				isCorrectCurrentPassword = adminUserService.checkPassword(passwordDto.getEmail(),
						passwordDto.getCurrentPassword(),passwordDto.getNewPassword(),passwordDto.getOldPassword());
				if (isCorrectCurrentPassword) {
					Boolean flag = userService.changePartnerPassword(passwordDto);
					if (flag) {
						result = new OutputDTO<>();
						response.setResponseCode(Constants.SUCCESS_CODE);
						response.setResponseMessage("Partner Password updated Successfully.");
						result.setResponseData(flag);
						response.setResult(result);
					}
				} else {
					log.error(MFSErrorType.INVALID_CURRENT_PASSWORD.getDescription());
				
				}
			} else {
				log.error(MFSErrorType.USER_DOES_NOT_EXIST.getDescription());
			
			}

		} catch (MPDataNotFoundException e) {
			log.error("exception in change Admin user" + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		} catch (Exception e) {
			log.error("exception occurred in change admin password" + e.getMessage());
			if (e instanceof MPHibernateCommonException) {
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage("Password updation unsuccessful");
			}
			response.setResponseCode(Constants.ERROR_CODE);
		}
		//log.info("Exiting changeOrgPassword() method of --->" + Constants.USER_CONTROLLER);
		return response;
	}

	//delete User
	@RequestMapping(value = RestServiceURIConstant.DELETE_USER, method = RequestMethod.POST)
	public UserResponse<Boolean> deleteUser(@RequestBody ProfileDto prof) {
		//log.info("Inside deleteUser() method of ---->" + Constants.USER_CONTROLLER + Constants.BEGIN_REQUEST + "--->"+ prof);
		UserResponse<Boolean> response = null;
		OutputDTO<Boolean> result = null;
		response = new UserResponse<Boolean>();

		try {
//			log.info("Inside deleteUser() method of " + Constants.USER_CONTROLLER + "--->" + Constants.BEFORE_TOKEN);
			jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().getProfileDto().getEmail(),
					response);
			//log.info("Inside deleteUser() method of " + Constants.USER_CONTROLLER + "--->" + Constants.AFTER_TOKEN);
			if (Format.isObjectNotEmptyAndNotNull(prof)) {
				if (Format.isStringNotEmptyAndNotNull(prof.getEmail())) {
//					log.info("DeleteUser: User email being deleted: " + prof.getEmail());
					boolean flag = userService.deleteUserByEmail(prof.getEmail());
					if (flag) {
//						log.info("DeleteUser: User deleted Successfully");
						result = new OutputDTO<Boolean>();
						response.setResponseCode(Constants.SUCCESS_CODE);
						response.setResponseMessage("User deleted Successfully");
						result.setResponseData(flag);
						response.setResult(result);
					}
				}
			} else {
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage("User could not be deleted");
				//log.info("DeleteUser: User email not found");
			}
		} catch (Exception e) {
			log.error("DeleteUser: Exception while deleting user"+e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("User could not be deleted");
		}
		//log.info("Exiting deleteUser() method of --->" + Constants.USER_CONTROLLER);
		return response;
	}
}

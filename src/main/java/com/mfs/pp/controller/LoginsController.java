package com.mfs.pp.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.mdm.common.dto.LoginAuditLogDTO;
import com.mfs.mdm.common.dto.UserDto;
import com.mfs.pp.dto.OutputDTO;
import com.mfs.pp.dto.UserResponse;
import com.mfs.pp.security.userdetails.CustomUserDetails;
import com.mfs.pp.service.AuditLogService;
import com.mfs.pp.service.UserSecurityService;
import com.mfs.pp.type.MFSErrorType;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.Format;
import com.mfs.pp.util.RestServiceURIConstant;

/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */
@RestController
public class LoginsController extends BaseController {

	@Autowired
	private AuditLogService auditLogService;

	@Autowired
	private UserSecurityService userSecurityService;

	protected final Log log = LogFactory.getLog(getClass());

	//check login
	@RequestMapping(value = RestServiceURIConstant.LOGIN, method = RequestMethod.POST)
	public UserResponse<Boolean> login(@RequestHeader HttpHeaders headers, @RequestBody UserDto userDto,
			HttpServletRequest request) {

		//log.info("Inside login() method of ---->" + Constants.LOGIN_CONTROLLER + Constants.BEGIN_REQUEST + "--->"+ userDto);
		UserResponse<Boolean> response = null;
		OutputDTO<Boolean> outputDTO = null;
		CustomUserDetails customeUserDetails = null;
		LoginAuditLogDTO loginAuditLogDTO = null;
		UserDto userDtoforOrg = null;
		String orgNmae = "";
		
		response = new UserResponse<>();

		try {
			if (null != userDto && null != userDto.getProfileDto() && null != userDto.getProfileDto().getEmail()) {
				userDtoforOrg = userService.getUserByUserEmailId(userDto.getProfileDto().getEmail());

				customeUserDetails = customUserDetailsService.loadUserByUsername(userDto.getProfileDto().getEmail());

				if (null != customeUserDetails && null != customeUserDetails.getUser()
						&& null != userDto.getPassword()) {

					boolean flag = customUserDetailsService.login(customeUserDetails.getUser().getUsername(),
							userDto.getPassword());

					if (Format.isObjectNotEmptyAndNotNull(userDtoforOrg)) {
						if (Format.isObjectNotEmptyAndNotNull(userDtoforOrg.getOrgProfileDto())) {
							orgNmae = userDtoforOrg.getOrgProfileDto().getOrganisationName();
						}
					}

					if (flag) {
						outputDTO = new OutputDTO<>();
						outputDTO.setResponseData(Boolean.TRUE);
						response.setResult(outputDTO);
						response.setResponseCode(Constants.SUCCESS_CODE);
						response.setResponseMessage(Constants.LOGIN_STATUS);
						//log.info("Inside login() method of " + Constants.LOGIN_CONTROLLER + "--->" + Constants.BEFORE_TOKEN);
						jwtTokenUtil.generateTokenAndSetInRestResponse(userDto.getProfileDto().getEmail(), response);
						//log.info("Inside login() method of " + Constants.LOGIN_CONTROLLER + "--->" + Constants.AFTER_TOKEN);
						loginAuditLogDTO = auditLogService.getLoginAuditDtoForLogin(customeUserDetails.getUsername(),
								request.getRemoteAddr(), "Login ", Constants.SUCCESS, orgNmae);
						auditLogService.saveAuditLog(loginAuditLogDTO);
					}

				}

			}
		} catch (AuthenticationException e) {
			log.error("Login: Exception occurred"+e.getMessage());
			if (null != userDto && null != userDto.getProfileDto()) {
				loginAuditLogDTO = auditLogService.getLoginAuditDtoForLogin(userDto.getProfileDto().getEmail(),
						request.getRemoteAddr(), Constants.LOGIN_FAIl,
						MFSErrorType.INVALID_USERNAME_PASSWORD.getDescription(), orgNmae);
				auditLogService.saveAuditLog(loginAuditLogDTO);
			}
		
			response.setResponseCode(Constants.ERROR_CODE);
			if (e instanceof DisabledException) {
				log.error("Login: Exception occurred"+e.getMessage());
			} else if (e instanceof AccountExpiredException) {
				log.error(MFSErrorType.INACTIVE_USER.getDescription()+e.getMessage());
			
			} else if (e instanceof CredentialsExpiredException) {
				log.error(MFSErrorType.PASSWORD_STATUS.getDescription()+e.getMessage());
			
			} else {
				log.error(MFSErrorType.INVALID_USERNAME_PASSWORD.getDescription()+e.getMessage());
				
			}

		} catch (Exception e) {
			log.error("Login: Exception occurred"+e.getMessage());
			if (null != userDto && null != userDto.getProfileDto()) {
				loginAuditLogDTO = auditLogService.getLoginAuditDtoForLogin(userDto.getProfileDto().getEmail(),
						request.getRemoteAddr(), Constants.LOGIN_FAIl, e.getMessage(), orgNmae);
				auditLogService.saveAuditLog(loginAuditLogDTO);
			}
		
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting login() method of --->" + Constants.LOGIN_CONTROLLER);
		return response;
	}

	//create New Password
	@RequestMapping(value = RestServiceURIConstant.NEW_PASSWORD, method = RequestMethod.POST)
	public UserResponse<Boolean> createNewPassword(@RequestBody UserDto userDto) {
		//log.info("Inside createNewPassword() method of ---->" + Constants.LOGIN_CONTROLLER + Constants.BEGIN_REQUEST + "--->"+ userDto);

		UserResponse<Boolean> response = null;
		OutputDTO<Boolean> outputDTO = null;
		response = new UserResponse<>();

		try {

			boolean isPasswordSaved = userService.createOrUpdatePassword(userDto);
			//check isPasswordSaved
			if (isPasswordSaved) {
				//log.info("CreateNewPassword: The Password is saved");
				outputDTO = new OutputDTO<>();
				outputDTO.setResponseData(isPasswordSaved);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("The Password is saved");
			} else {
				//log.info("CreateNewPassword: The Password could not be saved, Exception occurred or Account closed");
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseMessage("The Password could not be saved.");
			}
		} catch (Exception e) {
			log.error("CreateNewPassword: Exception occurred");
		
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Exception while saving password");
		}
		//log.info("Exiting createNewPassword() method of --->" + Constants.LOGIN_CONTROLLER);
		return response;
	}

	//send Reset Password Link
	@RequestMapping(value = RestServiceURIConstant.SEND_RESET_PASSWORD_LINK, method = RequestMethod.POST)
	public UserResponse<Boolean> sendResetPasswordLink(@RequestBody UserDto userDto) {
//		log.info("Inside sendResetPasswordLink() method of ---->" + Constants.LOGIN_CONTROLLER + Constants.BEGIN_REQUEST + "--->"+ userDto);
		UserResponse<Boolean> response = null;
		OutputDTO<Boolean> outputDTO = null;
		Boolean isResetLinkSent = Boolean.FALSE;
		String isReset = "reset";
		response = new UserResponse<Boolean>();
		try {
			
			//check user
			if (null != userDto && null != userDto.getProfileDto()
					&& Format.isStringNotEmptyAndNotNull(userDto.getProfileDto().getEmail())) {
				isResetLinkSent = userSecurityService.constructResetTokenAndSendEmail(
						userDto.getProfileDto().getEmail(), userDto.getEmailUrl(), isReset);
				if (isResetLinkSent) {
					outputDTO = new OutputDTO<Boolean>();
					outputDTO.setResponseData(Boolean.TRUE);
					response.setResult(outputDTO);
					response.setResponseCode(Constants.SUCCESS_CODE);
					response.setResponseMessage("Reset link sent successfully to your email id");
				} else {
					response.setResponseCode(Constants.ERROR_CODE);
					response.setResponseMessage("Reset Link not Sent due to internal error.");
					
				}
			}
		} catch (Exception e) {
			log.error("exception occurred in reset password link" + e.getMessage());
		
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting sendResetPasswordLink() method of --->" + Constants.LOGIN_CONTROLLER);
		return response;

	}

	//logout
	@RequestMapping(value = RestServiceURIConstant.LOGOUT, method = RequestMethod.GET)
	public UserResponse<Boolean> logout() {
		//log.info("Inside logout() method of ---->" + Constants.LOGIN_CONTROLLER );
		UserResponse<Boolean> response = null;
		boolean isLoggedOut = false;
		response = new UserResponse<>();
		try {
			//log.info("Inside logout() method of " + Constants.LOGIN_CONTROLLER + "--->" + Constants.BEFORE_TOKEN);
			/*
			 * jwtTokenUtil.generateTokenAndSetInRestResponse(super.getLoggedInUsersDto().
			 * getProfileDto().getEmail(), response);
			 */
			//log.info("Inside logout() method of " + Constants.LOGIN_CONTROLLER + "--->" + Constants.AFTER_TOKEN);
			isLoggedOut = userService.invalidateSession();
			if (isLoggedOut) {
				OutputDTO<Boolean> outputDTO = new OutputDTO<>();
				outputDTO.setResponseData(true);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("User logged out successfully");
			}
		} catch (Exception e) {
			log.error("exception in Logout " + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
			
		}
		//log.info("Exiting logout() method of --->" + Constants.LOGIN_CONTROLLER);
		return response;
	}

}

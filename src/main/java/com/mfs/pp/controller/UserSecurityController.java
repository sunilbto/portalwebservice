package com.mfs.pp.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.pp.dto.OutputDTO;
import com.mfs.pp.dto.UserResponse;
import com.mfs.pp.service.UserSecurityService;
import com.mfs.pp.util.Constants;
import com.mfs.pp.util.RestServiceURIConstant;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */
@RestController
public class UserSecurityController extends BaseController {

	@Autowired
	private UserSecurityService userSecurityService;

	protected final Log log = LogFactory.getLog(getClass());

	//validate User Token
	@RequestMapping(value = RestServiceURIConstant.VALIDATE_USER_TOKEN, method = RequestMethod.GET)
	public UserResponse<Boolean> validateUserToken(@RequestParam("id") String id, @RequestParam("token") String token,
			@RequestParam("type") String type) {
		//log.info("Inside validateUserToken() method of ---->" + Constants.USERSECURITY_CONTROLLER+ Constants.BEGIN_REQUEST + "--->" + id + "-->>" + token + "-->>" + type);
		UserResponse<Boolean> response = null;
		OutputDTO<Boolean> outputDTO = null;
		Boolean isTokenValid = Boolean.FALSE;
		response = new UserResponse<Boolean>();
		try {

			isTokenValid = userSecurityService.validateUserToken(id, token, type);
			//check isTokenValid
			if (isTokenValid) {
				outputDTO = new OutputDTO<Boolean>();
				outputDTO.setResponseData(isTokenValid);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("The password activation link has not expired.");
			}
		} catch (Exception e) {
			log.error("exception occurred while validating token" + e.getMessage());
			response.setResponseCode(Constants.ERROR_CODE);
		}
		//log.info("Exiting validateUserToken() method of --->" + Constants.USERSECURITY_CONTROLLER);
		return response;
	}

	/**
	 * This method
	 * 
	 * @return true if User email is verified after user registration.
	 * activate User By Email Verification
	 */
	@RequestMapping(value = RestServiceURIConstant.ACTIVATE_EMAIL, method = RequestMethod.GET)
	public UserResponse<Boolean> activateUserByEmailVerification(@PathVariable(value = "email") String email) {
		//log.info("Inside activateUserByEmailVerification() method of ---->" + Constants.USERSECURITY_CONTROLLER+ Constants.BEGIN_REQUEST + "--->" + email);
		UserResponse<Boolean> response = null;
		OutputDTO<Boolean> outputDTO = null;
		response = new UserResponse<Boolean>();
		try {

			boolean isEmailVerified = userSecurityService.activateUserByEmailVerification(email);
			if (isEmailVerified) {
				outputDTO = new OutputDTO<Boolean>();
				outputDTO.setResponseData(isEmailVerified);
				response.setResult(outputDTO);
				response.setResponseCode(Constants.SUCCESS_CODE);
				response.setResponseMessage("The email has been verified");
			}
		} catch (Exception e) {
			log.error("exception occurred in email verification " + e.getMessage());
			response = new UserResponse<>();
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseMessage("Email not verified");
		}
		//log.info("Exiting activateUserByEmailVerification() method of --->" + Constants.USERSECURITY_CONTROLLER);
		return response;
	}

}

package com.mfs.pp.util;

import com.google.gson.Gson;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class JSONToObjectConversion {

//	private static final Logger LOGGER = Logger.getLogger(JSONToObjectConversion.class);

	public static Object getObjectFromJson(String json, Class<?> responseClass) {
	//	LOGGER.debug("JSONToObjectConversion for String => " + json);
		Gson gson = new Gson();
		Object obj = gson.fromJson(json, responseClass);
		return obj;
	}
}

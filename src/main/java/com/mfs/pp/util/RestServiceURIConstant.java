package com.mfs.pp.util;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class RestServiceURIConstant {

	public static final String GEN_TRANSACTION_EXCEL = "/pp/genTransactionExcel";

	public static final String GEN_BALANCE_EXCEL = "/pp/genBalanceExcel";

	public static final String GEN_REPORT_EXCEL = "/pp/genReportExcel";

	public static final String GET_ADMIN_ROLE_DETAIL = "/pp/AdminRoleDetail";

	public static final String GET_ORG_ROLE_DETAIL = "/pp/OrgRoleDetail";

	public static final String GET_MENU_LIST = "/pp/getMenuList";

	public static final String GET_ADMIN_ROLE_LIST = "/pp/AdminRoleList";

	public static final String GET_ORG_ROLE_LIST = "/pp/OrgRoleList";

	public static final String CREATE_ADMIN_ROLE = "/pp/createAdminRole";

	public static final String CREATE_ORG_ROLE = "/pp/createOrgRole";

	public static final String UPDATE_ADMIN_ROLE = "/pp/updateAdminRole";

	public static final String UPDATE_ORG_ROLE = "/pp/updateOrgRole";

	public static final String GET_All_COUNTRYlIST = "/pp/getAllCountryDtoList";

	public static final String LOGIN = "/pp/login";

	public static final String NEW_PASSWORD = "/pp/NewPassword";

	public static final String CREATE_ZONE = "/pp/createZone";

	public static final String UPDATE_ZONE = "/pp/updateZone";

	public static final String ZONE_DETAILS = "/pp/getZoneDetails";

	public static final String ZONE_LIST = "/pp/getZoneList";

	public static final String CREATE_GROUP = "/pp/createGroup";

	public static final String UPDATE_GROUP = "/pp/updateGroup";

	public static final String GROUP_LIST = "/pp/GroupList";

	public static final String GET_ALL_CURRENCY_LIST = "/pp/getAllCurrencyDtoList";

	public static final String GET_ALL_ORG_TYPELIST = "/pp/getOrgTypeList";

	public static final String GET_ALL_CATEGORY_LIST = "/pp/categoryList";

	public static final String GET_ALL_STATUS_LIST = "/pp/statusList";

	public static final String CREATE_ORGANIZATION = "/pp/createOrganization";

	public static final String CREATE_ORGANIZATION_CURBI = "/pp/createOrganizationforCurbi";

	public static final String UPDATE_ORGANIZATION = "/pp/updateOrganization";

	public static final String GET_ORGANIZATIONLIST = "/pp/getOrganizationList";

	public static final String SEND_RESET_PASSWORD_LINK = "/pp/sendResetPasswordLink";

	public static final String LOGOUT = "/pp/logout";

	public static final String GET_All_TIMEZONELIST = "/pp/getAllTimeZoneDtoList";

	public static final String REGISTER_USER = "/pp/registerUser";

	public static final String GET_ORGANIZATION_DETAILS = "/pp/getOrganizationDetails";

	public static final String GET_USER_BY_EMAIL = "/pp/getUserByEmailId";

	public static final String UPDATE_USER = "/pp/updateUser";

	public static final String DELETE_USER = "/pp/deleteUser";

	public static final String INVITE_ADMIN_USER = "/pp/inviteAdminUser";

	public static final String CHANGE_ADMIN_PASSWORD = "/pp/changeAdminPassword";

	public static final String GET_ALL_SYSTEM_ADMIN_USERS = "/pp/getAllSystemAdminUsers";

	public static final String UPDATE_ADMIN_USER = "/pp/updateAdminUser";

	public static final String DELETE_ADMINUSER = "/pp/deleteAdminUser";

	public static final String GET_All_USERS_BY_ORGANIZATION_NAME = "/pp/getAllUserByOrganisation/{orgName}";

	public static final String CHANGE_ORG_PASSWORD = "/pp/changeOrganisationPassword";

	public static final String BASE_URI = "base_uri";

	public static final String CURBI_GETALLTRANSACTION = "/getAllTransaction";
	
	public static final String CURBI_TRANSACTION_EXCEL_GEN = "/genExcelForTransaction";

	public static final String CURBI_Balance_EXCEL_GEN = "/genExcelForBalance";

	public static final String CURBI_REPORT_EXCEL_GEN = "/generateExcelForReport";

	public static final String CURBI_GET_ORGTRANSACTION_BY_ID = "/getTransactionById";

	public static final String CURBI_GETALLORGANIZATION = "/getAllOrganisation";

	public static final String CURBI_GETDIRECTIONLIST = "/getDirectionList";

	public static final String CURBI_GETALLCOUNTRIES = "/getAllCountries";

	public static final String CURBI_GETALLPRODUCT = "/getAllProduct";

	public static final String CURBI_GETDESTINATIONTYPE = "/getDestinationTypeList";

	public static final String CURBI_GETBALANCESUMMURY = "/getBalanceSummury";

	public static final String CURBI_GETTRANSACTIONSTATUS = "/getTransactionStatus";

	public static final String CURBI_GET_ALLORGANIZATION_LIST = "/getOrganization";

	public static final String CURBI_GET_ALL_BALANCE = "/getAllBalance";

	public static final String CURBI_GET_ALL_BALANCE_BY_ID = "/getAllBalanceById";

	public static final String CURBI_GET_DASHBOARD = "/getDashBoard";

	public static final String GET_ALLORGANIZATION_LIST = "/pp/getCurbiOrganizationList";

	public static final String GET_ALLTRANSACTION = "/pp/getAllTransaction";

	public static final String GET_TRANSACTION_BY_ID = "/pp/getTransactionById";

	public static final String GET_ALL_BALANCE = "/pp/getAllBalance";

	public static final String GET_ALL_BALANCE_BY_ID = "/pp/getAllBalanceById";

	public static final String GET_DIRECTIONLIST = "/pp/getDirectionList";

	public static final String GET_ALLCOUNTRIES = "/pp/getAllCountries";

	public static final String GET_ALLPRODUCT = "/pp/getAllProduct";

	public static final String GET_DESTINATIONTYPE = "/pp/getDestinationTypeList";

	public static final String GET_BALANCESUMMURY = "/pp/getBalanceSummury";

	public static final String GET_TRANSACTIONSTATUS = "/pp/getTransactionStatus";

	public static final String GET_DASHBOARD = "/pp/getDashBoard";

	public static final String IS_USER_ACTIVE = "/pp/isUserActive/{email}";

	public static final String GET_ADMIN_LOGIN_AUDIT_BY_DATES = "/pp/getAdminAuditLogListByDates";

	public static final String GET_ALL_ORGANISATIONS_FOR_ADMIN = "/pp/getAllOrganisationList";

	public static final String GET_ALL_REPORT_TYPE = "/pp/getAllReportTypesList";

	public static final String GET_LOGIN_AUDIT_BY_DATES = "/pp/getAuditLogListByDates";

	public static final String GET_ORGANIZATION_DETAILS_BY_ORGANIZATION_NAME = "/pp/getOrganizationByName/{orgName}";

	public static final String GET_API_KEY_BY_ORGANIZATION_NAME = "/pp/getApiKey/{orgName}";

	public static final String GET_ALL_STATUS_TYPES = "/pp/getAllStatusTypes";

	public static final String GET_ALL_ORGANISATION_ROLES = "/pp/getAllOrganisationRoles";

	public static final String VALIDATE_USER_TOKEN = "/pp/validateUserToken";

	public static final String ACTIVATE_EMAIL = "/pp/activateUserByEmailVerification/{email}";

	public static final String DELETE_ADMIN_ROLE = "/pp/deleteadminole";

	public static final String DELETE_ORG_ROLE = "/pp/deleteorgrole";

	public static final String ROLE_ASSIGN = "/pp/roleassign";

	public static final String CURBI_REGULATOR_GET_PRICING_DETAILS = "/getPricingDetails";
	
	public static final String CURBI_REGULATOR_GET_COMPLIAINCE_DETAILS = "/getCompliance";
	
	public static final String CURBI_REGULATOR_GET_STATICSTIC_CONSUME_DETAILS = "/getStatisticCounsume";
	
	public static final String CURBI_REGULATOR_GET_STATICSTIC_TRANSACTION_DETAILS = "/getStatisticTransaction";

	public static final String CURBI_REGULATOR_GET_STATICSTIC_FILTER = "/getStaticsticFilter";
	
	public static final String CURBI_REGULATOR_MONEY_FILTER = "/getMoneyFilter";

	public static final String MONEY_FLOWS = "/pp/moneyFlow";

	public static final String MONEY_FLOWS_CURBI = "/getMoneyFlows";
	
	public static final String GET_ORGANIZATION_DETAIL = "/pp/getOrganizationDetail";
	
	public static final String GET_ORGANIZATIONLISTS = "/pp/getOrganizationLists";
}
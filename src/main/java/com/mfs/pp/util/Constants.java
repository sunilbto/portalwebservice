package com.mfs.pp.util;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class Constants {
	
    public static final String STATUS_PENDING = "Pending";
    public static final String SUCCESS = "Success";
    public static final String USER_STATUSES[]={"Active", "Pending"};
    public static final String SYSTEM = "System";
    public static final String SUCCESS_CODE="200";
    public static final String MONEY_FILTER_SUCCESS="Getting Money Filters";
    public static final String COMPLIANCE_SUCCESS_MESSAGE="Getting Compliance Details Successfully";
    public static final String STATICSTICS_CONSUME_SUCCESS_MESSAGE="Getting Staticstic Consume Details Successfully";
    public static final String STATICSTICS_TRANSACTION_SUCCESS_MESSAGE="Getting Staticstic Transaction Details Successfully";
    public static final String STATICSTICS_TRANSACTION_FILTER_SUCCESS_MESSAGE="Getting Staticstic Transaction Filter Successfully";
    public static final String PRICING_SUCCESS_MESSAGE="Getting Pricing Details Successfully";
    public static final String ERROR_CODE="E401";
    public static final String ADMINCONTROLLER="AdminUserController";
    public static final String BEFORE_TOKEN="before genrateing token";
    public static final String AFTER_TOKEN="After genrateing token";
    public static final String BEGIN_REQUEST=" begin method with request";
    public static final String COMMON_CONTROLLER="CommonController";
    public static final String CURBI_CONTROLLER="CurbiController";
    public static final String GROUP_CONTROLLER="GroupController";
    public static final String GROUP_CREATION="Group created";
    public static final String GROUP_DUPLICATE="Duplicate Group";
    public static final String GROUP_UPDATE= "Group Updated";
    public static final String GROUP_LIST= "Group List Success";
    public static final String LOGIN_CONTROLLER= "LoginController";
    public static final String LOGIN_STATUS= "Login Successful";
    public static final String LOGIN_FAIl=" Login Failed";
    public static final String ORGANISATION_CONTROLLER= "OrganisationController";
    public static final String ROLE_CONTROLLER= "RoleController";
    public static final String USER_CONTROLLER= "UserController";
    public static final String USERSECURITY_CONTROLLER= "UserSecurityController";
    public static final String ZONE_CONTROLLER= "ZoneController";
    public static final String ZONE_CREATION= "Zone Created";
    public static final String ZONE_DUPLICATE= "Duplicate Zone";
    public static final String ZONE_UPDATE="Zone Updated";
    public static final String ZONE_LIST= "Zone List Success";
    public static final String HOST= "host";
    public static final String BASE_URl="base_uri";
    public static final String REGULATOR_CONTROLLER="RegulatorController";
    public static final String MONEYFLOW_CONTROLLER="MoneyFlowController";
    public static final String EMAIL="email";
}


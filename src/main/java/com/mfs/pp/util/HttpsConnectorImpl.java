/**
 * 
 */
package com.mfs.pp.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class HttpsConnectorImpl {

	private static final int CHECK_RESPONSE_CODE = 200;

	/**
	 * @param connectURL
	 * @param data
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws ProtocolException
	 * @throws MTAdapterConEx
	 */
	public String connectionUsingHTTPS(String connectionData, final HttpsConnectionRequest httpsConnectionRequest)
			throws UnknownHostException, IOException {
		SSLContext sslContext = null;
		String output = null;
		try {
			sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, new javax.net.ssl.TrustManager[] { new TrustAllTrustManager() },
					new java.security.SecureRandom());
		} catch (Exception e) {

		}
		HttpsURLConnection httpsConnection = null;
		try {
			URL httpsUrl = new URL(httpsConnectionRequest.getServiceUrl());

			sslContext.getSocketFactory().createSocket(httpsUrl.getHost(), httpsConnectionRequest.getPort());

			httpsConnection = (HttpsURLConnection) httpsUrl.openConnection();

			httpsConnection.setSSLSocketFactory(sslContext.getSocketFactory());

			httpsConnection.setRequestMethod(httpsConnectionRequest.getHttpmethodName());

			// Send post request
			httpsConnection.setDoOutput(true);
			httpsConnection.setDoInput(true);

			setHeaderPropertiesToHTTPSConnection(httpsConnectionRequest, connectionData, httpsConnection);

			if (connectionData != null) {

				DataOutputStream wr = new DataOutputStream(httpsConnection.getOutputStream());
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr, "UTF-8"));

				writer.write(connectionData);
				writer.flush();
				writer.close();
				wr.flush();
				wr.close();
			}
			int responseCode = httpsConnection.getResponseCode();

			BufferedReader httpsResponse = null;

			if (responseCode == CHECK_RESPONSE_CODE) {

				httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));

			} else if (responseCode == 204) {
				// In case of 204 need to wait for 30sec and send request again
				wait(30);
				// Retry again and set BufferedReader and response string finally
				httpsResponse = connectionUsingHTTPSRetry(connectionData, httpsConnectionRequest);

			} else {

				httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getErrorStream()));

			}

			output = readConnectionDataWithBuffer(httpsResponse);

			if (httpsResponse != null) {
				httpsResponse.close();
			}

		} catch (Exception e) {

			throw e;
		} finally {
			if (httpsConnection != null) {
				httpsConnection.disconnect();
			}
		}

		return output;
	}

	/**
	 * @param connectURL
	 * @param data
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws ProtocolException
	 * @throws MTAdapterConEx
	 */
	private BufferedReader connectionUsingHTTPSRetry(String connectionData,
			final HttpsConnectionRequest httpsConnectionRequest) throws UnknownHostException, IOException {
		SSLContext sslContext = null;

		try {
			sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, new javax.net.ssl.TrustManager[] { new TrustAllTrustManager() },
					new java.security.SecureRandom());
		} catch (Exception e) {

		}

		URL httpsUrl = new URL(httpsConnectionRequest.getServiceUrl());

		sslContext.getSocketFactory().createSocket(httpsUrl.getHost(), httpsConnectionRequest.getPort());

		HttpsURLConnection httpsConnection = (HttpsURLConnection) httpsUrl.openConnection();

		httpsConnection.setSSLSocketFactory(sslContext.getSocketFactory());

		httpsConnection.setRequestMethod(httpsConnectionRequest.getHttpmethodName());

		// Send post request
		httpsConnection.setDoOutput(true);
		httpsConnection.setDoInput(true);

		setHeaderPropertiesToHTTPSConnection(httpsConnectionRequest, connectionData, httpsConnection);

		if (connectionData != null) {

			DataOutputStream wr = new DataOutputStream(httpsConnection.getOutputStream());

			wr.writeBytes(connectionData);
			wr.flush();
			wr.close();
		}
		int responseCode = httpsConnection.getResponseCode();

		BufferedReader httpsResponse = null;

		if (responseCode == CHECK_RESPONSE_CODE) {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));

		} else {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getErrorStream()));

		}

		return httpsResponse;
	}

	public static void wait(int seconds) {
		try {

			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {

		}
	}

	/**
	 * @param httpsResponse
	 * @return
	 * @throws IOException
	 */
	private String readConnectionDataWithBuffer(BufferedReader httpsResponse) throws IOException {
		String output;
		String responseLine;
		StringBuffer response = new StringBuffer();

		while ((responseLine = httpsResponse.readLine()) != null) {
			response.append(responseLine);
		}

		output = response.toString();
		return output;
	}

	/**
	 * Set Header Properties To HTTPS Connection
	 * 
	 * @param httpsConnectionRequest
	 * @param connectionData
	 * @param httpsConnection
	 */
	private void setHeaderPropertiesToHTTPSConnection(final HttpsConnectionRequest httpsConnectionRequest,
			String connectionData, HttpsURLConnection httpsConnection) {

		setHeadersToHTTPSConnection(httpsConnectionRequest.getHeaders(), httpsConnection);

		if (connectionData != null && connectionData.length() > 0) {

			httpsConnection.setRequestProperty("Content-Length", String.valueOf(connectionData.length()));
		}

	}

	/**
	 * Set headers that are required by calling app
	 * 
	 * @param headers
	 * @param httpsConnection
	 */
	private void setHeadersToHTTPSConnection(Map<String, String> headers, HttpsURLConnection httpsConnection) {

		if (headers != null && !headers.isEmpty() && httpsConnection != null) {

			for (Entry<String, String> entry : headers.entrySet()) {

				httpsConnection.setRequestProperty(entry.getKey(), entry.getValue());
			}
		}
	}

}

package com.mfs.pp.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class CallServices {

	protected static final Log log = LogFactory.getLog(CallServices.class);

	public static String getResponseFromService(String url, String request) {

		//log.info("in call service url " + url);

		String response = null;
		try {

			HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
			connectionRequest.setServiceUrl(url);
			connectionRequest.setHttpmethodName("POST");

			Map<String, String> headerMap = new HashMap<String, String>();
			headerMap.put("charset", "utf-8");
			headerMap.put("Content-Type", "application/json");

			connectionRequest.setHeaders(headerMap);
			HttpsConnectionResponse connectionResponse = null;
			if (url.startsWith("https")) {
				HttpsConnectorImpl httpConnectorImpl = new HttpsConnectorImpl();
				response = httpConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
			} else {
				HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
				connectionResponse = httpConnectorImpl.httpUrlConnection(connectionRequest, request);
				if (connectionResponse != null) {
					response = connectionResponse.getResponseData();
				}
			}

		} catch (Exception e) {
			log.error("Exception occured in call service :: "+ e.getMessage());
		}

		return response;

	}

	public static String getResponseFromService(String url) {

		//log.debug("in call service url " + url);

		String response = null;
		try {

			HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();

			HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
			connectionRequest.setServiceUrl(url);
			connectionRequest.setHttpmethodName("GET");

			Map<String, String> headerMap = new HashMap<String, String>();
			headerMap.put("charset", "utf-8");

			connectionRequest.setHeaders(headerMap);

			HttpsConnectionResponse connectionResponse = httpConnectorImpl.httpUrlConnections(connectionRequest);

			response = connectionResponse.getResponseData();
		} catch (Exception e) {
			log.error("Exception occured in call service :: "+ e.getMessage());
		}
		return response;

	}
}

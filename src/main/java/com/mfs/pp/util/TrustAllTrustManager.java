package com.mfs.pp.util;

/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class TrustAllTrustManager implements javax.net.ssl.X509TrustManager {

	//protected final Log log = LogFactory.getLog(getClass());

	public java.security.cert.X509Certificate[] getAcceptedIssuers() {
		return new java.security.cert.X509Certificate[] {};
	}

	public void checkClientTrusted(java.security.cert.X509Certificate[] certs,
			String authType) {
	//	log.debug("Start checkClientTrusted() of TrustAllTrustManager");
	//	log.debug("trust certificate"+certs);
	//	log.debug("auth type"+authType);
	//	log.debug("End checkClientTrusted() of TrustAllTrustManager");
	}

	public void checkServerTrusted(java.security.cert.X509Certificate[] certs,
			String authType) {
		for (int index = 0; certs != null && index < certs.length; index++) {
	//		log.debug("Certificate [" + index + "]: "+ certs[0].getSubjectDN().getName());
		}
	//	log.debug("auth type"+authType);
	//	log.debug("End checkServerTrusted() of TrustAllTrustManager");
	}

}

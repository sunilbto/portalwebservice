package com.mfs.pp.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */


public class MathUtility {
	
	
	private MathUtility() {
		super();
	}

	public static int compareTwoNumbers(double num1,double num2)
	{
		int retval1;
		retval1=Double.compare(num1, num2);		
		return retval1;		
	}
	
	public static double truncate(double value) {
	    return new BigDecimal(value)
	        .setScale(4, RoundingMode.DOWN)
	        .doubleValue();
	}
	
}


package com.mfs.pp.util;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mfs.pp.exception.MFSServiceException;
import com.mfs.pp.exception.ServiceErrFactory;
import com.mfs.pp.type.MFSErrorType;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

@Component
public class FieldValidator {
	private StringBuilder errMsgBuilder;

	@Autowired
	ServiceErrFactory serviceErrFactory;

	/**
	 * get Err Msg Buffer.
	 * 
	 * @return StringBuilder
	 */
	private StringBuilder getPreparedErrMsgBuffer() {
		if (errMsgBuilder == null) {
			errMsgBuilder = new StringBuilder(96);
		} else {
			errMsgBuilder.append("\n");
		}
		return errMsgBuilder;
	}

	/**
	 * Throws <code>MFSServiceException</code> if any of the validate methods found
	 * validation errors.
	 * 
	 * @throws MFSServiceException
	 *             the service exception
	 */
	public void throwValidationExceptionIfRequired() throws MFSServiceException {
		if (isAnyError()) {
			throw new MFSServiceException(serviceErrFactory.createServiceErr(MFSErrorType.INVALID_INPUT),
					getErrorMessages());
		}
	}

	/**
	 * Checks if is any error.
	 * 
	 * @return Whether any error is reported by validateField method calls.
	 */
	public boolean isAnyError() {
		return errMsgBuilder != null;
	}

	/**
	 * Gets the error messages.
	 * 
	 * @return String containing newline separated error messages for all errors
	 *         reported by validateField method calls.
	 */
	public String getErrorMessages() {
		return errMsgBuilder != null ? errMsgBuilder.toString() : "";
	}

	/**
	 * This method appends user error message to the existing error messages.
	 * 
	 * @param userMessage
	 *            String, user specific error message to be appended
	 */
	public void appendErrorMessage(String userMessage) {
		getPreparedErrMsgBuffer().append(userMessage);
	}

	/**
	 * Used to check if the "int" field is a positive number or not.
	 * 
	 * @param fieldName
	 *            name of the "int" field
	 * @param value
	 *            "int" value being validated.
	 * 
	 * @return true if the "int" field is >= 0 else returns false.
	 */
	public boolean validatePositiveIntField(String fieldName, int value) {
		if (value >= 0)
			return true;
		else {
			getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName).append(" should have value >= 0");
			return false;
		}
	}

	/**
	 * Used to check if the "long" field is a positive number or not.
	 * 
	 * @param fieldName
	 *            name of the "long" field
	 * @param value
	 *            "long" value being validated.
	 * 
	 * @return true if the "long" field is >= 0 else returns false.
	 */
	public boolean validatePositiveLongField(String fieldName, long value) {
		if (value >= 0)
			return true;
		else {
			getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName).append(" should have value >= 0");
			return false;
		}
	}

	/**
	 * Used to validate float field.
	 * 
	 * @param fieldName
	 *            String
	 * @param value
	 *            float
	 * @return boolean - true if the float field is > 0 else returns false.
	 */
	public boolean validatePositiveFloatField(String fieldName, float value) {
		if (value >= 0)
			return true;
		else {
			getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName).append(" should have value >= 0");
			return false;
		}
	}

	/**
	 * Used to validate double field.
	 * 
	 * @param fieldName
	 *            String
	 * @param value
	 *            double
	 * @return boolean - true if the double field is > 0 else returns false.
	 */
	public boolean validatePositiveDoubleField(String fieldName, double value) {
		if (value >= 0)
			return true;
		else {
			getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName).append(" should have value >= 0");
			return false;
		}
	}

	/**
	 * Used to validate the fields to have only not null/empty value<br>
	 * .
	 * 
	 * @param fieldName
	 *            name of the field that is being validated, this will be appended
	 *            to the error message.
	 * @param fieldValue
	 *            value of the field that is being validated.
	 * @return true if the fieldValue is not null else returns false.
	 */
	public boolean validateStringFieldForNotNullNotEmpty(String fieldName, Object fieldValue) {
		if (fieldValue == null || fieldValue.equals("")) {
			getPreparedErrMsgBuffer().append("Error: ").append(fieldName).append(" has null/empty value!");
			return false;
		}
		return true;
	}

	/**
	 * Used to validate the fields to have only not null value<br>
	 * .
	 * 
	 * @param fieldName
	 *            name of the field that is being validated, this will be appended
	 *            to the error message.
	 * @param fieldValue
	 *            value of the field that is being validated.
	 * @return true if the fieldValue is not null else returns false.
	 */
	public boolean validateFieldForNotNull(String fieldName, Object fieldValue) {
		if (fieldValue == null) {
			getPreparedErrMsgBuffer().append("Error: ").append(fieldName).append(" has null value!");
			return false;
		}
		return true;
	}

	/**
	 * Used to validate <code>String</code> fields to have only allowed valid
	 * values.
	 * 
	 * @param fieldName
	 *            name of the field that is being validated, this will be appended
	 *            to error message.
	 * @param fieldValue
	 *            value of the field that is being validated.
	 * @param onlyValidValues
	 *            valid array of values that are allowed in the fieldValue.
	 * @return true if the given fieldValue falls into allowed valid values else
	 *         returns false.
	 */
	public boolean validateStringFieldWithValidValues(String fieldName, String fieldValue, Object[] onlyValidValues) {
		if (validateFieldForNotNull(fieldName, fieldValue)) {
			if (onlyValidValues != null && onlyValidValues.length > 0) {
				for (int i = 0; i < onlyValidValues.length; i++) {
					if (fieldValue.equals(onlyValidValues[i])) {
						return true;
					}
				}
				getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName).append(" can only have values {")
						.append(onlyValidValues[0]);
				for (int i = 1; i < onlyValidValues.length; i++) {
					errMsgBuilder.append(", ").append(onlyValidValues[i]);
				}
				errMsgBuilder.append("}. Specified ").append(fieldValue).append("!");
				return false;
			}
		} else {
			return false;
		}
		return true;
	}

	/**
	 * Used to validate <code>T</code> fields to have only allowed valid values.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param fieldName
	 *            name of the field that is being validated, this will be appended
	 *            to error message.
	 * @param fieldValue
	 *            value of the field that is being validated.
	 * @param onlyValidValues
	 *            valid array of values that are allowed in the fieldValue.
	 * @return true if the given fieldValue falls into allowed valid values else
	 *         returns false.
	 */
	public <T> boolean validateFieldWithValidValues(String fieldName, T fieldValue, T[] onlyValidValues) {
		if (validateFieldForNotNull(fieldName, fieldValue)) {
			if (onlyValidValues != null && onlyValidValues.length > 0) {
				for (int i = 0; i < onlyValidValues.length; i++) {
					if (fieldValue.equals(onlyValidValues[i])) {
						return true;
					}
				}
				getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName).append(" can only have values {")
						.append(onlyValidValues[0]);
				for (int i = 1; i < onlyValidValues.length; i++) {
					errMsgBuilder.append(", ").append(onlyValidValues[i]);
				}
				errMsgBuilder.append("}. Specified ").append(fieldValue).append("!");
				return false;
			}
		} else {
			return false;
		}
		return true;
	}

	/**
	 * Used to validate <code>String</code> fields to have only numeric value.
	 * 
	 * @param fieldName
	 *            name of the field that is being validated, this will be appended
	 *            to error message.
	 * @param fieldValue
	 *            value of the field that is being validated. the value can be from
	 *            ....-3.0,-2.0,-1.0,0,1.0,2.0,3.0....
	 * @return true if the fieldValue contains numeric value as String else returns
	 *         false.
	 */
	public boolean validateStringFieldContainingAllNumericValue(String fieldName, String fieldValue) {
		boolean result = true;
		if (result = validateFieldForNotNull(fieldName, fieldValue)) {
			Pattern p = Pattern.compile("^[-+]?[0-9]*\\.?[0-9]+$");
			if (p.matcher(fieldValue).matches()) {
				return true;
			} else {
				getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName)
						.append(" can only have string with whole numeric value. But specified ").append(fieldValue)
						.append("!");
				return false;
			}
		}
		return result;
	}

	/**
	 * Used to validate <code>String</code> fields for not null and to have up to
	 * allowedMaxLength.
	 * 
	 * @param fieldName
	 *            name of the field that is being validated, this will be appended
	 *            to error message.
	 * @param fieldValue
	 *            value of the field that is being validated.
	 * @param allowedMaxLength
	 *            maximum allowed length for the given field value.
	 * @return true if the fieldValue is not null and fieldValue's length less than
	 *         allowedMinLength else returns false.
	 */
	public boolean validateNonNullStringFieldWithMaxLength(String fieldName, String fieldValue, int allowedMaxLength) {
		boolean result = true;
		if (result = validateFieldForNotNull(fieldName, fieldValue)) {
			if (fieldValue.length() > allowedMaxLength) {
				getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName)
						.append(" can not have length more than ").append(allowedMaxLength);
				return false;
			}
		}
		return result;
	}

	/**
	 * Used to validate <code>String</code> fields to have up to allowedMaxLength.
	 * 
	 * @param fieldName
	 *            name of the field that is being validated, this will be appended
	 *            to error message.
	 * @param fieldValue
	 *            value of the field that is being validated.
	 * @param allowedMaxLength
	 *            maximum allowed length for the given field value.
	 * @return true if the fieldValue's length less than allowedMinLength else
	 *         returns false.
	 */
	public boolean validateStringFieldWithMaxLength(String fieldName, String fieldValue, int allowedMaxLength) {
		boolean result = true;
		if (fieldValue != null) {
			if (fieldValue.length() > allowedMaxLength) {
				getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName)
						.append(" can not have length more than ").append(allowedMaxLength);
				return false;
			}
		}
		return result;
	}

	/**
	 * Used to validate <code>String</code> fields to have at least
	 * allowedMinLength. Hint: Use this method to check non empty
	 * <code>String</code> by using allowedMinLength=1.
	 * 
	 * @param fieldName
	 *            name of the field that is being validated, this will be appended
	 *            to error message.
	 * @param fieldValue
	 *            value of the field that is being validated.
	 * @param allowedMinLength
	 *            minimum allowed length for the given field value.
	 * @return true if the fieldValue's length is greater than allowedMinLength else
	 *         returns false.
	 */
	public boolean validateNonNullStringFieldWithMinLength(String fieldName, String fieldValue, int allowedMinLength) {
		boolean result = true;
		if (result = validateFieldForNotNull(fieldName, fieldValue)) {
			if (fieldValue.trim().length() < allowedMinLength) {
				getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName);
				if (allowedMinLength == 1) {
					errMsgBuilder.append(" can not be empty string!");
				} else {
					errMsgBuilder.append(" can not have length less than ").append(allowedMinLength);
				}
				return false;
			}
		}
		return result;
	}

	/**
	 * Validate string field containing all numeric value with min length.
	 * 
	 * @param fieldName
	 *            the field name
	 * @param fieldValue
	 *            the field value
	 * @param allowedMinLength
	 *            the allowed min length
	 * @return true, if successful
	 */
	public boolean validateStringFieldContainingAllNumericValueWithMinLength(String fieldName, String fieldValue,
			int allowedMinLength) {
		boolean result = true;
		if (result = validateFieldForNotNull(fieldName, fieldValue)) {
			Pattern p = Pattern.compile("^[-+]?[0-9]*\\.?[0-9]+$");
			if (p.matcher(fieldValue).matches() && fieldValue.trim().length() > allowedMinLength) {
				return true;
			} else {
				getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName)
						.append(" can only have string with whole numeric value with minimumLenghth : "
								+ allowedMinLength + ". But specified ")
						.append(fieldValue).append("!");
				return false;
			}
		}
		return result;
	}

	/**
	 * Used to validate <code>Object</code> fields to have at least
	 * allowedMinLength. Hint: Use this method to check non empty
	 * <code>Object</code> by using allowedMinLength=1.
	 * 
	 * @param fieldName
	 *            name of the field that is being validated, this will be appended
	 *            to error message.
	 * @param fieldValue
	 *            value of the field that is being validated.
	 * @param allowedMinLength
	 *            minimum allowed length for the given field value.
	 * @return true if the fieldValue's length is greater than allowedMinLength else
	 *         returns false.
	 */
	public boolean validateObjectForNotNullWithStringFieldMinLength(String fieldName, Object fieldValue,
			int allowedMinLength) {
		if (validateFieldForNotNull(fieldName, fieldValue)) {
			if (validateNonNullStringFieldWithMinLength(fieldName, fieldValue.toString(), allowedMinLength)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Used to validate <code>String</code> fields to have exactly the
	 * allowedExactLength.
	 * 
	 * @param fieldName
	 *            name of the field that is being validated, this will be appended
	 *            to error message.
	 * @param fieldValue
	 *            value of the field that is being validated.
	 * @param allowedExactLength
	 *            exact value of allowed length for the given field value.
	 * @return true if the given fieldValue's length equals to allwedExactLength
	 *         else returns false.
	 */
	public boolean validateNonNullStringFieldWithExactLength(String fieldName, String fieldValue,
			int allowedExactLength) {
		boolean result = true;
		if ((result = validateFieldForNotNull(fieldName, fieldValue))) {
			if (fieldValue.length() != allowedExactLength) {
				getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName)
						.append(" should have only length equal to ").append(allowedExactLength);
				return false;
			}
		}
		return result;
	}

	/**
	 * Used to validate <code>String</code> fields to have either of the two
	 * allowedExactLength1,allowedExactLength2.
	 * 
	 * @param fieldName
	 *            name of the field that is being validated, this will be appended
	 *            to error message.
	 * @param fieldValue
	 *            value of the field that is being validated.
	 * @param allowedExactLength1
	 *            length that is allowed for fieldValue.
	 * @param allowedExactLength2
	 *            length that is allowed for fieldValue.
	 * @return true if the given fieldValue's length is exactly equal to any one of
	 *         allowedExactLength1 or allowedExactLength2.
	 */
	public boolean validateNonNullStringFieldWithExactLength(String fieldName, String fieldValue,
			int allowedExactLength1, int allowedExactLength2) {
		boolean result = true;
		if ((result = validateFieldForNotNull(fieldName, fieldValue))) {
			int valeLen = fieldValue.length();
			if (valeLen != allowedExactLength1 && valeLen != allowedExactLength2) {
				getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName)
						.append(" should have either length=").append(allowedExactLength1).append(" or length=")
						.append(allowedExactLength2);
				return false;
			}
		}
		return result;
	}

	/**
	 * Used to validate <code>String</code> field with pattern and
	 * allowedExactLength1.
	 * 
	 * @param fieldName
	 *            String - name of the field that is being validated, this will be
	 *            appended to error message.
	 * @param fieldValue
	 *            String - value of the field that is being validated.
	 * @param allowedExactLength
	 *            int - length that is allowed for fieldValue.
	 * @return boolean - true if the given fieldValue's length is exactly equal and
	 *         pattern matches.
	 */
	public boolean validateStringFieldContainingAllNumericAndExactLength(String fieldName, String fieldValue,
			int allowedExactLength) {
		boolean result = true;
		if (result = validateFieldForNotNull(fieldName, fieldValue)) {
			Pattern p = Pattern.compile("^[-+]?[0-9]*\\.?[0-9]+$");
			if (p.matcher(fieldValue).matches()) {
				if (fieldValue.length() == allowedExactLength) {
					return true;
				} else {
					getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName)
							.append(" should have only length equal to ").append(allowedExactLength);
					return false;
				}
			} else {
				getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName)
						.append(" can only have string with whole numeric value. But specified ").append(fieldValue)
						.append("!");
				return false;
			}
		}
		return result;
	}

	/**
	 * Used to validate <code>String</code> field with pattern and
	 * allowedExactLength1 and allowedExactLength2.
	 * 
	 * @param fieldName
	 *            String - name of the field that is being validated, this will be
	 *            appended to error message.
	 * @param fieldValue
	 *            String - value of the field that is being validated.
	 * @param allowedExactLength1
	 *            the allowed exact length1
	 * @param allowedExactLength2
	 *            the allowed exact length2
	 * @return boolean - true if the given fieldValue's length is exactly equals and
	 *         pattern matches.
	 */
	public boolean validateStringFieldContainingAllNumericAndExactLengths(String fieldName, String fieldValue,
			int allowedExactLength1, int allowedExactLength2) {
		boolean result = true;
		if (result = validateFieldForNotNull(fieldName, fieldValue)) {
			Pattern p = Pattern.compile("^[-+]?[0-9]*\\.?[0-9]+$");
			if (p.matcher(fieldValue).matches()) {
				if (fieldValue.length() == allowedExactLength1) {
					return true;
				} else if (fieldValue.length() == allowedExactLength2) {
					return true;
				} else {
					getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName)
							.append(" should have only length equal to ").append(allowedExactLength1).append(" ")
							.append("or").append(" ").append(allowedExactLength2);
					return false;
				}
			} else {
				getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName)
						.append(" can only have string with whole numeric value. But specified ").append(fieldValue)
						.append("!");
				return false;
			}
		}
		return result;
	}

	/**
	 * Used to validate <code>String</code> fields to have length in between
	 * minLength,maxLength.
	 * 
	 * @param fieldName
	 *            name of the field that is being validated, this will be appended
	 *            to error message.
	 * @param fieldValue
	 *            value of the field that is being validated.
	 * @param minLength
	 *            lower bound of length that is allowed for fieldValue.
	 * @param maxLength
	 *            upper bound of length that is allowed for fieldValue.
	 * @return true if the given fieldValue's length falls in between minLength and
	 *         maxLength else returns false.
	 */
	public boolean validateNonNullStringFieldWithLengthRange(String fieldName, String fieldValue, int minLength,
			int maxLength) {
		boolean result = true;
		if (result = validateFieldForNotNull(fieldName, fieldValue)) {
			int valeLen = fieldValue.length();
			if (valeLen < minLength || valeLen > maxLength) {
				getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName)
						.append(" should have length between ").append(minLength).append(" and ").append(maxLength);
				return false;
			}
		}
		return result;
	}

	/**
	 * Used to validate <code>int</code> fields to be in between minValue and
	 * maxValue .
	 * 
	 * @param fieldName
	 *            name of the field that is being validated, this will be appended
	 *            to error message.
	 * @param value
	 *            value of the field that is being validated.
	 * @param minValue
	 *            lower bound for given int fieldValue
	 * @param maxValue
	 *            upper bound for given int fieldValue
	 * @return true if the given not null fieldValue's to falls in between minValue
	 *         and maxValue else returns false.
	 */
	public boolean validateIntFieldWithValueRange(String fieldName, int value, int minValue, int maxValue) {
		boolean result = true;
		if (value < minValue || value > maxValue) {
			getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName).append(" should have value between ")
					.append(minValue).append(" and ").append(maxValue);
			return false;
		}
		return result;
	}

	/**
	 * Used to validate <code>List<?></code> fields to have non empty List and not
	 * null objects.
	 * 
	 * @param fieldName
	 *            String
	 * @param fieldValue
	 *            List<?>
	 * @return boolean - true if List has is not null and has not null objects else
	 *         returns false.
	 */
	public boolean validateNonNullNonEmptyListField(String fieldName, List<?> fieldValue) {
		boolean hasErrors = false;
		boolean result = true;
		List<?> list = fieldValue;
		if (list == null || list.size() == 0) {
			getPreparedErrMsgBuffer().append("Input " + fieldName + " is null or empty");
			result = false;
		} else {
			for (Iterator<?> it = list.iterator(); it.hasNext();) {
				Object nextElement = it.next();
				if (nextElement == null) {
					result = false;
					hasErrors = true;
				}
			}
		}
		if (hasErrors) {
			getPreparedErrMsgBuffer().append("Input " + fieldName + " contains null values");
			for (Iterator<?> it = list.iterator(); it.hasNext();) {
				Object nextElement = it.next();
				if (nextElement != null) {
					getPreparedErrMsgBuffer()
							.append(nextElement + " , type :- " + nextElement.getClass().getName() + " ; ");
				}
			}
			getPreparedErrMsgBuffer().append("]");
		}
		return result;
	}

	/**
	 * Used to validate <code>List<?></code> fields to have not null objects.
	 * 
	 * @param fieldName
	 *            String
	 * @param fieldValue
	 *            List<?>
	 * @return boolean - true if List has not null objects else returns false.
	 */
	public boolean validateNonNullListField(String fieldName, List<?> fieldValue) {
		boolean hasErrors = false;
		boolean result = true;
		List<?> list = fieldValue;
		for (Iterator<?> it = list.iterator(); it.hasNext();) {
			Object nextElement = it.next();
			if (nextElement == null) {
				result = false;
				hasErrors = true;
			}
		}
		if (hasErrors) {
			getPreparedErrMsgBuffer().append("Input " + fieldName + " contains null values");
			for (Iterator<?> it = list.iterator(); it.hasNext();) {
				Object nextElement = it.next();
				if (nextElement != null) {
					getPreparedErrMsgBuffer()
							.append(nextElement + " , type :- " + nextElement.getClass().getName() + " ; ");
				}
			}
			getPreparedErrMsgBuffer().append("]");
		}
		return result;
	}

	/**
	 * Used to validate <code>List<?></code> fields to have non empty List and not
	 * null objects.
	 * 
	 * @param fieldName
	 *            String
	 * @param fieldValue
	 *            List<?>
	 * @return boolean
	 */
	public boolean validateListFieldWithAtleastOneNotNullElement(String fieldName, List<?> fieldValue) {
		boolean hasErrors = true;
		boolean result = false;
		List<?> list = fieldValue;
		if (list == null || list.size() == 0) {
			getPreparedErrMsgBuffer().append("Input " + fieldName + " is null or empty");
			result = false;
		} else {
			for (Iterator<?> it = list.iterator(); it.hasNext();) {
				Object nextElement = it.next();
				if (nextElement != null) {
					result = true;
				}
			}
		}
		if (result) {
			hasErrors = false;
		}
		if (hasErrors) {
			getPreparedErrMsgBuffer().append("All elements of " + fieldName + " are null.");
			if (list != null && list.size() > 0) {
				for (Iterator<?> it = list.iterator(); it.hasNext();) {
					Object nextElement = it.next();
					if (nextElement != null) {
						getPreparedErrMsgBuffer()
								.append(nextElement + " , type :- " + nextElement.getClass().getName() + " ; ");
					}
				}
				getPreparedErrMsgBuffer().append("]");
			}
		}
		return result;
	}

	/**
	 * Used to validate Array fields to have non empty and not null objects.
	 * 
	 * @param fieldName
	 *            the field name
	 * @param fieldValue
	 *            the field value
	 * @return boolean
	 */
	public boolean validateArrayFieldWithAtleastOneNotNullElement(String fieldName, Object[] fieldValue) {
		boolean hasErrors = true;
		boolean result = false;
		if (fieldValue == null || fieldValue.length == 0) {
			getPreparedErrMsgBuffer().append("Input " + fieldName + " is null or empty");
			result = false;
		} else {
			for (int i = 0; i < fieldValue.length; i++) {
				Object nextElement = fieldValue[i];
				if (nextElement != null) {
					result = true;
				}
			}
		}
		if (result) {
			hasErrors = false;
		}
		if (hasErrors) {
			getPreparedErrMsgBuffer().append("All elements of " + fieldName + " are null.");
			if (fieldValue != null && fieldValue.length > 0) {
				for (int i = 0; i < fieldValue.length; i++) {
					Object nextElement = fieldValue[i];
					if (nextElement != null) {
						getPreparedErrMsgBuffer()
								.append(nextElement + " , type :- " + nextElement.getClass().getName() + " ; ");
					}
				}
				getPreparedErrMsgBuffer().append("]");
			}
		}
		return result;
	}

	/**
	 * Used to validate <code>List<?></code> fields to have non empty List and not
	 * null objects.
	 * 
	 * @param fieldName
	 *            String
	 * @param fieldValue
	 *            List<?>
	 * @param allowedMaxSize
	 *            int
	 * @return boolean - true if List has is not null and has not null objects else
	 *         returns false.
	 */
	public boolean validateNonNullNonEmptyListFieldWithMaxSize(String fieldName, List<?> fieldValue,
			int allowedMaxSize) {
		boolean result = false;
		if (result = validateNonNullNonEmptyListField(fieldName, fieldValue)) {
			if (fieldValue.size() > allowedMaxSize) {
				getPreparedErrMsgBuffer().append("Error: Field ").append(fieldName)
						.append(" can not have size more than ").append(allowedMaxSize);
				result = false;
			}
		}
		return result;
	}

}

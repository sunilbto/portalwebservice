package com.mfs.pp.util;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public class RandomGeneratorUtil {
	
	private static final AtomicLong COUNTER = new AtomicLong(System.nanoTime());
  
	private RandomGeneratorUtil() {
		super();
	}

	public static String getNextRandomId () {
		return COUNTER.incrementAndGet()+"";
	}

	public static int fetchRandomNumber(int startRange, int endRange, Random aRandom) throws Exception{
    if (startRange > endRange) {
      throw new IllegalArgumentException("Start cannot exceed End.");
    }
    //get the range, casting to long to avoid overflow problems
    long range = (long)endRange - (long)startRange + 1;
    // compute a fraction of the range, 0 <= frac < range
    long fraction = (long)(range * aRandom.nextDouble());
    int randomNumber =  (int)(fraction + startRange); 
    return randomNumber;
  }
  
}
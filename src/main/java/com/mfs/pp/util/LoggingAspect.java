package com.mfs.pp.util;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */


@Aspect
public class LoggingAspect {
  
	//private static final Logger LOGGER = LoggerFactory.getLogger(LoggingAspect.class);
	
    @Before("execution(* com.mfs.pp.*.*(..))")
	public void logBefore(JoinPoint joinPoint) {

	//	LOGGER.info("logBefore is running!!!!!!!!!!!!!!!!!!!!!!!");
	//	LOGGER.info("meth name : " +joinPoint.getClass().getName()+" : "+ joinPoint.getSignature().getName());
	//	LOGGER.info("******");
	}
    @After("execution(* com.mfs.pp.*.*(..))")
	public void logAfter(JoinPoint joinPoint) {

	//	LOGGER.info("LogAfter is running!!!!!!!!!!!!!!!!!!!!!!!");
	//	LOGGER.info("meth name : " + joinPoint.getClass().getName()+" : "+joinPoint.getSignature().getName());
	//	LOGGER.info("******");
	}
    
    @AfterThrowing (pointcut = "execution(* com.mfs.pp.service.impl.*Impl.update*(..))", throwing = "ex")
    public void logAfterThrowingAllUpdateMethods(Exception ex) throws Throwable
    {
       
    }
}
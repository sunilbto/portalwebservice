package com.mfs.pp.util;
/**
 * 
 * @author : Beauto Systems
 * @email  : ashwini.mahind@beautosys.com
 *
 */

public enum RoleTypeEnum {
	
	DEFAULT	("default"), 
	CUSTOM ("custom");

	private String roleTypeName;

	RoleTypeEnum (String roleTypeName) {
		this.roleTypeName = roleTypeName;
	}

	public String roleTypeName() {
		return roleTypeName;
	}

}
